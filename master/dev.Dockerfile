ARG TAG=latest

FROM debito-docker-php:$TAG AS debito-docker-php-dev

#Why 10.254.254.254: https://gist.github.com/ralphschindler/535dc5916ccbd06f53c1b0ee5a868c93

RUN set -ex \\
    && apt-get update \
    && apt-get install -y \
    php7.3-xdebug \
    && apt-get clean \
    && sed -i 's/opcache.enable=1/opcache.enable=0/g' /etc/php/7.3/apache2/php.ini \
    && sed -i 's/opcache.enable=1/opcache.enable=0/g' /etc/php/7.3/cli/php.ini \
    && echo 'xdebug.remote_enable=1' >> /etc/php/7.3/apache2/php.ini \
    && echo 'xdebug.idekey=PHPSTORM' >> /etc/php/7.3/apache2/php.ini \
    && echo 'xdebug.remote_host=10.254.254.254' >> /etc/php/7.3/apache2/php.ini \
    && echo 'xdebug.remote_enable=1' >> /etc/php/7.3/cli/php.ini \
    && echo 'xdebug.idekey=PHPSTORM' >> /etc/php/7.3/cli/php.ini \
    && echo 'xdebug.remote_host=10.254.254.254' >> /etc/php/7.3/cli/php.ini


CMD [ "apache2-foreground" ]
