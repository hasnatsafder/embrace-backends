# Readme

## How do I start using these?

- Install `docker`
- Run `$ docker build . -f Dockerfile -t debito-docker-php`
- Run `$ docker build . -f dev.Dockerfile -t debito-docker-php-dev`

## How do I update my local builds?

- You run the 2x `docker build` commands after changing things in the Dockerfiles

## Why do we not have these things in a Docker registry?

- We do not yet need that as we only use them locally for dev envs for now
