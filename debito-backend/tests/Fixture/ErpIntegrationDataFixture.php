<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ErpIntegrationDataFixture
 */
class ErpIntegrationDataFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created_by_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified_by_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'erp_integration_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'data_key' => [
            'type' => 'string',
            'length' => 255,
            'null' => false,
            'default' => null,
            'collate' => 'latin1_swedish_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'data_value' => [
            'type' => 'text',
            'length' => null,
            'null' => false,
            'default' => null,
            'collate' => 'latin1_swedish_ci',
            'comment' => '',
            'precision' => null,
        ],
        'org_id' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'latin1_swedish_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'BY_CREATED_BY_ID' => ['type' => 'index', 'columns' => ['created_by_id'], 'length' => []],
            'BY_MODIFIED_BY_ID' => ['type' => 'index', 'columns' => ['modified_by_id'], 'length' => []],
            'BY_KEY' => ['type' => 'index', 'columns' => ['data_key'], 'length' => []],
            'BY_ERP_INTEGRATION_ID' => ['type' => 'index', 'columns' => ['erp_integration_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_unicode_520_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '64d81f91-164d-46f5-939e-b7f841e15a17',
                'created_by_id' => '9b6dc867-eb29-413f-930f-f8e9e4cde950',
                'modified_by_id' => '96b47ab5-dde2-41f9-9c7e-9c7fa171d21a',
                'erp_integration_id' => 'f73b0aa8-ad2e-4543-a0c5-b693c88fe71f',
                'data_key' => 'Lorem ipsum dolor sit amet',
                'data_value' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'org_id' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-11 10:03:09',
                'modified' => '2020-02-11 10:03:09',
                'deleted' => '2020-02-11 10:03:09',
            ],
        ];
        parent::init();
    }
}
