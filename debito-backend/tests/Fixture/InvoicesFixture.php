<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InvoicesFixture
 *
 */
class InvoicesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created_by_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified_by_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'account_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'invoice_status_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'currency_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'erp_integration_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'erp_integration_foreign_key' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'debtor_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'invoice_number' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'issue_date' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'due_date' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'hidden_until' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'net_amount' => ['type' => 'decimal', 'length' => 12, 'precision' => 3, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'gross_amount' => ['type' => 'decimal', 'length' => 12, 'precision' => 3, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'pdf_file_path' => [
            'type' => 'string',
            'length' => 255,
            'null' => false,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'BY_CREATED_BY_ID' => ['type' => 'index', 'columns' => ['created_by_id'], 'length' => []],
            'BY_MODIFIED_BY_ID' => ['type' => 'index', 'columns' => ['modified_by_id'], 'length' => []],
            'BY_ACCOUNT_ID' => ['type' => 'index', 'columns' => ['account_id'], 'length' => []],
            'BY_INVOICE_STATUS_ID' => ['type' => 'index', 'columns' => ['invoice_status_id'], 'length' => []],
            'BY_DEBTOR_ID' => ['type' => 'index', 'columns' => ['debtor_id'], 'length' => []],
            'BY_INVOICE_NUMBER' => ['type' => 'index', 'columns' => ['invoice_number'], 'length' => []],
            'BY_ISSUE_DATE' => ['type' => 'index', 'columns' => ['issue_date'], 'length' => []],
            'BY_DUE_DATE' => ['type' => 'index', 'columns' => ['due_date'], 'length' => []],
            'BY_ERP_INTEGRATION_ID' => ['type' => 'index', 'columns' => ['erp_integration_id'], 'length' => []],
            'BY_ERP_INTEGRATION_FOREIGN_KEY' => ['type' => 'index', 'columns' => ['erp_integration_foreign_key'], 'length' => []],
            'BY_HIDDEN_UNTIL' => ['type' => 'index', 'columns' => ['hidden_until'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 'fd38efa5-31a2-4af5-934b-543400deebc6',
                'created_by_id' => '24cb6572-3e3e-4526-8fc2-9bdce7ba9d82',
                'modified_by_id' => 'c177545c-0dc7-4dd8-bdf9-c13ebace5aa4',
                'account_id' => 'b89cae93-169c-4836-8946-37b7fba9bdb4',
                'invoice_status_id' => '2e0b5093-e0c8-4430-9dde-000db18e4af9',
                'currency_id' => '24089296-cb87-4aa6-9704-b8174fcc37e3',
                'erp_integration_id' => '79215afb-f7da-4956-a1c2-e1a3b7211ee8',
                'erp_integration_foreign_key' => 'Lorem ipsum dolor sit amet',
                'debtor_id' => '1af56128-7dc6-4a4c-bec5-240a576a422f',
                'invoice_number' => 1,
                'issue_date' => '2019-02-16',
                'due_date' => '2019-02-16',
                'hidden_until' => '2019-02-16 16:43:43',
                'net_amount' => 1.5,
                'gross_amount' => 1.5,
                'pdf_file_path' => 'Lorem ipsum dolor sit amet',
                'created' => '2019-02-16 16:43:43',
                'modified' => '2019-02-16 16:43:43',
                'deleted' => '2019-02-16 16:43:43',
            ],
        ];
        parent::init();
    }
}
