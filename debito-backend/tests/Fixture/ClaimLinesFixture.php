<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClaimLinesFixture
 *
 */
class ClaimLinesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'claim_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'claim_line_type_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'currency_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'invoice_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'placement' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'invoice_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'maturity_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'amount' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'invoice_reference' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'file_name' => [
            'type' => 'upload.file',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'file_mime_type' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'file_size' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'file_dir' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'old_id' => [
            'type' => 'string',
            'length' => 255,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_general_ci',
            'comment' => '',
            'precision' => null,
            'fixed' => null,
        ],
        '_indexes' => [
            'BY_CLAIM_ID' => ['type' => 'index', 'columns' => ['claim_id'], 'length' => []],
            'BY_CLAIM_LINE_TYPE_ID' => ['type' => 'index', 'columns' => ['claim_line_type_id'], 'length' => []],
            'BY_CURRENCY_ID' => ['type' => 'index', 'columns' => ['currency_id'], 'length' => []],
            'BY_PLACEMENT' => ['type' => 'index', 'columns' => ['placement'], 'length' => []],
            'BY_INVOICE_DATE' => ['type' => 'index', 'columns' => ['invoice_date'], 'length' => []],
            'BY_MATURITY_DATE' => ['type' => 'index', 'columns' => ['maturity_date'], 'length' => []],
            'BY_AMOUNT' => ['type' => 'index', 'columns' => ['amount'], 'length' => []],
            'BY_INVOICE_REFERENCE' => ['type' => 'index', 'columns' => ['invoice_reference'], 'length' => []],
            'BY_INVOICE_ID' => ['type' => 'index', 'columns' => ['invoice_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 'af4b6cea-c870-419b-b406-fde98dde0954',
                'claim_id' => '5e5d3190-991a-43d8-bf4d-dfea47830be8',
                'claim_line_type_id' => '5b58dc90-3f54-4188-9001-6a282024c730',
                'currency_id' => '529b8ae8-54c8-422e-867f-c1a53d19426c',
                'invoice_id' => '76fcde37-c430-463e-925f-eb1f41dd08da',
                'placement' => 1,
                'invoice_date' => '2019-02-12',
                'maturity_date' => '2019-02-12',
                'amount' => 1,
                'invoice_reference' => 'Lorem ipsum dolor sit amet',
                'file_name' => '',
                'file_mime_type' => 'Lorem ipsum dolor sit amet',
                'file_size' => 1,
                'file_dir' => 'Lorem ipsum dolor sit amet',
                'created' => '2019-02-12 21:40:03',
                'modified' => '2019-02-12 21:40:03',
                'deleted' => '2019-02-12 21:40:03',
                'old_id' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
