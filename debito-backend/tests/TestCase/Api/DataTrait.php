<?php

namespace App\Test\TestCase\Api;

use Cake\ORM\TableRegistry;
use Cake\Database\Exception;
use Cake\Datasource\ConnectionManager;

trait DataTrait
{

    public $usersTable;
    public $accountsTable;
    public $rolesTable;
    public $accountsUsersTable;
    public $debtorsTable;
    public $claimsTable;
    public $claimPhasesTable;
    public $collectorTable;
    public $debtorLawyersTable;

    public $userEmail = "test@debito.com";
    public $accountName = "debito-test-account";
    public $debtorEmail = "test-debtor@debito.com";
    public $userRemeberMeToken = "9469a6d2-7242-48a9-ba22-6670d1b60d0b";
    public $savedData = [];

    private function initializeData()
    {
        $this->usersTable = TableRegistry::getTableLocator()->get('Users');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->rolesTable = TableRegistry::getTableLocator()->get('Roles');
        $this->accountsUsersTable = TableRegistry::getTableLocator()->get('AccountsUsers');
        $this->debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        $this->claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimPhasesTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $this->collectorTable = TableRegistry::getTableLocator()->get('Collectors');
        $this->debtorLawyersTable = TableRegistry::getTableLocator()->get('DebtorLawyers');

        // clear all data before saving
        $this->clearData();

        //create new data for tables
        $this->createUsers();
        $this->createAccounts();
        $this->createAccountUsers();
        $this->createDebtors();
        $this->createClaimPhases();
        $this->createCollectors();
        $this->createClaims();
        $this->createDebtorLawyer();
    }

    private function clearData()
    {
        $this->usersTable->deleteAll([]);
        $this->accountsTable->deleteAll([]);
        $this->accountsUsersTable->deleteAll([]);
        $this->debtorsTable->deleteAll([]);
        $this->claimsTable->deleteAll([]);
        $this->claimPhasesTable->deleteAll([]);
        $this->collectorTable->deleteAll([]);
        $this->debtorLawyersTable->deleteAll([]);
    }

    private function createUsers()
    {
        $data = [
            'first_name' => 'Lorem ipsum dolor sit amet',
            'last_name' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum dolor sit amet',
            'email' => $this->userEmail,
            'password' => '$2y$10$a61qKFgIN3KkX5JOMSVOiutn60/SQ/EM9SFs06pR6fl5jV.XrwA.O',
            'is_confirmed' => 1,
            'is_active' => 1,
            'created' => '2018-08-28 12:25:51',
            'modified' => '2018-08-28 12:25:51',
            'deleted' => '2018-08-28 12:25:51',
            'token' => $this->userRemeberMeToken,
        ];
        $user = $this->usersTable->newEntity($data);
        if ($this->usersTable->save($user)) {
            $this->savedData['user'] = $user;
        } else {
            throw new \Exception("User not saved");
        }
    }

    private function createAccounts()
    {
        $user = $this->usersTable->find()->where(['email' => $this->userEmail])->first();
        $data = [
            'name' => $this->accountName,
            'address' => 'Lorem ipsum dolor sit amet',
            'zip_code' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum dolor sit amet',
            'vat_number' => 'Lorem ipsum dolor sit amet',
            'ean_number' => 'Lorem ipsum dolor sit amet',
            'website' => 'Lorem ipsum dolor sit amet',
            'bank_reg_number' => 'Lorem ipsum dolor sit amet',
            'bank_account_number' => 'Lorem ipsum dolor sit amet',
            'is_company' => 1,
            'latitude' => 'Lorem ipsum dolor sit amet',
            'longitude' => 'Lorem ipsum dolor sit amet',
            'created' => '2018-08-28 12:25:48',
            'modified' => '2018-08-28 12:25:48',
            'deleted' => '2018-08-28 12:25:48',
            'email' => 'Lorem ipsum dolor sit amet',
        ];
        $account = $this->accountsTable->newEntity($data);
        $account->created_by_id = $user->id;

        if ($this->accountsTable->save($account, ['skipAutoCreateUser' => true])) {
            $this->savedData['account'] = $account;
            //set active account is users table
            $userData = $this->usersTable->get($user->id);
            $userData->active_account_id = $account->id;
            $this->usersTable->save($userData);
        } else {
            throw new \Exception("Account not saved");
        }
    }

    private function createAccountUsers()
    {

        $roleUser = $this->rolesTable->find()->where(['identifier' => 'user'])->first();

        $data = [
            'account_id' => $this->savedData['account']->id,
            'user_id' => $this->savedData['user']->id,
            'role_id' => $roleUser->id,
        ];

        $accountUser = $this->accountsUsersTable->newEntity($data);
        if ($this->accountsUsersTable->save($accountUser)) {
            $this->savedData['accountUser'] = $accountUser;
        } else {
            throw new \Exception("Account User not saved");
        }
    }

    private function createDebtors()
    {
        $user = $this->usersTable->find()->where(['email' => $this->userEmail])->first();
        $data = [
            'vat_number' => 'Lorem ipsum dolor sit amet',
            'company_name' => 'Lorem ipsum dolor sit amet',
            'first_name' => 'Lorem ipsum dolor sit amet',
            'last_name' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'zip_code' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum dolor sit amet',
            'email' => $this->debtorEmail,
            'latitude' => 'Lorem ipsum dolor sit amet',
            'langitude' => 'Lorem ipsum dolor sit amet',
            'created' => '2018-08-28 11:11:03',
            'modified' => '2018-08-28 11:11:03',
            'deleted' => '2018-08-28 11:11:03',
            'country_id' => '03bfcf66-043a-11e8-9a3c-080027548b1b',
            'is_company' => 1,
        ];

        $debtor = $this->debtorsTable->newEntity($data);
        $debtor->created_by_id = $user->id;
        $debtor->account_id = $this->savedData['account']->id;
        if ($this->debtorsTable->save($debtor)) {
            $this->savedData['debtor'] = $debtor;
        } else {
            throw new \Exception("Debtor not saved");
        }
    }

    private function createClaimPhases()
    {

        // Breacuase of keyword "order" we need to use raw sql query
        $claim_phase_id = "5768a0a5-34b7-4b44-9257-feae964054c5";
        $conn = ConnectionManager::get('test');
        $data = "INSERT INTO `claim_phases` (`id`, `identifier`, `name`, `description`, `order`, `created`, `modified`, `deleted`)
        VALUES
            ('5768a0a5-34b7-4b44-9257-feae964054c5', 'case_received', 'Received', 'Case received (from frontend or dashboard)', 1, '2018-04-22 16:21:06', '2018-04-22 16:21:06', NULL)";
        $stmt = $conn->execute($data);
        $this->savedData['claimPhaseId'] = $claim_phase_id;
    }

    private function createCollectors()
    {
        $data = [
            'id' => '65b79e5d-0781-406d-8e6e-24faf3097903',
            'name' => 'Lorem ipsum dolor sit amet',
            'identifier' => 'mortang',
            'is_active' => 1,
            'created' => '2018-08-28 12:25:51',
            'modified' => '2018-08-28 12:25:51',
            'deleted' => '2018-08-28 12:25:51',
            'email' => 'Lorem ipsum dolor sit amet',
        ];
        $collector = $this->collectorTable->newEntity($data);
        if ($this->collectorTable->save($collector)) {
            $this->savedData['collector'] = $collector;
        } else {
            throw new \Exception("Collector not saved");
        }
    }

    private function createClaims()
    {
        $user = $this->usersTable->find()->where(['email' => $this->userEmail])->first();
        $data = [
            'id' => '3fd947b5-e605-4cd4-b510-e4520175d208',
            'debtor_id' => $this->savedData['debtor']->id,
            'claim_phase_id' => $this->savedData['claimPhaseId'],
            'customer_reference' => 'Lorem ipsum dolor sit amet',
            'collector_reference' => 'Lorem ipsum dolor sit amet',
            'dispute' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'created' => '2018-08-28 12:25:51',
            'modified' => '2018-08-28 12:25:51',
        ];
        $claim = $this->claimsTable->newEntity($data);
        $claim->created_by_id = $user->id;
        $claim->account_id = $this->savedData['account']->id;
        $claim->collector_id = $this->savedData['collector']->id;
        if ($this->claimsTable->save($claim)) {
            $this->savedData['claim'] = $claim;
        } else {
            throw new \Exception("Claim not saved");
        }
    }

    // ====== Clear Data ======

    public function createDebtorLawyer()
    {
        $data = [
            'name' => 'Debtor Lawyer',
            'address' => 'Debtor Lawyer address',
            'email' => 'debtor@lawyer.com',
            'vat_number' => '12345678',
            'claim_id' => $this->savedData['claim']->id,
            'phone' => '12312312',
        ];
        $debtorLawyer = $this->debtorLawyersTable->newEntity($data);
        if ($this->debtorLawyersTable->save($debtorLawyer)) {
            $this->savedData['debtorLawyer'] = $debtorLawyer;
        } else {
            throw new \Exception("Debtor Lawyer not saved");
        }
    }
}
