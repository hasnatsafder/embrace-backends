<?php

namespace App\Test\TestCase\Api;

use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\Api\ApiTestTrait;
use Exception;
use function GuzzleHttp\json_encode;

class DebtorLawyerTest extends IntegrationTestCase
{
    use ApiTestTrait;

    public function testCanAddDebtorLawyer()
    {
        $this->setUserToken();
        $data = [
            'name' => 'Debtor Lawyer',
            'address' => 'Debtor Lawyer address',
            'email' => 'newdebtor@lawyer.com',
            'vat_number' => '12345678',
            'claim_id' => $this->savedData['claim']->id,
            'phone' => '12312312',
        ];
        $this->post('/api/v1/debtor-lawyers', json_encode($data));
        $this->assertResponseOk();
        $this->assertResponseContains('id');

        $debtorLawyer = $this->debtorLawyersTable->find()->where(['email' => "newdebtor@lawyer.com"])->first();

        //check debtor exists
        if (!$debtorLawyer) {
            throw new Exception("Debtor Lawyer not regisetered");
        }
    }

    public function testCanGetDebtorLawyer()
    {
        $user = $this->setUserToken();
        $this->get('/api/v1/debtor-laywers/');
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
        $this->assertResponseContains('vat_number');
    }

    public function testCanEditDebtor()
    {
        $this->setUserToken();
        $this->put('/api/v1/debtor-lawyers/' . $this->savedData['debtorLawyer']->id, json_encode(['email' => 'editemail@debito.com']));
        $this->assertResponseOk();

        $debtor = $this->debtorLawyersTable->find()->where(['email' => "editemail@debito.com"])->first();

        //check debtor exists
        if (!$debtor) {
            throw new Exception("Debtor Lawyer not edited");
        }
    }
}
