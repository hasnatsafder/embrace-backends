<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimLinesTable Test Case
 */
class ClaimLinesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimLinesTable
     */
    public $ClaimLinesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimLines',
        'app.Claims',
        'app.ClaimLineTypes',
        'app.Currencies',
        'app.Invoices',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimLines') ? [] : ['className' => ClaimLinesTable::class];
        $this->ClaimLinesTable = TableRegistry::getTableLocator()->get('ClaimLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimLinesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findHighestPlacementInClaim method
     *
     * @return void
     */
    public function testFindHighestPlacementInClaim()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
