<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CollectorFilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CollectorFilesTable Test Case
 */
class CollectorFilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CollectorFilesTable
     */
    public $CollectorFilesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CollectorFiles',
        'app.CollectorFileTypes',
        'app.ClaimActions',
        'app.ClaimFinancials',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CollectorFiles') ? [] : ['className' => CollectorFilesTable::class];
        $this->CollectorFilesTable = TableRegistry::getTableLocator()->get('CollectorFiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CollectorFilesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
