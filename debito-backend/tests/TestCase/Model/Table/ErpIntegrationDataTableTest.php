<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ErpIntegrationDataTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ErpIntegrationDataTable Test Case
 */
class ErpIntegrationDataTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ErpIntegrationDataTable
     */
    public $ErpIntegrationDataTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ErpIntegrationData',
        'app.ModifiedBy',
        'app.ErpIntegrations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ErpIntegrationData') ? [] : ['className' => ErpIntegrationDataTable::class];
        $this->ErpIntegrationDataTable = TableRegistry::getTableLocator()->get('ErpIntegrationData', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ErpIntegrationDataTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findIntegrationIs method
     *
     * @return void
     */
    public function testFindIntegrationIs()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
