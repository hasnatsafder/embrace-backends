<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccountsUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccountsUsersTable Test Case
 */
class AccountsUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AccountsUsersTable
     */
    public $AccountsUsersTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.AccountsUsers',
        'app.Accounts',
        'app.Users',
        'app.Roles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AccountsUsers') ? [] : ['className' => AccountsUsersTable::class];
        $this->AccountsUsersTable = TableRegistry::getTableLocator()->get('AccountsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AccountsUsersTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
