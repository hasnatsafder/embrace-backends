<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimLineTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimLineTypesTable Test Case
 */
class ClaimLineTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimLineTypesTable
     */
    public $ClaimLineTypesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimLineTypes',
        'app.ClaimLines',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimLineTypes') ? [] : ['className' => ClaimLineTypesTable::class];
        $this->ClaimLineTypesTable = TableRegistry::getTableLocator()->get('ClaimLineTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimLineTypesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getFromIdentifier method
     *
     * @return void
     */
    public function testGetFromIdentifier()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
