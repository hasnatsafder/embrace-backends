<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimActionTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimActionTypesTable Test Case
 */
class ClaimActionTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimActionTypesTable
     */
    public $ClaimActionTypesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimActionTypes',
        'app.ClaimPhases',
        'app.Collectors',
        'app.ClaimActions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimActionTypes') ? [] : ['className' => ClaimActionTypesTable::class];
        $this->ClaimActionTypesTable = TableRegistry::getTableLocator()->get('ClaimActionTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimActionTypesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
