<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimsTable Test Case
 */
class ClaimsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimsTable
     */
    public $ClaimsTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Claims',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Claims') ? [] : ['className' => ClaimsTable::class];
        $this->ClaimsTable = TableRegistry::getTableLocator()->get('Claims', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimsTable);

        parent::tearDown();
    }

    /**
     * Should return the correct code
     */
    public function testGenerateFiCheckCipher()
    {
        $this->assertEquals('8', $this->ClaimsTable->generateFiCheckCipher('02684014996532'));
        $this->assertEquals('8', $this->ClaimsTable->generateFiCheckCipher('00000000000001'));
        $this->assertEquals('6', $this->ClaimsTable->generateFiCheckCipher('00000000000002'));
        $this->assertEquals('4', $this->ClaimsTable->generateFiCheckCipher('00000000000003'));
        $this->assertEquals('0', $this->ClaimsTable->generateFiCheckCipher('29277694358756'));
        $this->assertEquals('6', $this->ClaimsTable->generateFiCheckCipher('22522690001139'));
        $this->assertEquals('7', $this->ClaimsTable->generateFiCheckCipher('85991464485621'));
    }

    /**
     * Must not work with too long string
     */
    public function testGenerateFiCheckCipherTooManyDigits()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->ClaimsTable->generateFiCheckCipher('026840149965321');
    }

    /**
     * Must not work with too short string
     */
    public function testGenerateFiCheckCipherTooLittleDigits()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->ClaimsTable->generateFiCheckCipher('0268401499653');
    }
}
