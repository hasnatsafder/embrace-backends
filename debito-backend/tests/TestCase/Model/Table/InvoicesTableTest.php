<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InvoicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InvoicesTable Test Case
 */
class InvoicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InvoicesTable
     */
    public $InvoicesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Invoices',
        'app.ModifiedBy',
        'app.Accounts',
        'app.InvoiceStatuses',
        'app.Currencies',
        'app.ErpIntegrations',
        'app.Debtors',
        'app.ClaimDunnings',
        'app.ClaimLines',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Invoices') ? [] : ['className' => InvoicesTable::class];
        $this->InvoicesTable = TableRegistry::getTableLocator()->get('Invoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoicesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test searchManager method
     *
     * @return void
     */
    public function testSearchManager()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test createClaim method
     *
     * @return void
     */
    public function testCreateClaim()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test filesystem method
     *
     * @return void
     */
    public function testFilesystem()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
