<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ErpIntegrationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ErpIntegrationsTable Test Case
 */
class ErpIntegrationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ErpIntegrationsTable
     */
    public $ErpIntegrationsTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ErpIntegrations',
        'app.ModifiedBy',
        'app.Accounts',
        'app.ErpIntegrationData',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ErpIntegrations') ? [] : ['className' => ErpIntegrationsTable::class];
        $this->ErpIntegrationsTable = TableRegistry::getTableLocator()->get('ErpIntegrations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ErpIntegrationsTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
