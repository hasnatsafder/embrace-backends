# Get started with the repo
There are multiple ways that you can prepare this repo to run locally.

## Docker (docker-compose)
- Install Docker (https://www.docker.com)
- Clone this repo, and follow readme points about `docker build`: https://bitbucket.org/debito/docker-php/src/master/
- Go to the folder containing this repo
- Make sure you have a `.env` file in the `./config` folder
- Run `$ docker-compose up -d` to startup all services needed
- Install dependencies `$ docker-compose exec -u www-data debito_web_dev composer install -n`
- Run migrations `$ docker-compose exec -u www-data debito_web_dev bin/cake migrations migrate`
- Visit `http://localhost:8081` to see the web ui, and `localhost:3306` to connect to the db from your host machine

### Pro tips
- Run `$ docker-compose down` to shutdown services again when you are done
- SSH into web container: `$ docker-compose exec -u www-data debito_web_dev bash`
- You can run `$ docker-compose up` (without `-d`) to see exactly what is going on rather than keeping them in background

## Valet
**Note: Valet only works on mac**

First install Valet on your local machine and sync folder with domain
[Guide on how to do that](https://laravel.com/docs/5.8/valet#installation)
After you have Valet installed and parked the domain, you should be able to visit ``` your-folder-name.test ``` in your browser.

## Nginx config
However if you want to have a different URL or different subdomains, you will need to configure your NGINX locally.
To do that follow steps below

- `$ cd ~/.config/valet/Nginx`
- `$ touch your-desired-domain.conf`
- `$ vi your-desired-domain.conf`
- Paste this nginx config

```
server {
    listen 80;
    server_name your-desired-domain.test;
    charset utf-8;
    root path-to-source-code/webroot;
    index index.php index.html;
    access_log /usr/local/var/log/nginx/access.log;
    error_log /usr/local/var/log/nginx/error.log;
    location ~ ^(.+\.php)(.*)$ {
    fastcgi_split_path_info ^(.+\.php)(.*)$;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param SCRIPT_NAME $fastcgi_script_name;
    fastcgi_pass unix:your-home-directory/.config/valet/valet.sock;
    fastcgi_index index.php;
    include fastcgi_params;
    }
}
```

- **NOTE**
    - *server name* property needs to be updated to whatever domain you like most
    - *root* replace path-to-source-code to your actual source code directory
    - *fast_cgi_pass* replace your-home-directory with your actual home directory. Usually it is ``` /Users/foobar ```
- Exit Vim [Guide on how to exit vim](https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor)
- Now if you visit ```your-desired-domain.test``` in your browser, you should see Hello world popping up

After that you updated nginx config run ``` sudo brew services restart mysql ```
[Online guide on how to set nginx up](https://outofcontrol.ca/blog/laravel-valet-usage-with-special-sites)

## Database migrations
Ask your colleagues for .env and app.php files.
Find DATABASE_URL variable and update it with your own credentials
```
export DATABASE_URL="mysql://{user}:{password}@{host}/{database_name}?encoding=utf8&timezone=UTC&cacheMetadata=true&quoteIdentifiers=false&persistent=false"
```

**NOTE**
When creating database make sure to select ```UTF-8``` encoding and ```utf8_bin``` database collation, otherwise the migrations will not work.

After you have your database created run ```bin/cake migrations migrate```

After migrations ran successfully, try to visit /admin page. If there is an error saying that database is not found you need open your ```config/app.php``` file. Find Datasources config and paste this with updated credentials

```
'debug_kit' => [
'className' => 'Cake\Database\Connection',
'driver' => 'Cake\Database\Driver\Mysql',
'persistent' => false,
'host' => '127.0.0.1', // Your DB host here
//'port' => 'nonstandard_port_number',
'username' => 'root', // Your DB username here
'password' => null, // Your DB password here
'database' => 'debug_kit',
'encoding' => 'utf8',
'timezone' => 'UTC',
'cacheMetadata' => true,
'quoteIdentifiers' => false,
//'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
],
```
**If you are using laradock and Sequel Pro**
There is a known bug in Mysql 8.0 and sequel pro. If you would connect to mysql service using Sequel pro it time out. To fix it you can downgrade Mysql version to 5.7. To do that open up ``` laradock/.env ``` file, find MYSQL_VERSION and update its value to 5.7. Finally run ``` docker-compose down && docker-compose build mysql && docker-compose up mysql nginx ```

## Laradock
- Start out by installing laradock [Guide on installing Laradock](https://laradock.io/)
- After that is done make sure your folder structure looks like this
```
|
|-- laradock
|-- debito-project-1
|-- debito-project-2
|
```
- ``` $ cd laradock/nginx/sites ```
- ``` $ touch your-desired-domain.conf ```
- Paste the contents of nginx config seen in Nginx config section
- Change directory back to laradock root folder
- Run ```docker-compose down && docker-compose up -d mysql nginx```
- Open up your hosts file by typing in ``` sudo nano /etc/hosts ```
- Add a new entry ``` 127.0.0.1 your-desired-domain.test ```
- Now if you type in domain in browser you should see Hello world! showing up.
- Now just repeat the steps written in database migrations section. **Note** All commands needs to be run inside the container. To 'get' inside the container run ``` docker-compose exec workspace bash ```

### Useful docker commands
1) ``` $ docker-compose up {list services that you want to start} ``` Will start your services. Most of the time you will just use nginx and mysql
2) ``` $ docker-compose down ``` Will stop all running containers
3) ``` $ docker-compose up -d {list services that needs to rebuild} ``` Sometimes you change service config and you need to rebuild it. This is where -d flag comes in.

## Laragon (if you are XAMPP fan and windows user)
Probably it will just work after installing executable and changing nginx config to read from /webroot directory.. 😅

## Sneaky shell commands
- Create user from CLI: `bin/cake CreateUser`