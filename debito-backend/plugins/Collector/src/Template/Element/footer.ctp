<?php
/**
 * @var \App\View\AppView $this
 */
?>
<footer class="page-footer">
    <!-- BEGIN PAGA BACKDROPS-->
    <!-- <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div> -->
    <!-- END PAGA BACKDROPS-->
    <div class="font-13"><?= date("Y") ?> <b> Debito</b> ApS.</div>
    <!-- CORE PLUGINS-->
    <?= $this->Html->script('/Admin/js/vendors/metisMenu.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/jquery.slimscroll.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/jquery.validate.js') ?>
    <?= $this->Html->script('/Admin/js/app.js') ?>
    <!-- PAGE LEVEL SCRIPTS-->
</footer>