<?php
/**
 * @var \App\View\AppView $this
 */
?>
<head>
    <title>Debito Collector</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <?= $this->Html->css('/Admin/css/vendors/bootstrap.css') ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <?= $this->Html->css('/Admin/css/vendors/line-awesome/css/line-awesome.css') ?>
    <?= $this->Html->css('/Admin/css/vendors/themify/css/themify-icons.css') ?>
    <?= $this->Html->css('/Admin/css/vendors/bootstrap-datepicker3.css') ?>
    <?= $this->Html->css('/Admin/css/vendors/select2.css') ?>
    <?= $this->Html->css('/Admin/css/vendors/bootstrap-select.css') ?>
    <?= $this->Html->css('/Admin/css/vendors/datatables.min.css') ?>
    <?= $this->Html->css('/Admin/css/main.css') ?>
    <?= $this->Html->script('/Admin/js/vendors/jquery.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/popper.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/bootstrap.js') ?>
    <!-- PAGE LEVEL STYLES-->
</head>
