<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claim
 * @var mixed $users
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Add Claim</div>
                </div>
                <?= $this->Form->create($claim, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('created_by_id',
                            [
                                'options' => $users,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary mr-2']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
