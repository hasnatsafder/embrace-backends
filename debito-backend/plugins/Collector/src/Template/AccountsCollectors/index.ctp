<?php
/**
 * @var \App\View\AppView $this
 * @var string $LOCALE_SITE
 * @var mixed $accountsCollectors
 */
?>
<div class="page-heading">
    <h1 class="page-title"><?= __d('Collector', 'collector_references') ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><?= __d('Collector', 'references') ?></li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title"><?= __d('Collector', 'references') ?> <?= __d('Collector', 'list') ?></div>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="input-group-icon input-group-icon-left mr-3">
                    <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                    <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                           placeholder="<?= __d('Collector', 'search') ?> ...">
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="langdatatable">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th><?= __d('Collector', 'name') ?></th>
                        <th><?= __d('Collector', 'created_at') ?></th>
                        <th><?= __d('Collector', 'zip_code') ?></th>
                        <th><?= __d('Collector', 'city') ?></th>
                        <th><?= __d('Collector', 'vat') ?></th>
                        <th class="no-sort"><?= __d('Collector', 'action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($accountsCollectors as $accountsCollector): ?>
                        <tr>
                            <td> <?= $accountsCollector->account->name; ?> </td>
                            <td> <?= date_format($accountsCollector->created, 'Y-m-d') ?> </td>
                            <td> <?= $accountsCollector->account->zip_code; ?> </td>
                            <td> <?= $accountsCollector->account->city; ?> </td>
                            <td> <?= $accountsCollector->account->vat_number; ?> </td>
                            <td>
                                <?= $this->Html->link(__d('Collector', 'edit'),
                                    ['action' => 'edit', $accountsCollector->id], ['class' => 'btn btn-success']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>

    <script>

        $(function () {
            var locale = "<?= $LOCALE_SITE ?>"; // comes from app controller, e.g English, Danish, German
            $('#langdatatable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/" + locale + ".json"
                },
                pageLength: 10,
                fixedHeader: true,
                responsive: true,
                "sDom": 'rtip',
                columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
            });

            var table = $('#langdatatable').DataTable();
            $('#key-search').on('keyup', function () {
                table.search(this.value).draw();
            });
        });

    </script>
