<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $accountsCollector
 */
?>
<div class="page-heading">
    <h1 class="page-title"><?= __('Collector References') ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('References', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item"><?= __('Edit') ?></li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?= __('Add Reference') ?></div>
                </div>
                <?= $this->Form->create($accountsCollector, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?= $this->Form->control('collector_identifier', ['type' => 'text']) ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary mr-2']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
