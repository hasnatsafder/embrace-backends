<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * ClaimTypes Controller
 *
 * @property \App\Model\Table\ClaimTypesTable $ClaimTypes
 */
class ClaimTypesController extends AppController
{
    public $modelClass = 'App.ClaimTypes';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
