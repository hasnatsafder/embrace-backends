<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * ClaimActionTypes Controller
 *
 * @property \App\Model\Table\ClaimActionTypesTable $ClaimActionTypes
 */
class ClaimActionTypesController extends AppController
{
    public $modelClass = 'App.ClaimActionTypes';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
