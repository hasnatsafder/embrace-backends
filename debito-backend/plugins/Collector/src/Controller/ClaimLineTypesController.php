<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * ClaimLineTypes Controller
 *
 * @property \App\Model\Table\ClaimLineTypesTable $ClaimLineTypes
 */
class ClaimLineTypesController extends AppController
{
    public $modelClass = 'App.ClaimLineTypes';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
