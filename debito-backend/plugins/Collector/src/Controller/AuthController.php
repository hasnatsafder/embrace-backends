<?php
declare(strict_types=1);

namespace Collector\Controller;

use Cake\Event\Event;

/**
 * Auth Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 */
class AuthController extends AppController
{
    public $modelClass = null;
    protected $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['login']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                //check if user role is consultant
                $this->loadModel('Roles');
                $this->loadModel('AccountsUsers');
                $accountsUser = $this->AccountsUsers->find(
                    'all',
                    ['conditions' => ['user_id' => $user['id']]]
                )->last();
                $roles = $this->Roles->get($accountsUser->role_id);
                if ($roles->identifier === "consultant") {
                    $this->Auth->setUser($user);

                    return $this->redirect($this->Auth->redirectUrl());
                }
            }
            $this->Flash->error(
                __d('Collector', 'incorrect_email_password'),
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
    }

    public function logout()
    {
        $this->Flash->success(
            __d('Collector', 'logged_out_now'),
            [
                'params' => [
                    'class' => 'alert alert-success',
                ],
            ]
        );

        return $this->redirect($this->Auth->logout());
    }
}
