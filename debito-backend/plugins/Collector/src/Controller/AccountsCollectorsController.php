<?php
declare(strict_types=1);

namespace Collector\Controller;

use Cake\I18n\FrozenTime;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersCollectorsTable $UsersCollectors
 * @property \App\Model\Table\AccountsCollectorsTable $AccountsCollectors
 */
class AccountsCollectorsController extends AppController
{
    public $modelClass = 'App.AccountsCollectors';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('UsersCollectors');
    }

    public function index()
    {
        $uid = $this->Auth->user('id');

        $userCollector = $this->UsersCollectors->find('all', ['conditions' => ['user_id' => $uid]])->first();

        //get collector ID from
        $accountsCollectors = $this->AccountsCollectors
            ->find(
                'all',
                [
                    'conditions' => [
                        'collector_identifier' => '',
                        'collector_id' => $userCollector->collector_id,
                        'invalid' => '0',
                    ],
                ]
            )
            ->contain(['Accounts']);
        $this->set(compact('accountsCollectors'));
    }

    public function edit($accoutsCollectorsId)
    {
        $accountsCollector = $this->AccountsCollectors->get($accoutsCollectorsId);

        if ($this->request->is(['post', 'put'])) {
            $accountsCollector->collector_identifier = $this->request->getData("collector_identifier");

            if ($this->AccountsCollectors->save($accountsCollector)) {
                $this->AccountsCollectors->Accounts->Claims->updateAll(
                    ['completed' => FrozenTime::now()],
                    ['account_id' => $accountsCollector->account_id]
                );

                $this->Flash->success(
                    __d('Collector', 'reference_added'),
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(
                __d('Collector', 'reference_not_added'),
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->set(compact(['accountsCollector']));
    }
}
