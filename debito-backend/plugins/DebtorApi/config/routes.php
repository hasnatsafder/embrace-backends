<?php
declare(strict_types=1);

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

$apiResources = [
    'Claims' => [
        'ClaimActions',
        'ClaimLines',
    ],
];

Router::plugin('DebtorApi', ['path' => '/debtor-api/v1'],
    function (RouteBuilder $routes) use ($apiResources) {
        foreach ($apiResources as $apiResource => $subResources) {
            if (is_int($apiResource)) {
                $routes->resources($subResources, ['inflect' => 'dasherize']);
            }

            if (is_array($subResources)) {
                $routes->resources(
                    $apiResource,
                    ['inflect' => 'dasherize'],
                    function (RouteBuilder $routes) use ($subResources, $apiResource) {
                        if (!empty($subResources)) {
                            foreach ($subResources as $resource) {
                                $routes->resources($resource, [
                                    'inflect' => 'dasherize',
                                    'prefix' => $apiResource,
                                ]);
                            }
                        }
                    }
                );
            }
        }
        $routes->fallbacks(DashedRoute::class);
    });
