<?php
declare(strict_types=1);

namespace DebtorApi\Auth;

use Cake\Auth\FormAuthenticate;
use Cake\Http\Response;
use Cake\Http\ServerRequest;

class DebtorAuthenticate extends FormAuthenticate
{
    /**
     * Find a user record using the username and password provided.
     *
     * Input passwords will be hashed even when a user doesn't exist. This
     * helps mitigate timing attacks that are attempting to find valid usernames.
     *
     * @param string $username The username/identifier.
     * @param string|null $password The password, if not provided password checking is skipped
     *   and result of find is returned.
     * @return array|false Either false on failure, or an array of user data.
     */
    protected function _findUser($username, $password = null)
    {
        $result = $this->_query($username)->first();

        if ($result === null) {
            // Waste time hashing the password, to prevent
            // timing side-channels. However, don't hash
            // null passwords as authentication systems
            // like digest auth don't use passwords
            // and hashing *could* create a timing side-channel.
            if ($password !== null) {
                $hasher = $this->passwordHasher();
                $hasher->hash($password);
            }

            return false;
        }

        $passwordField = $this->_config['fields']['password'];
        if ($password !== null) {
            if ($result->get($passwordField) !== $password) {
                return false;
            }

            $result->unsetProperty($passwordField);
        }
        $hidden = $result->getHidden();

        if ($password === null && in_array($passwordField, $hidden, true)) {
            $key = array_search($passwordField, $hidden, true);
            unset($hidden[$key]);
            $result->setHidden($hidden);
        }

        return $result->toArray();
    }

    /**
     * Get query object for fetching user from database.
     *
     * @param string $username The username/identifier.
     * @return \Cake\ORM\Query
     */
    protected function _query($username)
    {
        $config = $this->_config;
        $table = $this->getTableLocator()->get($config['userModel']);

        $options = [
        ];

        if (!empty($config['scope'])) {
            $options['conditions'] = array_merge($options['conditions'], $config['scope']);
        }
        if (!empty($config['contain'])) {
            $options['contain'] = $config['contain'];
        }

        $finder = $config['finder'];
        if (is_array($finder)) {
            $options += current($finder);
            $finder = key($finder);
        }

        if (!isset($options['username'])) {
            $options['username'] = $username;
        }

        return $table->find($finder, $options);
    }
}