<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

use Cake\Event\Event;
use Cake\ORM\Query;

/**
 * ClaimActions Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 */
class AccountsController extends AppController
{
    public $modelClass = 'App.Accounts';
    protected $allowedActions = ['index', 'view'];

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
                'Crud.beforeFind' => 'crudFinders',
                'Crud.beforePaginate' => 'crudFinders',
            ];
    }

    /**
     * @param \Cake\Event\Event $e
     */
    public function crudFinders(Event $e)
    {
        /** @var \Cake\ORM\Query $query */
        $query = $e->getSubject()->query;

        $query
            ->select([
                'id',
                'name',
                'address',
                'zip_code',
                'city',
                'phone',
                'vat_number',
            ])
            ->innerJoinWith('Claims', function (Query $q) {
                return $q->where(['Claims.debtor_id' => $this->Auth->user('id')]);
            })
            ->group(['Accounts.id']);
    }
}
