<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

use Api\Controller\AppController as ApiBaseController;
use Cake\Controller\Component\AuthComponent;
use Cake\Network\Exception\UnauthorizedException;

/**
 * Class AppController
 *
 * @package DebtorApi\Controller
 */
class AppController extends ApiBaseController
{
    public $paginate = [
    ];

    public $requireActiveAccount = false;

    /**
     * @throws \Exception
     */
    protected function loadAuthComponent(): void
    {
        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                AuthComponent::ALL => [
                    'finder' => 'DebtorPortalAuth',
                    'userModel' => 'Debtors',
                ],
                'DebtorApi.Debtor' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'login_code',
                    ],
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'finder' => 'all',
                    'fields' => [
                        'username' => 'id',
                    ],
                    'parameter' => 'token',
                    // Boolean indicating whether the "sub" claim of JWT payload
                    // should be used to query the Users model and get user info.
                    // If set to `false` JWT's payload is directly returned.
                    'queryDatasource' => true,
                    'unauthenticatedException' => UnauthorizedException::class,
                ],
            ],
            'authorize' => 'Controller',
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize',

            // If you don't have a login action in your application set
            // 'loginAction' to false to prevent getting a MissingRouteException.
            'loginAction' => false,
        ]);
    }
}
