<?php
declare(strict_types=1);

namespace DebtorApi\Controller\Claims;

use DebtorApi\Controller\AppController;

/**
 * ClaimLines Controller
 *
 * @property \App\Model\Table\ClaimLinesTable $ClaimLines
 */
class ClaimLinesController extends BaseClaimController
{
    public $modelClass = 'App.ClaimLines';
    protected $allowedActions = ['index', 'view'];
}
