<?php
declare(strict_types=1);

namespace DebtorApi\Controller\Claims;

/**
 * ClaimActions Controller
 *
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 */
class ClaimActionsController extends BaseClaimController
{
    public $modelClass = 'App.ClaimActions';
    protected $allowedActions = ['index', 'view'];
}
