<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

use Cake\Event\Event;
use Cake\ORM\Query;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 */
class ClaimsController extends AppController
{
    public $modelClass = 'App.Claims';
    protected $allowedActions = ['index', 'view', 'summary'];

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
                'Crud.beforeFind' => 'crudFinders',
                'Crud.beforePaginate' => 'crudFinders',
            ];
    }

    /**
     * @param \Cake\Event\Event $e
     */
    public function crudFinders(Event $e)
    {
        /** @var \Cake\ORM\Query $query */
        $query = $e->getSubject()->query;

        $query->matching('Debtors', function (Query $q) {
            return $q->where(['Debtors.id' => $this->Auth->user('id')]);
        });
    }

    public function summary(string $claimId)
    {
        $success = true;

        $data = $this->Claims->ClaimLines->find()
            ->select([
                'ClaimLines.claim_line_type_id',
                'sum_amount' => 'ROUND(SUM(ClaimLines.amount/100),2)',
            ])
            ->disableHydration()
            ->innerJoinWith('Claims', function (Query $q) use ($claimId) {
                return $q->where([
                    'Claims.id' => $claimId,
                    'Claims.debtor_id' => $this->Auth->user('id'),
                ]);
            })
            ->where(['ClaimLines.claim_id' => $claimId])
            ->group('ClaimLines.claim_line_type_id')
            ->toArray();

        $this->set(compact('data', 'success'));
    }
}
