<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

/**
 * ClaimActions Controller
 *
 * @property \App\Model\Table\CurrenciesTable $Currencies
 */
class CurrenciesController extends AppController
{
    public $modelClass = 'App.Currencies';
    protected $allowedActions = ['index', 'view'];
}
