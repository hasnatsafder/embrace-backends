<?php
declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use App\Model\Entity\Claim;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use League\Csv\RFC4180Field;
use League\Csv\CharsetConverter;
use League\Csv\Writer;
use League\Flysystem\MountManager;
use Psr\Log\LogLevel;
use RuntimeException;

class Mortang extends AbstractCsvAbstractBaseAdapter
{

    protected const CSV_HEADERS = [
        'customer_number',
        'name',
        'first_name',
        'last_name',
        'address',
        'address2',
        'address3',
        'zip',
        'city',
        'country_code',
        'phone_number1',
        'phone_number2',
        'cvr',
        'cpr',
        'extra_description',
        'invoice_number',
        'invoice_amount',
        'remaining_principle',
        'invoice_date',
        'due_date',
        'compensation_fee',
        'minimum_settlement_value',
        'interest_rate',
        'company_name',
        'case_type',
    ];
    protected const DESTINATION_FILESYSTEM_DIR = '/uploads/';
    protected const DESTINATION_FILESYSTEM_NAME = 'mortang';

    protected function setupCsvFileFromStream($fileStream)
    {
        $csvWriter = Writer::createFromStream($fileStream);

        //Handle RFC4180 compliance (https://csv.thephpleague.com/9.0/interoperability/rfc4180-field/)
        $csvWriter->setNewline("\r\n");
        RFC4180Field::addTo($csvWriter);

        $csvWriter
            //Prepare for MS Excel
            ->setDelimiter(';');

        $encoder = (new CharsetConverter())
            ->inputEncoding('utf-8')
            ->outputEncoding('iso-8859-1');
        $csvWriter->addFormatter($encoder);

        return $csvWriter;
    }

    /**
     * @param \League\Csv\Writer $csvWriter
     * @param \App\Model\Entity\Claim $claim
     * @return bool
     * @throws \Exception
     */
    protected function insertCsvDataFromClaim(Writer $csvWriter, Claim $claim)
    {
        $claimTable = TableRegistry::getTableLocator()->get('Claims');
        $claimPhaseTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $debtorTable = TableRegistry::getTableLocator()->get('Debtors');
        $accountTable = TableRegistry::getTableLocator()->get('Accounts');
        $claimLineTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claimLineTypeTable = TableRegistry::getTableLocator()->get('ClaimLineTypes');
        $currencyTable = TableRegistry::getTableLocator()->get('Currencies');
        $accountCollectorTable = TableRegistry::getTableLocator()->get('AccountsCollectors');
        $countriesTable = TableRegistry::getTableLocator()->get('Countries');

        //evaluate collectors reference
        $accountsCollector = $accountCollectorTable->find(
            "all",
            ['conditions' => ['account_id' => $claim->account_id, 'collector_id' => $claim->collector_id]]
        )->first();

        //valdiating data
        $data = [];
        $data['account_collector'] = $accountsCollector->collector_identifier;
        $data['vat_number'] = $accountsCollector->collector_identifier;

        $debtor = $debtorTable->get($claim->debtor_id);
        $account = $accountTable->get($claim->account_id);
        $claimLines = $claimLineTable->find(
            "all",
            ['conditions' => ['claim_id' => $claim->id]]
        );

        //evaluate debtors name
        $debtorName = $debtor->first_name . " " . $debtor->last_name;
        if (!empty($debtor->company_name)) {
            $debtorName = $debtor->company_name;
        }
        //Debtor Company or Private
        $debtorType = $debtor->is_company == 1 ? "F" : "P";

        //country name
        $countryName = "";
        if ($debtor->country_id) {
            $countryName = $countriesTable->get($debtor->country_id);
        }

        foreach ($claimLines as $claimLine) {
            $curreny = $currencyTable->get($claimLine->currency_id);

            //evaluate claim line type
            $amount = $claimLine->amount;
            $claimLineType = $claimLineTypeTable->find(
                "all",
                ['conditions' => ['id' => $claimLine->claim_line_type_id]]
            )->first();
            $costDeduction = "";
            if ($claimLineType->identifier == "late_payment_fee") {
                $amount = "";
                $costDeduction = $claimLine->amount;
            }

            //format dates
            if (!$claimLine->invoice_date || !$claimLine->maturity_date) {
                return "Dates are not set for some Claim Line ";
            }

            $debtor->address = str_replace(";", " ", $debtor->address);
            $debtor->address = str_replace(",", " ", $debtor->address);

            $claimLineArray = [
                $accountsCollector->collector_identifier, // K-Number
                "P", // Type of Claim
                $claim->customer_reference, // Creditor's reference
                $debtorName, // Surname or full name
                "", // first name
                $debtor->address, // Addressline 1
                "", // Addressline 2
                $countryName->iso_code, // Country ISO of Debtor
                $debtor->zip_code, //Debtor Zip Code
                $debtor->city, //Debtor City
                $debtor->vat_number, //Debtor VAT Number,
                "", // birthday
                $debtorType, // P or F for debtor
                $debtor->phone, // Debtor Phone
                "", // Debtor home phone
                $claimLineType->name, //Misc
                str_replace(";", " ", $claim->dispute), // Dispute
                date_format($claimLine->invoice_date, 'd.m.Y'), // Claim Invoice Date
                date_format($claimLine->maturity_date, 'd.m.Y'), // Claim Maturity Date
                "STD", // interest rate
                $amount, // Claim Line amount
                $claimLine->invoice_reference, // Invoice Reference on Invoice
                $costDeduction, // Cost Deduction
                "", //Customer interest rate
                $curreny->iso_code, // three digit code of currency
                $account->name . " " . $account->vat_number, // Account name and vat number of account
                $account->bank_reg_number . " " . $account->bank_account_number, // Bank Reg number and bank account number
                $account->email, // will be changed once email is added to accounts,
                "END",
            ];
            $csvWriter->insertOne($claimLineArray);
        }
        //Moved Claim to phase 3
        $claimPhaseOrder3 = $claimPhaseTable->find(
            'all',
            ['conditions' => ["`order`" => 3]]
        )->first();
        $claim->claim_phase_id = $claimPhaseOrder3->id;
        $claim->synced = date("Y-m-d H:i:s");
        $save = $claimTable->save($claim);

        if ($save) {
            return true;
        }

        return "Unable to save Claim in Database";
    }

    protected function getValidator(): Validator
    {
        /**
         * 'customer_number',
         * 'name',
         * 'first_name',
         * 'last_name',
         * 'address',
         * 'address2',
         * 'address3',
         * 'zip',
         * 'city',
         * 'country_code',
         * 'phone_number1',
         * 'phone_number2',
         * 'cvr',
         * 'cpr',
         * 'extra_description',
         * 'invoice_number',
         * 'invoice_amount',
         * 'remaining_principle',
         * 'invoice_date',
         * 'due_date',
         * 'compensation_fee',
         * 'minimum_settlement_value',
         * 'interest_rate',
         * 'company_name',
         * 'case_type',
         */

        $validator = new Validator();

        $validator->requirePresence('accounts-collector')
            ->requirePresence('vat_number');

        return $validator;
    }

    /**
     * @param string $fileName
     */
    protected function uploadCsvFileToCollector(string $fileName, $claim)
    {

        $claimTable = TableRegistry::getTableLocator()->get('Claims');
        $accountCollectorTable = TableRegistry::getTableLocator()->get('AccountsCollectors');
        $collectorTable = TableRegistry::getTableLocator()->get('Collectors');

        //set collector reference to that claim
        $mortang = $collectorTable->find('all')->where(['identifier' => 'mortang'])->first();
        $collector_reference = $accountCollectorTable->find('all')->where(['collector_id' => $mortang->id, 'account_id' => $claim->account_id])->first();
        if (!$collector_reference) {
            return "No collector reference found for" . $claim->collector_identifier . " Aborting now !!!";
        }
        $claim->collector_reference = $collector_reference->collector_identifier;
        $claimTable->save($claim);

        if (empty($this::DESTINATION_FILESYSTEM_NAME) || empty($this::DESTINATION_FILESYSTEM_DIR)) {
            throw new RuntimeException('No destination filesystem for the file has been set');
        }

        $mountManager = new MountManager([
            'source' => $this->casesFileSystem,
            'destination' => $this->filesystem($this::DESTINATION_FILESYSTEM_NAME),
        ]);

        $destinationFileName = $claim->collector_reference . "_" . "sagsfil_" . date("Y-m-d") . "_" . $claim->customer_reference . "_" . $claim->id . '.csv';
        $destinationFilePath = $this::DESTINATION_FILESYSTEM_DIR . $destinationFileName;

        // if file already exists remove it, so that we can overwrite
        if ($mountManager->has('destination://' . $destinationFilePath)) {
            $mountManager->delete('destination://' . $destinationFilePath);
        }

        if ($mountManager->copy('source://' . $fileName, 'destination://' . $destinationFilePath)) {
            $this->log(
                sprintf(
                    'Saved file: "%s" on remote filesystem: "%s"',
                    $destinationFilePath,
                    $this::DESTINATION_FILESYSTEM_NAME
                ),
                LogLevel::INFO
            );
        }
    }
}
