<?php
declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use App\Model\Entity\Claim;
use Cake\Collection\CollectionInterface;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use League\Csv\RFC4180Field;
use League\Csv\Writer;
use League\Flysystem\MountManager;
use Psr\Log\LogLevel;
use RuntimeException;
use WyriHaximus\FlyPie\FilesystemsTrait;
use Cake\Mailer\Email;

abstract class AbstractCsvAbstractBaseAdapter extends AbstractBaseAdapter
{
    use FilesystemsTrait;

    protected const CSV_HEADERS = [];
    protected const CASE_UPLOAD_DIR = 'uploads' . DS;
    protected const DESTINATION_FILESYSTEM_NAME = '';
    protected const DESTINATION_FILESYSTEM_DIR = '/';

    /**
     * @var \League\Flysystem\AdapterInterface
     */
    protected $casesFileSystem;

    public function __construct()
    {
        // from https://csv.thephpleague.com/9.0/connections - important only on macs
        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        $this->casesFileSystem = $this->filesystem('internal_case_files');
    }

    public function uploadClaims(CollectionInterface $claims)
    {
        if ($claims->isEmpty()) {
            throw new RuntimeException('No claims to uploader...');
        }
        foreach ($claims as $claim) {
            $fileStream = tmpfile();
            $csvWriter = $this->setupCsvFileFromStream($fileStream);
            $insertData = $this->insertCsvDataFromClaim($csvWriter, $claim);
            // if some issue with data then dont submit data
            if ($insertData !== true) {
                //issue an email for error with the claim
                $emailMessage = "Error " . $insertData . " Occured for claim " . $claim->customer_reference;
                $email = new Email('sparkpost');
                $myemail = $email->from(['kundeservice@debito.dk' => 'Debito'])
                    ->to("christoffer@debito.dk")
                    ->subject("Error Occured while syncing claim")
                    ->emailFormat('html')
                    ->send($emailMessage);
                echo $emailMessage . PHP_EOL;
                continue;
            }
            $this->dispatchEvent('CollectorFiles.UploadFileHandler.ClaimProgressUpdate');
            $fileName = $this->saveStreamToCsv($fileStream);
            $this->uploadCsvFileToCollector($fileName, $claim);
        }
    }

    protected function setupCsvFileFromStream($fileStream)
    {
        $csvWriter = Writer::createFromStream(tmpfile());

        //Handle RFC4180 compliance (https://csv.thephpleague.com/9.0/interoperability/rfc4180-field/)
        $csvWriter->setNewline("\r\n");
        RFC4180Field::addTo($csvWriter);

        //Prepare for MS Excel
        $csvWriter->setOutputBOM(Writer::BOM_UTF8);

        $this->insertCsvHeaders($csvWriter);
    }

    protected function insertCsvHeaders(Writer $csvWriter): void
    {
        if (!empty($this::CSV_HEADERS)) {
            $csvWriter->insertOne($this::CSV_HEADERS);
        }
    }

    abstract protected function insertCsvDataFromClaim(Writer $csvWriter, Claim $claim);

    /**
     * @param resource $stream
     * @return string$
     */
    protected function saveStreamToCsv($stream): string
    {
        $fileName = $this::CASE_UPLOAD_DIR . $this->getCollectorName() . DS . Text::uuid() . '.csv';

        if (!$this->casesFileSystem->writeStream($fileName, $stream)) {
            throw new RuntimeException('Could not save stream to file');
        }

        return $fileName;
    }

    /**
     * @param string $fileName
     */
    protected function uploadCsvFileToCollector(string $fileName, $claim)
    {
        if (empty($this::DESTINATION_FILESYSTEM_NAME) || empty($this::DESTINATION_FILESYSTEM_DIR)) {
            throw new RuntimeException('No destination filesystem for the file has been set');
        }

        $mountManager = new MountManager([
            'source' => $this->casesFileSystem,
            'destination' => $this->filesystem($this::DESTINATION_FILESYSTEM_NAME),
        ]);

        $destinationFileName = "sagsfil_" . date("Y-m-d") . "_" . $claim->customer_reference . "_" . $claim->id . '.csv';
        $destinationFilePath = $this::DESTINATION_FILESYSTEM_DIR . $destinationFileName;

        // if file already exists remove it, so that we can overwrite
        if ($mountManager->has('destination://' . $destinationFilePath)) {
            $mountManager->delete('destination://' . $destinationFilePath);
        }

        if ($mountManager->copy('source://' . $fileName, 'destination://' . $destinationFilePath)) {
            $this->log(
                sprintf(
                    'Saved file: "%s" on remote filesystem: "%s"',
                    $destinationFilePath,
                    $this::DESTINATION_FILESYSTEM_NAME
                ),
                LogLevel::INFO
            );
        }
    }

    /**
     * @param $data
     * @return array|bool
     */
    protected function validate($data)
    {
        $validator = $this->getValidator();

        $errors = $validator->errors($data);

        return count($errors) === 0 ?: $errors;
    }

    abstract protected function getValidator(): Validator;
}
