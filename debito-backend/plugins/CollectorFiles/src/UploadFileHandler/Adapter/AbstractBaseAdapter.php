<?php
declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use Cake\Collection\CollectionInterface;
use Cake\Event\EventDispatcherTrait;
use Cake\Log\LogTrait;
use ReflectionClass;
use RuntimeException;
use Cake\ORM\TableRegistry;

abstract class AbstractBaseAdapter
{
    use EventDispatcherTrait;
    use LogTrait;

    abstract public function uploadClaims(CollectionInterface $claims);

    public function getCollectorName()
    {
        $collectorName = null;
        $nameSpacePrefix = 'CollectorFiles\\UploadFileHandler\\Adapter\\';

        if (mb_strpos(static::class, $nameSpacePrefix) === 0) {
            $collectorName = mb_substr(static::class, mb_strlen($nameSpacePrefix));
        }

        //Check if abstract
        $reflectionClass = new ReflectionClass(static::class);

        if ($reflectionClass->isAbstract()) {
            throw new RuntimeException('No specific collector chosen - this is not intentional');
        }

        return mb_convert_case($collectorName, MB_CASE_LOWER);
    }

    /**
     * @param string $identifier
     * @return string id of claim Phase
     * @description Get Id of an order from claim phase table
     */
    protected function getClaimPhase(string $identifier): string
    {
        //get order key from claim phases
        $claimPhaseOrder = TableRegistry::getTableLocator()->get('ClaimPhases')
            ->find('all')
            ->select('id')
            ->where(['ClaimPhases.identifier' => $identifier])
            ->first();

        return $claimPhaseOrder->id;
    }
}
