<?php

declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use App\Model\Entity\Claim;
use Cake\Collection\CollectionInterface;
use Cake\Validation\Validator;
use GuzzleHttp\Client;
use RuntimeException;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use App\Utility\NotifyAdmin;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Psr\Log\LogLevel;

abstract class AbstractJsonApiAbstractBaseAdapter extends AbstractBaseAdapter
{

    protected const BASE_URL = "";
    protected const COLLECTOR_NAME = "";
    public $http;
    public $claimsTable;
    public $claimLineTable;
    public $debtorsTable;
    public $accountsTable;
    public $countriesTable;
    public $usersTable;
    public $claimPhaseTable;

    public function __construct()
    {
        $this->claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimLineTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $this->claimPhaseTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $this->debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->countriesTable = TableRegistry::getTableLocator()->get('Countries');
        $this->usersTable = TableRegistry::getTableLocator()->get('Users');
    }

    public function uploadClaims(CollectionInterface $claims)
    {
        if ($claims->isEmpty()) {
            throw new RuntimeException('No claims to uploader...');
        }
    }

    /**
     * Upload Json API
     *
     * @param array $data
     * @return Guzzle Response
     */
    public function sendApiRequest(array $data, $end_point, $headers = [])
    {
        $this->http = new Client([
            'base_uri' => Configure::read($this::COLLECTOR_NAME . '.api'),
            'headers' => $headers,
        ]);
        try {
            $response = $this->http->post($end_point, [
                'json' => $data,
            ]);

            return ["created" => true, "response" => json_decode($response->getBody()->getContents(), true)];
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = json_decode($e->getResponse()->getBody()->getContents());
                $this->logAndTerminal($response->message);
                NotifyAdmin::uploadClaimsToCollectorErrors($response->message, $this::COLLECTOR_NAME, $data['caseInfo']['reference']);
            }

            return ["created" => false, "response" => $response->message];
        }
    }

    /**
     * Show in terminal and log
     *
     * @param string text
     * @return bool
     */
    protected function logAndTerminal(string $text)
    {
        $this->log(
            sprintf(
                $text
            ),
            LogLevel::INFO
        );
    }

    abstract protected function buildJsonData(Claim $claim);

    /**
     * @param $data
     * @return array|bool
     */
    protected function validate($data)
    {
        $validator = $this->getValidator();

        $errors = $validator->errors($data);

        return count($errors) === 0 ?: $errors;
    }

    abstract protected function getValidator(): Validator;

    /**
     * Get users email as default if not found
     *
     * @param user_id
     */
    protected function getUserEmail($user_id)
    {
        $user = $this->usersTable->get($user_id);

        return trim($user->email);
    }
}
