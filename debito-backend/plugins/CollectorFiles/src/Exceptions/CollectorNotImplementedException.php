<?php
declare(strict_types=1);

namespace CollectorFiles\Exceptions;

use Cake\Core\Exception\Exception;

class CollectorNotImplementedException extends Exception
{
    protected $_messageTemplate = 'Collector "%s" has not been implemented';
    protected $code = 500;
}
