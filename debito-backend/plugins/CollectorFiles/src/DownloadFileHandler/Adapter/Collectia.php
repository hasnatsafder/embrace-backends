<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

use App\Utility\HelperFunctions;
use App\Utility\NotifyAdmin;
use Cake\Core\Configure;

class Collectia extends AbstractJsonApiAbstractBaseAdapter
{
    protected const COLLECTOR_NAME = "Collectia";

    /**
     * @return bool
     */
    protected function getReceiptFile()
    {
        // get all claims that are in sent phase and have collectia as collector
        $collectiaClaims = $this->claimTable->find()
            ->where([
                'claim_phase_id' => $this->getClaimPhase('case_sent'),
                'collector_id' => $this->getCollectorID('collectia'),
            ]);

        foreach ($collectiaClaims as $claim) {
            // get account information
            $account = $this->accountsTable->get($claim->account_id);
            // if no vat number for account found then notfiy admin that we have for collectia
            // a private account(empty vat_number)
            if ($account->vat_number == "" || !$account->vat_number) {
                NotifyAdmin::emptyVatForCollectia($account->name, $claim->customer_reference);
                continue;
            }

            $headers = [
                "Authorization" => "Basic " . Configure::read($this::COLLECTOR_NAME . '.key'),
                "Content-Type" => "application/json",
            ];
            $getReceipts = $this->getApiRequest("SingleReceipt/" . $account->vat_number . "/" . $claim->customer_reference, $headers, $claim->id);

            if ($getReceipts["fetched"]) {
                // set claim customer_reference, common for all switches
                $claim->collector_reference = $getReceipts['response']['caseNumber'];
                switch ($getReceipts['response']['statusId']) {
                    case -1:
                        $rejectedPhaseID = $this->getClaimPhase('rejected');
                        $claim->claim_phase_id = $rejectedPhaseID;
                        // enter rejected reasons, we expect soemthing like "Case has been rejected, reason = Person kunne ikke verificeres"
                        // so we need to split "reasons = "
                        if (strpos($getReceipts['response']['status'], 'reason = ') !== false) {
                            $split = explode("reason = ", $getReceipts['response']['status']);
                            $claim->rejected_reason = $split[1];
                        } else {
                            $claim->rejected_reason = $getReceipts['response']['status'];
                        }
                        $this->claimTable->save($claim);
                        $this->logAndTerminal("Claim rejected " . $claim->customer_reference . " due to reason " . $getReceipts['response']['status']);
                        NotifyAdmin::claimRejected($claim->customer_reference, $getReceipts['response']['status'], $this::COLLECTOR_NAME);
                        break;
                    case 0:
                        // dont do anything to claim
                        break;
                    case 1:
                        // move claim to reminder phase and we receive a case number from them
                        if ($getReceipts['response']['caseNumber'] && $getReceipts['response']['caseNumber'] != 0) {
                            $reminderPhaseID = $this->getClaimPhase('reminder');
                            $claim->claim_phase_id = $reminderPhaseID;
                            $claim->collector_reference = $getReceipts['response']['caseNumber'];
                            $this->claimTable->save($claim);
                            $this->logAndTerminal("Claim moved to reminder " . $claim->customer_reference . " status is" . $getReceipts['response']['status']);
                        }
                        break;
                    case 2:
                        // move claim to debt collection and we receive a case number from them
                        if ($getReceipts['response']['caseNumber'] && $getReceipts['response']['caseNumber'] != 0) {
                            $debtCollectionPhaseID = $this->getClaimPhase('debt_collection');
                            $claim->claim_phase_id = $debtCollectionPhaseID;
                            $claim->collector_reference = $getReceipts['response']['caseNumber'];
                            $this->claimTable->save($claim);
                            $this->logAndTerminal("Claim moved to debt collection " . $claim->customer_reference . " status is " . $getReceipts['response']['status']);
                        }
                        break;
                    default:
                        // dont do anything to claim
                }
            }
        }
    }

    /**
     * Get the id of collector
     * @param string $identifier
     * @return mixed
     */
    private function getCollectorID($identifier = 'collectia')
    {
        $collector = $this->collectorTable->find()
            ->where(['identifier' => $identifier])
            ->select(['id'])
            ->first();

        return $collector->id;
    }

    protected function getStatusFile()
    {
        $collectiaId = $this->getCollectorID('collectia');
        $debitoCollectorId = $this->getCollectorID('debito');
        $debtCollectionPhase = $this->getClaimPhase('debt_collection');
        $endedPhase = $this->getClaimPhase('ended');
        $legalPhase = $this->getClaimPhase('legal_collection');
        $headers = [
            "Authorization" => "Basic " . Configure::read($this::COLLECTOR_NAME . '.key'),
            "Content-Type" => "application/json",
        ];
        // run api for last 1 month statuses
        $getStatuses = $this->getApiRequest("gethistory/" . date('Y-m-d', strtotime("-3 month")) . "/ " . date('Y-m-d') . "/", $headers)["response"]["kunder"];
        //$getStatuses = $getStatuses;
        $statusArray = [];
        $counter = 0;
        foreach ($getStatuses as $key => $status) {
            $counter++;
            foreach ($status["sager"] as $key => $claimCollectia) {
                // move claim to debt-collection
                $claim = $this->claimTable->find()->where(['collector_id' => $collectiaId, 'collector_reference' => $key])->first();
                // if no claim found continue and log error
                if (!$claim) {
                    $this->logAndTerminal("Claim not found for " . $key);
                    continue;
                }

                // for each history save to claim actions
                // delete all previous actions for that claim
                $actions = $this->claimActionTable->find('all')
                    ->where(['claim_id' => $claim->id, 'ClaimActionTypes.collector_id !=' => $debitoCollectorId])
                    ->contain('ClaimActionTypes');
                foreach ($actions as $action) {
                    $this->claimActionTable->delete($action);
                }

                // set default status of claim either debt collecion or RETSLIG
                if ($claimCollectia['status'] == "RETSLIG") {
                    $phase = $legalPhase;
                }
                else {
                    $phase = $debtCollectionPhase;
                }
                foreach ($claimCollectia["historikNoter"] as $action) {
                    $date = date_create($action["dato"]);
                    $date = date_format($date, "Y-m-d");

                    // if afsluttet found then move claim to ended phase, intialzie from debtcollection
                    if (strpos($action['hændelse'], "Afsluttet") !== false) {
                        $phase = $endedPhase;
                    }

                    $this->updateClaimAndClaimActions(
                        $action['hændelse'],
                        $date,
                        0, // paid
                        0, // interest_fee
                        0, // debt_collection
                        1, // is_public
                        $phase,
                        $claim,
                        $collectiaId
                    );
                }

                // if payment betalinger found then save them to claim actions
                // with paid = indebetalt and date = betalingsdato
                foreach ($claimCollectia["betalinger"] as $paidAction) {
                    $date = date_create($paidAction["betalingsdato"]);
                    $date = date_format($date, "Y-m-d");

                    $this->updateClaimAndClaimActions(
                        "betaling",
                        $date,
                        HelperFunctions::convertToDanishCurrency(strval($paidAction['indbetalt'])), // paid
                        0, // interest_fee
                        0, // debt_collection
                        1, // is_public
                        null,
                        $claim,
                        $collectiaId
                    );
                }
            }
        }
    }

    /**
     * @return bool
     * @description Download and process financial files from Collectors Server
     */

    protected function getFinancialFile()
    {
        echo "No Financial file for mortang yet";
    }

    protected function getInbFile()
    {
        echo "No Financial file for mortang yet";
    }
}
