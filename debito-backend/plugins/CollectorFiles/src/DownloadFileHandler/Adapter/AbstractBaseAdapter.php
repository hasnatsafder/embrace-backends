<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

use Cake\Event\EventDispatcherTrait;
use Cake\Log\LogTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use ReflectionClass;
use RuntimeException;

abstract class AbstractBaseAdapter
{
    use EventDispatcherTrait;
    use LocatorAwareTrait;
    use LogTrait;

    abstract public function downloadClaimReceipts();

    abstract public function downloadClaimStatus();

    public function getCollectorName()
    {
        $collectorName = null;
        $nameSpacePrefix = 'CollectorFiles\\DownloadFileHandler\\Adapter\\';

        if (mb_strpos(static::class, $nameSpacePrefix) === 0) {
            $collectorName = mb_substr(static::class, mb_strlen($nameSpacePrefix));
        }

        //Check if abstract
        $reflectionClass = new ReflectionClass(static::class);

        if ($reflectionClass->isAbstract()) {
            throw new RuntimeException('No specific collector chosen - this is not intentional');
        }

        return mb_convert_case($collectorName, MB_CASE_LOWER);
    }

    /**
     * @param string $identifier
     * @return string id of claim Phase
     * @description Get Id of an order from claim phase table
     */
    protected function getClaimPhase(string $identifier): string
    {
        //get order key from claim phases
        $claimPhaseOrder = $this->getTableLocator()->get('ClaimPhases')
            ->find('all')
            ->select('id')
            ->where(['ClaimPhases.identifier' => $identifier])
            ->first();

        return $claimPhaseOrder->id;
    }
}
