<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * CompanyTypes Controller
 *
 * @property \App\Model\Table\CompanyTypesTable $CompanyTypes
 */
class CompanyTypesController extends AppController
{
    public $modelClass = 'App.CompanyTypes';
    protected $allowedActions = ['index', 'view'];
}
