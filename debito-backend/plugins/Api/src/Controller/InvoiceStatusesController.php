<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * InvoiceStatuses Controller
 *
 * @property \App\Model\Table\InvoiceStatusesTable $InvoiceStatuses
 */
class InvoiceStatusesController extends AppController
{
    public $modelClass = 'App.InvoiceStatuses';
    protected $allowedActions = ['index', 'view'];
}
