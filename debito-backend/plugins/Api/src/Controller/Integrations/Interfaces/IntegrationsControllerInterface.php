<?php
declare(strict_types=1);

namespace Api\Controller\Integrations\Interfaces;

interface IntegrationsControllerInterface
{
    public function status();

    public function startAuth();

    public function finishAuth();

    public function delete();
}
