<?php
declare(strict_types=1);

namespace Api\Controller\Integrations\Traits;

use App\Model\Entity\ErpIntegration;
use App\Utility\ErpIntegrations;

trait ErpControllerTrait
{
    /**
     * Checks if we already have a token for e-conomic
     *
     * @param string $accountId
     * @return bool
     */
    private function _hasExistingTokenSaved(string $accountId, $erpVendor): bool
    {
        $existingToken = $this->ErpIntegrationData->find()
            ->find('IntegrationIs', [
                'integration' => $erpVendor,
                'account_id' => $accountId,
            ])
            ->where([
                'ErpIntegrationData.data_key' => 'auth_token',
            ]);

        return !$existingToken->isEmpty();
    }

    /**
     * Save the token returned from e-conomic for further usage
     *
     * @param string $token
     * @param string $accountId
     * @param string $userId
     * @param string $erpVendor
     * @return bool
     */
    private function _saveToken(string $token, string $accountId, string $userId, string $erpVendor, string $org_id = null): bool
    {
        $erpIntegration = $this->_getErpConnection($accountId, $userId, $erpVendor);

        $this->_deleteExistingToken($erpIntegration->id, 'auth_token');

        $newToken = $this->ErpIntegrationData->newEntity()
            ->setAccess('*', true);

        $newToken = $this->ErpIntegrationData->patchEntity($newToken, [
            'created_by_id' => $userId,
            'modified_by_id' => $userId,
            'account_id' => $accountId,
            'erp_integration_id' => $erpIntegration->id,
            'data_key' => 'auth_token',
            'data_value' => $token,
            'org_id' => $org_id,
        ]);

        $this->ErpIntegrationData->save($newToken);

        return $this->ErpIntegrationData->save($newToken) !== false;
    }

    /**
     * Gets the ErpIntegration instance - creates one if none exists
     *
     * @param string $accountId
     * @param string $userId
     * @return array|\Cake\Datasource\EntityInterface
     */
    private function _getErpConnection(string $accountId, string $userId, string $erpVendor)
    {
        return $this->ErpIntegrationData->ErpIntegrations->findOrCreate([
            'ErpIntegrations.account_id' => $accountId,
            'ErpIntegrations.vendor_identifier' => $erpVendor,
        ], function (ErpIntegration $erpIntegration) use ($accountId, $userId, $erpVendor) {
            $erpIntegration = $erpIntegration->setAccess('*', true);

            $erpIntegration->set([
                'created_by_id' => $userId,
                'modified_by_id' => $userId,
                'vendor_identifier' => $erpVendor,
                'account_id' => $accountId,
            ]);
        });
    }

    /**
     * @param $erpIntegrationId
     * @param string $dataKey
     */
    private function _deleteExistingToken($erpIntegrationId, string $dataKey): void
    {
        $existingTokens = $this->ErpIntegrationData->find()
            ->where([
                'ErpIntegrationData.erp_integration_id' => $erpIntegrationId,
                'ErpIntegrationData.data_key' => $dataKey,
            ]);

        if ($existingTokens->isEmpty()) {
            return;
        }

        foreach ($existingTokens as $existingToken) {
            $this->ErpIntegrationData->delete($existingToken);
        }
    }
}
