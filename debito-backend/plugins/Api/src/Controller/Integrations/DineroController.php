<?php
declare(strict_types=1);

namespace Api\Controller\Integrations;

use Api\Controller\AppController;
use Api\Controller\Integrations\Interfaces\IntegrationsControllerInterface;
use Api\Controller\Integrations\Traits\ErpControllerTrait;
use Cake\Core\Exception\Exception;
use Cake\Http\Exception\BadRequestException;
use EconomyPie\Client\Adapter\DineroAdapter;
use InvalidArgumentException;

/**
 * D Controller
 *
 * @property \App\Model\Table\ErpIntegrationDataTable $ErpIntegrationData
 * @property \Queue\Model\Table\QueuedJobsTable $QueuedJobs
 */
class DineroController extends AppController implements IntegrationsControllerInterface
{
    use ErpControllerTrait;

    public $modelClass = 'App.ErpIntegrationData';
    protected $requireActiveAccount = true;
    protected $allowedActions = [
        'status',
        'delete',
        'startAuth',
        'finishAuth',
    ];

    /**
     * Used to check if the connection to Dinero is active and working
     */
    public function status()
    {
        $this->getRequest()->allowMethod('GET');

        $data = [
            'success' => true,
            'data' => [],
        ];

        $erpIntegration = $this->ErpIntegrationData->ErpIntegrations->find()
            ->where([
                'ErpIntegrations.vendor_identifier' => 'dinero',
                'ErpIntegrations.account_id' => $this->Auth->user('active_account_id'),
            ])
            ->first();

        $authData = $erpIntegration->getAuthData();

        $dineroClient = new DineroAdapter($authData);

        $request = $dineroClient->getClient()->get('/organizations');
        $responseCode = $request->getStatusCode();

        if ($responseCode !== 200) {
            $data['success'] = false;
        }

        $this->set($data);
    }

    /**
     * Initializes the connection to e-conomic, returns a URL that can be used to Authenticate via OAuth at e-conomic
     */
    public function startAuth()
    {
        $this->getRequest()->allowMethod(['POST']);
        $accountId = (string)$this->Auth->user('active_account_id');

        if ($this->_hasExistingTokenSaved($accountId, 'dinero')) {
            throw new BadRequestException('It seems that you already added an accounting system. Go here ' . Router::url(Configure::read('Frontendurl') . "accountingsystem") . ' to check it out ');
        }

        $response = [
            'data' => [],
            'success' => true,
        ];

        $dineroApiKey = $this->request->getData('api_key');

        $DineroAuth = new \EconomyPie\Auth\Adapter\DineroAdapter([
            'api_key' => $dineroApiKey,
        ]);

        $response['data'] = $DineroAuth->getOrganizations();

        $this->set($response);
    }

    public function finishAuth()
    {
        $this->getRequest()->allowMethod('POST');

        $userId = (string)$this->Auth->user('id');
        $accountId = (string)$this->Auth->user('active_account_id');
        $dineroApiKey = $this->getRequest()->getData('api_key');
        $org_id = $this->request->getData('org_id');

        // Recheck Token for dinero
        $DineroAuth = new \EconomyPie\Auth\Adapter\DineroAdapter([
            'api_key' => $dineroApiKey,
        ]);

        $token = $DineroAuth->getToken();

        if (empty($token)) {
            throw new InvalidArgumentException('There was no token from Dinero');
        }
        // End Recheck Dinero Token

        if (!$this->_saveToken($dineroApiKey, $accountId, $userId, 'dinero', $org_id)) {
            throw new Exception('Token could not be saved');
        }

        // send to queuee token has been saved.
        $erpIntegration = $this->ErpIntegrationData->find()
            ->where([
                'data_value' => $dineroApiKey,
            ])->order(['created' => 'ASC'])
            ->last();
        if ($erpIntegration) {
            $this->loadModel('Queue.QueuedJobs');
            $this->QueuedJobs->createJob('Erp', ['erpIntegration_id' => $erpIntegration->erp_integration_id],
                ['reference' => $erpIntegration->erp_integration_id]);
        }

        $this->set([
            'data' => ['reference' => $erpIntegration->erp_integration_id],
            'success' => true,
        ]);
    }

    /**
     * Deletes the connection
     */
    public function delete()
    {
        $this->getRequest()->allowMethod(['DELETE', 'POST']);

        $token = $this->ErpIntegrationData->find()
            ->find('IntegrationIs', [
                'integration' => 'dinero',
                'account_id' => $this->Auth->user('active_account_id'),
            ])
            ->where([
                'ErpIntegrationData.data_key' => 'auth_token',
            ])
            ->firstOrFail();

        $this->ErpIntegrationData->deleteOrFail($token);

        $this->set([
            'success' => true,
            'data' => [],
        ]);
    }
}
