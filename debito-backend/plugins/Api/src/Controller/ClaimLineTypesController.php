<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * ClaimLineTypes Controller
 *
 * @property \App\Model\Table\ClaimLineTypesTable $ClaimLineTypes
 */
class ClaimLineTypesController extends AppController
{
    public $modelClass = 'App.ClaimLineTypes';
    protected $allowedActions = ['index', 'view'];
}
