<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * DebtorLawyers Controller
 *
 * @property \App\Model\Table\DebtorLawyersTable $DebtorLawyers
 */
class DebtorLawyersController extends AppController
{
    public $modelClass = 'App.DebtorLawyers';

    protected $allowedActions = ['view', 'edit', 'index', 'add', 'delete'];
}
