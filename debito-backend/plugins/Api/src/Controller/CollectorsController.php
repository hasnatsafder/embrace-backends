<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;

/**
 * Collectors Controller
 *
 * @property \App\Model\Table\CollectorsTable $Collectors
 */
class CollectorsController extends AppController
{
    public $modelClass = 'App.Collectors';
    protected $allowedActions = ['index', 'view'];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->RoleRequirer->requireRole('root');
    }
}
