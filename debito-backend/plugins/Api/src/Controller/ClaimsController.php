<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;
use Cake\Http\Response;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable Claims
 * @property \App\Model\Table\CollectorsTable $Collectors
 */
class ClaimsController extends AppController
{
    public $modelClass = 'App.Claims';
    protected $allowedActions = ['index', 'add', 'view', 'edit', 'delete', 'addFromInvoices', 'freeze', 'allClaimActions', 'summaryPdf'];
    protected $requireActiveAccount = true;
    protected $filterByAccountId = true;

    public function beforeFilter(Event $e): void
    {
        parent::beforeFilter($e);

        $this->Crud->mapAction('freeze', 'Crud.Edit');
        $this->Crud->mapAction('summaryPdf', 'Crud.View');

        //Set "collectia" as default collector
        $this->Crud->on('beforeSave', function (Event $event) {
            //setting the newly created query to be the active one
            if (!isset($event->getSubject()->id)) {
                //inside created method since only need to update on new entity save, not on edit
                $account = $this->Claims->Accounts->get($this->Auth->user('active_account_id'));

                //if account has vat_number then set to collectia
                if ($account->vat_number) {
                    $collector = $this->Claims->Collectors->find()
                        ->where(['Collectors.identifier' => 'collectia'])
                        ->select('id')
                        ->firstOrFail();
                } else {
                    // else set to mortang
                    $collector = $this->Claims->Collectors->find()
                        ->where(['Collectors.identifier' => 'mortang'])
                        ->select('id')
                        ->firstOrFail();
                }

                if ($event->getSubject()->entity->collector_id !== $collector->id) {
                    $event->getSubject()->entity->set('collector_id', $collector->id);
                }
            }
        });
    }

    public function index(): Response
    {
        $this->Crud->on('beforePaginate', function (Event $event) {
            if ($this->request->getQuery('includeClaimLineSum') === '1') {
                $event->getSubject()->query
                    ->find('withCalculatedSumOfClaimLineAmounts')
                    ->contain(['debtors', 'ClaimPhases', 'Currencies']);
            }

            if ($this->request->getQuery('count') === '1') {
                $event->getSubject()->query->select([
                    'count' => $event->getSubject()->query->func()->count('*'),
                ]);
            }
        });

        return $this->Crud->execute();
    }

    public function view(): Response
    {
        if ($this->request->getQuery('showNames') === '1') {
            $this->Crud->on('beforeFind', function (Event $event) {
                $event->getSubject()->query
                    ->select([
                        'Claims.id',
                        'Claims.created',
                        'Claims.customer_reference',
                        'Claims.created',
                        'Claims.rejected_is_public',
                        'Claims.rejected_reason',
                        'Debtors.first_name',
                        'Debtors.last_name',
                        'Debtors.company_name',
                        'Debtors.address',
                        'Debtors.zip_code',
                        'Debtors.city',
                        'Debtors.phone',
                        'Debtors.email',
                        'ClaimPhases.name',
                        'ClaimPhases.order',
                        'Currencies.iso_code',
                    ])
                    ->contain(['Debtors', 'ClaimPhases', 'Currencies']);
            });
        }

        return $this->Crud->execute();
    }

    /**
     * @throws \Exception
     */
    public function addFromInvoices()
    {
        $this->getRequest()->allowMethod('POST');
        $this->filterByAccountId = false;

        $invoicesAll = $this->Claims->ClaimLines->Invoices->find()
            ->where([
                'Invoices.id IN' => $this->request->getData('invoice_ids'),
                'Invoices.account_id' => $this->Auth->user('active_account_id'),
            ]);

        $invoicesPerDebtor = $invoicesAll->groupBy('debtor_id');
        $createdBy = $this->Claims->CreatedBy->get($this->Auth->user('id'));
        $claims = [];
        if ($invoicesPerDebtor->count() > 0) {
            foreach ($invoicesPerDebtor as $invoices) {
                // if reminder was created with certain dunning config id then add it to invoice
                $dunning_configuration_id = null;
                if ($this->request->getData('dunning_configuration_id')) {
                    $dunning_configuration_id = $this->request->getData('dunning_configuration_id');
                }
                $claims[] = $this->Claims->ClaimLines->Invoices->createClaim($invoices, $createdBy, $dunning_configuration_id);
            }
        }

        // Mark each invoice as converted to clain
        foreach ($invoicesAll as $invoice) {
            $invoice->converted_to_claim = true;
            $this->Claims->ClaimLines->Invoices->save($invoice);
        }

        $this->set('data', $claims);
        $this->set('success', true);
    }

    /**
     * Action to freeze a claim
     *
     * @param $claimId
     * @return \Cake\Http\Response
     * @throws \Crud\Error\Exception\ActionNotConfiguredException
     * @throws \Crud\Error\Exception\MissingActionException
     */
    public function freeze($claimId): Response
    {
        $this->RoleRequirer->requireAllOf(['root']);

        $this->Crud->action()->setConfig('saveOptions', [
            'validate' => 'FreezeClaim',
            'accessibleFields' => [
                '*' => false,
                'frozen_until' => true,
            ],
            'freezeMessage' => $this->request->getData('message') ?? null,
        ]);

        return $this->Crud->execute();
    }

    public function summaryPdf($claimId)
    {
        $this->viewBuilder()
            ->setClassName('CakePdf.Pdf');

        return $this->Crud->execute();
    }
    /**
     * Return all actions for phases
     */
    public function allClaimActions()
    {
        // get all claims for user account
        $this->filterByAccountId = false;
        $claims = $this->Claims->find()
                        ->where(['Claims.account_id' => $this->Auth->user('active_account_id')])
                        ->contain('Debtors');
        $actions_array = [];

        $claim_action_table = TableRegistry::getTableLocator()->get('claim_actions');
        $collectors_table = TableRegistry::getTableLocator()->get('collectors');
        $debitoCollector = $collectors_table->find('all')->where(['identifier' => 'debito'])->first();

        foreach ($claims as $claim) {
            $claim_actions = $claim_action_table->find('all', [
                'order' => ['claim_actions.created' => 'DESC'],
            ])
            ->contain('ClaimActionTypes')
            ->where(['claim_id' => $claim->id, 'ClaimActionTypes.collector_id' => $debitoCollector->id, 'ClaimActionTypes.is_public' => 1]);
            foreach($claim_actions as $claim_action) {
                $arr = [
                    'claim_reference' => $claim->customer_reference,
                    'date' => $claim_action->date,
                    'created' => $claim_action->created,
                    'debtor' => $claim->debtor->company_name ? $claim->debtor->company_name : ($claim->debtor->first_name) . " " .$claim->debtor->last_name,
                    'action_name' => $claim_action->claim_action_type->name,
                    'identifier' => $claim_action->claim_action_type->identifier,
                    'claim_id' => $claim_action->claim_id
                ];
                array_push($actions_array, $arr);
            }
        }
        $this->set('data', ['actions' => $actions_array]);
    }
    /**
     * Soft delete claims
     * @param String $claim_id
     * TODO Later
     */
    // public function delete($claim_id)
    // {

    // }
}
