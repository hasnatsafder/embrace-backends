<?php
declare(strict_types=1);

namespace Api\Controller\Claims;

/**
 * ClaimFinancials Controller
 *
 * @property \App\Model\Table\ClaimFinancialsTable $ClaimFinancials
 */
class ClaimFinancialsController extends BaseClaimController
{
    public $modelClass = 'App.ClaimFinancials';
    protected $allowedActions = ['index', 'view'];
}
