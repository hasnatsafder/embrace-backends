<?php
declare(strict_types=1);

namespace Api\Controller\Claims;

use Api\Controller\AppController;
use App\Exception\InvalidIdInSubResource;
use App\Model\Entity\User;
use App\Model\Table\ClaimsTable;
use Cake\Event\Event;
use Cake\ORM\Association;
use Cake\ORM\Query;

/**
 * Class BaseClaimController
 *
 * @package Api\Controller\Claims
 */
abstract class BaseClaimController extends AppController
{
    /**
     * @var \App\Model\Entity\Claim|null
     */
    protected $claim;

    /**
     * @var \App\Model\Entity\User|null
     */
    private $user;

    protected $requireActiveAccount = true;

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
                'Crud.beforeFind' => 'crudFinders',
                'Crud.beforePaginate' => 'crudFinders',
            ];
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->setClaim($this->getRequest()->getParam('claim_id'));
    }

    /**
     * @param string $claimId
     */
    protected function setClaim(string $claimId): void
    {
        $where = [
            'Claims.id' => $claimId,
        ];

        $user = $this->getAuthenticatedUser();

        if (!$user->hasRole('root')) {
            $where['Claims.account_id'] = $this->Auth->user('active_account_id');
        }

        $claim = $this->getClaimAssociation()->getTarget()->find()
            ->select(['id'])
            ->where($where)
            ->first();

        if (empty($claim)) {
            throw new InvalidIdInSubResource();
        }

        $this->claim = $claim;
    }

    /**
     * @return Association
     */
    protected function getClaimAssociation(): Association
    {
        [, $alias] = pluginSplit($this->modelClass, true);

        return $this->{$alias}->Claims;
    }

    /**
     * @param \Cake\Event\Event $e
     */
    public function crudFinders(Event $e)
    {
        $this->setClaim($this->getRequest()->getParam('claim_id'));

        if ($this->getClaimAssociation() instanceof Association\BelongsTo) {
            $fk = $this->getClaimAssociation()->getForeignKey();

            $e->getSubject()->query->where([
                $this->getClaimAssociation()->getSource()->aliasField($fk) => $this->claim->id,
            ]);

            return;
        }

        $e->getSubject()->query
            ->matching('Claims', function (Query $q) {
                //User has no root? Only in account, then
                $where = ['Claims.id' => $this->claim->id];

                if (!$this->getAuthenticatedUser()->hasRole('root')) {
                    $where['Claims.account_id'] = $this->getAuthenticatedUser()->active_account_id;
                }

                return $q->where($where);
            });
    }

    /**
     * @return \App\Model\Entity\User
     */
    private function getAuthenticatedUser(): User
    {
        if ($this->user !== null) {
            return $this->user;
        }

        $claimsTable = $this->getClaimAssociation()->getTarget();
        assert($claimsTable instanceof ClaimsTable);

        return $this->user = $claimsTable->Users->get($this->Auth->user('id'));
    }
}
