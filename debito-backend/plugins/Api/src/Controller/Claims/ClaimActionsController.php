<?php
declare(strict_types=1);

namespace Api\Controller\Claims;

use Cake\Event\Event;
use Cake\Http\Response;

/**
 * ClaimActions Controller
 *
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 */
class ClaimActionsController extends BaseClaimController
{
    public $modelClass = 'App.ClaimActions';
    protected $allowedActions = ['index', 'view', 'add', 'edit'];

    public function index(): Response
    {
        if ($this->request->getQuery('showNames') === '1') {
            $this->Crud->on('beforePaginate', function (Event $event) {
                $event->getSubject()->query->contain(['ClaimActionTypes'])->select([
                    'ClaimActions.date',
                    'ClaimActions.paid',
                    'ClaimActions.interest_added',
                    'ClaimActions.debt_collection_fees',
                    'ClaimActions.actual_paid',
                    'ClaimActions.message',
                    'ClaimActionTypes.name',
                    'ClaimActionTypes.identifier',
                ]);
            });
        }

        return $this->Crud->execute();
    }

    public function add(): Response
    {
        $this->RoleRequirer->requireAllOf(['root']);

        $this->Crud->action()->setConfig('saveOptions', [
            'accessibleFields' => [
                '*' => false,
                'claim_id' => true,
                'claim_action_type_id' => true,
                'claim_line_id' => true,
                'user_id' => true,
                'date' => true,
                'done' => true,
                'message' => true,
            ],
            'validate' => 'WithSpecificClaimLine',
        ]);

        $this->setRequest(
            $this->getRequest()
                ->withData('user_id', $this->Auth->user('id'))
                ->withData('claim_id', $this->getRequest()->getParam('claim_id'))
        );

        return $this->Crud->execute();
    }

    public function edit(): Response
    {
        $this->RoleRequirer->requireAllOf(['root']);

        $this->Crud->action()->setConfig('saveOptions', [
            'accessibleFields' => [
                '*' => false,
                'user_id' => true,
                'date' => true,
                'done' => true,
                'message' => true,
            ],
            'validate' => 'WithSpecificClaimLine',
        ]);

        $this->setRequest(
            $this->getRequest()
                ->withData('user_id', $this->Auth->user('id'))
        );

        return $this->Crud->execute();
    }
}
