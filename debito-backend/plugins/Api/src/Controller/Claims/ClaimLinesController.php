<?php
declare(strict_types=1);

namespace Api\Controller\Claims;

use Cake\Event\Event;
use Cake\Network\Response;
use InvalidArgumentException;

/**
 * ClaimLines Controller
 *
 * @property \App\Model\Table\ClaimLinesTable $ClaimLines
 */
class ClaimLinesController extends BaseClaimController
{
    public $modelClass = 'App.ClaimLines';
    protected $allowedActions = ['index', 'add', 'view', 'edit', 'delete'];

    public function index(): \Cake\Http\Response
    {
        if ($this->request->getQuery('showNames') === '1') {
            $this->Crud->on('beforePaginate', function (Event $event) {
                $event->getSubject()->query->contain(['claimLinetypes'])
                    ->select([
                        'ClaimLines.invoice_date',
                        'ClaimLines.invoice_reference',
                        'ClaimLines.amount',
                        'ClaimLines.maturity_date',
                        'ClaimLines.file_name',
                        'ClaimLineTypes.name',
                        'ClaimLineTypes.identifier',
                        'ClaimLines.file_dir',
                    ]);
            });
        }

        return $this->Crud->execute();
    }

    /**
     * Make sure you can only set the
     *
     * @return \Cake\Network\Response
     * @throws \Exception
     */
    public function add(): Response
    {
        $this->setRequest(
            $this->getRequest()
                ->withData('claim_id', $this->claim->id)
        );

        $this->Crud->on('beforeSave', function (Event $event) {
            //set faktura as default claim line type, if no claim_line type given
            if (!$event->getSubject()->entity->claim_line_type_id) {
                $invoice = $this->ClaimLines->ClaimLineTypes->find()->where(['identifier' => 'invoice'])->first();
                $event->getSubject()->entity->claim_line_type_id = $invoice->id;
            }

            //set DKK as default currency
            if (!$event->getSubject()->entity->currency_id) {
                $currency = $this->ClaimLines->Currencies->find()->where(['iso_code' => 'DKK'])->first();
                $event->getSubject()->entity->currency_id = $currency->id;
            }

            if (isset($event->getSubject()->entity->claim_id) && $event->getSubject()->entity->claim_id !== $this->claim->id) {
                throw new InvalidArgumentException('Claim line can only be added to the selected claim');
            }
        });

        return $this->Crud->execute();
    }

    /**
     * Make sure the claim_id cannot be changed when editing
     *
     * @param $id
     * @return \Cake\Network\Response
     * @throws \Exception
     */
    public function edit($id): Response
    {
        $this->Crud->on('afterFind', function (Event $e) {
            $e->getSubject()->entity->setAccess('claim_id', false);
        });

        return $this->Crud->execute();
    }
}
