<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;
use DebtCollectionTools\FiPayments\SekFileHandler;

/**
 * FiPaymentsFile Controller
 *
 */
class FiPaymentsFileController extends AppController
{
    protected $allowedActions = ['index'];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->RoleRequirer->requireRole('root');
    }

    /**
     *
     */
    public function index()
    {
        $this->getRequest()->allowMethod(['POST']);
        $this->viewBuilder()->setClassName('Json');

        $file = $this->getRequest()->getData('file');

        if (!isset($file['error']) || $file['error'] !== UPLOAD_ERR_OK) {
            throw new \RuntimeException('File not uploaded properly');
        }

        $fs = fopen($file['tmp_name'], 'r');
        SekFileHandler::processPaymentsFromStream($fs);
        fclose($fs);

        $data = [];
        $success = true;

        $this->set('_serialize', true);
        $this->set(compact('data', 'success'));
    }
}
