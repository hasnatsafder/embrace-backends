<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;
use Cake\Network\Response;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Network\Exception\UnauthorizedException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public $modelClass = 'App.Users';
    protected $allowedActions = ['view', 'edit', 'index'];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Crud->on('beforePaginate', function (Event $event) {
            $event->getSubject()->query->where(['Users.id' => $this->Auth->user('id')]);
        });

        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->where(['Users.id' => $this->Auth->user('id')]);
        });
    }

    //for edit if we have password
    public function edit($id): Response
    {
        if (isset($this->request->data['password'])) {
            $this->Crud->on('beforeSave', function (Event $event) {
                //if no old password sent
                if (!isset($this->request->data['old_password'])) {
                    throw new UnauthorizedException('Invalid old password');
                }
                $user = $this->Users->get($event->getSubject()->entity->id);
                $default_hasher = new DefaultPasswordHasher();
                if (!$default_hasher->check($event->getSubject()->entity->old_password, $user->password)) {
                    throw new UnauthorizedException('Invalid old password');
                }
            });
        }

        return $this->Crud->execute();
    }
}
