<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * ClaimPhases Controller
 *
 * @property \App\Model\Table\ClaimPhasesTable $ClaimPhases
 */
class ClaimPhasesController extends AppController
{
    public $modelClass = 'App.ClaimPhases';
    protected $allowedActions = ['index', 'view'];
}
