<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * ClaimTransactionTypes Controller
 *
 * @property \App\Model\Table\ClaimTransactionTypesTable $ClaimTransactionTypes
 */
class ClaimTransactionTypesController extends AppController
{
    public $modelClass = 'App.ClaimTransactionTypes';
    protected $allowedActions = ['index', 'view'];
}
