<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * Countries Controller
 *
 * @property \App\Model\Table\CountriesTable $Countries
 */
class CountriesController extends AppController
{
    public $modelClass = 'App.Countries';
    protected $allowedActions = ['index', 'view'];
}
