<?php
declare(strict_types=1);

namespace Api\Controller\Traits;

use App\Exception\InvalidIdInSubResource;
use Cake\ORM\Association;
use Cake\ORM\Query;

trait HasAccountTrait
{
    /**
     * @var \App\Model\Entity\Account|null
     */
    protected $account;

    protected function setAccount(string $accountId): void
    {
        $account = $this->getAccountsRelation()->find()
            ->select(['id'])
            ->where(['Accounts.id' => $accountId])
            ->matching('Users', function (Query $q) {
                return $q->where(['Users.id' => $this->Auth->user('id')]);
            })
            ->first();

        if (empty($account)) {
            throw new InvalidIdInSubResource();
        }

        $this->account = $account;
    }

    protected function getAccountsRelation(): Association
    {
        [, $alias] = pluginSplit($this->modelClass, true);

        return $this->{$alias}->Accounts;
    }
}
