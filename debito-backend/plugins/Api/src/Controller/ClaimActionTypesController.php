<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Http\Response;

/**
 * ClaimActionTypes Controller
 *
 * @property \App\Model\Table\ClaimActionTypesTable $ClaimActionTypes
 */
class ClaimActionTypesController extends AppController
{
    public $modelClass = 'App.ClaimActionTypes';
    protected $allowedActions = ['index', 'view', 'add', 'edit', 'delete'];

    public function add(): Response
    {
        $this->RoleRequirer->requireRole('root');

        return $this->Crud->execute();
    }

    public function edit(): Response
    {
        $this->RoleRequirer->requireRole('root');

        return $this->Crud->execute();
    }

    public function delete(): Response
    {
        $this->RoleRequirer->requireRole('root');

        return $this->Crud->execute();
    }

}
