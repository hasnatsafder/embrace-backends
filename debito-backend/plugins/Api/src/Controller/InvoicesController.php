<?php
declare(strict_types=1);

namespace Api\Controller;

use Api\Controller\Traits\PaginationTrait;
use Cake\Event\Event;
use Cake\Core\Exception\Exception;
use Cake\Http\Response;
use http\Exception\InvalidArgumentException;
use Psr\Log\LogLevel;
use App\Utility\ErpIntegrations;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 * @property \App\Model\Table\ErpIntegrationsTable $ErpIntegrations
 * @property \App\Model\Table\ErpIntegrationDataTable $ErpIntegrationData
 * @property \Queue\Model\Table\QueuedJobsTable $QueuedJobs
 */
class InvoicesController extends AppController
{
    public $modelClass = 'App.Invoices';
    protected $allowedActions = ['index', 'view', 'add', 'edit', 'delete', 'integrationcheck', 'refreshAccountSync', 'unSyncAccount', 'checkQueue', 'invoicesMonthlyStats', 'myInvoices'];
    protected $filterByAccountId = true;
    protected $requireActiveAccount = true;

    use PaginationTrait;

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index(): Response
    {
        $this->Crud->on('beforePaginate', function (Event $event) {
            $event->getSubject()->query
                ->select([
                    'Invoices.id',
                    'Invoices.invoice_number',
                    'Invoices.due_date',
                    'Invoices.issue_date',
                    'Invoices.net_amount',
                    'Invoices.gross_amount',
                    'Invoices.snoozed',
                    'Invoices.debtor_id',
                    'Invoices.pdf_file_path',
                    'Debtors.first_name',
                    'Debtors.last_name',
                    'Debtors.company_name',
                    'Debtors.erp_integration_foreign_key',
                    'Debtors.address',
                    'Debtors.city',
                    'Debtors.email',
                    'Debtors.zip_code',
                    'Debtors.phone',
                    'Debtors.vat_number',
                    'Debtors.is_company',
                    'Currencies.iso_code',
                    'Currencies.id',
                ])
                ->contain(['currencies', 'debtors']);
        });

        return $this->Crud->execute();
    }

    public function view($id): Response
    {
        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query
                ->select([
                    'Invoices.id',
                    'Invoices.invoice_number',
                    'Invoices.due_date',
                    'Invoices.issue_date',
                    'Invoices.net_amount',
                    'Invoices.gross_amount',
                    'Invoices.snoozed',
                    'Invoices.debtor_id',
                    'Invoices.pdf_file_path',
                    'Debtors.first_name',
                    'Debtors.last_name',
                    'Debtors.company_name',
                    'Debtors.erp_integration_foreign_key',
                    'Debtors.address',
                    'Debtors.city',
                    'Debtors.email',
                    'Debtors.zip_code',
                    'Debtors.phone',
                    'Debtors.vat_number',
                    'Debtors.is_company',
                    'Currencies.iso_code',
                    'Currencies.id',
                ])
                ->contain(['currencies', 'debtors']);
        });

        return $this->Crud->execute();
    }

    public function integrationcheck()
    {
        $this->filterByAccountId = false;

        $this->loadModel('ErpIntegrations');
        $erp = $this->ErpIntegrations->find()
            ->select(['id', 'vendor_identifier', 'deleted', 'created'])
            ->where(['account_id' => $this->Auth->user('active_account_id')])
            ->order(['created' => 'ASC'])
            ->last();
        if ($erp) {
            $this->set('data', $erp);
            $this->set('success', true);

            return;
        }
        $this->set('success', false);
        throw new Exception("No Accounting system linked with this account");
    }

    public function refreshAccountSync()
    {
        $this->filterByAccountId = false;
        $this->loadModel('ErpIntegrations');
        $erpIntegrationData = $this->ErpIntegrations->find()
            ->select(['id'])
            ->where(['account_id' => $this->Auth->user('active_account_id')])
            ->order(['created' => 'ASC'])
            ->last();
        if (!$erpIntegrationData) {
            $this->set('success', false);
            throw new Exception("No Accounting system linked with this account");
        }
        $this->loadModel('Queue.QueuedJobs');
        // we wil create reference of erp_integrtion_id,
        // and will use this id to get if queue is completed.
        $this->QueuedJobs->createJob('Erp', ['erpIntegration_id' => $erpIntegrationData->id],
            ['reference' => $erpIntegrationData->id]);
        $this->set('data', $erpIntegrationData->id);
        $this->set('success', true);
        return;
    }

    /**
     * Unsync an ERP integration so user can make a new one
     *
     * @return Object
     */
    public function unSyncAccount()
    {
        $this->filterByAccountId = false;
        try {
            $this->loadModel('ErpIntegrations');
            $erpIntegration = $this->ErpIntegrations->find()
                ->where(['account_id' => $this->Auth->user('active_account_id')])
                ->order(['created' => 'ASC'])
                ->last();
            if ($erpIntegration) {
                $erpIntegration->deleted = new FrozenTime();
                $erpIntegration->modified_by_id = $erpIntegration->modified_by_id;
                $this->ErpIntegrations->save($erpIntegration);

                //delete invoices too
                $this->Invoices->deleteAll(['erp_integration_id' => $erpIntegration->id]);

                //delete Erp tokens too
                $this->loadModel('ErpIntegrationData');
                $this->ErpIntegrationData->trashAll(['erp_integration_id' => $erpIntegration->id]);

                $this->set('data', $erpIntegration);
                $this->set('success', true);
            } else {
                $this->set('data', []);
                $this->set('success', true);
            }
        } catch (\Exception $e) {
            $this->set('success', false);
            throw new Exception("Could not fetch new integrations");
        }
    }

    /**
     * Check whether an invoice fetching queue is completed or not
     */
    public function checkQueue()
    {
        $this->filterByAccountId = false;
        $this->loadModel('Queue.QueuedJobs');
        if (isset($this->request->data['reference'])) {
            $jobQueue = $this->QueuedJobs->find()->where(['reference' => $this->request->data['reference']])->last();
            if($jobQueue && $jobQueue->completed) {
                $this->set('data', ['finished' => true]);
            }
            else {
                $this->set('data', ['finished' => false]);
            }
        } else {
            $this->set('success', false);
            throw new Exception("Reference is requried");
        }
    }

    /**
     * Get last 18 months invoices and prepare data for frontend dashboard
     * @param String $account_id
     */
    public function invoicesMonthlyStats(String $account_id){
        $this->filterByAccountId = false;
        $this->loadModel('PaidInvoices');
        $query = $this->Invoices->find();
        $query->select([
            'amount' => $query->func()->sum('gross_amount'),
            'byYear' => 'YEAR(issue_date)',
            'byMonth' => 'MONTH(issue_date)',
        ])->group(['byMonth', 'byYear']);
        $query->enableHydration(false); // Results as arrays instead of entities
        $query->where(['account_id' => $account_id, 'issue_date >=' => '(now() - interval 18 month)']);
        $invoices = $query->toList();

        // get paid invoices data
        $query = $this->PaidInvoices->find();
        $query->select([
            'amount' => $query->func()->sum('gross_amount'),
            'byYear' => 'YEAR(issue_date)',
            'byMonth' => 'MONTH(issue_date)',
        ])->group(['byMonth', 'byYear']);
        $query->enableHydration(false); // Results as arrays instead of entities
        $query->where(['account_id' => $account_id, 'issue_date >=' => '(now() - interval 18 month)']);
        $paidInvoices = $query->toList();
        $time_array = [];
        // build data for last 18 months
        for ($i = 0; $i < 18; $i++) {
            $time = FrozenTime::now()->subMonths($i);
            $time_array[$time->format('M').  "-" . $time->format('Y')] = [
                'month' => $time->month,
                'year' => $time->year,
                "total_paid" => 0,
                "total_invoiced" => 0,
                "name" => $time->format('M').  "-" . $time->format('Y'),
                "month_name" => $time->format('M')
            ];
        }

        // check in database and add values to key base on paid or not
        foreach($invoices as $invoice) {
            $date = FrozenDate::create($invoice['byYear'], $invoice['byMonth']);
            $date_string = $date->format('M') . "-" . $date->format('Y');
            // add to the key of date, made in previous loop
            if( isset ($time_array[$date_string])) {
                $time_array[$date_string]['total_invoiced'] += $invoice['amount'];
            }
        }
        foreach($paidInvoices as $paidInvoice) {
            $date = FrozenDate::create($paidInvoice['byYear'], $paidInvoice['byMonth']);
            $date_string = $date->format('M') . "-" . $date->format('Y');
            // add to the key of date, made in previous loop
            if( isset ($time_array[$date_string])) {
                $time_array[$date_string]['total_invoiced'] += $paidInvoice['amount'];
                $time_array[$date_string]['total_paid'] += $paidInvoice['amount'];
            }
        }
        $this->set('data', ['invoices' => $time_array]);
    }

    /**
     * MyInvoices
     */
    public function myInvoices() {
        $this->filterByAccountId = false;

        $search_term = $this->request->getQuery('search_term') ?? "";
        $search_term = str_replace(" " , "", $search_term);
        $search_term = strtolower($search_term);
        $search_term = str_replace(" ", "", $search_term);

        $snoozed = false;
        if ($this->request->getQuery('snoozed') && $this->request->getQuery('snoozed') == "true") {
            $snoozed = true;
        }

        $user_account_id = $this->_getCurrentUser()->active_account_id;
        // get pending status
        // get erp_integration_id
        $user_erp_integration = TableRegistry::getTableLocator()->get('erp_integrations')
            ->find()->where(['account_id' => $user_account_id])->last();

        $page = $this->request->getQuery('page') ?? 0;
        $page = (int)$page;
        $invoices = $this->Invoices->find('all')
            ->contain(['currencies'])
            ->select([
                'Invoices.id',
                'Invoices.invoice_number',
                'Invoices.due_date',
                'Invoices.issue_date',
                'Invoices.net_amount',
                'Invoices.gross_amount',
                'Invoices.snoozed',
                'Invoices.debtor_id',
                'Invoices.pdf_file_path',
                'Invoices.customer_name',
                'Invoices.erp_integration_foreign_key',
//                'Debtors.first_name',
//                'Debtors.last_name',
//                'Debtors.company_name',
//                'Debtors.erp_integration_foreign_key',
//                'Debtors.address',
//                'Debtors.city',
//                'Debtors.email',
//                'Debtors.zip_code',
//                'Debtors.phone',
//                'Debtors.vat_number',
//                'Debtors.is_company',
                'Currencies.iso_code',
                'Currencies.id',
            ])
            ->limit($this->getPaginationCount())->offset(($page-1) * $this->getPaginationCount())
            ->where([
                'Invoices.erp_integration_id' => $user_erp_integration->id,
                'converted_to_claim' => 0,
                'Invoices.due_date <=' => FrozenTime::now(),
//                'Invoice_status_id' => $pending_status->id
            ])
            ->where(['OR' => [
                'LOWER(REPLACE(Invoices.invoice_number, " ", "")) LIKE ' => "%".$search_term."%",
                'LOWER(REPLACE(Invoices.customer_name, " ", "")) LIKE ' => "%".$search_term."%",
                'LOWER(REPLACE(Invoices.erp_integration_foreign_key, " ", "")) LIKE ' => "%".$search_term."%",
            ]])
            ->where(['snoozed' => $snoozed]);

        $this->set(['data' => $invoices, 'success' => true, 'pagination' => $this->generatePaginateArray($invoices->count(), $page)]);
        return;
    }

    /**
     * Over edit of claim
     * @param $invoice_id
     */
    public function edit($invoice_id) {
        $invoice = $this->Invoices->get($invoice_id);
        $this->Invoices->patchEntity($invoice, $this->request->getData());
        if ($this->request->getData('net_amount')) {
            $invoice->net_amount = (int)preg_replace('/[^0-9]/', '', $this->request->getData('net_amount')) / 100;
        }
        if ($this->request->getData('gross_amount')) {
            $invoice->gross_amount = (int)preg_replace('/[^0-9]/', '', $this->request->getData('gross_amount')) / 100;
        }
        if ($this->request->getData('Currencies')) {
            $invoice->currency_id = $this->request->getData('Currencies')['id'] ?? $invoice->currency_id;
        }
        if ($this->Invoices->save($invoice)) {
            $this->set(['data' => $invoice, 'success' => true]);
        }
        else {
            throw InvalidArgumentException;
        }
    }
}
