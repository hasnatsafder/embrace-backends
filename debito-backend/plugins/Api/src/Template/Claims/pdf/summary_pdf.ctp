<?php
declare(strict_types=1);

/**
 * @var \App\Model\Entity\Claim $claim
 */
?>

<?php pj($claim); ?>

<h1>Hello, world!</h1>

<h2>FI code: <?= $claim->getOrGeneratePaymentFiCode() ?></h2>