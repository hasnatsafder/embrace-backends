<?php
declare(strict_types=1);

namespace Admin\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Admin\Controller\UsersController Test Case
 *
 * @uses \Admin\Controller\UsersController
 */
class UsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.users',
        'plugin.admin.active_accounts',
        'plugin.admin.accounts',
        'plugin.admin.created_by',
        'plugin.admin.accounts_users',
        'plugin.admin.roles',
        'plugin.admin.company_types',
        'plugin.admin.countries',
        'plugin.admin.debtors',
        'plugin.admin.claims',
        'plugin.admin.claim_phases',
        'plugin.admin.claim_action_types',
        'plugin.admin.collectors',
        'plugin.admin.collector_files',
        'plugin.admin.collector_file_types',
        'plugin.admin.claim_actions',
        'plugin.admin.claim_financials',
        'plugin.admin.currencies',
        'plugin.admin.claim_lines',
        'plugin.admin.claim_line_types',
        'plugin.admin.accounts_collectors',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
