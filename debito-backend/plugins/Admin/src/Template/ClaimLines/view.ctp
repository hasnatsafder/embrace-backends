<?php
/**
 * @var \App\View\AppView $this
 * @var object $claim
 * @var mixed $claimLines
 * @var object $claimPhaseOrder
 * @var mixed $claim_id
 * @var mixed $collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Claim Lines</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Claims Lines List</div>
            <div>
                <?php if ($claimPhaseOrder->order == 1): ?>
                    <?= $this->Html->link('Submit to collector', ['controller' => 'Claims', 'action' => 'submit', $claim->id, $claim->collector_id],
                        [
                            'id' => 'open_submit_to_collector_model_btn',
                            'class' => 'btn btn-primary',
                            'data-toggle' => "modal",
                            'data-target' => "#collectorsListModel",
                        ]); ?>
                <?php endif; ?>
                <?= $this->Html->link('Add', ['action' => 'add', $claim_id], ['class' => 'btn btn-success']); ?>
            </div>
        </div>
        <div class="ibox-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Invoice Date</th>
                    <th>Amount</th>
                    <th>Invoice Reference</th>
                    <th>Actions</th>
                    <th>Synced</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($claimLines as $claimLine): ?>
                    <tr>
                        <td>
                            <?php if ($claimLine->claim_line_type): ?>
                                <?= $claimLine->claim_line_type->name; ?>
                            <?php endif; ?>
                        </td>
                        <td> <?= $claimLine->invoice_date; ?> </td>
                        <td> <?= $claimLine->currency ? $claimLine->currency->iso_code : "" ?> <span class="amount"> <?= $claimLine->amount; ?> </span></td>
                        <td> <?= $claimLine->invoice_reference; ?> </td>
                        <td>
                            <?= $this->Html->link('Edit',
                                ['controller' => 'ClaimLines', 'action' => 'edit', $claimLine->id],
                                ['class' => 'btn btn-warning']); ?>

                            <?= $this->Html->link(($claimLine->synced ? 'Desync' : 'Sync'),
                                ['controller' => 'ClaimLines', 'action' => 'changeSyncStatus', $claimLine->id],
                                ['class' => 'btn btn-outline-' . ($claimLine->synced ? 'warning' : 'success')]); ?>

                        </td>
                        <td> <?= ($claimLine->synced) ? 'Yes' : 'No'; ?> </td>

                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <?= $this->Html->link('Desync All',
                            ['controller' => 'ClaimLines', 'action' => 'changeSyncStatusAll', $claim_id, 0],
                            ['class' => 'btn btn-outline-warning']); ?>
                        <?= $this->Html->link('Sync all',
                            ['controller' => 'ClaimLines', 'action' => 'changeSyncStatusAll', $claim_id, 1],
                            ['class' => 'btn btn-outline-success']); ?>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="collectorsListModel">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Submit Claim to collector</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <?= $this->Form->control('collector_id',
                    [
                        'options' => $collectors,
                        'value' => $claim->collector_id,
                        'label' => false,
                    ]);
                ?>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <?= $this->Html->link('Submit', ['controller' => 'Claims', 'action' => 'submit', $claim->id, $claim->collector_id],
                    ['id' => 'submit_to_collector_btn', 'class' => 'btn btn-primary']);
                ?>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>

        </div>
    </div>
</div>

<script>
    numeral.locale("da-dk");
    $(document).ready(function () {
        $('.amount').each(function (i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.toString().splice(-3, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original).format("$0,0.00");
            $(this).html(number)
        });
        <?php if ($claimPhaseOrder->order == 1): ?>
        $("#collector-id").change(function () {

            var selectedCollector = $(this).children("option:selected").val();
            var currentHref = $("#submit_to_collector_btn").attr("href").split("/").slice(0, -1).join("/");
            $("#submit_to_collector_btn").attr("href", currentHref + "/" + selectedCollector);
            console.log($("#submit_to_collector_btn").attr("href"));

        });
        <?php endif; ?>
    });
</script>
