<?php
/**
 * @var \App\View\AppView $this
 * @var object $claimLine
 * @var mixed $claimLineTypes
 * @var string $claim_count
 * @var mixed $currencies
 * @var string $number_of_pages
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <?php if ($number_of_pages) { ?>
        <h5 class="pull-right">Number of Pages are <strong><?= $number_of_pages ?></strong> while total Claims Are <strong><?= $claim_count ?></strong></h5>
    <?php } ?>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claim Lines', ['action' => 'view', $claimLine->claim_id]); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit Claim Line</div>
                    <?= $this->Html->link('Duplicate', ['action' => 'duplicate', $claimLine->id],
                        ['class' => 'btn btn-success']); ?>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModel">
                        <?= __("Delete") ?>
                    </button>
                    <a class="btn btn-blue" download target="_blank"
                       href="<?php echo $this->request->webroot; ?>webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>">
                        <?= __("Download") ?>
                    </a>
                </div>
                <?= $this->Form->create($claimLine, ['type' => 'file', 'class' => 'form-info', 'id' => 'my_form']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('file_name', ['type' => 'file', 'required' => 'false']);
                        echo $this->Form->control('currency_id',
                            [
                                'options' => $currencies,
                                'value' => $claimLine->currency_id,
                                'empty' => true,
                            ]);
                        echo $this->Form->control('amount', ['type' => 'text', 'maxlength' => 20]);
                        echo $this->Form->control('invoice_date', ['type' => 'text']);
                        echo $this->Form->control('maturity_date', ['type' => 'text']);
                        echo $this->Form->control('claim_line_type_id',
                            [
                                'options' => $claimLineTypes,
                                'value' => $claimLine->claim_line_type_id,
                            ]);
                        echo $this->Form->control('placement', ['type' => 'text']);
                        echo $this->Form->control('invoice_reference', ['type' => 'text']);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="button" class="btn btn-primary mr-2" onclick="submitform()">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="col-lg-6">
            <?php if ($claimLine->file_mime_type == "application/pdf") { ?>
                <object width="700" height="800"
                        data="/webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>"></object>
            <?php } else { ?>
                <img
                        src="/webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>">
            <?php } ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this Claim Line?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link('Delete', ['action' => 'delete', $claimLine->id],
                    ['class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-datepicker.js') ?>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>
<?= $this->Html->script('/Admin/js/vendors/moment.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>

<script>
    $(document).ready(function () {

        $('#maturity-date').val(moment($('#maturity-date').val()).format("YYYY-MM-DD"));
        $('#invoice-date').val(moment($('#invoice-date').val()).format("YYYY-MM-DD"));

        $("#maturity-date, #invoice-date").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });

        numeral.locale("da-dk");
        //Amount danish format
        var number_original = $('#amount').val().toString();
        number_original = number_original.toString().splice(-2, 0, ".");
        number_original = parseFloat(number_original);
        var number = numeral(number_original).format("$0,0.00");
        $('#amount').val(number);

        if ($('#currency-id').val() == "") {
            $("#currency-id option:contains('Danish Krone - DKK')").attr('selected', 'selected');
        }
        $('#currency-id').select2();

        //when changing invoice field auto change maturity field
        $('#invoice-date').change(function () {
            let invoice_date = $(this).val();
            $('#maturity-date').val(invoice_date);
        });

    });

    function submitform() {
        var removedText = $("#amount").val().replace(/\D+/g, '');
        $("#amount").val(removedText);
        $("#my_form").submit();
    }
</script>
