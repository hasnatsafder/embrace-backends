<?php
/**
 * @var \App\View\AppView $this
 * @var object $claim
 * @var mixed $claimLines
 * @var mixed $contentArray
 * @var mixed $status
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item"><?= __("Collectors FTP") ?></li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Collectors FTP <span class="text-blue"><?= $status ? $status : "" ?></span></div>
        </div>
        <div class="ibox-body">
            <table class="table">
                <thead>
                <tr>
                    <th><?= __("Reference") ?></th>
                    <th><?= __("Collector Iden.") ?></th>
                    <th><?= __("Customer Ref.") ?></th>
                    <th><?= __("Debtor") ?></th>
                    <th><?= __("Address") ?></th>
                    <th><?= __("Phone") ?></th>
                    <th><?= __("Type") ?></th>
                    <th><?= __("Invoice D.") ?></th>
                    <th><?= __("Maturity D.") ?></th>
                    <th><?= __("Amount") ?></th>
                    <th><?= __("Currency") ?></th>
                    <th><?= __("Account") ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($contentArray) {
                    foreach ($contentArray as $content) {
                        $contentsArrayInner = array_map("str_getcsv", explode(";", $content));
                        ?>
                        <tr>
                            <td>
                                <?= $contentsArrayInner[21][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[0][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[2][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[3][0] ?>
                            </td>
                            <td>
                                <?= utf8_encode($contentsArrayInner[5][0]) ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[13][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[15][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[17][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[18][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[20][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[24][0] ?>
                            </td>
                            <td>
                                <?= $contentsArrayInner[25][0] ?>
                            </td>
                        </tr>
                        <?php
                    }
                    foreach ($claimLines as $line) { ?>
                        <tr class="text-success">
                            <td>
                                <?= $line->invoice_reference ?>
                            </td>
                            <td>
                                <?= $claim->collector_reference ?>
                            </td>
                            <td>
                                <?= $claim->customer_reference ?>
                            </td>
                            <td>
                                <?php
                                echo $claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name;
                                ?>
                            </td>
                            <td>
                                <?= $claim->debtor->address ?>
                            </td>
                            <td>
                                <?= $claim->debtor->phone ?>
                            </td>
                            <td>
                                <?= $line->claim_line_type->name ?>
                            </td>
                            <td>
                                <?= $line->invoice_date ? date_format($line->invoice_date, 'd.m.Y') : "" ?>
                            </td>
                            <td>
                                <?= $line->maturity_date ? date_format($line->maturity_date, 'd.m.Y') : "" ?>
                            </td>
                            <td>
                                <?= $line->amount ?>
                            </td>
                            <td>
                                <?= $line->currency->iso_code ?>
                            </td>
                            <td>
                                <?= $claim->account->name ?>
                            </td>
                        </tr>
                    <?php }
                } else {
                    echo "<tr><h2 class='text-danger'>" . __("No Data Found") . "</h2><tr>";
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</div>