<?php

/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $debtors
 * @var object $user
 */
?>
<div class="page-heading">
  <h1 class="page-title">Debtor's claims</h1>
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="index.html"><i class="la la-home font-20"></i></a>
    </li>
    <?php if ($this->request->getQuery('redirect')) : ?>
      <li class="breadcrumb-item">
        <?= $this->Html->link("Claims", $this->request->getQuery('redirect')); ?>
      </li>
    <?php else : ?>
      <li class="breadcrumb-item">
        <?= $this->Html->link("Claims", ['controller' => 'Claims']); ?>
      </li>
    <?php endif; ?>
    <li class="breadcrumb-item">Debtor's claims</li>
  </ol>
</div>

<div class="page-content">
<?= $this->Form->create(
    null,
    [
        'url' =>
        [
            'action' => 'mergeClaims/'.$original_claim->id,
            'redirect' => $this->request->getQuery('redirect') ? $this->request->getQuery('redirect') : ""
        ],
        'id' => 'merge-form'
    ]
) ?>
  <div class="ibox">
    <?= $this->Flash->render() ?>
    <div class="ibox-head">
      <div class="ibox-title">Claims List for debtor <span class="text-pink"><?= $debtor->company_name ? $debtor->company_name : $debtor->first_name . " " . $debtor->last_name ?> </span></div>
    </div>
    <div class="ibox-body">
      <div class="table-responsive row">
        <table class="table table-bordered table-hover">
          <thead class="thead-default thead-lg">
            <tr>
              <th class="no-sort">Merge</th>
              <th class="no-sort">Date</th>
              <th class="no-sort">Phase</th>
              <th class="no-sort">Ref</th>
              <th class="no-sort">Collector-Ref</th>
              <th class="no-sort">Admin</th>
              <th class="no-sort">Preview</th>
              <th class="no-sort">Handle</th>
            </tr>
          </thead>
          <tbody>
            <tr class="info font-bold">
              <td></td>
              <td><?= date_format($original_claim->created, "d M Y") ?></td>
              <td>
                <?php if ($original_claim->claim_phase->order === -1) : ?>
                  <button class="btn btn-link" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="left" data-content="<?= $original_claim->rejected_reason ?>" data-original-title="" title="">
                    <?= $original_claim->claim_phase->name ?>
                  </button>
                <?php else : ?>
                  <?= $original_claim->claim_phase->order ?> - <?= $original_claim->claim_phase->name ?>
                <?php endif; ?>
              </td>
              <td>
                <?= $original_claim->customer_reference; ?>
              </td>
              <td>
                <?= $original_claim->collector_reference; ?>
              </td>
              <td>
                <?php if (isset($original_claim->admin)) : ?>
                  <button class="btn btn-primary btn-icon-only btn-circle btn-air avatar" id="<?= isset($original_claim->admin) ? $original_claim->admin->first_name . " " . $original_claim->admin->last_name : "" ?>" data-toggle="button">
                  </button>
                <?php else : ?>
                  N/A
                <?php endif; ?>
              </td>
              <td>
                <?php if (isset($original_claim->admin) && $original_claim->admin->id != $auth_user_id) :
                  echo $this->Html->link(
                    'View',
                    ['controller' => 'Claims', 'action' => 'edit', $original_claim->id],
                    ['class' => 'btn btn-info', 'target' => "_blank"]
                  );
                else :
                  echo $this->Html->link(
                    'Edit',
                    ['controller' => 'Claims', 'action' => 'edit', $original_claim->id],
                    ['class' => 'btn btn-info', 'target' => "_blank"]
                  );
                endif;
                ?>
              </td>
              <td>
                <?= $this->Html->link(
                  'Handle',
                  ['controller' => 'Claims', 'action' => 'handle', $original_claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                  ['class' => 'btn btn-info']
                ); ?>
              </td>
            </tr>

            <?php foreach ($claims as $claim) : ?>
              <tr>
                <td>
                  <?php if ($claim->claim_phase->order <= 2) : ?>
                    <label class="text-center checkbox">
                      <input name="<?= $claim->id ?>" type="checkbox">
                      <span class="input-span"></span>
                    </label>
                  <?php endif; ?>
                </td>
                <td> <?= date_format($claim->created, "d M Y") ?> </td>
                <td>
                  <?php if ($claim->claim_phase->order === -1) : ?>
                    <button class="btn btn-link" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="left" data-content="<?= $claim->rejected_reason ?>" data-original-title="" title="">
                      <?= $claim->claim_phase->name ?>
                    </button>
                  <?php else : ?>
                    <?= $claim->claim_phase->order ?> - <?= $claim->claim_phase->name ?>
                  <?php endif; ?>
                </td>
                <td>
                  <?= $claim->customer_reference; ?>
                </td>
                <td>
                  <?= $claim->collector_reference; ?>
                </td>
                <td>
                  <?php if (isset($claim->admin)) : ?>
                    <button class="btn btn-primary btn-icon-only btn-circle btn-air avatar" id="<?= isset($claim->admin) ? $claim->admin->first_name . " " . $claim->admin->last_name : "" ?>" data-toggle="button">
                    </button>
                  <?php else : ?>
                    N/A
                  <?php endif; ?>
                </td>
                <td>
                  <?php if (isset($claim->admin) && $claim->admin->id != $auth_user_id) :
                    echo $this->Html->link(
                      'View',
                      ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                      ['class' => 'btn btn-info', 'target' => "_blank"]
                    );
                  else :
                    echo $this->Html->link(
                      'Edit',
                      ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                      ['class' => 'btn btn-info', 'target' => "_blank"]
                    );
                  endif;
                  ?>
                </td>
                <td>
                  <?= $this->Html->link(
                    'Handle',
                    ['controller' => 'Claims', 'action' => 'handle', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                    ['class' => 'btn btn-info']
                  ); ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <div class="text-center mb-4">
          <button type="submit" class="btn btn-lg btn-primary btn-rounded btn-air" style="width:200px;">Confirm merge</button>
      </div>
    </div>
  </div>
  <?= $this->Form->end(); ?>
</div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>

<script>
  $('.avatar').each(function() {
    let name = $(this).attr('id');
    name = name.toUpperCase()
    if (name) {
      name_with_spaces = name.split(' ')
      if (name_with_spaces.length <= 1 || name_with_spaces[1] == "") {
        name_without_spaces = name.split("")
        $(this).html(name_without_spaces[0] + name_without_spaces[1])
      } else {
        first_name_without_spaces = name_with_spaces[0].split("")
        last_name_without_spaces = name_with_spaces[1].split("")
        $(this).html(first_name_without_spaces[0] + last_name_without_spaces[0])
      }
    } else {
      $(this).html("?")
    }
  })
</script>