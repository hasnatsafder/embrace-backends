<?php
/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $countries
 * @var object $debtor
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Debtors</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <?php if ($this->request->getQuery('redirect')):?>
            <li class="breadcrumb-item">
                <?= $this->Html->link('Back', $this->request->getQuery('redirect')); ?>
            </li>
        <?php else: ?>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['controller' => 'users', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Accounts", ['controller' => 'Accounts', 'action' => 'user', $user->id]); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Debtors", ['action' => 'accounts', $account->id]); ?>
        </li>
        <?php endif; ?>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <?= $this->Flash->render() ?>
                <div class="ibox-head">
                    <div class="ibox-title">Debtors List for user <span class="text-pink"><?= $user->email ?> </span> and Account <span
                                class="text-pink"><?= $account->name ?> </span></div>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModel">
                        <?= __("Delete") ?>
                    </button>
                </div>
                <?= $this->Form->create(
                    $debtor,
                    [
                        'url' =>
                        [
                            'action' => 'edit',
                            'redirect' => $this->request->getQuery('redirect') ? $this->request->getQuery('redirect') : ""
                        ],
                        'id' => 'debtor-form'
                    ]
                ) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('first_name');
                        echo $this->Form->control('last_name');
                        echo $this->Form->control('company_name');
                        echo $this->Form->control('address');
                        echo $this->Form->control('zip_code');
                        echo $this->Form->control('city');
                        echo $this->Form->control('phone');
                        echo $this->Form->control('vat_number');
                        echo $this->Form->control('email');
                        echo $this->Form->control('country_id', array('label'=>'Country', 'type'=>'select', 'options'=>$countries, 'empty' => '(choose one)'));
//                        echo $this->Form->select(
//                            'country_id',
//                            $countries,
//                            ['empty' => '(choose one)']
//                        );
                        echo $this->Form->control('is_company',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => $debtor->is_company,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this Debtor?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link('Delete', ['action' => 'delete', $debtor->id],
                    ['class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>

<script>
    $(document).ready(function () {
        $("select").select2();
    });
</script>
