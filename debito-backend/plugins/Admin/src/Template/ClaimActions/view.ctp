<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimActions
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Claims Actions</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Actions List</div>
        </div>
        <div class="ibox-body">
            <div class="ibox">
                <div class="ibox-body">
                    <form method="post" id="action-form" action="/admin/claim-actions/save/<?= $claim_id ?>" class="row form-success">
                        <div class="col-md-5">
                            <?php
                                echo $this->Form->control('claim_action_type_id',
                                [
                                    'options' => $allClaimActions,
                                ]);
                            ?>
                            <label for="ex-pass">Action Date</label>
                            <input value="<?= date('Y-m-d') ?>" class="form-control mb-2 mr-sm-2 mb-sm-0" name="date" id="action-date" type="text">
                            <div class="form-group mt-2" id="message-area">
                                <label for="ex-pass">Internal note</label>
                                <?= $this->Form->textarea('message', ['type' => 'text', 'maxlength' => 1000]); ?>
                            </div>
                            <br>
                            <button class="btn btn-success pull-right" type="submit">Save</button>
                        </div>
                    </form>
                </div> 
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th width="15%">Collector Identifier</th>
                    <th width="15%">Name</th>
                    <th width="15%">Date</th>
                    <th width="10%">Paid</th>
                    <th width="10%">Interest Added</th>
                    <th width="10%">Debt Coll. Fees</th>
                    <th width="10%">Actual Paid</th>
                    <th width="20%">Message</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($claimActions as $claimAction): ?>
                    <tr>
                        <td>
                            <i data-href="/admin/claim-actions/delete/<?= $claimAction->id ?>" class="fa fa-trash cursor-pointer" data-toggle="modal" data-target="#delete-action" label="Delete action"></i>
                        </td>
                        <td> <?= $claimAction->claim_action_type->collector_identifier ?> </td>
                        <td> <?= $claimAction->claim_action_type->name ?> </td>
                        <td> <?= date_format($claimAction->date, "d-M-Y") ?> </td>
                        <td class="amount"> <?= $claimAction->paid ?> </td>
                        <td class="amount"> <?= $claimAction->interest_added ?> </td>
                        <td class="amount"> <?= $claimAction->debt_collection_fees ?> </td>
                        <td class="amount"> <?= $claimAction->actual_paid ?> </td>
                        <td> <?= $claimAction->message ?> </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Modal for delete claim actions -->
<div class="modal fade" id="delete-action" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this action?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-datepicker.js') ?>

<script>
    numeral.locale("da-dk");
    $(document).ready(function () {
        $('.amount').each(function (i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.toString().splice(-3, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original).format("$0,0.00");
            $(this).html(number)
        });
    });
    $(document).ready(function () {
        $("select").select2();
    });
    $("#action-date").datepicker({
            format: "yyyy-mm-dd",
            defaultDate: new Date(),
            autoclose: true,
    });

    // delete claim action
    $('#delete-action').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

</script>
<style>
    .quarter-width {
        width: 550px;
    }
</style>