<?php
/**
 * @var \App\View\AppView $this
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= h($this->fetch('title')) ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <?= $this->Html->css('/Admin/css/vendors/bootstrap.css') ?>
    <?= $this->Html->css('/Admin/css/main.css') ?>
</head>
<body>
<div style="max-width: 400px;margin: 100px auto 50px;" class="bg-white">
    <?= $this->fetch('content') ?>
</div>
</body>
</html>

<style>
    body {
        background-color: #4a5ab9;
    }

    .login-content {
        max-width: 900px;
        margin: 100px auto 50px;
    }

    }
</style>