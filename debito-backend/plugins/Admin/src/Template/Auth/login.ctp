<?php
/**
 * @var \App\View\AppView $this
 */
declare(strict_types=1);
$this->assign('title', "Debito Login");
?>
<div class="ibox m-0" style="box-shadow: none;">
    <?= $this->Flash->render() ?>
    <?= $this->Form->create(null, ['class' => 'ibox-body']) ?>
    <h2 class="font-strong text-center mb-5">
        <?= __('Please sign in') ?>
    </h2>

    <?= $this->Form->control('email') ?>
    <?= $this->Form->control('password') ?>

    <?= $this->Form->submit('Sign in', ['class' => 'btn btn-success btn-rounded btn-block btn-air']); ?>
    <?= $this->Form->end(); ?>

</div>
