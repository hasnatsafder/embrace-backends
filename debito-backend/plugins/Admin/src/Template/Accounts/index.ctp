<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[] $accounts
 */
?>
<div class="page-heading">
    <h1 class="page-title">Users and accounts</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">Accounts</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <div class="ibox-body">
            <?= $this->Flash->render() ?>
            <div class="flexbox mb-4">
                <?= $this->Form->create(null, ['valueSources' => 'query', 'id' => 'search-form', 'class' => 'form-inline form-success']) ?>
                    <h6 class="font-strong mr-2">Accounts (<span id="sort-text"></span>)</h6>
                    <?= $this->Form->control('accountsAndUsers', [
                        'type' => 'text',
                        'placeholder' => __('Press enter to search (case sensitive)'),
                        'label' => false,
                        'id' => 'search-input',
                        'class' => 'form-control mb-2 ml-2 mr-sm-2 mb-sm-0',
                        'style' => 'min-width: 500px;',
                    ]) ?>
                    <label class="mb-0 mr-2 ml-2">Sort:</label>
                    <?= $this->Form->select(
                        'sortBy',
                        [
                            'recent' => 'Recent', 'oldest' => 'Oldest'],
                        [
                        'class' => 'form-control mb-2 mr-sm-2 mb-sm-0',
                        'id' => 'sort-by'
                        ]
                    );?>
                    <label class="mb-0 mr-2 ml-2">Filter:</label>
                    <?= $this->Form->select(
                        'filterBy',
                        [
                            '' => 'All', 'free' => 'Free', 'paid' => 'Paid', 'not-integrated' => 'Not integrated', 'integrated' => 'Integrated'],
                        [
                        'class' => 'form-control mb-2 mr-sm-2 mb-sm-0',
                        'id' => 'filter-by'
                        ]
                    );?>
                <?= $this->Form->end(); ?>
                <?= $this->Html->link(
                    "Add",
                    ['controller' => 'Accounts', 'action' => 'add', 'redirect' => '/admin/accounts'],
                    ['class' => 'btn btn-success']
                ); ?>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="datatable">
                    <thead class="thead-default thead-lg">
                        <tr>
                            <th width="10%">CVR number</th>
                            <th width="10%">Name</th>
                            <th width="10%">User(owner)</th>
                            <th width="10%">Phone</th>
                            <th width="10%">Email</th>
                            <th width="10%">Integration</th>
                            <th width="10%">Subscription</th>
                            <th width="10%">Debtors</th>
                            <th width="10%">Claims</th>
                            <th width="10%">Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($accounts as $account) : ?>

                            <tr>
                                <td> <?= $account->vat_number ?> </td>
                                <td> <?= $account->name ?> </td>
                                <td>
                                    <?php foreach ($account->users as $user) : ?>
                                        <?php if ($user->id == $account->created_by_id):?>
                                        <?= $this->Html->link(
                                            $user->email,
                                            ['controller' => 'Accounts', 'action' => 'user', $user->id],
                                            ['class' => 'btn btn-link']
                                        ); 
                                        endif; ?>
                                    <?php endforeach ?>
                                </td>
                                <td> <?= $account->phone ?> </td>
                                <td> <?= $account->email ?> </td>
                                <td>
                                    <?php 
                                        if (count($account->erp_integrations) > 0):
                                            echo $account->erp_integrations[count($account->erp_integrations) - 1]->vendor_identifier;
                                        else:
                                            echo "None";
                                        endif;
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if (count($account->account_plans) > 0):
                                            echo $plans[$account->account_plans[count($account->account_plans) - 1]->plan_id]['name'];
                                        else:
                                            echo "None";
                                        endif;
                                    ?>
                                </td>
                                <td>
                                    <?= $this->Html->link(
                                        'Debtors',
                                        ['controller' => 'Debtors', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/'],
                                        ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
                                    ); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link(
                                        'Claims',
                                        ['controller' => 'Claims', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/'],
                                        ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
                                    ); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link(
                                        'Edit',
                                        ['action' => 'editUserAccount', $account->id, 'redirect' => '/admin/accounts'],
                                        ['class' => 'btn btn-primary btn-sm']
                                    ); ?>
                                </td>
                                <!-- <td> <?= $account->zip_code ?> </td>
                                <td> <?= $account->city ?> </td>
                                <td> <?= $account->country->name ? $account->country->name : "-" ?> </td>
                                <td>
                                    <?php if ($account->country->name == "Denmark") : ?>
                                        <?= $account->bank_reg_number ?> - <?= $account->bank_account_number ?>
                                    <?php else : ?>
                                        <?= $account->iban ?> - <?= $account->swift_code ?>
                                    <?php endif; ?>
                                </td>
                                <td> <?php echo $account->is_company ? "Yes" : "No"; ?> </td>
                                <td> <?= $account->address ?> </td> -->
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
            <div class="dataTables_paginate paging_simple_numbers pull-right">
                <ul class="pagination">
                    <?php if ($this->Paginator->numbers()) : ?>
                        <?php echo $this->Paginator->prev(__d('Collector', 'Previous')); ?>
                        <?php echo $this->Paginator->numbers(); ?>
                        <?php echo $this->Paginator->next(__d('Collector', 'Next')); ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>

<script>
    $(document).ready(function() {

        // to make sorting disabled
        $('#datatable>thead>tr>th').each(function() {
            $(this).addClass("no-sort sorting_disabled");
        })

        // submit form on enter
        $('#search-input').keypress(function(e) {
            if (e.which == 13) {
                $('#search-form').submit();
            }
        });
        $('#search-input').change(function() {
            $('#search-form').submit();
        })

        $('#sort-by').change(function() {
            $('#search-form').submit();
        })
        $('#filter-by').change(function() {
            $('#search-form').submit();
        })

        $('#sort-text').html("most " + $('#sort-by').val())
    })
</script>

<style>
    #datatable_paginate {
        display: none;
    }
    #search-form .font-strong{
        display: contents;
    }
</style>