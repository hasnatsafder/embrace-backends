<?php
/**
 * @var \App\View\AppView $this
 * @var object $claim
 * @var mixed $claimLines
 * @var object $claimPhaseOrder
 * @var mixed $claim_id
 * @var mixed $collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title">Invoices</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <!-- <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li> -->
        <li class="breadcrumb-item">Invoices</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Invoices List</div>
        </div>
        <div class="ibox-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Due Date</th>
                    <th>Amount</th>
                    <th>Invoice Reference</th>
                    <th>Snoozed</th>
                    <th>Converted</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($invoices as $invoice): ?>
                      <tr>
                          <td>
                              <?php if ($invoice->invoice_status): ?>
                                  <?= $invoice->invoice_status->name == "Pending" ? "Unpaid" : $invoice->invoice_status->name; ?>
                              <?php endif; ?>
                          </td>
                          <td> <?= date_format($invoice->due_date, "d-m-Y"); ?> </td>
                          <td> <?= $invoice->currency ? $invoice->currency->iso_code : "" ?> <span class=""> <?= $invoice->gross_amount; ?> </span></td>
                          <td> <?= $invoice->invoice_number; ?> </td>
                          <td> <?= ($invoice->snoozed) ? 'Yes' : 'No'; ?> </td>
                          <td> <?= ($invoice->converted_to_claim) ? 'Yes' : 'No'; ?> </td>

                      </tr>
                  <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>

<script>
    numeral.locale("da-dk");
    $(document).ready(function () {
        $('.amount').each(function (i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.toString().splice(-3, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original);
            $(this).html(number)
        });
    });
</script>
