<?php
/**
 * @var \App\View\AppView $this
 */
?>
<header class="header">
    <!-- START TOP-LEFT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Accounts', ['controller' => 'Accounts', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Users', ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Claims', ['controller' => 'Claims', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('New Claims', ['controller' => 'Claims', 'action' => 'newClaims'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Rejected Claims', ['controller' => 'Claims', 'action' => 'rejected'], ['class' => 'nav-link']); ?>
        </li>
    </ul>
    <!-- END TOP-LEFT TOOLBAR-->
    <!--LOGO-->
    <a href="index.html">
        <?= $this->Html->image('/Admin/img/logo_web.png', ['alt' => 'Debito']); ?>
    </a>
    <!-- START TOP-RIGHT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li class="dropdown mega-menu">
            Debito Admin
        </li>
        <li>
            <?= $this->Html->link('Logout', ['controller' => 'Auth', 'action' => 'logout']); ?>
        </li>
    </ul>
    <!-- END TOP-RIGHT TOOLBAR-->
</header>
