<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claims
 * @var mixed $collector
 * @var mixed $collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title">Stuck claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Stuck claims</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">List of stuck claims (for more than a day)</div>
            <div>
                <?= $this->Html->link('All claims', ['action' => 'index'], ['class' => 'btn btn-info']); ?>
                <?= $this->Html->link('Idle claims', ['action' => 'idle'], ['class' => 'btn btn-danger']); ?>
            </div>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2 ml-4">Collectors:</label>
                    <?= $this->Form->control('collectors',
                        [
                            'options' => $collectors,
                            'value' => $collector,
                            'class' => "selectpicker show-tick",
                            'empty' => true,
                            'label' => false,
                            'data-style' => 'btn-solid',
                            'data-width' => "150px",
                            'empty' => ["" => 'All'],
                        ]) ?>
                    <button class="btn btn-blue btn-sm ml-4" id="filter-button">Filter</button>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="datatable">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th>Pos. Reason</th>
                        <th>Days Stuck</th>
                        <th>Created Date</th>
                        <th>Account</th>
                        <th>Collector</th>
                        <th>Debtor</th>
                        <th>Phase</th>
                        <th>Ref</th>
                        <th>Hold</th>
                        <th class="no-sort">Send Reminder</th>
                        <th class="no-sort">Status</th>
                        <th class="no-sort">Claim Lines</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claims as $claim): ?>
                        <tr class="<?= $claim->on_hold ? 'warning' : '' ?>">
                            <td>
                                <?php if ($claim->claim_phase->order === 2) { ?>
                                    Collector Ref not assigned
                                <?php } elseif ($claim->claim_phase->order === 3) { ?>
                                    Receipt not received
                                <?php } ?>
                            </td>
                            <td class='text-danger'>
                                <?php
                                $now = time(); // or your date as well
                                $your_date = strtotime($claim->modified);
                                $datediff = $now - $your_date;
                                echo round($datediff / (60 * 60 * 24));
                                ?>
                            </td>
                            <td> <?= date_format($claim->created, "d M Y") ?> </td>
                            <td>
                                <?= $this->Html->link($claim->account->name,
                                    ['controller' => 'Claims', 'action' => 'accounts', $claim->account_id],
                                    ['class' => 'btn btn-link']);
                                ?>
                            </td>
                            <td>
                                <?= $claim->collector->name ?>
                            </td>
                            <td>
                                <?php
                                echo $claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name;
                                ?>
                            </td>
                            <td> <?= $claim->claim_phase->order ?> - <?= $claim->claim_phase->name ?> </td>
                            <td>
                                <?= $claim->customer_reference; ?>
                            </td>
                            <td>
                                <?= $claim->on_hold ? "Yes" : "No" ?>
                            </td>
                            <td>
                                <?php if ($claim->claim_phase->order === 3) { ?>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#mailModel"
                                            data-href="/admin/claims/stuck-email/<?= $claim->id ?>">
                                        <?= __("Send") ?>
                                    </button>
                                <?php } ?>
                            </td>
                            <td>
                                <?= $this->Html->link('Edit',
                                    ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                                    ['class' => 'btn btn-warning']); ?>
                            </td>
                            <td>
                                <?= $this->Html->link('View',
                                    ['controller' => 'ClaimLines', 'action' => 'view', $claim->id],
                                    ['class' => 'btn btn-primary']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
            <div class="dataTables_paginate paging_simple_numbers pull-right">
                <ul class="pagination">
                    <?php if ($this->Paginator->numbers()): ?>
                        <?php echo $this->Paginator->prev(__d('Collector', 'Previous')); ?>
                        <?php echo $this->Paginator->numbers(); ?>
                        <?php echo $this->Paginator->next(__d('Collector', 'Next')); ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mailModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Mail Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to send Email to Collector?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link('Send', ['action' => 'stuckEmail', $claim->id], ['class' => 'btn btn-danger', "id" => 'confirm-mail-button']); ?>
            </div>
        </div>
    </div>

    <?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>

    <script>

        $('#filter-button').click(function () {
            url = window.location.href.split('?')[0];
            var collector = $('#collectors').val();
            if (collector) {
                url += "?";
                url += "collector=" + collector
            }
            window.location.assign(url)
        });
        $('#clear-button').click(function () {
            url = window.location.href.split('?')[0];
            window.location.assign(url)
        });
        $('#mailModel').on('show.bs.modal', function (e) {
            $(this).find('#confirm-mail-button').attr('href', $(e.relatedTarget).data('href'));
        });
    </script>
    <?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>

    <script>
        $('#mailModel').on('show.bs.modal', function (e) {
            $(this).find('#confirm-mail-button').attr('href', $(e.relatedTarget).data('href'));
        });

    </script>
