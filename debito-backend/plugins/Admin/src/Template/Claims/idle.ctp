<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimsActions
 * @var mixed $collector
 * @var array $collectorarr
 * @var mixed $collectors
 * @var object $debtCollectionPhaseId
 * @var mixed $offset
 * @var object $reminderPhaseId
 */
?>
<div class="page-heading">
    <h1 class="page-title">Idle claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Idle claims</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Claims that are idle in system (for over two weeks)</div>
            <div>
                <?= $this->Html->link('All claims', ['action' => 'index'], ['class' => 'btn btn-info']); ?>
                <?= $this->Html->link('Stuck claims', ['action' => 'stuck'], ['class' => 'btn btn-warning']); ?>
            </div>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Collectors:</label>
                    <?php echo $this->Form->control('collectors',
                        [
                            'options' => $collectors,
                            'value' => $collector,
                            'class' => "selectpicker show-tick",
                            'empty' => true,
                            'label' => false,
                            'data-style' => 'btn-solid',
                            'data-width' => "150px",
                            'empty' => ["" => 'All'],
                        ]); ?>
                    <button class="btn btn-blue btn-sm ml-4" id="filter-button">Filter</button>
                    <div class="ml-5">
                        <?php if ($offset > 99) { ?>
                            <button class="btn btn-secondary btn-sm ml-4" onclick="offset(<?php echo $offset - 100 ?>);">Previous</button>
                        <?php } ?>
                        <button class="btn btn-secondary btn-sm ml-4" onclick="offset(<?php echo $offset + 100 ?>);">Next</button>
                    </div>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th>Claim reference</th>
                        <th>Phase</th>
                        <th>Collector</th>
                        <th>Claim created date</th>
                        <th>Last action date</th>
                        <th>Last action</th>
                        <th>Days from last action</th>
                        <th>Send reminder</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claimsActions as $action): ?>
                        <tr>
                            <td> <?= $action['customer_reference'] ?> </td>
                            <td>
                                <?php
                                if ($action['claim_phase_id'] === $reminderPhaseId->id) {
                                    echo "Reminder";
                                } elseif ($action['claim_phase_id'] === $debtCollectionPhaseId->id) {
                                    echo "Debt Collection";
                                }
                                ?>
                            </td>
                            <td> <?= $collectorarr[$action['collector_id']] ?></td>
                            <td> <?php $date_claim_created = date_create_from_format("Y-m-d H:i:s", $action['created']);
                                echo date_format($date_claim_created, "d M Y") ?> </td>
                            <td> <?php $date_last_action = date_create_from_format("Y-m-d", $action['date']);
                                echo date_format($date_last_action, "d M Y") ?> </td>
                            <td> <?= $action['name'] ?> </td>
                            <td class='text-danger'>
                                <?php
                                $now = new DateTime();
                                $time_since_last_action = $date_last_action->diff($now);
                                echo $time_since_last_action->days;
                                ?>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                        data-target="#mailModel">
                                    <?= __("Send") ?>
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mailModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Mail Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to send Email to Collector?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link('Send Mail', ['action' => 'idleEmailConfirm', $action['id'], $time_since_last_action->days],
                    ['class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<script>

    $('#filter-button').click(function () {
        url = window.location.href.split('?')[0];
        var collector = $('#collectors').val();
        if (collector) {
            url += "?" + "collector=" + collector
        }
        window.location.assign(url)
    });

    function offset(param) {
        url = window.location.href;
        if (url.includes("?")) {
            url += "&" + "offset=" + param;
        } else {
            url += "?" + "offset=" + param;
        }
        window.location.assign(url)
    }
</script>
<style>
    #datatable_paginate {
        display: none;
    }
</style>