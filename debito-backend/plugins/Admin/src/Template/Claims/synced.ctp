<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claims
 */
?>
    <div class="page-heading">
        <h1 class="page-title">Claims that are synced today</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Claims</li>
        </ol>
    </div>

    <div class="page-content">
        <div class="ibox">
            <?= $this->Flash->render() ?>
            <div class="ibox-head">
                <div class="ibox-title">Today synced claim List</div>
            </div>
            <div class="ibox-body">
                <div class="flexbox mb-4">
                    <div class="flexbox">
                        <div class="input-group-icon input-group-icon-left mr-3">
                            <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                            <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                                   placeholder="Search ...">
                        </div>
                    </div>
                </div>
                <div class="table-responsive row">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead class="thead-default thead-lg">
                        <tr>
                            <th>Created By</th>
                            <th>Date</th>
                            <th>Account</th>
                            <th>Debtor</th>
                            <th>Phase</th>
                            <th>Collector</th>
                            <th>Ref</th>
                            <th>Collector-Ref</th>
                            <th>Hold</th>
                            <th class="no-sort">Action</th>
                            <th class="no-sort">Claim Lines</th>
                            <th class="no-sort">Statuses</th>
                            <th class="no-sort">Lawyers</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($claims as $claim): ?>
                            <tr class="<?= $claim->on_hold ? 'warning' : '' ?>">
                                <td> <?= $claim->user->email; ?> </td>
                                <td> <?= date_format($claim->created, "d-M-Y") ?> </td>
                                <td>
                                    <?= $this->Html->link($claim->account->name,
                                        ['controller' => 'Claims', 'action' => 'accounts', $claim->account_id],
                                        ['class' => 'btn btn-link']);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($claim->debtor) {
                                        echo $claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name;
                                    }
                                    ?>
                                </td>
                                <td> <?= $claim->claim_phase->order ?> - <?= $claim->claim_phase->name ?> </td>
                                <td>
                                    <?= $claim->collector->name; ?>
                                </td>
                                <td>
                                    <?= $claim->customer_reference; ?>
                                </td>
                                <td>
                                    <?= $claim->collector_reference; ?>
                                </td>
                                <td>
                                    <?= $claim->on_hold ? "Yes" : "No" ?>
                                </td>
                                <td>
                                    <?= $this->Html->link('Edit',
                                        ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                                        ['class' => 'btn btn-warning']); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link('View',
                                        ['controller' => 'ClaimLines', 'action' => 'view', $claim->id],
                                        ['class' => 'btn btn-primary']); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link('View',
                                        ['controller' => 'ClaimActions', 'action' => 'view', $claim->id],
                                        ['class' => 'btn btn-success']); ?>
                                </td>
                                <td>
                                    <?php if ($claim->debtor_lawyer) { ?>
                                        <?= $this->Html->link('Lawyer',
                                            ['controller' => 'Claims', 'action' => 'lawyer', $claim->id],
                                            ['class' => 'btn btn-info']); ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>