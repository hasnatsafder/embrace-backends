<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $debtor_lawyer
 */
?>
<div class="page-heading">
    <h1 class="page-title">Debtor Lawyer</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Lawyer</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit Lawyer</div>
                </div>
                <?= $this->Form->create($debtor_lawyer, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('name');
                        echo $this->Form->control('email');
                        echo $this->Form->control('vat_number');
                        echo $this->Form->control('phone');
                        echo $this->Form->control('zip_code');
                        echo $this->Form->control('city');
                        echo $this->Form->control('address');
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Save Lawyer'), ['class' => 'btn btn-primary mr-2']) ?>
                    <?= $this->Html->link('Cancel', ['action' => 'index'], ['class' => 'btn btn-secondary']); ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>