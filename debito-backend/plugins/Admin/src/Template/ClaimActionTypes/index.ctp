<?php
declare(strict_types=1);
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClaimActionType[] $claimActionTypes
 */
?>
    <div class="page-heading">
        <h1 class="page-title">Claims Action Types</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Claims Actions</li>
        </ol>
    </div>

    <div class="page-content">
        <div class="ibox">
            <?= $this->Flash->render() ?>
            <div class="ibox-head">
                <div class="ibox-title">Action List</div>
                <?= $this->Html->link('Add new', ['action' => 'add'],
                    ['class' => 'btn btn-success']); ?>
            </div>
            <div class="ibox-body">
                <div class="flexbox mb-4">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                               placeholder="Search ...">
                    </div>
                </div>
                <div class="table-responsive row">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead class="thead-default thead-lg">
                        <tr>
                            <th>Collector</th>
                            <th>Phase</th>
                            <th>Identifier</th>
                            <th>Collector Identifier</th>
                            <th>Name</th>
                            <th>Public</th>
                            <th class="no-sort">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($claimActionTypes as $claimActionType): ?>
                            <tr>
                                <td> <?= $claimActionType->collector->name ?> </td>
                                <td> <?= $claimActionType->claim_phase ? $claimActionType->claim_phase->name : "-" ?> </td>
                                <td> <?= $claimActionType->identifier ?> </td>
                                <td> <?= $claimActionType->collector_identifier ?> </td>
                                <td> <?= $claimActionType->name ?> </td>
                                <td>
                                    <?php echo $claimActionType->is_public == 0 ? "No" : "Yes" ?>
                                </td>
                                <td>
                                    <?= $this->Html->link('Edit', ['action' => 'edit', $claimActionType->id],
                                        ['class' => 'btn btn-warning']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>