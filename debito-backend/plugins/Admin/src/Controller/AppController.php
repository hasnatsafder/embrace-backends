<?php
declare(strict_types=1);

namespace Admin\Controller;

use App\Controller\AppController as BaseController;
use Cake\Event\Event;
use CrudView\Menu\MenuDivider;
use CrudView\Menu\MenuItem;

/**
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class AppController extends BaseController
{
    protected $isAdmin = true;

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                    'finder' => 'RootUsers',
                ],
            ],
            'authorize' => 'Controller',
            'loginAction' => [
                'prefix' => null,
                'plugin' => 'Admin',
                'controller' => 'Auth',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'prefix' => null,
                'plugin' => 'Admin',
                'controller' => 'Accounts',
                'action' => 'index',
            ],
            'authError' => __('Du skal logge ind for at se denne side'),
            'storage' => 'Session',
            'checkAuthIn' => 'Controller.initialize',
        ]);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        if ($this->Crud->isActionMapped()) {
            $this->Crud->action()
                ->setConfig('scaffold.utility_navigation', $this->getTopBarMenu())
                ->setConfig('scaffold.sidebar_navigation', $this->getSideBarMenu())
                ->setConfig('scaffold.site_title', __('Debito Admin'));
        }
    }

    protected function getTopBarMenu()
    {
        return [
            new MenuItem(
                __('Sign out'),
                ['controller' => 'Auth', 'action' => 'logouts']
            ),
        ];
    }

    protected function getSideBarMenu()
    {
        return [
            new MenuItem(
                __('Accounts'),
                ['controller' => 'Accounts', 'action' => 'index']
            ),
            new MenuItem(
                __('Users'),
                ['controller' => 'Users', 'action' => 'index']
            ),
            new MenuItem(
                __('Claims'),
                ['controller' => 'Claims', 'action' => 'index']
            ),
            new MenuItem(
                __('Collectors'),
                ['controller' => 'Collectors', 'action' => 'index']
            ),
            new MenuDivider(),
            new MenuItem(
                __('Claim action types'),
                ['controller' => 'ClaimActionTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Claim line types'),
                ['controller' => 'ClaimLineTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Claim phases'),
                ['controller' => 'ClaimPhases', 'action' => 'index']
            ),
            new MenuItem(
                __('Collector file types'),
                ['controller' => 'CollectorFileTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Company types'),
                ['controller' => 'CompanyTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Countries'),
                ['controller' => 'Countries', 'action' => 'index']
            ),
            new MenuItem(
                __('Currencies'),
                ['controller' => 'Currencies', 'action' => 'index']
            ),
            new MenuItem(
                __('Roles'),
                ['controller' => 'Roles', 'action' => 'index']
            ),
        ];
    }

    /**
     * Check if the provided user is authorized for the request.
     *
     * @param array|\ArrayAccess|null $user The user to check the authorization of.
     *   If empty the user fetched from storage will be used.
     * @return bool True if $user is authorized, otherwise false
     */
    public function isAuthorized($user = null): bool
    {
        $user = $this->getTableLocator()->get('Users')->get($user['id']);

        return $user->hasRole('root');
    }
}
