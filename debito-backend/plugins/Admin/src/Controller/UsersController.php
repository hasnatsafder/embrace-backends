<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 * @property \App\Model\Table\RolesTable $Roles
 */
class UsersController extends AppController
{
    public $modelClass = 'App.Users';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);

        $this->loadComponent('Paginator'); // Include the PaginateComponent
    }

    public function index()
    {
        $email = $this->request->query('email') ? $this->request->query('email') : "";
        $condition = [];
        if ($email) {
            $condition['Users.email LIKE '] = "%" . $email . "%";
        }
        $users = $this->Users->find('all', ['order' => ['Users.created' => 'DESC']])->where($condition);

        $this->set(compact('users', 'email'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $save = $this->Users->save($user);
            if ($save) {
                $role = $this->request->getData('role');
                if ($role) {
                    $this->loadModel('Accounts');
                    $admin_Account = $this->Accounts->find()->where(['name' => 'admin_account'])->first(); //temp account for admin, used also for collectors
                    if ($admin_Account) {
                        $this->loadModel('AccountsUsers');
                        $account_users = $this->AccountsUsers->newEntity();
                        $account_users->user_id = $save->id;
                        $account_users->account_id = $admin_Account->id;
                        $account_users->role_id = $role;
                        $saveInner = $this->AccountsUsers->save($account_users);
                        if ($saveInner) {
                            $this->Flash->success(
                                'Role has has been assigned',
                                [
                                    'params' => [
                                        'class' => 'alert alert-success',
                                    ],
                                ]
                            );
                        }
                    }
                }

                $this->Flash->success(
                    'User has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unabel to add your Users',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->loadModel('Roles');
        $role = $this->Roles->find(
            'list',
            ['conditions' => ['identifier in' => ['root', 'consultant']]]
        );

        $this->set(compact(['user', 'role']));
    }

    public function edit($id)
    {
        $user = $this->Users->get($id);
        if ($this->request->is('put')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $save = $this->Users->save($user);
            if ($save) {
                $this->Flash->success(
                    'User has been saved',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to save your User',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->set(compact(['user']));
    }

    /**
     * Soft delete users
     * @param String $account_id
     */
    public function delete($user_id)
    {
        $this->loadModel('Claims');
        $this->loadModel('Debtors');
        $this->loadModel('Accounts');
        $this->loadModel('Users');
        $this->loadModel('AccountsUsers');

        $owner_id = "ee7a94f3-646a-4e95-8f5f-17b1ad59c3ec";

        $accountsUsers = $this->AccountsUsers->find('all')
        ->where(['user_id' => $user_id, "role_id" => $owner_id]);
        foreach ($accountsUsers as $accountsUser) {
            $account = $this->Accounts->get($accountsUser->account_id);
            // check1 if account has claims assigned to it.
            $claims = $this->Claims->find()->where(['account_id' => $account->id])->count();
            if ($claims !== 0) {
                $this->Flash->error(
                    'Cannot delete, its account ' . $account->name . ' has ' . $claims . " claim(s) assigned to it",
                    [
                        'params' => [
                            'class' => 'alert alert-danger',
                        ],
                    ]
                );
                return $this->redirect(['action' => 'edit', $user_id]);
            }
            // delete all debtors of account
            $this->Debtors->trashAll(['account_id' => $account->id]);

            //delete account
            $account = $this->Accounts->get($account->id);
            $this->Accounts->trash($account);

            // delete account_users
            $this->AccountsUsers->trash($accountsUser);
        }

        $user = $this->Users->get($user_id);
        if ($this->Users->trash($user)) {
            $this->Flash->success(
                'Successfully deleted user '. $user->email,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        return $this->redirect(['action' => 'index']);
        }
    }     
}
