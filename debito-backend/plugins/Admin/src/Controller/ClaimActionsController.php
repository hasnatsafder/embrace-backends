<?php
declare(strict_types=1);

namespace Admin\Controller;

use Cake\ORM\TableRegistry;

/**
 * ClaimActions Controller
 *
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 */
class ClaimActionsController extends AppController
{
    public $modelClass = 'App.ClaimActions';
    public $isAdmin = false;
    public $collectorsTable;
    public $claimActionTypes;

    public function initialize()
    {
        parent::initialize();
        $this->collectorsTable = TableRegistry::getTableLocator()->get('Collectors');
        $this->claimActionTypesTable = TableRegistry::getTableLocator()->get('ClaimActionTypes');
    }

    public function view($claim_id)
    {
        $claimActions = $this->ClaimActions->find(
            'all',
            [
                'order' => ['ClaimActions.date' => 'ASC'],
                'conditions' => ['claim_id' => $claim_id],
            ])
            ->contain(["ClaimActionTypes"]);

        $debitoCollector = $this->collectorsTable->find('all')->where(['identifier' => 'debito'])->first();
        $allClaimActions = $this->claimActionTypesTable->find('list')
                            ->where(['collector_id' => $debitoCollector->id, 'is_automated' => false])
                            ->order(['identifier']);
        $this->set(compact('claimActions', 'allClaimActions', 'claim_id'));
    }

    public function save($claim_id) {
        $this->autoRender = false; 
        $claimAction = $this->ClaimActions->newEntity();
        $claimAction = $this->ClaimActions->patchEntity($claimAction, $this->request->getData());
        $claimAction->claim_id = $claim_id;
        $save = $this->ClaimActions->save($claimAction);
        if ($save) {
            $this->Flash->success(
                'Action has been added',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

        }
        else {
            $this->Flash->error(
                'Unable to add your Action ',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        return $this->redirect(['controller' => 'Claims', 'action' => 'edit', $claim_id]);
    }

    /** 
     * Detele claim actions
     */
    public function delete($claim_action_id) {
        $this->autoRender = false;
        $claim_action = $this->ClaimActions->get($claim_action_id);
        if($this->ClaimActions->delete($claim_action)) {
            $this->Flash->success(
                'Action removed from claim',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        }
        else{ 
            $this->Flash->error(
                'Unable to update your delete action',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        return $this->redirect(['controller' => 'Claims', 'action' => 'edit', $claim_action->claim_id]);
    }

}
