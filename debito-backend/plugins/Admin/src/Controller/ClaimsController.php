<?php
declare(strict_types=1);

namespace Admin\Controller;

use App\Utility\HelperFunctions;
use App\Utility\NotifyUser;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimPhasesTable $ClaimPhases
 * @property \App\Model\Table\CollectorsTable $Collectors
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\AccountsCollectorsTable $AccountsCollectors
 * @property \App\Model\Table\DebtorsTable $Debtors
 * @property \App\Model\Table\CurrenciesTable $Currencies
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 * @property \App\Model\Table\ClaimLinesTable $ClaimLines
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \App\Model\Table\DebtorLawyersTable $DebtorLawyers
 * @property \App\Model\Table\ClaimLineTypesTable $ClaimLineTypes
 * @property \App\Model\Table\ClaimActionTypesTable $ClaimActionTypes
 */
class ClaimsController extends AppController
{
    public $modelClass = 'App.Claims';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);

        $this->loadComponent('Paginator'); // Include the PaginateComponent
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Crud->setConfig('actions', [
            'approve' => [
                'className' => 'Crud.Edit',
                'messages' => [
                    'success' => [
                        'text' => 'Successfully approved {name}',
                        'params' => ['class' => 'alert alert-success alert-dismissible'],
                    ],
                    'error' => [
                        'params' => ['class' => 'alert alert-danger alert-dismissible'],
                    ],
                ],
            ],
            'setCollector' => [
                'className' => 'Crud.Edit',
                'view' => 'edit',
            ],
        ]);
        //$this->Crud->action()->setConfig('scaffold.actions', ['index', 'setCollector', 'view', 'approve']);
    }

    public function index()
    {
        $debito_reference = $this->request->query('ref') ? $this->request->query('ref') : "";
        $status = $this->request->query('status') ? $this->request->query('status') : "";
        $collector = $this->request->query('collector') ? $this->request->query('collector') : "";
        $condition = [];
        if ($status) {
            $condition['Claims.claim_phase_id'] = $status;
        }
        $claims = $this->Claims->find('all')->where($condition)
        ->contain([
            'Admin',
            'Accounts',
            'Debtors',
            'ClaimPhases',
            'Collectors',
            'Currencies'
        ]);

        if ($this->request->getQuery('claimsSearch')) {
            $claims = $claims->where(['OR' => [
                'LOWER(Accounts.name) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%",
                'LOWER(Debtors.company_name) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%",
                'LOWER(Debtors.first_name) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%",
                'LOWER(Debtors.last_name) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%",
                'LOWER(Claims.customer_reference) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%",
                'LOWER(Claims.collector_reference) LIKE ' => "%".strtolower($this->request->getQuery('claimsSearch'))."%"
            ]]);
        }

        $sortBy = $this->request->getQuery('sortBy') ? $this->request->getQuery('sortBy') : "recent";
        if ($sortBy == 'oldest') {
            $claims = $claims->order(['Claims.created' => 'ASC']);
        }
        else {
            $claims = $claims->order(['Claims.created' => 'DESC']);
        }
        $claims = $this->paginate($claims);

        $claimPhases = $this->Claims->ClaimPhases->find('all')->order(['`order`' => 'ASC']);
        $collectors = $this->Claims->Collectors->find('list');

        $auth_user_id = $this->Auth->user('id');

        $this->set(compact('claims', 'claimPhases', 'status', 'collectors', 'collector', 'auth_user_id'));
    }

    public function accounts($account_id)
    {
        $claims = $this->Claims->find('all', ['`order`' => ['Claims.created' => 'DESC']])->where(["Claims.account_id" => $account_id])->contain([
            'Accounts',
            'Debtors',
            'ClaimPhases',
            'Collectors',
        ]);

        $this->loadModel('ClaimPhases');
        $claimPhases = $this->ClaimPhases->find(
            'list',
            ['`order`' => ['ClaimPhases.order' => 'ASC']]
        );

        $this->loadModel('Collectors');
        $collectors = $this->Collectors->find(
            'list'
        );

        $this->loadModel('Accounts');
        $account = $this->Accounts->get($account_id);

        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);

        $this->set(compact('claims', 'claimPhases', 'collectors', 'account', 'user'));
    }

    public function accountsAdd($account_id)
    {
        $claim = $this->Claims->newEntity();

        $this->loadModel('Accounts');
        $account = $this->Accounts->get($account_id);

        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);

        if ($this->request->is('post')) {
            $claim->created_by_id = $user->id;
            $claim->account_id = $account->id;

            $claim = $this->Claims->patchEntity($claim, $this->request->getData());

            // No collector reference provided then try to find from collectors account table
            if (!$this->request->getData("collector_reference")) {
                $this->loadModel('AccountsCollectors');
                $accountsCollector = $this->AccountsCollectors->find(
                    'all',
                    ['conditions' => ["account_id" => $account_id, 'collector_id' => $this->request->getData("collector_id")]]
                )->last();
                if ($accountsCollector) {
                    $claim->collector_reference = $accountsCollector->collector_identifier;
                }
            }

            $save = $this->Claims->save($claim);
            if ($save) {
                $this->Flash->success(
                    'Claim has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'accounts', $account->id]);
            }
            $this->Flash->error(
                'Unable to add your Claim',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->loadModel('Collectors');
        $collectors = $this->Collectors->find('list');

        $this->loadModel('Debtors');
        $debtors = $this->Debtors->find('all')->where(['account_id' => $account_id])->select(['company_name', 'id', 'first_name', 'last_name', 'vat_number']);

        $deb = [];

        foreach ($debtors as $debtor) {
            $deb[$debtor->id] = $debtor->company_name ?? ($debtor->first_name . " " . $debtor->last_name);
            $deb[$debtor->id] .= "-" . $debtor->vat_number;
        }

        $this->set(compact(['user', 'collectors', 'claim', 'account', 'deb']));
    }

    public function edit($claim_id)
    {
        $claim = $this->Claims->find('all')->where(['Claims.id' => $claim_id])
            ->contain(['Accounts', 'Admin', 'Currencies', 'ClaimPhases', 'Collectors', 'Debtors'])
            ->first();

        //For saving Claim
        if ($this->request->is(['post', 'put'])) {
            $newStatus = $this->request->getData('claim_phase_id');
            $this->loadModel('ClaimPhases');
            $claimPhase = $this->ClaimPhases->get($newStatus);
            $claimCurrentStatus = $this->ClaimPhases->get($claim->claim_phase_id);

            // if claim is moved to end date, then set end date of claim
            if ($claimPhase->identifier == "ended") {
                $claim->ended_at = date('Y-m-d H:i:s');
            }

            // if claims is moved case created (2nd phase), mark claim synced as null
            if ($claimPhase->identifier == "case_created") {
                $claim->synced = null;
                $claim->completed = date('Y-m-d H:i:s');
            }

            //if there is a new claim phase
            if ($claimPhase->id != $claimCurrentStatus->id) {
                // if claim was at ended stage and is now moved backwards, then mark end date as empty
                if ($claimCurrentStatus->identifier == 'ended' && $claimPhase->identifier != 'ended') {
                    $claim->ended_at = null;
                }
            }

            //if collector changed then remove entry of account from account collectors, provided that accounts collector identifer is empty
            if ($this->request->getData('collector_id') !== $claim->collector_id) {
                $this->loadModel('AccountsCollectors');
                //get old collector id's identifier for account
                $account_collector = $this->AccountsCollectors->find()
                    ->where(['account_id' => $claim->account_id, 'collector_id' => $claim->collector_id])
                    ->first();

                //set claim phase to one since it has to be send to collector again.
                $claimPhaseOne = $this->ClaimPhases->find()
                    ->where(['identifer' => 'case_received'])
                    ->first();
                $this->request->data['claim_phase_id'] = $claimPhaseOne->id;

                $this->Flash->success(
                    'Claim is moved to phase one',
                    [
                        'params' => [
                            'class' => 'alert alert-danger',
                        ],
                    ]
                );

                if ($account_collector && ($account_collector->collector_identifier == "" || $account_collector->collector_identifier == null)) {
                    $this->loadModel('Collectors');
                    $collector = $this->Collectors->get($claim->collector_id);
                    $this->AccountsCollectors->delete($account_collector);
                    $this->Flash->success(
                        'Claim was removed from collector identifier list of ' . $collector->name,
                        [
                            'params' => [
                                'class' => 'alert alert-warning',
                            ],
                        ]
                    );
                }
            }

            $this->Claims->patchEntity($claim, $this->request->getData());
            if ($this->Claims->save($claim)) {
                $this->Flash->success(
                    'Claim has been updated',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );
                $redirect_url = $this->request->getQuery('back_to_page');
                if (!$redirect_url) {
                    $redirect_url = ['action' => 'index'];
                }

                return $this->redirect(['action' => 'edit', $claim->id]);
            }
            $this->Flash->error(
                'Unable to Update your Claim',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        // check if current admin is handling claim
        $handling = $this->Auth->user('id') ? true : false;

        $back_to_page = $this->referer();
        $this->loadModel('Collectors');
        $collectors = $this->Collectors->find('list');

        $this->loadModel('ClaimPhases');
        $claimPhases = $this->ClaimPhases->find(
            'list'
        )->order(['`order`']);

        $this->loadModel('Currencies');

        //total of claim
        $principle = HelperFunctions::getTotalClaimSum($claim);

        // Get claim actions of claim and all claim actions too
        $this->loadModel('ClaimActions');
        $this->loadModel('ClaimActionTypes');
        $claimActions = $this->ClaimActions->find(
            'all',
            [
                'order' => ['ClaimActions.date' => 'ASC', 'ClaimActions.created' => 'ASC'],
                'conditions' => ['claim_id' => $claim_id],
            ])
            ->contain(["ClaimActionTypes"]);

        $debitoCollector = $this->Collectors->find('all')->where(['identifier' => 'debito'])->first();
        $allClaimActions = $this->ClaimActionTypes->find('list')
                            ->where(['collector_id' => $debitoCollector->id, 'is_automated' => false])
                            ->order(['identifier']);

        $currencies = $this->Currencies->find('list', [
            'fields' => ['name' => 'CONCAT(Currencies.name, " - ", Currencies.iso_code)', 'Currencies.id'],
        ]);

        // Claim Lines of claim
        $this->loadModel('ClaimLines');
        $claimLines = $this->ClaimLines->find('all')
                    ->where(['ClaimLines.claim_id' => $claim->id])->contain(['ClaimLineTypes']);

        $this->set(compact(['claim', 'collectors', 'claimPhases', 'currencies', 'handling', 'principle', 'back_to_page', 'allClaimActions', 'claimActions', 'claimLines']));
    }

    public function delete($claim_id)
    {
        //delete claim actions
        $this->loadModel('ClaimActions');
        $this->ClaimActions->trashAll(['claim_id' => $claim_id]);

        //delete claim lines
        $this->loadModel('ClaimLines');
        $this->ClaimLines->trashAll(['claim_id' => $claim_id]);

        //delete claim
        $claim = $this->Claims->get($claim_id);

        //get user info
        $this->loadModel('Users');
        $user = $this->Users->get($claim->created_by_id);

        //get account info
        $this->loadModel('Accounts');
        $account = $this->Accounts->get($claim->account_id);

        //get debtor info
        $this->loadModel('Debtors');
        $debtor = $this->Debtors->get($claim->debtor_id);

        //get collector info
        $this->loadModel('Collectors');
        $collector = $this->Collectors->get($claim->collector_id);

        if ($this->Claims->trash($claim)) {
            $this->Flash->success(
                'Claim has been deleted',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            //send email to admin
            $email = new Email('sparkpost');
            $emailMessage = "Claim information <br><br>";
            $emailMessage .= "User: " . $user->email . "<br><br>";
            $emailMessage .= "Account.: " . $account->name . "<br><br>";
            $emailMessage .= "Debtor: " . $debtor->company_name . $debtor->first_name . "<br><br>";
            $emailMessage .= "Collector: " . $collector->name . "<br><br>";
            $emailMessage .= "Collector Reference: " . $claim->collector_reference . "<br><br>";
            $emailMessage .= "Customer Reference: " . $claim->customer_reference . "<br><br>";
            $myemail = $email->from(['kundeservice@debito.dk' => 'Debito'])
                ->to(Configure::read('Adminemail'))
                ->subject("A claim was deleted")
                ->emailFormat('html')
                ->send($emailMessage);

            return $this->redirect(['action' => 'index']);
        }
    }

    public function lawyer($claim_id)
    {
        $this->loadModel('DebtorLawyers');
        $debtor_lawyer = $this->DebtorLawyers->find()->where(['claim_id' => $claim_id])->first();

        if ($this->request->is('put')) {
            $debtor_lawyer = $this->DebtorLawyers->patchEntity($debtor_lawyer, $this->request->getData());
            $save = $this->DebtorLawyers->save($debtor_lawyer);
            if ($save) {
                $this->Flash->success(
                    'Debtor lawyer has been saved',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to save your Debtor Lawyer',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->set(compact('debtor_lawyer'));
    }

    public function submit($claim_id, $collector_id)
    {
        $this->autoRender = false;
        $claim = $this->Claims->get($claim_id);

        //For saving Claim
        if ($this->request->is(['get'])) {
            $claim->collector_id = $collector_id;
            $this->loadModel('ClaimPhases');
            $claimPhaseOrder2 = $this->ClaimPhases->find(
                'all',
                ['conditions' => ["`order`" => 2]]
            )->first();
            //check if we need to send email to Collector
            $sendEmail = $this->sendSubmissionEmailCollector($claim);
            if (!$sendEmail) { //account was already registered at collector, move claim to approved
                $claim->completed = date("Y-m-d H:i:s");
            }
            $claim->approved = date("Y-m-d H:i:s");
            $claim->claim_phase_id = $claimPhaseOrder2->id;
            $this->Claims->patchEntity($claim, $this->request->getData());

            if ($this->Claims->save($claim)) {
                $redirect_url = $this->request->getQuery('back_to_page');
                if (!$redirect_url) {
                    $redirect_url = ['action' => 'index'];
                }

                return $this->redirect($redirect_url);
            }
            $this->Flash->error(
                'Unable to submit your Claim',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
    }

    // For new claims index page

    private function sendSubmissionEmailCollector($claim)
    {

        $returnMessage = "Claim has been submitted! ";

        $this->loadModel('AccountsCollectors');
        $accountsCollector = $this->AccountsCollectors->find(
            'all',
            ['conditions' => ["account_id" => $claim->account_id, 'collector_id' => $claim->collector_id]]
        )->last();

        //get email of collector
        $this->loadModel('Collectors');
        $collector = $this->Collectors->find(
            'all',
            ['conditions' => ['id' => $claim->collector_id]]
        )->first();

        // if collector is mortang and lindorff only then we need to send email
        if ($collector->identifier != "mortang" && $collector->identifier != "lindorff") {
            $this->Flash->success(
                $returnMessage . "Claim ready to be synced at " . $collector->name,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            return false;
        }

        //Email will now be send even if already sent one to collector.

        //if no collector reference against this company send email to Collector
        if (!$accountsCollector) {
            //Add a new row to collector reference
            $accountsCollector = $this->AccountsCollectors->newEntity();
            $accountsCollector->account_id = $claim->account_id;
            $accountsCollector->collector_id = $claim->collector_id;
            $accountsCollector->collector_identifier = '';
            $this->AccountsCollectors->save($accountsCollector);

            //set default email subject to Mortangs
            $emailSubject = "Ny kunde til Debito";
            $kNumber = "kundenummer";
            if ($collector->identifier == "lindorff") {
                $emailSubject = "K-nummer til 0DB";
                $kNumber = "K-nummer";
            }

            //get account info and create email message
            $this->loadModel("Accounts");
            $account = $this->Accounts->get($claim->account_id);
            $emailMessage = "Firmanavn: " . $account->name . "<br><br>";
            $emailMessage .= "Adresse: " . $account->address . "<br><br>";
            $emailMessage .= "Postnr.: " . $account->zip_code . "<br><br>";
            $emailMessage .= "By: " . $account->city . "<br><br>";
            $emailMessage .= "Email: " . $account->email . "<br><br>";
            $emailMessage .= "Tlf: " . $account->phone . "<br><br>";
            $emailMessage .= "CVR: " . $account->vat_number . "<br><br>";
            $emailMessage .= "Reg. nr.: " . $account->bank_reg_number . "<br>";
            $emailMessage .= "Kontonr: " . $account->bank_account_number . "<br><br>";
            $emailMessage .= "<a href='https://admin.debito.dk/collector'>Klik her for at tilføje " . $kNumber . " til debitoren.</a> <br><br>";
            $emailMessage .= "Debito ApS <br>";
            $emailMessage .= "Effektiv online inkasso <br>";
            $emailMessage .= "<a href='https://debito.dk/'>debito.dk</a> <br>";

            $email = new Email('sparkpost');
            $myemail = $email->from(['kundeservice@debito.dk' => 'Debito'])
                ->to($collector->email)
                ->subject($emailSubject)
                ->emailFormat('html')
                ->bcc("christoffer@debito.dk")
                ->send($emailMessage);
            $this->Flash->success(
                $returnMessage . "An email has been sent to " . $collector->name . " for registering account",
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );

            return true; // an email was sent to collector
        } //end if
        $this->Flash->success(
            $returnMessage . "Account is already registered at " . $collector->name,
            [
                'params' => [
                    'class' => 'alert alert-success',
                ],
            ]
        );

        return false; // no email was required to be sent
    }

    //To show Claims Stuck ( in Created Mode or Sent Mode for over one day)

    public function newClaims()
    {
        //get the name of first claim phase
        $firstPhase = $this->Claims->ClaimPhases->find()
            ->where(['ClaimPhases.identifier' => 'case_received'])
            ->firstOrFail();

        $condition = [];
        $condition['Claims.claim_phase_id'] = $firstPhase->id;
        $claims = $this->Claims->find('all', ['`order`' => ['Claims.created' => 'DESC']])->where($condition)->contain([
            'Users',
            'Accounts',
            'Debtors',
            'ClaimPhases',
            'Collectors',
            'DebtorLawyers',
        ]);

        $this->set(compact('claims', 'collectors'));
    }

    public function stuck()
    {
        $collector = $this->request->query('collector') ? $this->request->query('collector') : "";

        $claims = $this->Claims->find('all', ['`order`' => ['Claims.modified' => 'ASC']])
            ->where(['ClaimPhases.identifier IN' => ['case_created', 'case_sent']])
            ->contain([
                'Users',
                'Accounts',
                'Debtors',
                'ClaimPhases',
                'Collectors',
            ]);

        if (!empty($collector)) {
            $claims = $claims->where(['Claims.collector_id' => $collector]);
        }

        $claims = $this->paginate($claims);
        $collectors = $this->Claims->Collectors->find('list');

        $this->set(compact('claims', 'collectors', 'collector'));
    }

    //To show Claims Stuck ( in Reminder Mode or DebtCollection Mode for over 3 days)
    public function stuckEmail($claim_id)
    {

        $this->loadModel('Collectors');
        $this->loadModel('ClaimLines');
        $this->loadModel('ClaimLineTypes');
        $this->loadModel('Accounts');
        $this->loadModel('Currencies');

        $claim = $this->Claims->get($claim_id);

        $collector = $this->Collectors->get($claim->collector_id);

        $claim_lines = $this->ClaimLines->find()->where(['claim_id' => $claim->id]);

        $credit_note = $this->ClaimLineTypes->find()->where(['identifier' => 'credit_note'])->select(['id'])->first();

        $account = $this->Accounts->get($claim->account_id);

        $currency = $this->Currencies->get($claim->currency_id);

        $amount = 0;
        foreach ($claim_lines as $claim_line) {
            if ($claim_line->claim_line_type_id === $credit_note->id) {
                $amount = $amount - $claim_line->amount;
            } else {
                $amount = $amount + $claim_line->amount;
            }
        }

        $date = "";
        // if synced date is not null else use modified date
        if ($claim->synced) {
            $date = date_format($claim->synced, 'm-d-Y');
        } else {
            $date = date_format($claim->modified, 'm-d-Y');
        }

        // send email to collector
        $email = new Email('sparkpost');
        $emailMessage = "Kære $collector->name,<br><br>";
        $emailMessage .= "Nedenstående sag er ikke blevet oprettet hos jer endnu .<br>";
        $emailMessage .= "Den blev sendt fra os $date . <br>";
        $emailMessage .= "$claim->customer_reference . <br>";
        $emailMessage .= "$account->name . <br>";
        $emailMessage .= "$account->address . <br>";
        $emailMessage .= "$account->zip_code $account->city . <br>";
        $emailMessage .= "$currency->iso_code $amount . <br>";
        $email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to($collector->email)
            ->subject("Sag mangler oprettelse")
            ->emailFormat('html')
            ->send($emailMessage);

        $this->Flash->success(
            'Email Sent to Collector About ' . $claim->customer_reference,
            [
                'params' => [
                    'class' => 'alert alert-success',
                ],
            ]
        );

        return $this->redirect(['action' => 'stuck']);
    }

    /*
      Send Email Collector for Lack of Actions on a claim
      @params: Claim Action Id , Days since Last Action
    */

    public function idle()
    {
        $this->loadModel('ClaimPhases');
        $this->loadModel('Collectors');

        $reminderPhaseId = $this->ClaimPhases->find()->select('id')->where(['identifier' => 'reminder'])->first();
        $debtCollectionPhaseId = $this->ClaimPhases->find()->select('id')->where(['identifier' => 'debt_collection'])->first();

        $collector = $this->request->query('collector') ? $this->request->query('collector') : "";
        $offset = $this->request->query('offset') ? $this->request->query('offset') : 0;
        //if less than 0 then set to 0 to avoid sql error
        if ($offset < 0) {
            $offset = 0;
        }
        $collectorFilter = "";
        if (!$collector) {
            $collectorFilter = " != '0'"; //never possible
        } else {
            $collectorFilter = " = '" . $collector . "'";
        }

        //set collector name array
        $collectorarr = [];
        $collectors = $this->Collectors->find(
            'list'
        );
        $collectorsAll = $this->Collectors->find()->all();
        foreach ($collectorsAll as $collectorInner) {
            $collectorarr[$collectorInner->id] = $collectorInner->name;
        }

        $this->loadModel('ClaimActions');
        set_time_limit(300);
        $connection = ConnectionManager::get('default');
        $claimsActions = $connection->execute("SELECT claims.claim_phase_id, claim_actions.id, claim_actions.date,claims.customer_reference, claims.created, claims.collector_id, claim_action_types.name as name FROM claim_actions
      join claims on claim_actions.claim_id = claims.id
      join claim_action_types on claim_actions.claim_action_type_id = claim_action_types.id
      where date in ( Select MAX(date) from claim_actions as ca where ca.claim_id = claim_actions.claim_id group by ca.claim_id)
      and claims.claim_phase_id IN ('$reminderPhaseId->id', '$debtCollectionPhaseId->id')
      and claims.collector_id " . $collectorFilter . "
      and claim_actions.date <= DATE_SUB(CURDATE(), INTERVAL 14 DAY)
      `order` by claim_actions.date
      limit 100 OFFSET " . $offset)->fetchAll('assoc');
        $old_claim = "";
        foreach ($claimsActions as $key => $action) {
            if ($old_claim == $action['customer_reference']) {
                unset($claimsActions[$key]);
            } else {
                $old_claim = $action['customer_reference'];
            }
        }
        $this->set(compact('claimsActions', 'reminderPhaseId', 'debtCollectionPhaseId', 'collectorarr', 'collectors', 'collector', 'offset'));
    }

    public function idleEmailConfirm($claim_action_id, $time_since_last_action)
    {
        $this->loadModel('Collectors');
        $this->loadModel('ClaimActions');
        $this->loadModel('ClaimActionTypes');

        $claim_action = $this->ClaimActions->get($claim_action_id);

        $claim = $this->Claims->get($claim_action->claim_id);

        $collector = $this->Collectors->get($claim->collector_id);

        $claim_action_type = $this->ClaimActionTypes->get($claim_action->claim_action_type_id);
        $emailMessage = "Kære $collector->name,<br><br>";
        $emailMessage .= "Sagen $claim->collector_reference har ikke haft en handling i $time_since_last_action dage .<br>";
        $emailMessage .= "Den seneste handling var $claim_action_type->name .";
        $this->set(compact('emailMessage', 'claim_action_id', 'collector'));
    }

    // To send email, logic of whether to send an email or not is inside this function as well.
    //Return boolean

    public function idleEmail($claim_action_id)
    {
        if ($this->request->is(['post', 'put'])) {
            echo $mailText = $this->request->getData('mail_text');
            $this->loadModel('Collectors');
            $this->loadModel('ClaimActions');
            $this->loadModel('ClaimActionTypes');

            $claim_action = $this->ClaimActions->get($claim_action_id);

            $claim = $this->Claims->get($claim_action->claim_id);

            $collector = $this->Collectors->get($claim->collector_id);

            // send email to collector
            $email = new Email('sparkpost');
            $email->from(['kundeservice@debito.dk' => 'Debito'])
                ->to($collector->email)
                ->subject("Manglende handling på sag")
                ->emailFormat('html')
                ->send($mailText);

            $this->Flash->success(
                'Email Sent to Collector About ' . $claim->customer_reference,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        }

        return $this->redirect(['action' => 'idle']);
    }

    public function rejected()
    {
        $collector = $this->request->query('collector') ? $this->request->query('collector') : "";
        $condition = [];

        $this->loadModel('ClaimPhases');
        $rejectedPhase = $this->ClaimPhases->find()
            ->where(['ClaimPhases.identifier' => 'rejected'])
            ->select(['id'])
            ->firstOrFail();

        if ($collector) {
            $condition['Claims.collector_id'] = $collector;
        }

        $claims = $this->Claims->find('all', ['order' => ['Claims.modified' => 'ASC']])
            ->where(['Claims.claim_phase_id' => $rejectedPhase->id, $condition])
            ->contain([
                'Users',
                'Accounts',
                'Debtors',
                'ClaimPhases',
                'Collectors',
            ]);

        $claims = $this->paginate($claims);

        $this->loadModel('Collectors');
        $collectors = $this->Collectors->find(
            'list'
        );
        $this->set(compact('claims', 'collectors', 'collector'));
    }

    public function confirmRejection($claim_id)
    {

        $claim = $this->Claims->get($claim_id);
        if (!$claim) {
            $this->Flash->error(
                'Claim not found',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        //For saving Claim
        if ($this->request->is(['post', 'put'])) {
            $reason = $this->request->getData("rejected_reason");
            $claim->rejected_is_public = 1;
            $claim->rejected_reason = (strlen($reason) > 255 ? substr($reason, 0, 255) : $reason);

            if (!$this->Claims->save($claim)) {
                $this->Flash->error(
                    'Unable to reject Claim',
                    [
                        'params' => [
                            'class' => 'alert alert-danger',
                        ],
                    ]
                );
            }

            // send email to user with CC to account
            if ($this->request->getData("send_email") == "true") {
                NotifyUser::sendRejected($claim, $this->request->getData("email_subject"), $this->request->getData("mail_text"));
            }

            return $this->redirect(['action' => 'rejected']);
        }

        // get account and debtors names
        $this->loadModel('Accounts');
        $account = $this->Accounts->get($claim->account_id);
        $accountName = $account->name;
        $debtorName = HelperFunctions::getDebtorsName($claim);

        $this->set(compact(['claim', 'accountName', 'debtorName']));
    }

    /**
     * Synced today claims
     */

    public function synced()
    {
        $claims = $this->Claims->find('all', ['order' => ['Claims.created' => 'DESC']])->where(['DATE(Claims.synced)' => date('Y-m-d')])->contain([
            'Users',
            'Accounts',
            'Debtors',
            'ClaimPhases',
            'Collectors',
            'DebtorLawyers',
        ]);
        $this->set(compact(['claims']));
    }

    /**
     * Handle claims by admin
     */
    public function handle($claim_id)
    {
        $claim = $this->Claims->get($claim_id);
        $claim->user_handle = $this->Auth->user('id');
        if ($this->Claims->save($claim)) {
            $this->Flash->success(
                'Claim assigned to you',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
            if ($this->request->getQuery('redirect')) {
                return $this->redirect($this->request->getQuery('redirect'));
            }
            return $this->redirect(['action' => 'index']);
        }
    }
}
