<?php
declare(strict_types=1);

namespace Admin\Controller;

use App\Model\Entity\Account;
use Cake\I18n\FrozenTime;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\RolesTable $Roles
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 * @property \App\Model\Table\CountriesTable $Countries
 */
class AccountsController extends AppController
{
    public $modelClass = 'App.Accounts';
    public $isAdmin = false;
    protected $searchActions = ['index'];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $accounts = $this->Accounts
            ->find('all')
            ->enableAutoFields()
            ->contain([
                'Countries',
                'Users',
                'AccountPlans',
                'ErpIntegrations'
            ])
            ->leftJoinWith('Users')
            ->leftJoinWith('AccountPlans')
            ->leftJoinWith('ErpIntegrations')
            ->group(['Accounts.id']);
        if ($this->request->getQuery('accountsAndUsers')) {
            $accounts = $accounts->where(['OR' => [
                'LOWER(Users.email) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%",
                'LOWER(Accounts.name) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%",
                'LOWER(Accounts.email) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%",
                'LOWER(Accounts.phone) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%",
                'LOWER(Users.first_name) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%",
                'LOWER(Users.last_name) LIKE ' => "%".strtolower($this->request->getQuery('accountsAndUsers'))."%"
            ]]);
        }
        $filterBy = $this->request->getQuery('filterBy') ? $this->request->getQuery('filterBy') : "";
        if($filterBy) {
            if ($filterBy == "paid") {
                $accounts = $accounts->where(['AccountPlans.plan_id IN' => array_keys($this->plansArray('paid'))]);
            }
            else if($filterBy == "free") {
                $accounts = $accounts->where(['AccountPlans.plan_id IN' => array_keys($this->plansArray('free'))]);
            }
            else if ($filterBy == "not-integrated") {
                $accounts = $accounts->where('ErpIntegrations.account_id is null');
            }
            else if($filterBy == "integrated") {
                $accounts = $accounts->where('ErpIntegrations.account_id IS NOT null');
            }
        }
        $sortBy = $this->request->getQuery('sortBy') ? $this->request->getQuery('sortBy') : "recent";
        if ($sortBy == 'oldest') {
            $accounts = $accounts->order(['Accounts.created' => 'ASC']);
        }
        else {
            $accounts = $accounts->order(['Accounts.created' => 'DESC']);
        }
        $this->set('accounts', $this->paginate($accounts));
        $this->set('plans', $this->plansArray());
    }

    /**
     * Add new account without specifly user on loading
     */
    public function add()
    {
        $this->loadModel('Users');
        $account = $this->Accounts->newEntity();
        if ($this->request->is('post')) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            $account->created_by_id = $this->request->getData('user_id');
            $save = $this->Accounts->save($account, ['skipAutoCreateUser' => true]);
            if ($save) {
                //get customer role
                $this->loadModel('Roles');
                $user_role = $this->Roles->find('all')->where(['identifier' => 'owner'])->first();

                $this->loadModel('AccountsUsers');
                $account_user = $this->AccountsUsers->newEntity();
                $account_user->account_id = $save->id;
                $account_user->user_id = $this->request->getData('user_id');
                $account_user->role_id = $user_role->id;
                $this->AccountsUsers->save($account_user);

                //make that account active
                $is_active = $this->request->getData("is_active");
                if ($is_active === "1") {
                    $user = $this->Users->get($this->request->getData('user_id'));
                    $user->active_account_id = $save->id;
                    $this->Users->save($user);
                }

                $this->Flash->success(
                    'Account has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to add your Account',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $users = $this->Users->find('list');

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list');
        $denmarkId = $this->Countries->find('all')->where(['name' => 'Denmark'])->select('id')->first();

        $this->set(compact(['account', 'users', 'countries', 'denmarkId']));
    }

    public function useraccounts($user_id)
    {
        $account = $this->Accounts->find('all')
            ->contain('Countries')
            ->leftJoinWith('AccountsUsers')
            ->where(['AccountsUsers.user_id' => $user_id])
            ->select(['id', 'vat_number', 'name']);
        $this->RequestHandler->respondAs('json');
        $this->response->type('application/json');
        $this->autoRender = false;
        echo json_encode($account);
    }

    public function user($id)
    {
        $accounts = $this->Accounts->find('all')
            ->contain([
                'Countries',
                'Users',
                'AccountPlans',
                'ErpIntegrations'
            ])
            ->leftJoinWith('AccountsUsers')
            ->leftJoinWith('AccountPlans')
            ->leftJoinWith('ErpIntegrations')
            ->where(['AccountsUsers.user_id' => $id]);
        $this->loadModel('Users');
        $user = $this->Users->get($id);
        $this->set(compact(['accounts', 'user']));
        $this->set('plans', $this->plansArray());
    }

    public function addUserAccount($user_account_id)
    {
        $this->loadModel('Users');
        $account = $this->Accounts->newEntity();
        if ($this->request->is('post')) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            $account->created_by_id = $user_account_id;
            $save = $this->Accounts->save($account, ['skipAutoCreateUser' => true]);
            if ($save) {
                //get customer role
                $this->loadModel('Roles');
                $user_role = $this->Roles->find('all')->where(['identifier' => 'owner'])->first();

                $this->loadModel('AccountsUsers');
                $account_user = $this->AccountsUsers->newEntity();
                $account_user->account_id = $save->id;
                $account_user->user_id = $user_account_id;
                $account_user->role_id = $user_role->id;
                $this->AccountsUsers->save($account_user);

                //make that account active
                $is_active = $this->request->getData("is_active");
                if ($is_active === "1") {
                    $user = $this->Users->get($user_account_id);
                    $user->active_account_id = $save->id;
                    $this->Users->save($user);
                }

                $this->Flash->success(
                    'Account has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to add your Account',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $user = $this->Users->get($user_account_id);

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list');
        $denmarkId = $this->Countries->find('all')->where(['name' => 'Denmark'])->select('id')->first();

        $this->set(compact(['account', 'user', 'countries', 'denmarkId']));
    }

    public function editUserAccount($account_id)
    {
        $account = $this->Accounts->get($account_id);
        if ($this->request->is('put')) {
            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            $save = $this->Accounts->save($account);
            if ($save) {
                $this->Flash->success(
                    'Account has been updated',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                // if save and stay
                if($this->request->getData('saveAndStay') == 1) {
                    return $this->redirect(['action' => 'editUserAccount', $account_id]);
                }

                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to update your account',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);
        $users = $this->Users->find('list');

        $this->loadModel('Roles');
        $roles = $this->Roles->find('list')->where(['identifier IN' => ['user', 'root']]);

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list');
        $denmarkId = $this->Countries->find('all')->where(['name' => 'Denmark'])->select('id')->first();

        $this->loadModel('Plans');
        $plans = $this->Plans->find('all')->order('created');

        $this->loadModel('ErpIntegrations');
        $integration = $this->ErpIntegrations->find('all')->where(['account_id' => $account->id])->last();

        $this->loadModel('AccountsUsers');
        $user_accounts = $this->AccountsUsers->find('all')->where(['account_id' => $account->id])->contain(["Roles", "Users"]);

        $this->loadModel('AccountPlans');
        $current_plan = $this->AccountPlans->find('all')->where(['account_id' => $account->id])->contain('Plans')->order('AccountPlans.created')->last();

        $invoices = [];
        if ($integration) {
            $this->loadModel('Invoices');
            $invoices = $this->Invoices->find('all')->where(['erp_integration_id' => $integration->id])->count();
        }

        // if not found then create one
        if (!$current_plan) {
            $free_plan = $this->Plans->find('all')->where(['name' => 'Trial'])->first();
            $current_plan = $this->AccountPlans->newEntity();
            $current_plan->plan_id = $free_plan->id;
            $current_plan->account_id = $account->id;
            $current_plan->discount = 0;
            $current_plan->internal_note = "";
            $this->AccountPlans->save($current_plan);
        }

        $all_plans = $this->AccountPlans->find('withTrashed')->where(['account_id' => $account->id])->contain('Plans')->order('AccountPlans.created');

        $this->set(compact(['account', 'user', 'countries', 'denmarkId', 'plans', 'current_plan', 'all_plans', 'integration', 'user_accounts', 'users', 'roles', 'invoices']));
    }

    /**
     * Save subscription plans for account
     */
    public function savePlan($account_id)
    {
        $this->autoRender = false; 
        $this->loadModel('AccountPlans');
        $account = $this->Accounts->get($account_id);
        // if get previous plan
        $current_plan = $this->AccountPlans->find('all')->where(['account_id' => $account_id])->order('AccountPlans.created')->last();

        // if either plan id or discount changes, then insert a new one
        if ($current_plan->plan_id != $this->request->getData('plan_id') || $current_plan->discount != $this->request->getData('discount')) {

            // soft delete previous plans for account
            $previous_plans = $this->AccountPlans->find('all')->where(['account_id' => $account_id]);
            foreach($previous_plans as $previous_plan) {
                if(!$previous_plan->delete) {
                    $previous_plan->deleted = new FrozenTime();
                    $this->AccountPlans->save($previous_plan);
                }
            }

            $new_plan = $this->AccountPlans->newEntity();
            $new_plan = $this->AccountPlans->patchEntity($new_plan, $this->request->getData());
            $new_plan->account_id = $account_id;
            $save = $this->AccountPlans->save($new_plan);
        }
        // else update the existing
        else {
            $current_plan->internal_note = $this->request->getData('internal_note');
            $save = $this->AccountPlans->save($current_plan);
        }

        if ($save) {
            $this->Flash->success(
                'Account Plan has been updated for - ' . $account->name,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            // if save and stay
            if($this->request->getData('saveAndStay') == 1) {
                return $this->redirect(['action' => 'editUserAccount', $account_id]);
            }

            if ($this->request->getQuery('redirect')) {
                return $this->redirect($this->request->getQuery('redirect'));
            }
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(
            'Unable to update your account',
            [
                'params' => [
                    'class' => 'alert alert-danger',
                ],
            ]
        );
    }

    /**
     * Save user accounts
     */
    public function userAccountsSave($account_id)
    {
        $this->autoRender = false; 
        $this->loadModel('AccountsUsers');
        if($this->request->getData('user_id') == "" || $this->request->getData('user_id') == null) {
            $this->Flash->error(
                'Unable to add user to account - user not provided',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
            return $this->redirect(['action' => 'editUserAccount/'. $account_id]);
        }
        if(!$this->request->getData('role_id')) {
            $this->Flash->error(
                'Unable to update your account - role not provided',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        $account = $this->Accounts->get($account_id);
        $account_user = $this->AccountsUsers->find('all')->where([
            'account_id' => $account_id,
            'user_id' => $this->request->getData('user_id')
        ])->first();
        if (!$account_user) {
            $account_user = $this->AccountsUsers->newEntity();
        }
        $account_user = $this->AccountsUsers->patchEntity($account_user, $this->request->getData());
        $account_user->account_id = $account_id;
        if ($this->AccountsUsers->save($account_user)) {
            $this->Flash->success(
                'Account Plan has been updated for - ' . $account->name,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            // if save and stay
            if($this->request->getData('saveAndStay') == 1) {
                return $this->redirect(['action' => 'editUserAccount', $account_id]);
            }

            if ($this->request->getQuery('redirect')) {
                return $this->redirect($this->request->getQuery('redirect'));
            }
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(
            'Unable to update your account',
            [
                'params' => [
                    'class' => 'alert alert-danger',
                ],
            ]
        );
    }

    /** 
     * Detele account user relationship
     */
    public function deleteUser($relationship_id) {
        $this->autoRender = false;
        $this->loadModel('AccountsUsers');
        $relation_record = $this->AccountsUsers->get($relationship_id);
        if($this->AccountsUsers->delete($relation_record)) {
            $this->Flash->success(
                'User removed from account',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        }
        else{ 
            $this->Flash->error(
                'Unable to update your delete user',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        return $this->redirect(['action' => 'editUserAccount', $relation_record->account_id]);
    }

    /**
     * Soft delete account
     * @param String $account_id
     */
    public function delete($account_id)
    {
        $this->loadModel('Claims');
        $this->loadModel('Debtors');
        $this->loadModel('Users');

        // check1 if account has claims assigned to it.
        $claims = $this->Claims->find()->where(['account_id' => $account_id])->count();
        if ($claims !== 0) {
            $this->Flash->error(
                'Cannot delete, Account has ' . $claims . " claim(s) assigned to it",
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
            return $this->redirect(['action' => 'editUserAccount', $account_id]);
        }

        // check2 if account is active user account
        $users = $this->Users->find()->where(['active_account_id' => $account_id])->count();
        if ($users !== 0) {
            $this->Flash->error(
                'Cannot delete, Account is active',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
            return $this->redirect(['action' => 'editUserAccount', $account_id]);
        }
        // delete all debtors of account
        $this->Debtors->trashAll(['account_id' => $account_id]);

        //delete account
        $account = $this->Accounts->get($account_id);
        if ($this->Accounts->trash($account)) {
            $this->Flash->success(
                'Successfully deleted account '. $account->name,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        return $this->redirect(['action' => 'index']);
        }
    }  

    /**
     * Active a specific account for user
     * @param String $account_id
     * @param String $user_id
     * @return redirect
     */
    public function activeAccount($account_id, $user_id)
    {
        $this->loadModel('Users');
        $account = $this->Accounts->get($account_id);
        $user = $this->Users->get($user_id);
        $user->active_account_id = $account_id;
        if ($this->Users->save($user)) {
            $this->Flash->success(
                'Successfully activated account '. $account->name,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
            if ($this->request->getQuery('redirect')) {
                return $this->redirect($this->request->getQuery('redirect'));
            }
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Function to return all plans in array
     * @param String $plan type
     */
    private function plansArray(String $type = 'all')
    {
        $this->loadModel('Plans');
        $plansArray = [];
        if ($type == 'all') {
            $plans = $this->Plans->find('all');
        }
        else if ($type == 'paid') {
            $plans = $this->Plans->find('all')->where(['name IN' => ['Professional', 'Premium']]);
        }
        else if ($type == 'free') {
            $plans = $this->Plans->find('all')->where(['name IN' => ['Trial', 'Free subscription']]);
        }
        foreach ($plans as $plan) {
            $plansArray[$plan->id] = $plan->toArray();
        }
        return $plansArray;
    }


}
