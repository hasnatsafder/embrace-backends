<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * CompanyTypes Controller
 *
 * @property \App\Model\Table\CompanyTypesTable $CompanyTypes
 * @property \App\Model\Table\InvoiceStatusesTable $InvoiceStatuses
 */
class InvoiceStatusesController extends AppController
{
    public $modelClass = 'App.InvoiceStatuses';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['edit', 'delete']);
    }
}
