<?php
declare(strict_types=1);

namespace Admin\Controller;

use CollectorFiles\DownloadFileHandler\CaseDownloaderFactory;

/**
 * Collectors Controller
 *
 * @property \App\Model\Table\CollectorsTable $Collectors
 */
class CollectorsController extends AppController
{
    public $modelClass = 'App.Collectors';
    public $isAdmin = false;
    public $adminActions = ['index', 'view', 'add'];

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['edit', 'delete']);
    }

    public function ftp($claim_id)
    {

        $claim = $this->Collectors->Claims->find("all")->where(['Claims.id' => $claim_id])->contain(["Debtors", "Accounts"])->first();
        $collector = $this->Collectors->get($claim->collector_id);
        $status = "";
        if ($collector->identifier == "mortang") {
            $fileInfo = $this->mortang($claim);
            $status = $fileInfo[0];
            $fileContents = $fileInfo[1];
        } else {
            $fileInfo = $this->lindorff($claim);
            $status = $fileInfo[0];
            $fileContents = $fileInfo[1];
        }

        $contentArray = [];
        if ($fileContents) {
            $contentsCSV = array_map("str_getcsv", explode("\n", $fileContents));
            foreach ($contentsCSV as $csv) {
                array_push($contentArray, $csv[0]);
            }
            if (!$contentArray[sizeof($contentArray) - 1]) {
                unset($contentArray[sizeof($contentArray) - 1]);
            }
        }

        //Get claim lines of CLaim
        $this->loadModel("ClaimLines");
        $claimLines = $this->ClaimLines->find()->contain(['ClaimLineTypes', 'Currencies'])->where(['claim_id' => $claim->id]);
        $this->set(compact('contentArray', 'claim', 'claimLines', "status"));
    }

    private function mortang($claim)
    {
        $caseDownloader = CaseDownloaderFactory::build("mortang");
        $caseDownloader->DESTINATION_FILESYSTEM_DIR = '/uploads';
        $caseDownloader->DESTINATION_FILESYSTEM_NAME = 'mortang';
        $filesList = $caseDownloader->mountManager->listContents('destination://' . $caseDownloader->DESTINATION_FILESYSTEM_DIR, true);
        foreach ($filesList as $file) {
            if ($file['type'] == 'file' && strpos($file['filename'], $claim->customer_reference) !== false) {
                return ["Uploaded", $caseDownloader->mountManager->read('destination://' . $file['path'], false)];
            }
        }
        $filesList = $caseDownloader->mountManager->listContents('destination://' . $caseDownloader->DESTINATION_FILESYSTEM_DIR . "/synced", true);
        foreach ($filesList as $file) {
            if ($file['type'] == 'file' && strpos($file['filename'], $claim->customer_reference) !== false) {
                return ["Synced", $caseDownloader->mountManager->read('destination://' . $file['path'], false)];
            }
        }

        return ["", []];
    }

    public function lindorff()
    {
        return [];
    }
}
