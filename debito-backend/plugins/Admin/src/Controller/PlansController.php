<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * Plans Controller
 *
 * @property \App\Model\Table\PlansTable $Roles
 */
class PlansController extends AppController
{
    public $modelClass = 'App.Plans';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        //$this->Crud->disable(['add', 'delete']);
    }

    /**
     * Show all account plans with edit forms
     */
    public function index()
    {
      $plans = $this->Plans->find('all');
      $interval_options = ['MONTHLY' => 'Monthly', 'YEARLY' => 'Yearly'];
      $this->set(compact(['plans', 'interval_options']));
    }

    /**
     * Edit the plans
     * @param String $plan_id
     */
    public function edit(string $plan_id)
    {
      $this->autoRender = false;
      $plan = $this->Plans->get($plan_id);
      $plan->name = $this->request->getData('name');
      $plan->price = $this->request->getData('price');
      $plan->billing_interval = $this->request->getData('billing_interval');
      if ($this->Plans->save($plan)) {
        $this->Flash->success(
          'Plan ' . $this->request->getData('name') . "-" . ucfirst( strtolower( $this->request->getData('billing_interval')) ). " updated",
          [
              'params' => [
                  'class' => 'alert alert-success',
              ],
          ]
        );
      }
      else {
        $this->Flash->error(
          'Unable to update your plan',
          [
              'params' => [
                  'class' => 'alert alert-danger',
              ],
          ]
        );
      }
      return $this->redirect(['action' => 'index']);
    }
}
