<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\CountriesTable $Countries
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \App\Model\Table\DebtorsTable $Debtors
 */
class DebtorsController extends AppController
{
    public $modelClass = 'App.Debtors';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function accountdebtors($account_id)
    {
        $debtors = $this->Debtors->find('all')->where(['account_id' => $account_id])->select(['company_name', 'id', 'first_name', 'vat_number']);
        $this->RequestHandler->respondAs('json');
        $this->response->type('application/json');
        $this->autoRender = false;
        echo json_encode($debtors);
    }

    public function accounts($account_id)
    {
        $debtors = $this->Debtors->find()
            ->contain('Countries')
            ->where(["account_id" => $account_id]);

        $this->loadModel('Accounts');
        $account = $this->Accounts->get($account_id);

        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);

        $this->set(compact(['debtors', 'user', 'account']));
    }

    public function accountsAdd($account_id)
    {

        $this->loadModel('Accounts');
        $account = $this->Accounts->get($account_id);

        $debtor = $this->Debtors->newEntity();
        if ($this->request->is('post')) {
            $debtor = $this->Debtors->patchEntity($debtor, $this->request->getData());
            $debtor->created_by_id = $account->created_by_id;
            $debtor->account_id = $account->id;
            $save = $this->Debtors->save($debtor);
            if ($save) {
                $this->Flash->success(
                    'Debtor has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['controller' => 'Debtors', 'action' => 'accounts', $account_id]);
            }
            $this->Flash->error(
                'Unable to add your Debtor',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list');
        $denmarkId = $this->Countries->find('all')->where(['name' => 'Denmark'])->select('id')->first();

        $this->set(compact(['debtor', 'user', 'account', 'countries', 'denmarkId']));
    }

    public function edit($id)
    {

        $debtor = $this->Debtors->get($id);

        $this->loadModel('Accounts');
        $account = $this->Accounts->get($debtor->account_id);
        if ($this->request->is('put')) {
            $debtor = $this->Debtors->patchEntity($debtor, $this->request->getData());
            $save = $this->Debtors->save($debtor);
            if ($save) {
                $this->Flash->success(
                    'Debtor has been saved',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );
                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['controller' => 'Debtors', 'action' => 'accounts', $account->id]);
            }
            $this->Flash->error(
                'Unable to save your Debtor',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
        $this->loadModel('Users');
        $user = $this->Users->get($account->created_by_id);

        $this->loadModel('Countries');
        $countries = $this->Countries->find('list');

        $this->set(compact(['debtor', 'user', 'account', 'countries']));
    }

    public function delete($debtor_id)
    {
        $this->loadModel('Claims');
        $claims = $this->Claims->find()->where(['debtor_id' => $debtor_id])->count();
        if ($claims !== 0) {
            $this->Flash->success(
                'Cannot delete, Debtor has ' . $claims . " claim(s) assigned to it",
                [
                    'params' => [
                        'class' => 'alert alert-warning',
                    ],
                ]
            );

            return $this->redirect(['controller' => 'Debtors', 'action' => 'edit', $debtor_id]);
        }

        $debtor = $this->Debtors->get($debtor_id);
        $account_id = $debtor->account_id;
        if ($this->Debtors->trash($debtor)) {
            $this->Flash->success(
                'Debtor has been deleted',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            return $this->redirect(['controller' => 'Debtors', 'action' => 'accounts', $account_id]);
        }
    }

    /**
     * List all claims of debtors to delete
     * @param String $claim_id
     */
    public function claims($claim_id)
    {
        $this->loadModel('Claims');
        $original_claim = $this->Claims->find('all')->where(['Claims.id' => $claim_id])
                        ->contain(['ClaimPhases', 'Admin'])
                        ->first();
        $debtor = $this->Debtors->get($original_claim->debtor_id);
        $claims = $this->Claims->find('all')
                ->contain(['ClaimPhases', 'Admin'])
                ->where(['Claims.debtor_id' => $debtor->id, 'Claims.id <>' => $original_claim->id])
                ->order('ClaimPhases.order');

        $auth_user_id = $this->Auth->user('id');
        $this->set(compact(['original_claim', 'claims', 'debtor', 'auth_user_id']));
    }

    /**
     * Merge claims of debtors
     * @param String $claim_id
     */
    public function mergeClaims($claim_id)
    {
        $this->autoRender = false; 
        $this->loadModel('ClaimLines');
        $this->loadModel('Claims');
        $string_of_trashed_claims = "";
        $original_claim = $this->Claims->get($claim_id);
        foreach($this->request->getData() as $key => $data) {
            if($data == "on") {
                $claim = $this->Claims->get($key);
                $claim_lines = $this->ClaimLines->find('all')->where(['claim_id' => $claim->id]);
                foreach ($claim_lines as $claim_line) {
                    $claim_line->claim_id = $original_claim->id;
                    $this->ClaimLines->save($claim_line);
                }
                if ($this->Claims->trash($claim)) {
                    $string_of_trashed_claims = $string_of_trashed_claims . " " . $claim->customer_reference;
                }
            }
        }
        if ($string_of_trashed_claims) {
            $this->Flash->success(
                'Claims' . $string_of_trashed_claims . ' have been merged into ' . $original_claim->customer_reference,
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
        }

        return $this->redirect(['controller' => 'Claims', 'action' => 'index']);
    }
}
