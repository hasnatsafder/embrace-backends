<?php
declare(strict_types=1);

namespace DebtCollectionTools\FiPayments;

use App\Model\Entity\Claim;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;

/**
 * Class SekFileHandler
 *
 * @package DebtCollectionTools\FiPayments
 */
class SekFileHandler
{
    use LocatorAwareTrait;

    /**
     * @param $stream
     */
    public static function processPaymentsFromStream($stream)
    {
        if (!is_resource($stream)) {
            throw new \InvalidArgumentException('"$stream" is not a stream');
        }

        //Refactor this for performance when this file gets too big to keep it in memory
        $fileContent = stream_get_contents($stream);

        $lines = static::prepareFileContent($fileContent);

        if (count($lines) === 0) {
            return;
        }

        Log::debug(sprintf('Processing %d lines', count($lines)));

        foreach ($lines as $line) {
            static::processPaymentLine($line);
        }
    }

    /**
     * @param array $line
     */
    private static function processPaymentLine(array $line)
    {
        /** @var \App\Model\Table\ClaimLinesTable $claimLinesTable */
        $claimLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claim = static::getClaimFromFiReference($line['FI_PAYMENT_REFERENCE']);
        $date = FrozenDate::createFromFormat('Ymd', $line['PAYMENT_DATE']);

        $claimLineTypePayment = $claimLinesTable->ClaimLineTypes->getFromIdentifier('payment');

        $claimLine = $claimLinesTable->newEntity([
            'claim_id' => $claim->id,
            'claim_line_type_id' => $claimLineTypePayment->id,
            'invoice_date' => $date->toDateString(),
            'maturity_date' => $date->toDateString(),
            'amount' => (int)$line['AMOUNT'],
        ]);

        $claimLinesTable->saveOrFail($claimLine);
    }

    /**
     * @param string $fileContent
     * @return array
     */
    private static function prepareFileContent(string $fileContent)
    {
        $lines = explode("\n", $fileContent);
        $lines = array_map(function ($row) {
            return trim($row);
        }, $lines);
        $lines = array_filter($lines);
        $lines = array_filter($lines, function ($row) {
            return mb_substr($row, 0, 5) === 'FI030';
        });
        $lines = array_map(function ($row) {
            return [
                'SYSTEM_ID' => mb_substr($row, 0, 2),
                'RECORD_TYPE' => mb_substr($row, 2, 3),
                'PAYMENT_DATE' => mb_substr($row, 5, 8),
                'CARD_CODE' => mb_substr($row, 13, 2),
                'FI_PAYMENT_REFERENCE' => mb_substr($row, 15, 19),
                'ARCHIVE_NUMBER' => mb_substr($row, 34, 22),
                'BOOKEEPING_DATE' => mb_substr($row, 56, 8),
                'AMOUNT' => mb_substr($row, 64, 15),
                'SUM_REFERENCE' => mb_substr($row, 79, 22),
                'FEE_TYPE' => mb_substr($row, 101, 2),
                'FEE_AMOUNT' => mb_substr($row, 103, 9),
                'CHARGEBACK' => mb_substr($row, 112, 1),
            ];
        }, $lines);

        return array_values($lines);
    }

    /**
     * @param $fiPaymentReference
     * @return \App\Model\Entity\Claim
     */
    private static function getClaimFromFiReference($fiPaymentReference): Claim
    {
        /** @var \App\Model\Table\ClaimsTable $claimsTable */
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');

        return $claimsTable->find('withTrashed')
            ->where([
                'Claims.fi_payment_reference' => (int)mb_substr($fiPaymentReference, 0, -1),
                'Claims.fi_check_digit' => mb_substr($fiPaymentReference, -1),
            ])
            ->firstOrFail();
    }
}
