<html lang="en">
<head>
    <title></title>
<body>
	 <div class="row">
		<span id="heading" class="col-12">
			<span class="float-right ">
		      Debito ApS
			</span>
	  	</span>
	 </div>
	 <div class="row">
	 	<div class="col-12">
		 	<div class="mt-2 normal-font">
                <div><?= $debtor->company_name ?? ($debtor->first_name . " " . $debtor->last_name) ?></div>
                <div><?= $debtor->address ?? "" ?></div>
				<div><?= $debtor->vat_number ?? "" ?></div>
			</div>
	 	</div>
	 </div>
	 <div class="row normal-font mt-4">
	 	<span class="col-6">
	 		<span>
	 			<?= $date ?>
	 		</span>
	 	</span>
	 	<span class="col-6">
	 		<span class="float-right">
		 		<?= $invoiceClaimLine->invoice_reference; ?>
	 		</span>
	 	</span>
	 </div>
	 <div class="row mt-4">
	 	<span class="normal-font">
	 		Dette er en betalingspåmindelse for faktura <?= $invoiceClaimLine->invoice_reference; ?> sendt af <?= $account->name ?> den <?= $invoiceClaimLine->formatted_invoice_date; ?> og skulle betales den <?= $invoiceClaimLine->formatted_maturity_date; ?>. Betal beløbet ved hjælp af nedenstående oplysninger.
	 	</span>
	 </div>
 	 <div class="row normal-font mt-3">
         <?php foreach ($claimLines as $claimLine): ?>
	 	<span class="col-6">
	 		<span>
	 			<?= $claimLine->ClaimLineTypes['name']; ?>
	 		</span>
	 	</span>
	 	<span class="col-6">
	 		<span class="float-right">
		 		<?= $claimLine->formatted_amount; ?>
	 		</span>
	 	</span>
         <br>
         <?php endforeach; ?>
	 </div>
  	 <div class="row normal-font mt-2">
	 	<span class="col-8">
	 	</span>
	 	<span class="col-4 sub-heading">
	 		<span>
	 			Total <?= $invoiceClaimLine->currency['iso_code'] ?>
	 		</span>
	 		<span class="float-right">
		 		<?= $total; ?>
	 		</span>
	 	</span>
	 </div>
  	 <div class="row normal-font mt-3">
	 	<span class="col-12 sub-heading">
	 		<span>
	 			Beløbet indbetales på:
	 		</span>
			<br>
	 		<?= $fi_code; ?>
	 	</span>
	 </div>
 	 <div class="row mt-4">
	 	<span class="normal-font">
	 		Denne betaling skal sendes indenfor 10 dage efter modtagelse af denne betalingspåmindelse og senest <?= $next_warning_date ?>. Manglende betaling af denne påmindelse vil medføre yderligere omkostninger i form af gebyrer og renter.
	 	</span>
	 </div>
	 <footer class="mt-30">
	 	<a target="_blank" href="https://app.debito.dk" class="footer-link">Debito ApS</a> / Skelagervej 15 / 9000 Aalborg
		<br>
		CVR-nr. DK36965762 Mail: kommunikation@debito.dk
	 </footer>
</body>
</html>
<style>
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i');
</style>
<style>
	.row {
		display: inline-flex;
		width: 100%;
	}
	.float-right {
		float: right;
	}
	body {
        font-family: "Arial", serif;
    	font-style: normal;
		padding: 72pt 72pt 72pt 72pt;
	}
	#heading {
		font-size: 14pt;
		font-weight: 700;
	}
  .sub-heading {
    font-weight: 700;
    font-size: 11pt;
  	}
	.normal-font {
	  font-size: 11pt;
	}
	footer {
	  background-color: #ffffff;
	  font-size: 8.5pt;
      font-family: "Roboto", serif;
	  color: #444444;
	  text-align: center;
	  font-weight: 400;
	  line-height: 1.15;
	}
	.footer-link {
		font-weight: 700;
		text-decoration: none;
		color: #000;
	}

	@page {
	  size: A4;
	  margin: 11mm 17mm 17mm 17mm;
	}

	@media print {
	  footer {
	    position: fixed;
	    bottom: 0;
	  }

	  .content-block, p {
	    page-break-inside: avoid;
	  }

	  html, body {
	    width: 210mm;
	    height: 297mm;
	  }
	}
	.col-3 {
		width: 25%;
	}
	.col-4 {
		width: 33.3%;
	}
	.col-6 {
		width: 50%;
	}
	.col-8 {
		width: 66.6%;
	}
	.col-12 {
		width: 100%;
	}
	.mt-2 {
		margin-top: 1rem;
	}
	.mt-3 {
		margin-top: 1.5rem;
	}
	.mt-4 {
		margin-top: 2rem;
	}
	.mt-5 {
		margin-top: 2.5rem;
	}
	.mt-6 {
		margin-top: 3rem;
	}
	.mt-7 {
		margin-top: 3.5rem;
	}
	.mt-8 {
		margin-top: 4rem;
	}
	.mt-9 {
		margin-top: 4.5rem;
	}
	.mt-10 {
		margin-top: 5rem;
	}
    .mt-20 {
        margin-top: 10rem;
    }
    .mt-30 {
        margin-top: 10rem;
    }
</style>
