<?php
declare(strict_types=1);

namespace DebtCollectionTools\ClaimActionTypeHandlers;

use App\Model\Entity\ClaimLine;
use App\Utility\HelperFunctions;
use Cake\Chronos\ChronosInterface;
use Cake\Chronos\Date;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;
use Cake\Utility\Text;
use CakePdf\Pdf\CakePdf;
use League\Flysystem\FileNotFoundException;
use Psr\Log\LogLevel;
use WyriHaximus\FlyPie\FilesystemsTrait;

class SendSoftWarningAdapter extends BaseAdapter
{
    use FilesystemsTrait;
    /**
     * @todo internal debt collection Implement soft warning handler - get inspiration from SendWarningAbstractAdapter
     */
    public function _handle(): void
    {
        $warningClaimLine = $this->addWarningFeeClaimLine(1);
        $this->sendEmailToDebtor($warningClaimLine);
        $this->createNextClaimActionEntity(1);
        //TODO Create PDF file

        //TODO Save as ClaimLine

        //TODO Initiate soft-warning email to Debtor
        //TODO  - Initiate soft warning text if debtor has mobile phone

        //TODO Set freeze period for claim according to DunningConfiguration
        //TODO Plan the next action (start_reminder_phase)
    }

    /**
     * @param int $warningNumber
     * @return \App\Model\Entity\ClaimLine
     */
    protected function addWarningFeeClaimLine(int $warningNumber = 1): ClaimLine
    {
        $claimLinesTable = $this->getTableLocator()->get('ClaimLines');
        $claimLineType = $claimLinesTable->ClaimLineTypes->getFromIdentifier('soft_warning');

        $claimLine = $claimLinesTable->newEntity([
            'claim_id' => $this->getClaim()->id,
            'claim_line_type_id' => $claimLineType->id,
            'currency_id' => $this->getClaim()->currency_id,
            'invoice_id' => null,
            'invoice_date' => $this->getClaimAction()->date->toDateString(),
            'maturity_date' => $this->getNextClaimActivityDate($warningNumber, false)->toDateString(),
            'amount' => 0,
            'invoice_reference' => sprintf('%s-%s', $this->getOriginalInvoiceNumber($this->getClaimLine()), "soft-warning"),
            'file_name' => $this->generateWarningPdf(),
        ], [
            'validate' => 'WithCustomFile',
        ]);

        $claimLinesTable->saveOrFail($claimLine);

        return $claimLine;
    }

    /**
     * @param int $warningNumber
     * @return array
     */
    protected function generateWarningPdf(): array
    {
        $fileName = sprintf('%s.pdf', Text::uuid());
        $fullPath = Configure::read('WyriHaximus.FlyPie.sys_temp.vars.path') . $fileName;
        $cakePdf = new CakePdf();

        $this->log('Generating PDF file for warning', LogLevel::DEBUG);

        $claimLines = $this->getAllClaimLines();
        $invoiceClaimLine = [];
        $currency = "";
        $total = 0;
        foreach ($claimLines as $claimLine) {
            $claimLine->formatted_amount = HelperFunctions::converToCurrency($claimLine->amount, $claimLine->currency['iso_code']);
            $claimLine->formatted_invoice_date = new FrozenDate($claimLine->invoice_date);
            $claimLine->formatted_invoice_date = $claimLine->formatted_invoice_date->format('d-m-y');
            $claimLine->formatted_maturity_date = new FrozenDate($claimLine->maturity_date);
            $claimLine->formatted_maturity_date = $claimLine->formatted_maturity_date->format('d-m-y');
            $total = $total + $claimLine->amount;
            if ($claimLine->ClaimLineTypes['identifier'] == "invoice") {
                $invoiceClaimLine = $claimLine;
            }
        }
        $total = HelperFunctions::converToCurrency($total, $invoiceClaimLine->currency['iso_code']);

        $cakePdf
            ->template('DebtCollectionTools.warning', 'default')
            ->viewVars([
                'claim' => $this->getClaim(),
                'debtor' => $this->getDebtor(),
                'date' => FrozenDate::today()->format('d-m-y'),
                'claimLines' => $claimLines,
                'account' => $this->getAccount(),
                'next_warning_date' => $this->getNextClaimActivityDate(1)->format('d-m-y'),
                'invoiceClaimLine' => $invoiceClaimLine,
                'total' => $total,
                'fi_code' => $this->getClaim()->getOrGeneratePaymentFiCode()
            ]);

        if (!$cakePdf->write($fullPath)) {
            throw new \RuntimeException('Could not save PDF file for warning');
        }

        $this->log(sprintf('Created file "%s"', $fullPath), LogLevel::DEBUG);

        try {
            $data = [
                'name' => sprintf('%s-%s.pdf', $this->getOriginalInvoiceNumber($this->getClaimLine()), "soft-warning"),
                'type' => 'application/pdf',
                'tmp_name' => $fullPath,
                'error' => UPLOAD_ERR_OK,
                'size' => $this->filesystem('sys_temp')->getSize($fileName),
            ];
        } catch (FileNotFoundException $e) {
            $this->log("PDF file not found");
        }

        $this->onEvent(self::EVENT_AFTER_HANDLE, function () use ($fileName) {
            $this->filesystem('sys_temp')->delete($fileName);
        });

        return $data;
    }

    /**
     * @param \App\Model\Entity\ClaimLine $warningClaimLine
     * @throws \League\Flysystem\FileExistsException
     * @throws \League\Flysystem\FileNotFoundException
     */
    protected function sendEmailToDebtor(ClaimLine $warningClaimLine)
    {
        $localfs = $this->filesystem('sys_temp');
        $genericsFs = $this->filesystem('generic');
        $localFileName = Text::uuid();
        $localFullPath = Configure::read('WyriHaximus.FlyPie.sys_temp.vars.path') . $localFileName;

        $warningFile = $genericsFs->readStream($warningClaimLine->file_dir . $warningClaimLine->file_name);

        if (!$localfs->writeStream($localFileName, $warningFile)) {
            throw new \RuntimeException('Could not copy warning PDF to local tmp file system');
        }

        $this->log(
            'Copied warning from remote filesystem to local tmp file',
            LogLevel::DEBUG,
            ['fileName' => $localFileName]
        );

        $this->getMailer('DebtCollectionTools.Dunning')->send(
            'softWarning',
            [
                $this->getDebtor(),
                $this->getClaim(),
                $this->getClaimAction(),
                $this->getAccount(),
                $localFullPath,
            ]
        );

        $this->log(
            'Sent dunning warning with email',
            LogLevel::DEBUG,
            ['attachmentFile' => $localFullPath]
        );

        $this->onEvent(self::EVENT_AFTER_HANDLE, function () use ($localFileName) {
            $this->filesystem('sys_temp')->delete($localFileName);
        });
    }

    /**
     * @param int $warningNumber
     * @todo internal debt collection Find a way to distinguish whether the Debtor's phone number is actually valid for text messages or not
     */
    protected function createNextClaimActionEntity(int $warningNumber): void
    {
        $debtor = $this->getDebtor();
        $debtorHasPhone = !empty($debtor->phone);

        // $nextClaimActionIdentifier = $debtorHasPhone
        //     ? 'send_warning_notification_text'
        //     : $this->getNextWarningActionTypeIdentifier($warningNumber);
        $nextClaimActionIdentifier = "start_reminder_phase";

        // $nextClaimActionDate = $debtorHasPhone
        //     ? FrozenDate::now()
        //     : $this->getNextClaimActivityDate($warningNumber, true);
        $nextClaimActionDate = $this->getNextClaimActivityDate($warningNumber, true);

        $this->log(
            sprintf('Planning next claim action: "%s', $nextClaimActionIdentifier),
            LogLevel::DEBUG
        );

        $this->createClaimActionEntity([
            'user_id' => $this->getClaimAction()->user_id,
            'claim_id' => $this->getClaimAction()->claim_id,
            'claim_line_id' => $this->getClaimAction()->claim_line_id,
            'claim_action_type_id' => $this->getClaimActionTypeIdFromIdentifier($nextClaimActionIdentifier),
            'date' => $nextClaimActionDate->toDateString(),
            'done' => null,
        ]);
    }

    /**
     * @param \App\Model\Entity\ClaimLine|null $claimLine
     * @return string|null
     */
    protected function getOriginalInvoiceNumber(?ClaimLine $claimLine): ?string
    {
        if ($claimLine !== null) {
            return $claimLine->invoice_reference;
        }

        return null;
    }

    /**
     * @param int $warningNumber
     * @param bool $onlyWeekDays
     * @return \Cake\I18n\FrozenDate
     */
    protected function getNextClaimActivityDate(int $warningNumber, bool $onlyWeekDays = false): FrozenDate
    {
        $maturityDate = $this->getClaimAction()->date
            ->addDays($this->getFreezeDaysAfterWarning($warningNumber));

        if ($onlyWeekDays && $maturityDate->isWeekend()) {
            $maturityDate = $maturityDate->next(ChronosInterface::MONDAY);
        }
        return $maturityDate;
    }

    /**
     * @param int $warningNumber
     * @return int
     */
    protected function getFreezeDaysAfterWarning(int $warningNumber = 1): int
    {
        $dunningConfiguration = $this->getDunningConfigurationForClaim($this->getClaim());

        switch ($warningNumber) {
            case 1:
                return $dunningConfiguration->warning_one_freeze;
            case 2:
                return $dunningConfiguration->warning_two_freeze;
            case 3:
                return $dunningConfiguration->warning_three_freeze;
        }

        throw new \RuntimeException(sprintf('Warning number %s is not supported', $warningNumber));
    }
}

