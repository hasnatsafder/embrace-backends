<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

/**
 * Class SendWarningThreeAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class SendWarningThreeAdapter extends SendWarningAbstractAdapter
{
    protected $warningNumber = 3;
}

