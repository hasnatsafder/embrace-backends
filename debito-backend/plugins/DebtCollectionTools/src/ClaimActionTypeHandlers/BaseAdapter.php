<?php
declare(strict_types=1);

namespace DebtCollectionTools\ClaimActionTypeHandlers;

use App\Model\Entity\Account;
use App\Model\Entity\Claim;
use App\Model\Entity\ClaimAction;
use App\Model\Entity\ClaimLine;
use App\Model\Entity\Debtor;
use App\Model\Entity\DunningConfiguration;
use Cake\Datasource\ModelAwareTrait;
use Cake\Event\EventDispatcherTrait;
use Cake\I18n\FrozenTime;
use Cake\Log\LogTrait;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Psr\Log\LogLevel;
use RuntimeException;

/**
 * Class BaseAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
abstract class BaseAdapter implements ClaimActionTypeHandlerInterface
{
    use ModelAwareTrait;
    use LocatorAwareTrait;
    use LogTrait;
    use MailerAwareTrait;
    use EventDispatcherTrait;

    public const EVENT_BEFORE_HANDLE = 'beforeHandle';
    public const EVENT_AFTER_HANDLE = 'afterHandle';
    public const EVENT_AFTER_PROCESS = 'afterProcess';

    /**
     * @var null|string
     */
    public $modelClass = null;

    /**
     * @var ClaimAction|null
     */
    private $claimAction;

    /**
     * @var Claim|null
     */
    private $claim;

    /**
     * @var ClaimLine|null
     */
    private $claimLine;

    /**
     * @var \App\Model\Entity\Debtor|null
     */
    private $debtor;

    /**
     * @var \App\Model\Entity\Account|null
     */
    private $account;

    /**
     * BaseAdapter constructor.
     *
     * @param \App\Model\Entity\ClaimAction $claimAction
     */
    public function __construct(ClaimAction $claimAction)
    {
        $this->claimAction = $claimAction;

        if ($this->modelClass !== null) {
            $this->loadModel();
        }
    }

    /**
     * Main method for the adapter that will be executed.
     */
    public function process(): void
    {
        $this->triggerEvent(self::EVENT_BEFORE_HANDLE);
        $this->_handle();
        $this->triggerEvent(self::EVENT_AFTER_HANDLE);

        $this->setClaimActionDone();
        $this->triggerEvent(self::EVENT_AFTER_PROCESS);
    }

    public abstract function _handle(): void;

    public function setClaimActionDone(): void
    {
        $claimAction = $this->getClaimAction();

        if ($claimAction->done !== null) {
            return;
        }

        $claimAction->done = new FrozenTime();

        $this->getTableLocator()->get('ClaimActions')->saveOrFail($claimAction);

        $this->setClaimAction($claimAction);
    }

    /**
     * @return \App\Model\Entity\ClaimAction|null
     */
    public function getClaimAction()
    {
        return $this->claimAction;
    }

    /**
     * @param \App\Model\Entity\ClaimAction|null $claimAction
     * @return BaseAdapter
     */
    public function setClaimAction(ClaimAction $claimAction): BaseAdapter
    {
        $this->claimAction = $claimAction;

        return $this;
    }

    /**
     * @return \App\Model\Entity\Account
     */
    public function getAccount(): Account
    {
        if (isset($this->account)) {
            return $this->account;
        }

        return $this->account = $this->getTableLocator()->get('Accounts')->get(
            $this->getClaim()->account_id,
            ['finder' => 'withTrashed']
        );

    }

    /**
     * @return \App\Model\Entity\Claim
     */
    public function getClaim(): Claim
    {
        if (isset($this->claim)) {
            return $this->claim;
        }

        return $this->claim = $this->getTableLocator()->get('Claims')->get(
            $this->getClaimAction()->claim_id,
            ['finder' => 'withTrashed']
        );
    }

    /**
     * @return \App\Model\Entity\Debtor
     */
    public function getDebtor(): Debtor
    {
        if (isset($this->debtor)) {
            return $this->debtor;
        }

        return $this->debtor = $this->getTableLocator()->get('Debtors')->get(
            $this->getClaim()->debtor_id,
            ['finder' => 'withTrashed']
        );
    }

    /**
     * @return \App\Model\Entity\ClaimLine|null
     */
    public function getClaimLine(): ?ClaimLine
    {
        if (isset($this->claimLine)) {
            return $this->claimLine;
        }

        $claimLineId = $this->getClaimAction()->claim_line_id;

        if ($claimLineId === null) {
            return null;
        }

        return $this->claimLine = $this->getTableLocator()->get('ClaimLines')->get(
            $claimLineId,
            ['finder' => 'withTrashed']
        );
    }

    /**
     * @return \App\Model\Entity\ClaimLine|null
     */
    public function setClaimLine($claimLine): ?ClaimLine
    {
        $this->claimLine = $claimLine;
        return $this->claimLine = $this->getTableLocator()->get('ClaimLines')
        ->saveOrFail($claimLine);
    }

    /**
     * @param \App\Model\Entity\Claim|null $claim
     * @return BaseAdapter
     */
    public function setClaim(Claim $claim): BaseAdapter
    {
        $this->claim = $claim;

        return $this;
    }

    /**
     * @return \App\Model\Entity\ClaimLine|null
     */
    public function getAllClaimLines() {
        if ($this->getClaim()) {
            return $this->getTableLocator()->get('ClaimLines')
                ->find('all')->where(['claim_id' => $this->getClaim()->id])
                ->orderAsc('ClaimLines.created')
                ->contain(['claimLineTypes', 'Currencies']);
        }
        return NULL;
    }

    /**
     * @param \Cake\I18n\FrozenTime $frozenUntil
     * @return \DebtCollectionTools\ClaimActionTypeHandlers\BaseAdapter
     */
    protected function setFreezePeriodForClaim(FrozenTime $frozenUntil): BaseAdapter
    {
        $claimsTable = $this->getTableLocator()->get('Claims');
        $claim = $this->getClaim();

        $claim->frozen_until = $frozenUntil;

        $claimsTable->saveOrFail($claim);

        $this->setClaim($claim);

        return $this;
    }

    /**
     * Fetch the DunningConfiguration for the claim
     *
     * @param \App\Model\Entity\Claim $claim
     * @return DunningConfiguration
     */
    protected function getDunningConfigurationForClaim(Claim $claim): DunningConfiguration
    {
        return $this->getTableLocator()->get('DunningConfigurations')->get(
            $claim->dunning_configuration_id,
            ['finder' => 'withTrashed']
        );
    }

    /**
     * @param string $nextClaimActionIdentifier
     * @return string
     */
    protected function getClaimActionTypeIdFromIdentifier(string $nextClaimActionIdentifier): string
    {
        $claimActionTypesTable = $this->getTableLocator()->get('ClaimActionTypes');

        $claimActionType = $claimActionTypesTable->find()
            ->select(['ClaimActionTypes.id'])
            ->enableHydration(false)
            ->where(['ClaimActionTypes.identifier' => $nextClaimActionIdentifier])
            ->matching('Collectors', function (Query $q) {
                return $q->where(['Collectors.identifier' => 'debito']);
            })
            ->first();

        return $claimActionType['id'];
    }

    /**
     * @param array $entityData
     */
    protected function createClaimActionEntity(array $entityData): void
    {
        $claimActionsTable = $this->getTableLocator()->get('ClaimActions');

        $this->log('Creating new claim action', LogLevel::DEBUG);

        $claimAction = $claimActionsTable->newEntity($entityData);

        $claimActionsTable->saveOrFail($claimAction);

        $this->log('New claim action was created successfully', LogLevel::DEBUG);
    }

    /**
     * @param $name
     * @return string Prefixed string
     */
    protected function getEventName($name): string
    {
        return sprintf('ClaimActionTypeHandlers.%s', $name);
    }

    /**
     * @param string $eventName
     */
    protected function triggerEvent(string $eventName)
    {
        $this->getEventManager()->dispatch($this->getEventName($eventName));
    }

    /**
     * @param string $eventName
     * @param callable $function
     */
    protected function onEvent(string $eventName, callable $function)
    {
        $this->getEventManager()->on($this->getEventName($eventName), $function);
    }
}
