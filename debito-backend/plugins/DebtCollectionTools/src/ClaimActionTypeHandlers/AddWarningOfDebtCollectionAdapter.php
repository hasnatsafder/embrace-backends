<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

use App\Model\Entity\ClaimLine;
use Cake\Core\Configure;
use Cake\I18n\FrozenDate;
use Psr\Log\LogLevel;
use Cake\I18n\FrozenTime;

class AddWarningOfDebtCollectionAdapter extends BaseAdapter
{
    public function _handle(): void
    {
        $this->addClaimLine();
        $this->createNextClaimActionEntity();
    }

    /**
     * @return \App\Model\Entity\ClaimLine
     */
    protected function addClaimLine(): ClaimLine
    {
        $claimLinesTable = $this->getTableLocator()->get('ClaimLines');
        $claimLineType = $claimLinesTable->ClaimLineTypes->getFromIdentifier('compensation_fee');

        $claimLine = $claimLinesTable->newEntity([
            'claim_id' => $this->getClaim()->id,
            'claim_line_type_id' => $claimLineType->id,
            'currency_id' => $this->getClaim()->currency_id,
            'invoice_id' => null,
            'invoice_date' => $this->getClaimAction()->date->toDateString(),
            'maturity_date' => null,
            'amount' => (int)($this->getWarningFee() * 100),
        ]);

        $claimLinesTable->saveOrFail($claimLine);

        return $claimLine;
    }

    /**
     *
     */
    protected function createNextClaimActionEntity(): void
    {
        $nextClaimActionIdentifier = $this->getNextClaimActionType();

        $this->log(
            sprintf('Planning next claim action: "%s', $nextClaimActionIdentifier . " " . FrozenDate::now()->toDateString()),
            LogLevel::DEBUG
        );

        $date = new FrozenTime();
        if ($nextClaimActionIdentifier == "send_warning_one" && $this->getClaimLine()->maturity_date->wasWithinLast('2 days')) {
            $date = $this->getClaimLine()->maturity_date->addDays(2);
        }

        $this->createClaimActionEntity([
            'user_id' => $this->getClaimAction()->user_id,
            'claim_id' => $this->getClaimAction()->claim_id,
            'claim_line_id' => $this->getClaimAction()->claim_line_id,
            'claim_action_type_id' => $this->getClaimActionTypeIdFromIdentifier($nextClaimActionIdentifier),
            'date' => $date,
            'done' => null,
        ]);
    }

    /**
     * @return int
     */
    protected function getWarningFee()
    {
        return (int)Configure::read('DebtCollection.DunningFees.DK.CompensationFee');
    }

    /**
     * @return string
     */
    protected function getNextClaimActionType(): string
    {
        if ($this->getClaim()->hasClaimActionOfType('send_warning_two')) {
            return 'send_warning_three';
        }

        if ($this->getClaim()->hasClaimActionOfType('send_warning_one')) {
            return 'send_warning_two';
        }

        return 'send_warning_one';
    }
}

