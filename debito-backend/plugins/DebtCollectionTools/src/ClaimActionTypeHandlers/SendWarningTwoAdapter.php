<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

/**
 * Class SendWarningTwoAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class SendWarningTwoAdapter extends SendWarningAbstractAdapter
{
    protected $warningNumber = 2;
}

