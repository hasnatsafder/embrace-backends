<?php
declare(strict_types=1);

namespace DebtCollectionTools\Claims;

use App\Model\Entity\Claim;
use App\Model\Entity\ClaimLine;
use App\Model\Entity\ClaimTransactionType;
use Cake\Collection\CollectionInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Class InterestCalculator
 *
 * @package DebtCollectionTools
 */
class InterestCalculator
{
    use LocatorAwareTrait;

    private $claim;

    /**
     * @var CollectionInterface|\App\Model\Entity\ClaimLine[]
     */
    private $claim_lines;

    /**
     * InterestCalculator constructor.
     *
     * @param \App\Model\Entity\Claim $claim
     */
    public function __construct(Claim $claim)
    {
        $this->claim = $claim;
    }

    public function addAllMissingInterest()
    {
        //Is the Claim Frozen?
        if ($this->getClaim()->frozen_until !== null && $this->getClaim()->frozen_until->gte(FrozenTime::now())) {
            return;
        }

        $this->getClaimLinesForInterestCalculation()
            ->each(function (ClaimLine $claimLine) {
                $this->calculateInterestForClaimLine($claimLine);
            });
    }

    /**
     * @return \App\Model\Entity\Claim
     */
    public function getClaim(): Claim
    {
        return $this->claim;
    }

    /**
     * @return \Cake\Collection\CollectionInterface
     */
    protected function getClaimLinesForInterestCalculation(): CollectionInterface
    {
        if (isset($this->claim_lines)) {
            return $this->claim_lines;
        }

        return $this->claim_lines = $this->getTableLocator()->get('ClaimLines')->find()
            ->find('AddingInterest')
            ->where(['ClaimLines.claim_id' => $this->getClaim()->id])
            ->all();
    }

    /**
     * @param \App\Model\Entity\ClaimLine $claimLine
     */
    protected function calculateInterestForClaimLine(ClaimLine $claimLine)
    {
        /** @var \App\Model\Table\ClaimTransactionsTable $claimTransactionsTable */
        $claimTransactionsTable = $this->getTableLocator()->get('ClaimTransactions');

        $firstDayWithInterest = (new FrozenTime($claimLine->maturity_date))->addDay()->startOfDay();
        $now = FrozenTime::now()->endOfDay();

        if ($firstDayWithInterest->gte($now)) {
            //Not due yet...
            return;
        }

        $currentDate = $firstDayWithInterest;
        /** @var ClaimTransactionType $interestClaimTransactionType */
        $interestClaimTransactionType = $claimTransactionsTable->ClaimTransactionTypes->getFromIdentifier('interest_8_05');

        while (!$currentDate->gte($now)) {
            $factor = 1;
            $dayOfMonth = (int)$currentDate->format('j');

            if ($currentDate->daysInMonth !== 30) {
                //Oh special cases...
                //There is exactly 30 days in a debt collection month - add up for that
                if ($dayOfMonth === 30) {
                    $currentDate = $currentDate->addDay()->startOfDay();
                    continue;
                }


                if ((int)$currentDate->format('n') === 2 && $dayOfMonth >= 28) {
                    if ($dayOfMonth === 28 && !$currentDate->isLeapYear()) {
                        $factor = 3;
                    }

                    if ($dayOfMonth === 29 && $currentDate->isLeapYear()) {
                        $factor = 2;
                    }
                }
            }

            $amount = round($claimLine->dailyInterestAmount($currentDate) * $factor, 4);

            $this->updateClaimTransaction(
                $interestClaimTransactionType,
                $claimLine,
                $currentDate,
                $amount
            );

            $currentDate = $currentDate->addDay()->startOfDay();
        }
    }

    /**
     * @param \App\Model\Entity\ClaimTransactionType $interestClaimTransactionType
     * @param \App\Model\Entity\ClaimLine $claimLine
     * @param \Cake\I18n\FrozenTime $currentDate
     * @param float $amount
     * @return \App\Model\Entity\ClaimTransaction|\Cake\Datasource\EntityInterface
     */
    private function updateClaimTransaction(ClaimTransactionType $interestClaimTransactionType, ClaimLine $claimLine, FrozenTime $currentDate, float $amount)
    {
        $claimTransactionsTable = $this->getTableLocator()->get('ClaimTransactions');

        $where = [
            'claim_transaction_type_id' => $interestClaimTransactionType->id,
            'claim_id' => $claimLine->claim_id,
            'claim_line_id' => $claimLine->id,
            'timestamp' => $currentDate,
        ];

        /** @var \App\Model\Entity\ClaimTransaction $existingClaimTransaction */
        $existingClaimTransaction = $claimTransactionsTable->find()
            ->where($where)
            ->first();

        if ($existingClaimTransaction === null) {
            $existingClaimTransaction = $claimTransactionsTable->newEntity(
                $where + ['amount' => $amount]
            );

            $claimTransactionsTable->saveOrFail($existingClaimTransaction);
        }

         if (abs($existingClaimTransaction->amount - $amount) < 0.0001) {
            //They are the same!
            return $existingClaimTransaction;
        }

        $existingClaimTransaction = $claimTransactionsTable->patchEntity($existingClaimTransaction, [
            'amount' => $amount,
        ]);

        $claimTransactionsTable->saveOrFail($existingClaimTransaction);

        return $existingClaimTransaction;
    }
}