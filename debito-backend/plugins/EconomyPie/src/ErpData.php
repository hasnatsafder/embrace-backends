<?php
declare(strict_types=1);

namespace EconomyPie;

use InvalidArgumentException;

class ErpData
{
    public $_erp_integration;
    public $id;

    /**
     * Construct a new InvoiceData object with the keys supported
     *
     * @param array $properties
     */
    public function __construct(array $properties = [])
    {
        if (!empty($properties)) {
            foreach ($properties as $key => $value) {
                if (property_exists($this, $key)) {
                    if (is_int($value)) {
                        $value = (string)$value;
                    }
                    $this->$key = $value;
                    continue;
                }

                throw new InvalidArgumentException(
                    sprintf('Key: "%s" is not supported for this class', $key)
                );
            }
        }
    }
}
