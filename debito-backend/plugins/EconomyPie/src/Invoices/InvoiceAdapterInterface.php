<?php
declare(strict_types=1);

namespace EconomyPie\Invoices;

interface InvoiceAdapterInterface
{

//    /**
//     * Get a list of unpaid invoices
//     *
//     * @return \EconomyPie\Invoices\InvoiceData[]
//     */
//    public function getAllUnpaid(): array;
//
//    /**
//     * @return \EconomyPie\Invoices\InvoiceData[]
//     */
//    public function getAllPaid(): array;

    /**
     *
     * Get all invoices - default to only show booked invoices
     *
     * @param bool $bookedOnly
     * @return \EconomyPie\Invoices\InvoiceData[]
     */
    public function getAll(array $filter, $date): array;

    /**
     * @param string $contactId
     * @return \EconomyPie\Invoices\InvoiceData
     */
    public function get(string $contactId): InvoiceData;

    /**
     * Get the PDF resource of a specified invoice
     *
     * @param string $invoiceId The invoice's ID
     * @return resource A PDF resource
     */
    public function getPdf(string $invoiceId);

    /**
     * @param integer $erp_id
     * return integer count of total booked invoices
     */

    public function getCount(): array;
    
}
