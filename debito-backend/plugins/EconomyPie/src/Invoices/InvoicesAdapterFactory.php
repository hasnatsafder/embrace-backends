<?php
declare(strict_types=1);

namespace EconomyPie\Invoices;

use EconomyPie\AdapterFactoryInterface;
use EconomyPie\Debtors\DebtorAdapterInterface;
use EconomyPie\FactoryTrait;

class InvoicesAdapterFactory implements AdapterFactoryInterface
{
    use FactoryTrait;

    public function build($erpName, $data): InvoiceAdapterInterface
    {
        return $this->genericBuilder($erpName, $data, __NAMESPACE__);
    }
}
