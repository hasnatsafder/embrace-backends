<?php
declare(strict_types=1);

namespace EconomyPie\Invoices;

use EconomyPie\ErpData;

class InvoiceData extends ErpData
{
    public $invoiceNumber;
    public $issueDate;
    public $dueDate;
    public $currency;
    public $netAmount;
    public $grossAmount;
    public $vatAmount;
    public $debtorNumber;
    public $pdfLink;
    public $paid;

    public function __construct(array $properties = [])
    {
        parent::__construct($properties);

        if (isset($this->grossAmount) && !isset($this->netAmount)) {
            $this->netAmount = $this->grossAmount + $this->vatAmount;
        }

        if (isset($this->netAmount) && !isset($this->grossAmount)) {
            $this->grossAmount = $this->netAmount - $this->vatAmount;
        }
    }
}
