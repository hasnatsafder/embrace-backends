<?php
declare(strict_types=1);

namespace EconomyPie\Invoices\Adapter;

use Cake\Http\Exception\UnauthorizedException;
use EconomyPie;
use EconomyPie\Exceptions\Exception;
use EconomyPie\Invoices\AbstractAdapter;
use EconomyPie\Invoices\InvoiceData;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\StreamWrapper;
use Psr\Log\LogLevel;
use RuntimeException;

/**
 * Class EconomicAdapter
 *
 * @property \EconomyPie\Client\Adapter\EconomicAdapter $erpClient
 */
class EconomicAdapter extends AbstractAdapter
{
    public const VENDOR = 'economic';
    private const BASE_ENDPOINT = '/invoices';

    protected $propertyMap = [
        'id' => 'bookedInvoiceNumber',
        'invoiceNumber' => 'bookedInvoiceNumber',
        'dueDate' => 'dueDate',
        'issueDate' => 'date',
        'currency' => 'currency',
        'netAmount' => 'netAmount',
        'grossAmount' => 'grossAmount',
        'vatAmount' => 'vatAmount',
        'debtorNumber' => 'customer.customerNumber',
        'pdfLink' => 'pdf.download',
        'paid' => 'paid'
    ];

    public function get(string $invoiceId): InvoiceData
    {
        try {
            $response = $this->erpClient->getClient()->get(static::BASE_ENDPOINT . '/booked/' . $invoiceId);
            $invoice = json_decode($response->getBody()->getContents(), true);

            return $this->_transformToInvoiceDataObject($invoice);
        } catch (ClientException $e) {
            throw new UnauthorizedException($e->getMessage());
        }
    }

    /**
     *
     * @param $invoiceUrl
     * @return resource
     */
    public function getPDF(string $invoiceUrl)
    {
        $response = $this->erpClient->getClient()->get($invoiceUrl)->getBody();

        return StreamWrapper::getResource($response);
    }

    /**
     *
     * Get all invoices - default to only show booked invoices
     *
     * @param bool $bookedOnly
     * @return \EconomyPie\Invoices\InvoiceData[]
     */
    public function getAll(array $filter = [], $date): array
    {
        $url = static::BASE_ENDPOINT;

        if (isset($filter['unpaid']) && $filter['unpaid'] === true) {
            $url .= '/unpaid';
        }
        if (isset($filter['paid']) && $filter['paid'] === true) {
            $url .= '/paid';
        }

        $finished = false;
        $invoices = [];
        $skipping = 0;
        while (!$finished) {
            try {
                $response = $this->erpClient->getClient()->get($url, [
                    'query' => ['pagesize' => 1000, 'skippages' => $skipping, 'filter' => 'date$gt:' . $date],
                ]);
                $body = json_decode($response->getBody()->getContents(), true);
            } catch (ClientException $e) {
                $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);
    
                $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);
    
                return ['error' => true];
            }
            if (count($body['collection'] ) === 0) {
                $finished = true;
                continue;
            }
            array_push($invoices, ...$body['collection']);
            $skipping ++;
        }
        return $this->_transformListOfInvoices($invoices);
    }

    /**
     * @param integer $erp_id
     * return integer count of total booked invoices
     */

    public function getCount(): array
    {
        $url = static::BASE_ENDPOINT . '/totals/booked/';

        try {
            $response = $this->erpClient->getClient()->get($url, []);
            $body = json_decode($response->getBody()->getContents(), true);

            return ['error' => false, 'total' => $body['invoiceCount']];
        } catch (ClientException $e) {
            $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);

            $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);

            return ['error' => true];
        }
    }
}
