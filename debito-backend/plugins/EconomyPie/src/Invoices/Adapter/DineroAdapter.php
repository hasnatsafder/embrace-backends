<?php
declare(strict_types=1);

namespace EconomyPie\Invoices\Adapter;

use EconomyPie\Invoices\AbstractAdapter;
use EconomyPie\Invoices\InvoiceData;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\StreamWrapper;
use Psr\Log\LogLevel;
use RuntimeException;
use Cake\Log\Log;

/**
 * Class DineroAdapter
 *
 *
 * @var \EconomyPie\Client\Adapter\DineroAdapter
 *
 */
class DineroAdapter extends AbstractAdapter
{
    public const VENDOR = 'dinero';
    private const BASE_ENDPOINT = 'invoices';

    protected $propertyMap = [
        'id' => 'Guid',
        'invoiceNumber' => 'Number',
        'dueDate' => 'PaymentDate',
        'issueDate' => 'Date',
        'vatAmount' => 'TotalInclVat',
        'pdfLink' => "Guid",
        'grossAmount' => 'TotalInclVat',
        'currency' => 'Currency',
        'debtorNumber' => 'ContactGuid',
        'netAmount' => "TotalExclVat",
    ];

    /**
     * @param bool $bookedOnly
     * @return array
     */
    public function getAll(array $filter = [], $date): array
    {

        $finished = false;
        $invoices = [];
        $skipping = 0;

        while (!$finished) {
            $paramDinero = "?fields=Guid,Number,PaymentDate,Date,TotalInclVat,TotalExclVat,Currency,ContactGuid";
            $paramDinero .= "&pageSize=1000&page=" . $skipping;
            if (isset($filter['booked']) && $filter['booked'] === true) {
                echo "getting booked";
                $paramDinero .= "&statusFilter=Booked,Overdue";
            }
            if (isset($filter['paid']) && $filter['paid'] === true) {
                $paramDinero .= "&statusFilter=paid";
            }
            try {
                $response = $this->erpClient->getClient()->get(static::BASE_ENDPOINT . $paramDinero, [
                    
                ]);
                $body = json_decode($response->getBody()->getContents(), true);
            } catch (ClientException $e) {
                $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);
    
                $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);
    
                return [];
            }
            if (count($body['Collection'] ) == 0) {
                $finished = true;
                continue;
            }
            array_push($invoices, ...$body['Collection']);
            $skipping ++;
        }

        return $this->_transformListOfInvoices($invoices);
    }

    /**
     * @param string $invoiceUrl
     * @return resource
     */
    public function getPDF(string $invoiceUrl)
    {
        try {
            $invoiceRequest = $this->erpClient->getClient()->get('invoices/' . $invoiceUrl, [
                'headers' => ['Accept' => 'application/octet-stream'],
            ]);

            if ((int)$invoiceRequest->getStatusCode() === 200) {
                return StreamWrapper::getResource($invoiceRequest->getBody());
            } else {
                throw new BaseException('Something went wrong when fetching invoice PDF from Dinero');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $contactId
     * @return \EconomyPie\Invoices\InvoiceData
     */
    public function get(string $contactId): InvoiceData
    {
        try {
            $response = $this->erpClient->getClient()->get(
                static::BASE_ENDPOINT . '/' . $contactId
            );
            $body = StreamWrapper::getResource($response->getBody());
        } catch (ClientException $e) {
            $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);

            $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);

            throw new Exception($e->getMessage(), $e->getCode());
        }

        //TODO Implement - works?
        die("NOT IMPLEMENTED YET");

        return $this->_transformToInvoiceDataObject($body['invoices']);
    }

    /**
     * @param integer $erp_id
     * return integer count of total booked invoices
     */

    public function getCount(): array
    {
        // to be worked on later
        return [];
    }
}
