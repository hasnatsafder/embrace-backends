<?php
declare(strict_types=1);

namespace EconomyPie;

use EconomyPie\Exceptions\ErpNotImplemented;

trait FactoryTrait
{
    /**
     * @param $erpName
     * @param $data
     * @param $namespace
     * @return mixed
     * @throws ErpNotImplemented
     */
    public function genericBuilder($erpName, $data, $namespace)
    {
        $capitalizedErpIdentifier = ucfirst($erpName);
        $className = $namespace . '\Adapter\\' . $capitalizedErpIdentifier . 'Adapter';

        if (!class_exists($className)) {
            throw new ErpNotImplemented("$erpName is not yet implemented");
        }

        return new $className($data);
    }
}
