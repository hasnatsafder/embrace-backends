<?php
declare(strict_types=1);

namespace EconomyPie\Exceptions;

class ErpNotImplemented extends Exception
{
    protected $message = 'ERP system is not yet implemented';
}
