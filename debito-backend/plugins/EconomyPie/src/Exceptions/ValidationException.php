<?php
declare(strict_types=1);

namespace EconomyPie\Exceptions;

class ValidationException extends Exception
{
}
