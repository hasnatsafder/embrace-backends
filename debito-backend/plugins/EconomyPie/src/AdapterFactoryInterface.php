<?php
declare(strict_types=1);

namespace EconomyPie;

interface AdapterFactoryInterface
{
    public function build($erpName, $data);
}
