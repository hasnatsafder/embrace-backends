<?php
declare(strict_types=1);

namespace EconomyPie\Auth;

use EconomyPie\ErpClientTrait;

abstract class AbstractAdapter
{
    use ErpClientTrait;

    public function __construct(array $authData)
    {
        if (!property_exists($this, 'erpClient') || $this->erpClient === null) {
            $this->createErpClient($authData);
        }
    }
}
