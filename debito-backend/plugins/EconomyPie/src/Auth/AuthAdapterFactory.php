<?php
declare(strict_types=1);

namespace EconomyPie\Auth;

use EconomyPie\AdapterFactoryInterface;
use EconomyPie\FactoryTrait;

class AuthAdapterFactory implements AdapterFactoryInterface
{
    use FactoryTrait;

    public function build($erpName, $data = [])
    {
        return $this->genericBuilder($erpName, $data, __NAMESPACE__);
    }
}
