<?php
declare(strict_types=1);

namespace EconomyPie\Client;

use EconomyPie\AdapterFactoryInterface;
use EconomyPie\FactoryTrait;

class ClientAdapterFactory implements AdapterFactoryInterface
{
    use FactoryTrait;

    public function build($erpName, $data)
    {
        return $this->genericBuilder($erpName, $data, __NAMESPACE__);
    }
}
