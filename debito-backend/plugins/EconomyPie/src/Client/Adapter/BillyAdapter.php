<?php
declare(strict_types=1);

namespace EconomyPie\Client\Adapter;

use EconomyPie\Client\ClientAdapterInterface;
use GuzzleHttp\Client;

class BillyAdapter implements ClientAdapterInterface
{
    public $http;

    public function __construct(array $data)
    {
        $headers = [];
        if (!empty($data['auth_token'])) {
            $headers = ['X-Access-Token' => $data['auth_token'],];
        }
        $this->http = new Client([
            'base_uri' => 'https://api.billysbilling.com/v2/',
            'headers' => $headers,
        ]);
    }

    public function getClient()
    {
        return $this->http;
    }
}
