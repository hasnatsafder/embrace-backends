<?php
declare(strict_types=1);

namespace EconomyPie\Debtors\Adapter;

use Cake\Http\Exception\UnauthorizedException;
use EconomyPie;
use EconomyPie\Debtors\AbstractAdapter;
use EconomyPie\Debtors\DebtorData;
use GuzzleHttp\Exception\ClientException;

class DineroAdapter extends AbstractAdapter
{
    public const VENDOR = 'dinero';
    private const BASE_ENDPOINT = 'contacts';

    protected $propertyMap = [
        'id' => 'ContactGuid',
        'name' => 'Name',
        'vatNumber' => 'registrationNo',
        'streetName' => 'Street',
        'zipCode' => 'ZipCode',
        'city' => 'City',
        'country' => 'CountryKey',
        'phone' => 'Phone',
        'email' => 'Email',
    ];

    /**
     * @param string $contactId
     * @return \EconomyPie\Debtors\DebtorData
     */
    public function get(string $contactId): DebtorData
    {
        try {
            $response = $this->erpClient->getClient()->get(
                static::BASE_ENDPOINT . '/' . $contactId . '?' .
                http_build_query([
                    'include' => 'contact.contactPersons:embed',
                ])
            );

            $contact = json_decode($response->getBody()->getContents(), true);

            return $this->_transformToDebtorDataObject($contact);
        } catch (ClientException $e) {
            throw new UnauthorizedException($e->getMessage());
        }
    }
}
