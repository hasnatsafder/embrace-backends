<?php
declare(strict_types=1);

namespace EconomyPie\Debtors;

use EconomyPie\ErpData;

class DebtorData extends ErpData
{
    public $name;
    public $vatNumber;
    public $streetName;
    public $zipCode;
    public $city;
    public $country;
    public $phone;
    public $email;
}
