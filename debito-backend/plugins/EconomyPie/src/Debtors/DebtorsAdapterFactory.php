<?php
declare(strict_types=1);

namespace EconomyPie\Debtors;

use EconomyPie\AdapterFactoryInterface;
use EconomyPie\FactoryTrait;

class DebtorsAdapterFactory implements AdapterFactoryInterface
{
    use FactoryTrait;

    public function build($erpName, $data): DebtorAdapterInterface
    {
        return $this->genericBuilder($erpName, $data, __NAMESPACE__);
    }
}
