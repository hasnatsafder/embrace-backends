<?php
declare(strict_types=1);

namespace App\Model\Table\Traits;

use Cake\ORM\Entity;
use Cake\ORM\Table;

trait IdentifierFindersTrait
{
    /**
     * @param string $identifier
     * @return \Cake\ORM\Entity
     */
    public function getFromIdentifier(string $identifier): Entity
    {
        assert($this instanceof Table);

        return $this->find()
            ->where([$this->aliasField('identifier') => $identifier])
            ->firstOrFail();
    }
}
