<?php
declare(strict_types=1);

namespace App\Model\Table\Traits;

use Cake\ORM\Association\BelongsTo;
use Cake\ORM\Association\BelongsToMany;
use Cake\ORM\Query;
use InvalidArgumentException;
use RuntimeException;

trait InAccountFinderTrait
{
    /**
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\ORM\Query
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function findInAccount(Query $query, array $options): Query
    {
        if (empty($options['account_id'])) {
            throw new InvalidArgumentException('Missing "account_id" key in options');
        }

        /** @var \Cake\ORM\Association $association */
        $association = $this->getAssociation('Accounts');

        if ($association instanceof BelongsToMany) {
            return $query->matching('Accounts', function (Query $q) use ($options) {
                return $q->where(['Accounts.id' => $options['account_id']]);
            });
        } elseif ($association instanceof BelongsTo) {
            return $query->where([$this->aliasField('account_id') => $options['account_id']]);
        }

        throw new RuntimeException("Didn't find the assocition to Accounts");
    }
}
