<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimDunning;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimDunnings Model
 *
 * @property \App\Model\Table\ClaimsTable|\Cake\ORM\Association\BelongsTo $Claims
 * @method \App\Model\Entity\ClaimDunning get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimDunning newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimDunning[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimDunning|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimDunning patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimDunning[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimDunning findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimDunning saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @property \App\Model\Table\InvoicesTable&\Cake\ORM\Association\BelongsTo $Invoices
 * @method \App\Model\Entity\ClaimDunning[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ClaimDunningsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_dunnings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->date('warning_1')
            ->allowEmptyString('warning_1');
        $validator
            ->date('warning_2')
            ->allowEmptyString('warning_2');
        $validator
            ->date('warning_3')
            ->allowEmptyString('warning_3');
        $validator
            ->boolean('warning_1_amount');
        $validator
            ->boolean('warning_2_amount');
        $validator
            ->boolean('warning_3_amount');
        $validator
            ->uuid('warning_1_file_path')
            ->allowEmptyString('warning_1_file_path');
        $validator
            ->uuid('warning_2_file_path')
            ->allowEmptyString('warning_2_file_path');
        $validator
            ->uuid('warning_3_file_path')
            ->allowEmptyString('warning_3_file_path');
        $validator
            ->scalar('email_text')
            ->allowEmptyString('email_text');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));

        return $rules;
    }
}
