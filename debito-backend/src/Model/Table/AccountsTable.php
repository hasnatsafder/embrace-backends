<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Account;
use ArrayObject;
use Cake\Event\Event;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Search\Manager;
use Cake\ORM\TableRegistry;

/**
 * Accounts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\CompanyTypesTable&\Cake\ORM\Association\BelongsTo $CompanyTypes
 * @property \App\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 * @property \App\Model\Table\DebtorsTable&\Cake\ORM\Association\HasMany $Debtors
 * @property \App\Model\Table\ErpIntegrationDataTable&\Cake\ORM\Association\HasMany $ErpIntegrationData
 * @property \App\Model\Table\CollectorsTable&\Cake\ORM\Association\BelongsToMany $Collectors
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Account get($primaryKey, $options = [])
 * @method \App\Model\Entity\Account newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Account|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Account[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Account findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @property \App\Model\Table\AccountsUsersTable&\Cake\ORM\Association\HasMany $AccountsUsers
 * @property \App\Model\Table\AccountsCollectorsTable&\Cake\ORM\Association\HasMany $AccountsCollectors
 * @method \App\Model\Entity\Account saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Account[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 */
class AccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('CreatedBy');
        $this->addBehavior('Search.Search');

        $this->belongsTo('CompanyTypes');
        $this->belongsTo('Countries');

        $this->hasMany('Claims');
        $this->hasMany('AccountsUsers');
        $this->hasMany('Debtors');
        $this->hasMany('ErpIntegrationData');
        $this->hasMany('ErpIntegrations');
        $this->hasMany('AccountPlans');

        $this->belongsToMany('Collectors', [
            'through' => 'AccountsCollectors',
        ]);
        $this->belongsToMany('Users', [
            'through' => 'AccountsUsers',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmptyString('address');

        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 255)
            ->allowEmptyString('zip_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->allowEmptyString('city');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->allowEmptyString('phone');

        $validator
            ->scalar('vat_number')
            ->maxLength('vat_number', 255)
            ->allowEmptyString('vat_number');

        $validator
            ->scalar('ean_number')
            ->maxLength('ean_number', 255)
            ->allowEmptyString('ean_number');

        $validator
            ->scalar('website')
            ->maxLength('website', 255)
            ->allowEmptyString('website');

        $validator
            ->scalar('bank_reg_number')
            ->maxLength('bank_reg_number', 255)
            ->allowEmptyString('bank_reg_number');

        $validator
            ->scalar('bank_account_number')
            ->maxLength('bank_account_number', 255)
            ->allowEmptyString('bank_account_number');

        $validator
            ->boolean('is_company')
            ->requirePresence('is_company', 'create')
            ->notEmptyString('is_company');

        $validator
            ->scalar('latitude')
            ->maxLength('latitude', 255)
            ->allowEmptyString('latitude');

        $validator
            ->scalar('longitude')
            ->maxLength('longitude', 255)
            ->allowEmptyString('longitude');

        $validator
            ->add('iban', 'iban', [
                'rule' => 'iban'
            ])
            ->allowEmptyString('iban');

        $validator
            ->scalar('swift_code')
            ->maxLength('swift_code', 255)
            ->allowEmptyString('swift_code');

        $validator
            ->scalar('notification_summary_interval')
            ->maxLength('notification_summary_interval', 255)
            ->inList('notification_summary_interval', array_keys(Account::notificationSummaryIntervals()));

        $validator
            ->numeric('principal_split')
            ->lessThanOrEqual('principal_split', 100.00)
            ->greaterThanOrEqual('principal_split', 0);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_type_id'], 'CompanyTypes'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));

        return $rules;
    }

    public function afterSave(Event $event, Account $account, ArrayObject $options)
    {
        if( $account->isNew() ) {
            // add a subcription
            // $free_plan = TableRegistry::getTableLocator()->get('Plans')->find('all')->where(['name' => 'Trial'])->first();
            // $accountPlanTable = TableRegistry::getTableLocator()->get('AccountPlans');
            // $account_plan = $accountPlanTable->newEntity();
            // $account_plan->plan_id = $free_plan->id;
            // $account_plan->account_id = $account->id;
            // $account_plan->discount = 0;
            // $account_plan->internal_note = "";
            // $accountPlanTable->save($account_plan);
        }
        if ($account->isNew() && (!array_key_exists('skipAutoCreateUser', $options) || $options['skipAutoCreateUser'] !== true)) {
            $account = $account->addUser($options['_footprint']->id, 'owner');
        }
    }

    /**
     * @return \Search\Manager
     */
    public function searchManager(): Manager
    {
        /** @var \Search\Manager $searchManager */
        $searchManager = $this->behaviors()->Search->searchManager();

        return $searchManager
            ->add('accountsAndUsers', 'Search.Like', [
                'before' => true,
                'after' => true,
                'wildcardAny' => '?',
                'field' => [
                    'Accounts.name',
                    'Accounts.phone',
                    'Accounts.email',
                    'Users.first_name',
                    'Users.last_name',
                    'Users.email',
                ],
            ]);
    }
}
