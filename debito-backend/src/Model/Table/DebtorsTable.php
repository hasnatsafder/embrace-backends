<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Table\Traits\InAccountFinderTrait;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Event\Event;
use App\Model\Entity\Debtor;
use Search\Model\Filter\Callback;

/**
 * Debtors Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\ErpIntegrationsTable&\Cake\ORM\Association\BelongsTo $ErpIntegrations
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 *
 * @method \App\Model\Entity\Debtor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Debtor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Debtor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Debtor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Debtor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Debtor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Debtor findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\Debtor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @method \App\Model\Entity\Debtor[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class DebtorsTable extends Table
{
    use InAccountFinderTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('debtors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('CreatedBy', ['setAccountIdOnNew' => true]);
        $this->addBehavior('Search.Search');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
        ]);
        $this->belongsTo('ErpIntegrations', [
            'foreignKey' => 'country_id',
        ]);
        $this->hasMany('Claims', [
            'foreignKey' => 'debtor_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('vat_number')
            ->maxLength('vat_number', 255)
            ->allowEmptyString('vat_number');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 255)
            ->allowEmptyString('company_name');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmptyString('address');

        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 255)
            ->allowEmptyString('zip_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->allowEmptyString('city');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->allowEmptyString('phone');

        $validator
            ->email('email')
            ->allowEmptyString('email');

        $validator
            ->scalar('latitude')
            ->maxLength('latitude', 255)
            ->allowEmptyString('latitude');

        $validator
            ->scalar('langitude')
            ->maxLength('langitude', 255)
            ->allowEmptyString('langitude');

        $validator
            ->boolean('is_company')
            ->requirePresence('is_company', 'create')
            ->notEmptyString('is_company');

        $validator
            ->integer('login_code')
            ->maxLength('login_code', 6);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['erp_integration_id'], 'ErpIntegrations'));

        return $rules;
    }

    /**
     * @param \Cake\ORM\Query $q
     * @param array $options
     * @return \Cake\ORM\Query
     * @todo Debtor portal
     */
    public function findDebtorPortalAuth(Query $q, array $options)
    {
        return $q->innerJoinWith('Claims', function (Query $q) use ($options) {
            return $q->where(['Claims.customer_reference' => $options['username']]);
        });
    }

    public function beforeSave(Event $event, Debtor $debtor, $options)
    {
        if ($debtor->isNew()) {
            if ($debtor->country_id == null) {
                $denmark = TableRegistry::getTableLocator()->get('Countries')->find()->where(['iso_code' => 'DK'])->first();
                $debtor->country_id = $denmark->id;
            }
            $loginCode = '';
            for ($i = 0; $i < 6; $i++) {
                $loginCode .= mt_rand(0, 9);
            }
            $debtor->login_code = $loginCode;
        }
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager()
    {
        /** @var \Search\Manager $searchManager */
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search_term', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    $search_term = $args['search_term'];
                    $search_term = trim($search_term);
                    $search_term = strtolower($search_term);
                    $search_term = str_replace(" ", "", $search_term);
                    $query->where(['OR' => [
                        'LOWER(TRIM(Debtors.vat_number)) LIKE ' => "%".$search_term."%",
                        'CONCAT(LOWER(TRIM(Debtors.first_name)),LOWER(TRIM(Debtors.last_name))) LIKE ' => "%".$search_term."%",
                        'LOWER(TRIM(Debtors.company_name)) LIKE ' => "%".$search_term."%",
                        'LOWER(TRIM(Debtors.erp_integration_foreign_key)) LIKE ' => "%".$search_term."%",
                    ]]);
                },
            ]);

        return $searchManager;
    }
}
