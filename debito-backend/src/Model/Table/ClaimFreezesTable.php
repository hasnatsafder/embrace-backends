<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimFreeze;
use App\Model\Rule\NotChangeableRule;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimFreezes Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 *
 * @method \App\Model\Entity\ClaimFreeze get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimFreeze newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimFreeze[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFreeze|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimFreeze saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimFreeze patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFreeze[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFreeze findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @method \App\Model\Entity\ClaimFreeze[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 */
class ClaimFreezesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_freezes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('CreatedBy');

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->dateTime('frozen_until')
            ->requirePresence('frozen_until', 'create')
            ->allowEmptyDateTime('frozen_until', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['created_by_id'], 'CreatedBy'));
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));

        $rules->add(
            new NotChangeableRule(['claim_id']),
            'readOnlyClaimId',
            [
                'errorField' => 'claim_id',
                'message' => __('This cannot be changed after it has been created'),
            ]
        );

        $rules->add(
            new NotChangeableRule(['frozen_until']),
            'readOnlyFrozenUntil',
            [
                'errorField' => 'frozen_until',
                'message' => __('This cannot be changed after it has been created'),
            ]
        );

        return $rules;
    }
}
