<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimLineTypes Model
 *
 * @property \App\Model\Table\ClaimLinesTable&\Cake\ORM\Association\HasMany $ClaimLines
 *
 * @method \App\Model\Entity\ClaimLineType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimLineType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimLineType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLineType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimLineType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLineType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLineType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimLineType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimLineType[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class ClaimLineTypesTable extends Table
{
    use IdentifierFindersTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_line_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->hasMany('ClaimLines', [
            'foreignKey' => 'claim_line_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add('identifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identifier']));

        return $rules;
    }

    /**
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findWillAddInterest(Query $query, array $options): Query
    {
        return $query->where([$this->aliasField('identifier') => 'invoice']);
    }
}
