<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimActionType;
use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimActionTypes Model
 *
 * @property \App\Model\Table\ClaimPhasesTable&\Cake\ORM\Association\BelongsTo $ClaimPhases
 * @property \App\Model\Table\CollectorsTable&\Cake\ORM\Association\BelongsTo $Collectors
 * @property \App\Model\Table\ClaimActionsTable&\Cake\ORM\Association\HasMany $ClaimActions
 *
 * @method \App\Model\Entity\ClaimActionType get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimActionType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimActionType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimActionType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimActionType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimActionType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimActionType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimActionType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimActionType[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class ClaimActionTypesTable extends Table
{
    use IdentifierFindersTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_action_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('ClaimPhases', [
            'foreignKey' => 'claim_phase_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('Collectors', [
            'foreignKey' => 'collector_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ClaimActions', [
            'foreignKey' => 'claim_action_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->uuid('claim_phase_id')
            ->allowEmptyString('claim_phase_id');

        $validator
            ->uuid('collector_id')
            ->notEmptyString('collector_id');

        $validator
            ->uuid('claim_phase_id')
            ->allowEmptyString('claim_phase_id');

        $validator
            ->uuid('collector_id')
            ->notEmptyString('collector_id');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add('identifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('collector_identifier')
            ->maxLength('collector_identifier', 255)
            ->allowEmptyString('collector_identifier');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->boolean('is_public')
            ->requirePresence('is_public', 'create')
            ->notEmptyString('is_public');

        $validator
            ->boolean('is_automated')
            ->allowEmptyString('is_automated');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identifier']));
        $rules->add($rules->existsIn(['claim_phase_id'], 'ClaimPhases'));
        $rules->add($rules->existsIn(['collector_id'], 'Collectors'));

        return $rules;
    }

    /**
     * @param string $claimActionIdentifier
     * @param string $collectorIdentifier
     * @return \App\Model\Entity\ClaimActionType
     */
    public function getClaimActionTypeForCollector(string $claimActionIdentifier, string $collectorIdentifier): ClaimActionType
    {
        return $this->find()
            ->where(['ClaimActionTypes.identifier' => $claimActionIdentifier])
            ->matching('Collectors', function (Query $q) use ($collectorIdentifier) {
                return $q->where(['Collectors.identifier' => $collectorIdentifier]);
            })
            ->firstOrFail();
    }
}
