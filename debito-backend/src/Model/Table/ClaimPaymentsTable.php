<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimPayment;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimPayments Model
 *
 * @property \App\Model\Table\ClaimsTable|\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\CollectorFilesTable|\Cake\ORM\Association\BelongsTo $CollectorFiles
 * @property \App\Model\Table\CurrenciesTable|\Cake\ORM\Association\BelongsTo $Currencies
 *
 * @method ClaimPayment get($primaryKey, $options = [])
 * @method ClaimPayment newEntity($data = null, array $options = [])
 * @method ClaimPayment[] newEntities(array $data, array $options = [])
 * @method ClaimPayment|bool save(EntityInterface $entity, $options = [])
 * @method ClaimPayment patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method ClaimPayment[] patchEntities($entities, array $data, array $options = [])
 * @method ClaimPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method ClaimPayment|bool saveOrFail(EntityInterface $entity, $options = [])
 */
class ClaimPaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CollectorFiles', [
            'foreignKey' => 'collector_file_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->date('payment_date')
            ->requirePresence('payment_date', 'create')
            ->notEmpty('payment_date');

        $validator
            ->integer('paid_fee_without_vat')
            ->allowEmptyString('paid_fee_without_vat');

        $validator
            ->integer('paid_fee_with_vat')
            ->allowEmptyString('paid_fee_with_vat');

        $validator
            ->integer('vat_of_fees')
            ->allowEmptyString('vat_of_fees');

        $validator
            ->integer('vat_of_commision')
            ->allowEmptyString('vat_of_commision');

        $validator
            ->integer('commision')
            ->allowEmptyString('commision');

        $validator
            ->integer('gross_paid')
            ->allowEmptyString('gross_paid');

        $validator
            ->integer('paid_to_creditor')
            ->allowEmptyString('paid_to_creditor');

        $validator
            ->integer('paid_principal')
            ->allowEmptyString('paid_principal');

        $validator
            ->integer('paid_fees')
            ->allowEmptyString('paid_fees');

        $validator
            ->integer('paid_interest')
            ->allowEmptyString('paid_interest');

        $validator
            ->integer('remaining_principal')
            ->allowEmptyString('remaining_principal');

        $validator
            ->integer('remaining_total')
            ->allowEmptyString('remaining_total');

        $validator
            ->scalar('raw_content')
            ->allowEmptyString('raw_content');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));
        $rules->add($rules->existsIn(['collector_file_id'], 'CollectorFiles'));
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));

        return $rules;
    }
}
