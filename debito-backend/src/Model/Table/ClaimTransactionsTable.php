<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimTransaction;
use App\Model\Rule\NoValidationErrorsRule;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Search\Manager;

/**
 * ClaimTransactions Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\ClaimTransactionTypesTable&\Cake\ORM\Association\BelongsTo $ClaimTransactionTypes
 * @property \App\Model\Table\ClaimLinesTable&\Cake\ORM\Association\BelongsTo $ClaimLines
 *
 * @method \App\Model\Entity\ClaimTransaction get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimTransaction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimTransaction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimTransaction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimTransaction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimTransaction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimTransaction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimTransaction findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @method \App\Model\Entity\ClaimTransaction[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ClaimTransactionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_transactions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy');
        $this->addBehavior('Search.Search');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
            'finder' => 'withTrashed',
        ]);
        $this->belongsTo('ClaimLines', [
            'foreignKey' => 'claim_line_id',
            'joinType' => 'INNER',
            'finder' => 'withTrashed',
        ]);
        $this->belongsTo('ClaimTransactionTypes', [
            'foreignKey' => 'claim_transaction_type_id',
            'joinType' => 'INNER',
            'finder' => 'withTrashed',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->uuid('claim_id')
            ->requirePresence('claim_id', 'create')
            ->notEmptyString('claim_id');

        $validator
            ->uuid('claim_line_id')
            ->allowEmptyString('claim_line_id');

        $validator
            ->uuid('claim_transaction_type_id')
            ->requirePresence('claim_transaction_type_id', 'create')
            ->notEmptyString('claim_transaction_type_id');

        $validator
            ->dateTime('timestamp')
            ->requirePresence('timestamp', 'create')
            ->notEmptyDateTime('timestamp');

        $validator
            ->decimal('amount')
            ->lessThanOrEqual('amount', 999999999.9999)
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->decimal('total')
            ->lessThanOrEqual('total', 999999999.9999);

        $validator
            ->scalar('subject')
            ->maxLength('subject', 255)
            ->allowEmptyString('subject');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('position')
            ->greaterThanOrEqual('position', 1)
            ->lessThanOrEqual('position', 65535)
            ->allowEmptyString('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['created_by_id'], 'CreatedBy'));
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));
        $rules->add($rules->existsIn(['claim_transaction_type_id'], 'ClaimTransactionTypes'));

        $rules->add(
            function (ClaimTransaction $entity, array $options) {
                if ($entity->claim_line_id === null) {
                    return true;
                }

                return $this->ClaimLines->exists([
                    'claim_id' => $entity->claim_id,
                    'id' => $entity->claim_line_id,
                ]);
            },
            'claimLineInClaim',
            [
                'errorField' => 'claim_line_id',
                'message' => __('Claim line must be in the claim'),
            ]
        );

        $rules->add(
            new NoValidationErrorsRule(
                $this->getSchema()->columns(),
                $this->getValidator('default')
            )
        );

        return $rules;
    }

    public function beforeFind(Event $event, Query $query, \ArrayObject $options)
    {
        if (!$query->clause('order') && !($options['skipDefaultOrdering'] ?? false)) {
            $query->order([$this->aliasField('position') => 'DESC']);
        }
    }

    /**
     * @param \Cake\Event\Event $event
     * @param \App\Model\Entity\ClaimTransaction $claimTransaction
     * @param \ArrayObject $options
     */
    public function beforeSave(Event $event, ClaimTransaction $claimTransaction, \ArrayObject $options)
    {
        $highestExistingPositionForClaim = $this->_getHighestPositionForClaim($claimTransaction->claim_id);

        if ($claimTransaction->isNew()) {
            $claimTransaction->set('position', $highestExistingPositionForClaim + 1);
        }

        $recalculateTotals = $options['recalculateTotals'] ?? false;

        if ($claimTransaction->isNew() || $claimTransaction->isDirty('amount') || $recalculateTotals) {
            $previousClaimTransaction = $claimTransaction->getPreviousClaimTransaction();
            $previousTotal = $previousClaimTransaction !== null ? $previousClaimTransaction->total : 0;
            $claimTransaction->set(
                'total',
                round($claimTransaction->amount + $previousTotal, 4)
            );
        }
    }

    /**
     * @return \Search\Manager
     */
    public function searchManager(): Manager
    {
        return $this->behaviors()->Search->searchManager()
            ->value('claim_id')
            ->value('claim_transaction_type_id')
            ->add('timestamp', 'MultiCompare', [
                'rangeRequired' => true,
                'allowedOperators' => [
                    'gte',
                    'lte',
                    'gt',
                    'lt',
                    'eq',
                ],
            ]);
    }

    /**
     * @param string $claimId
     * @return int
     */
    private function _getHighestPositionForClaim(string $claimId): int
    {
        // Find the last record in the set
        $last = $this->find()
            ->select(['ClaimTransactions.position'])
            ->where(['ClaimTransactions.claim_id' => $claimId])
            ->order(['ClaimTransactions.position' => 'DESC'])
            ->limit(1)
            ->enableHydration(false)
            ->first();

        if ($last === null) {
            return 0;
        }

        return $last['position'] ?? 0;
    }

    /**
     * @param string $claimId
     * @return int
     */
    private function _getPositionForNewestInClaim(string $claimId): int
    {
        // Find the last record in the set
        $last = $this->find()
            ->select(['ClaimTransactions.position'])
            ->where(['ClaimTransactions.claim_id' => $claimId])
            ->order(['ClaimTransactions.timestamp' => 'DESC'])
            ->limit(1)
            ->enableHydration(false)
            ->first();

        if ($last === null) {
            return 0;
        }

        return $last['position'] ?? 0;
    }
}
