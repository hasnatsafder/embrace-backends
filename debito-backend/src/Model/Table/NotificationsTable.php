<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Account;
use App\Model\Entity\ClaimAction;
use App\Model\Entity\Notification;
use App\Model\Entity\User;
use Cake\Database\Schema\TableSchema;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use RuntimeException;

/**
 * Notifications Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBies
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\NotificationSummariesTable&\Cake\ORM\Association\BelongsTo $NotificationSummaries
 *
 * @method \App\Model\Entity\Notification get($primaryKey, $options = [])
 * @method \App\Model\Entity\Notification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Notification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Notification|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notification saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Notification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Notification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Notification findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class NotificationsTable extends Table
{
    use LocatorAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('notifications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('NotificationSummaries', [
            'foreignKey' => 'notification_summary_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * @param \Cake\Database\Schema\TableSchema $schema
     * @return \Cake\Database\Schema\TableSchema
     */
    protected function _initializeSchema(TableSchema $schema)
    {
        parent::_initializeSchema($schema);

        $schema->setColumnType('data_values', 'json');

        return $schema;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('template')
            ->greaterThanOrEqual('template',0)
            ->lessThanOrEqual('template', 99)
            ->requirePresence('template', 'create')
            ->notEmptyString('template')
            ->inList('template', array_keys(Notification::templates()));

        $validator
            ->isArray('data_values')
            ->requirePresence('data_values', 'create')
            ->notEmptyArray('data_values');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['created_by_id'], 'CreatedBy'));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['notification_summary_id'], 'NotificationSummaries'));

        return $rules;
    }

    public function findNotSent(Query $q, array $options)
    {
        return $q->where(['Notifications.notification_summary_id IS' => null]);
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager()
    {
        return $this->behaviors()->Search->searchManager()
            ->value('user_id')
            ->value('account_id')
            ->add('created', 'App.MultiCompare');
    }

    /**
     * Send notification to all users in an account
     *
     * @param \App\Model\Entity\Account $account
     * @param array $notificationEntityData
     * @throws \Exception
     */
    public function sendNotificationToAccount(Account $account, array $notificationEntityData)
    {
        /** @var \Cake\Datasource\EntityInterface[] $notificationEntities */
        $notificationEntities = $this->Users->find()
            ->matching('Accounts', function (Query $q) use ($account) {
                return $q->where(['Accounts.id' => $account->id]);
            })
            ->map(function (User $user) use ($account, $notificationEntityData) {
                return $this->newEntity(
                    ['account_id' => $account->id, 'user_id' => $user->id] + $notificationEntityData
                );
            })
            ->toList();

        if (!$this->saveMany($notificationEntities)) {
            throw new RuntimeException('Could not save notifications');
        }
    }

    /**
     * Add a new data value for claim action to send user notifications
     * @param String $account_id
     * @param String $claim_action_id
     */
    public function AddNewActionNotification(string $account_id, string $claim_action_id) {
        // get account
        $account =  TableRegistry::getTableLocator()->get('Accounts')->get($account_id);
        $notification = $this->newEntity();
        $notification->account_id = $account->id;
        $notification->user_id = $account->created_by_id;
        $notification->created_by_id = $account->created_by_id;
        $notification->template = 1;
        $notification->data_values = json_encode(['claimActionId' => $claim_action_id], JSON_UNESCAPED_SLASHES);
        $this->save($notification);
    }

    /**
     * Remove notification for an id
     * @param String $action_id
     * @return bool
     */
    public function RemoveNotification(string $action_id) : bool {
        if ($this->deleteAll(['data_values LIKE' => "%" . $action_id . "%"])) {
            return true;
        }
        return false;
    }

}
