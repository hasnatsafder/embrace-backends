<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\CollectorFile;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CollectorFiles Model
 *
 * @property \App\Model\Table\CollectorFileTypesTable&\Cake\ORM\Association\BelongsTo $CollectorFileTypes
 * @property \App\Model\Table\ClaimActionsTable&\Cake\ORM\Association\HasMany $ClaimActions
 * @property \App\Model\Table\ClaimFinancialsTable&\Cake\ORM\Association\HasMany $ClaimFinancials
 *
 * @method \App\Model\Entity\CollectorFile newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CollectorFile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CollectorFile get($primaryKey, $options = [])
 * @method \App\Model\Entity\CollectorFile findOrCreate($search, callable $callback = null, $options = [])
 * @method \App\Model\Entity\CollectorFile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CollectorFile[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CollectorFile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CollectorFile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CollectorFile[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class CollectorFilesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('collector_files');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('CollectorFileTypes', [
            'foreignKey' => 'collector_file_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ClaimActions', [
            'foreignKey' => 'collector_file_id',
        ]);
        $this->hasMany('ClaimFinancials', [
            'foreignKey' => 'collector_file_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('extension')
            ->maxLength('extension', 255)
            ->requirePresence('extension', 'create')
            ->notEmptyString('extension');

        $validator
            ->scalar('mime_type')
            ->maxLength('mime_type', 255)
            ->requirePresence('mime_type', 'create')
            ->notEmptyString('mime_type');

        $validator
            ->integer('size')
            ->requirePresence('size', 'create')
            ->notEmptyString('size');

        $validator
            ->scalar('dir')
            ->maxLength('dir', 255)
            ->requirePresence('dir', 'create')
            ->notEmptyString('dir');

        $validator
            ->dateTime('completed')
            ->allowEmptyString('completed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['collector_file_type_id'], 'CollectorFileTypes'));

        return $rules;
    }
}
