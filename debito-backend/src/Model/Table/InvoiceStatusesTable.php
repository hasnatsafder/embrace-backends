<?php

namespace App\Model\Table;

use App\Model\Entity\InvoiceStatus;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoiceStatuses Model
 *
 * @method \App\Model\Entity\InvoiceStatus get($primaryKey, $options = [])
 * @method \App\Model\Entity\InvoiceStatus newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InvoiceStatus[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InvoiceStatus|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoiceStatus saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InvoiceStatus patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InvoiceStatus[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InvoiceStatus findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @method \App\Model\Entity\InvoiceStatus[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class InvoiceStatusesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('invoice_statuses');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('identifier');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmptyString('description');

        $validator
            ->integer('placement')
            ->requirePresence('placement', 'create')
            ->notEmptyString('placement');

        $validator
            ->dateTime('deleted')
            ->allowEmptyString('deleted');

        return $validator;
    }

    public function beforeSave(Event $event, InvoiceStatus $entity, ArrayObject $options)
    {
        if (empty($entity->placement)) {
            //Set it to 1 by default
            $entity->placement = 1;

            //Set it to +1 of the highest existing placement, in case anyone exists
            $highestExistingPlacement = $this->find('HighestPlacement')->first();

            if ($highestExistingPlacement !== null) {
                $entity->placement = $highestExistingPlacement->placement + 1;
            }
        }
    }

    public function findHighestPlacement(Query $q, array $options)
    {
        return $q
            ->order(['InvoiceStatuses.placement' => 'DESC'])
            ->limit(1);
    }
}
