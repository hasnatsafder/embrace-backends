<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimAction;
use App\Model\Entity\Notification;
use Cake\Event\Event;
use Cake\I18n\FrozenDate;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Search\Manager;

/**
 * ClaimActions Model
 *
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\ClaimLinesTable&\Cake\ORM\Association\BelongsTo $ClaimLines
 * @property \App\Model\Table\CollectorFilesTable&\Cake\ORM\Association\BelongsTo $CollectorFiles
 * @property \App\Model\Table\ClaimActionTypesTable&\Cake\ORM\Association\BelongsTo $ClaimActionTypes
 * @method \App\Model\Entity\ClaimAction newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimAction[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimAction get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimAction findOrCreate($search, callable $callback = null, $options = [])
 * @method \App\Model\Entity\ClaimAction patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimAction[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimAction|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimAction saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimAction[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 */
class ClaimActionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_actions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('CreatedBy');
        $this->addBehavior('Search.Search');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'finder' => 'withTrashed',
        ]);
        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ClaimLines', [
            'foreignKey' => 'claim_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('CollectorFiles', [
            'foreignKey' => 'collector_file_id',
        ]);
        $this->belongsTo('ClaimActionTypes', [
            'foreignKey' => 'claim_action_type_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->uuid('created_by_id')
            ->allowEmptyString('created_by_id');

        $validator
            ->uuid('modified_by_id')
            ->allowEmptyString('modified_by_id');

        $validator
            ->uuid('user_id')
            ->allowEmptyString('user_id');

        $validator
            ->uuid('claim_line_id')
            ->allowEmptyString('claim_line_id');

        $validator
            ->uuid('claim_id')
            ->requirePresence('claim_id', 'create')
            ->notEmptyString('claim_id');

        $validator
            ->uuid('collector_file_id')
            ->allowEmptyString('collector_file_id');

        $validator
            ->uuid('claim_action_type_id')
            ->requirePresence('claim_action_type_id', 'create')
            ->notEmptyString('claim_action_type_id');

        $validator
            ->integer('paid')
            ->allowEmptyString('paid')
            ->lessThanOrEqual('paid', 99999999999);

        $validator
            ->integer('interest_added')
            ->allowEmptyString('interest_added')
            ->lessThanOrEqual('interest_added', 99999999999);

        $validator
            ->integer('debt_collection_fees')
            ->allowEmptyString('debt_collection_fees')
            ->lessThanOrEqual('debt_collection_fees', 99999999999);

        $validator
            ->integer('actual_paid')
            ->allowEmptyString('actual_paid')
            ->lessThanOrEqual('actual_paid', 99999999999);

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmptyDate('date');

        $validator
            ->dateTime('done')
            ->allowEmptyDateTime('done');

        $validator
            ->utf8Extended('message')
            ->allowEmptyString('message');

        return $validator;
    }

    public function validationWithSpecificClaimLine(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('claim_line_id')
            ->notEmptyString('claim_line_id');

        return $validator;
    }

    public function validationDebtorApiMessage(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('message')
            ->utf8Extended('message')
            ->notEmptyString('message');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));
        $rules->add($rules->existsIn(['claim_line_id'], 'ClaimLines'));
        $rules->add($rules->existsIn(['collector_file_id'], 'CollectorFiles'));
        $rules->add($rules->existsIn(['claim_action_type_id'], 'ClaimActionTypes'));

        return $rules;
    }

    /**
     * @param \Cake\Event\Event $e
     * @param \App\Model\Entity\ClaimAction $claimAction
     * @param \ArrayObject $options
     */
    public function afterSave(Event $e, ClaimAction $claimAction, \ArrayObject $options)
    {
        if ($claimAction->isNew()) {
            $this->getEventManager()->dispatch(
                new Event('Model.ClaimAction.created', $this, ['claimAction' => $claimAction])
            );
        }
        if ($claimAction->isDirty('done') && $claimAction->done != null) {
            $claimActionType =  TableRegistry::getTableLocator()->get('ClaimActionTypes')->get($claimAction->claim_action_type_id);
            if ($claimActionType->is_public) {
                $claim =  TableRegistry::getTableLocator()->get('Claims')->get($claimAction->claim_id);
                $notificationsTable = TableRegistry::getTableLocator()->get('Notifications');
                $notificationsTable->AddNewActionNotification($claim->account_id, $claimAction->id);
            }
        }
    }

    /**
     * After delete actions
     * @param Event $e
     * @param ClaimAction $claimAction
     * @param \ArrayObject $options
     */
    public function afterDelete(Event $e, ClaimAction $claimAction, \ArrayObject $options)
    {
        $notificationsTable = TableRegistry::getTableLocator()->get('Notifications');
        $notificationsTable->RemoveNotification($claimAction->id);
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager(): Manager
    {
        return $this->behaviors()->Search->searchManager()
            ->value('user_id')
            ->value('claim_id')
            ->value('claim_line_id')
            ->value('claim_action_type_id')
            ->add('date', 'App.MultiCompare')
            ->add('done', 'App.MultiCompare');
    }

    public function findOverdue(Query $q, array $options): Query
    {
        return $q->where([
            'ClaimActions.date <=' => FrozenDate::now(),
            'ClaimActions.done IS' => null,
        ]);
    }

    public function findAutomatic(Query $q, array $options): Query
    {
        return $q->innerJoinWith('ClaimActionTypes', function (Query $q) {
            return $q->where(['ClaimActionTypes.is_automated' => true]);
        });
    }
}
