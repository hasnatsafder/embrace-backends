<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimFinancial;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimFinancials Model
 *
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\CollectorFilesTable|\Cake\ORM\Association\BelongsTo $CollectorFiles
 *
 * @method \App\Model\Entity\ClaimFinancial get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimFinancial newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimFinancial[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFinancial|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimFinancial patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFinancial[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimFinancial findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimFinancial saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimFinancial[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ClaimFinancialsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_financials');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));

        return $rules;
    }
}
