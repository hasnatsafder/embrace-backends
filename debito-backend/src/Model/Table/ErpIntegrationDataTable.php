<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ErpIntegrationData;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use RuntimeException;

/**
 * ErpIntegrationData Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\ErpIntegrationsTable&\Cake\ORM\Association\BelongsTo $ErpIntegrations
 *
 * @method \App\Model\Entity\ErpIntegrationData get($primaryKey, $options = [])
 * @method \App\Model\Entity\ErpIntegrationData newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ErpIntegrationData[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegrationData|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ErpIntegrationData saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ErpIntegrationData patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegrationData[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegrationData findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @method \App\Model\Entity\ErpIntegrationData[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ErpIntegrationDataTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('erp_integration_data');

        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('ErpIntegrations', [
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('data_key')
            ->maxLength('data_key', 255)
            ->requirePresence('data_key', 'create')
            ->notEmptyString('data_key');

        $validator
            ->requirePresence('data_value')
            ->notEmptyString('data_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add(function (ErpIntegrationData $entity) {
            //Check if there's already a combination between the vendor, account and key
            $query = $this->find();

            if (!$entity->isNew()) {
                $query = $query->where(['ErpIntegrationData.id !=' => $entity->id]);
            }

            return $query
                ->where([
                    'ErpIntegrationData.erp_integration_id' => $entity->erp_integration_id,
                    'ErpIntegrationData.data_key' => $entity->data_key,
                ])
                ->isEmpty();
        }, [
            'errorField' => 'data_key',
            'message' => __('There is already setting with that key in the integration'),
        ]);

        return $rules;
    }

    /**
     * Find data only for a specific integration
     * Required keys in the $options array: 'integration' & 'account_id'
     *
     * @param \Cake\ORM\Query $q ORM query
     * @param array $options Options array
     * @return \Cake\ORM\Query
     */
    public function findIntegrationIs(Query $q, array $options)
    {
        if (empty($options['integration'])) {
            throw new RuntimeException('`integration` is a required key for this finder');
        }

        if (empty($options['account_id'])) {
            throw new RuntimeException('`account_id` is a required key for this finder');
        }

        return $q->matching('ErpIntegrations', function (Query $q) use ($options) {
            return $q->where([
                'ErpIntegrations.vendor_identifier' => $options['integration'],
                'ErpIntegrations.account_id' => $options['account_id'],
            ]);
        });
    }
}
