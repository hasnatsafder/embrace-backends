<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ErpIntegration;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ErpIntegrations Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\ErpIntegrationDataTable&\Cake\ORM\Association\HasMany $ErpIntegrationData
 *
 * @method \App\Model\Entity\ErpIntegration get($primaryKey, $options = [])
 * @method \App\Model\Entity\ErpIntegration newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ErpIntegration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ErpIntegration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ErpIntegration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegration[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ErpIntegration findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @method \App\Model\Entity\ErpIntegration[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ErpIntegrationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('erp_integrations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy', ['setAccountIdOnNew' => true]);
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ErpIntegrationData', [
            'foreignKey' => 'erp_integration_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('vendor_identifier')
            ->maxLength('vendor_identifier', 255)
            ->inList('vendor_identifier', array_keys(Configure::read('Integrations.erp')))
            ->requirePresence('vendor_identifier', 'create')
            ->notEmptyString('vendor_identifier');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        $rules->add(function (ErpIntegration $entity) {
            //Check if that specific integration has already been connected for the account
            return $this->find()
                ->where([
                    'ErpIntegrations.id !=' => $entity->id,
                    'ErpIntegrations.account_id' => $entity->account_id,
                    'ErpIntegrations.vendor_identifier' => $entity->vendor_identifier,
                ])
                ->isEmpty();
        }, [
            'errorField' => 'vendor_identifier',
            'message' => __('This integration is already connected'),
        ]);

        return $rules;
    }
}
