<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\DunningConfiguration;
use App\Model\Rule\NoValidationErrorsRule;
use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DunningConfigurations Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 *
 * @method \App\Model\Entity\DunningConfiguration get($primaryKey, $options = [])
 * @method \App\Model\Entity\DunningConfiguration newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DunningConfiguration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DunningConfiguration|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DunningConfiguration saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DunningConfiguration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DunningConfiguration[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DunningConfiguration findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @method \App\Model\Entity\DunningConfiguration[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class DunningConfigurationsTable extends Table
{
    use IdentifierFindersTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dunning_configurations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('CreatedBy', ['setAccountIdOnNew' => true]);
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->maxLength('name', 255)
            ->utf8('name')
            ->allowEmptyString('name');

        $validator
            ->integer('soft_reminder_freeze')
            ->greaterThanOrEqual('soft_reminder_freeze', 2)
            ->lessThanOrEqual('soft_reminder_freeze', 99)
            ->allowEmptyString('soft_reminder_freeze');

        $validator
            ->integer('warning_one_freeze')
            ->greaterThanOrEqual('warning_one_freeze', 14)
            ->lessThanOrEqual('warning_one_freeze', 99)
            ->notEmptyString('warning_one_freeze');

        $validator
            ->integer('warning_two_freeze')
            ->greaterThanOrEqual('warning_two_freeze', 14)
            ->lessThanOrEqual('warning_two_freeze', 99)
            ->allowEmptyString('warning_two_freeze');

        $validator
            ->integer('warning_three_freeze')
            ->greaterThanOrEqual('warning_three_freeze', 14)
            ->lessThanOrEqual('warning_three_freeze', 99)
            ->allowEmptyString('warning_three_freeze');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['created_by_id'], 'CreatedBy'));
        $rules->add($rules->existsIn(['modified_by_id'], 'CreatedBy'));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        $rules->add(function (DunningConfiguration $dunningConfiguration) {
            if ($dunningConfiguration->warning_three_freeze === null) {
                return true;
            }

            return $dunningConfiguration->warning_two_freeze !== null && $dunningConfiguration->warning_one_freeze !== null;
        }, [
            'errorField' => 'warning_three_freeze',
            'message' => __('Warning 3 cannot be enabled without warning 2'),
        ]);

        $rules->add(
            new NoValidationErrorsRule(
                $this->getSchema()->columns(),
                $this->getValidator('default')
            )
        );

        return $rules;
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager()
    {
        return $this->behaviors()->Search->searchManager()
            ->like('name', [
                'before' => false,
                'after' => true,
                'field' => ['name'],
            ])
            ->value('soft_reminder_freeze')
            ->value('warning_one_freeze')
            ->value('warning_two_freeze')
            ->value('warning_three_freeze');
    }
}
