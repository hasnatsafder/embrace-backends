<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimLineType;
use App\Model\Entity\ClaimTransactionType;
use App\Model\Rule\NoValidationErrorsRule;
use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Search\Manager;

/**
 * ClaimTransactionTypes Model
 *
 * @property \App\Model\Table\ClaimTransactionsTable&\Cake\ORM\Association\HasMany $ClaimTransactions
 *
 * @method ClaimTransactionType get($primaryKey, $options = [])
 * @method ClaimTransactionType newEntity($data = null, array $options = [])
 * @method ClaimTransactionType[] newEntities(array $data, array $options = [])
 * @method ClaimTransactionType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method ClaimTransactionType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method ClaimTransactionType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method ClaimTransactionType[] patchEntities($entities, array $data, array $options = [])
 * @method ClaimTransactionType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @method ClaimTransactionType[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class ClaimTransactionTypesTable extends Table
{
    use IdentifierFindersTrait;

    const CLAIM_LINE_TYPES_MAPPING = [
        'invoice' => 'principal',
        'document' => false,
        'late_payment_fee' => 'warning',
        'compensation_fee' => 'debt_collection_compensation_fee',
        'payment' => 'payment_manual',
        'credit_note' => 'payment_manual',
        'created_by_error' => false,
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_transaction_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');

        $this->hasMany('ClaimTransactions', [
            'foreignKey' => 'claim_transaction_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add('identifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['identifier']));
        $rules->add(
            new NoValidationErrorsRule(
                $this->getSchema()->columns(),
                $this->getValidator('default')
            )
        );

        return $rules;
    }

    /**
     * @return \Search\Manager
     */
    public function searchManager(): Manager
    {
        return $this->behaviors()->Search->searchManager()
            ->like('identifier', [
                'after' => true,
            ]);
    }

    /**
     * @param \Cake\ORM\Query $q
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findPaymentTypes(Query $q, array $options): Query
    {
        return $q->where([
            $this->aliasField('identifier') . ' IN' => ['payment_fi', 'payment_manual'],
        ]);
    }

    public function findFeesAndInterest(Query $q, array $options): Query
    {
        return $q->where([
            $this->aliasField('identifier') . ' IN' => [
                'debt_collection_commission',
                'debt_collection_compensation_fee',
                'interest_8_05',
                'warning_one',
                'warning_two',
                'warning_three',
            ],
        ]);
    }

    /**
     * @param \App\Model\Entity\ClaimLineType $claimLineType
     * @return \App\Model\Entity\ClaimTransactionType|null
     */
    public function getTypeFromClaimLineTypeIdentifier(ClaimLineType $claimLineType): ?ClaimTransactionType
    {
        if (!isset($this::CLAIM_LINE_TYPES_MAPPING[$claimLineType->identifier])) {
            throw new \RuntimeException(
                sprintf(
                    'ClaimLineType %s not supported for internal debt collection',
                    $claimLineType->identifier
                )
            );
        }

        if ($this::CLAIM_LINE_TYPES_MAPPING[$claimLineType->identifier] === false) {
            return null;
        }

        //We might have to figure out if it is warning 1/2/3...
        return $this->getFromIdentifier($this::CLAIM_LINE_TYPES_MAPPING[$claimLineType->identifier]);
    }
}
