<?php
declare(strict_types=1);

namespace App\Model\Filter;

use InvalidArgumentException;
use Search\Model\Filter\Base;

class MultiCompare extends Base
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'multiValue' => true,
        'mode' => 'AND',
        'rangeRequired' => false,
        'allowedOperators' => [
            'gte',
            'lte',
            'gt',
            'lt',
            'eq',
            'null',
        ],
    ];

    /**
     * Allowed operators.
     *
     * @var array
     */
    protected $_compatibleOperators = [
        'gte',
        'lte',
        'gt',
        'lt',
        'eq',
        'null',
    ];

    /**
     * Process a comparison-based condition (e.g. $field <= $value).
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function process()
    {
        //Not sure why this is required... It should have used the default value above, but it does not for some reason
        $this->setConfig('multiValue', true);
        $conditions = [];

        $values = $this->value();

        if (!is_array($values)) {
            return false;
        }

        $upperLimitFound = false;
        $lowerLimitFound = false;

        foreach ($this->fields() as $field) {
            foreach ($values as $arg) {
                $operator = array_keys($arg)[0];
                $this->_checkIfOperatorIsAllowed($operator);
                $value = array_values($arg)[0];

                $upperLimitFound = in_array($operator, ['gt', 'gte']) ?: $upperLimitFound;
                $lowerLimitFound = in_array($operator, ['lt', 'lte']) ?: $lowerLimitFound;

                $conditions[] = [$field . $this->_getSqlOperator($operator) => $this->getSqlValue($operator, $value)];
            }
        }

        if ($this->getConfig('rangeRequired')
            && (($upperLimitFound && !$lowerLimitFound) || (!$upperLimitFound && $lowerLimitFound))
        ) {
            throw new InvalidArgumentException('You are required to supply both upper and lower limits');
        }

        if (empty($conditions)) {
            return false;
        }

        $this->getQuery()->andWhere([$this->getConfig('mode') => $conditions]);

        return true;
    }

    /**
     * @param string $operator
     * @return string
     */
    private function _getSqlOperator(string $operator)
    {
        switch ($operator) {
            case 'gt':
                return ' >';
            case 'gte':
                return ' >=';
            case 'lt':
                return ' <';
            case 'lte':
                return ' <=';
            case 'eq':
                return '';
            case 'null':
                return ' IS';
        }

        return '';
    }

    private function getSqlValue($operator, $value)
    {
        if ($operator === 'null') {
            return null;
        }

        return $value;
    }

    private function _checkIfOperatorIsAllowed($operator)
    {
        if (!in_array($operator, $this->getConfig('allowedOperators'), true)
            || !in_array($operator, $this->_compatibleOperators, true)
        ) {
            throw new InvalidArgumentException(sprintf('The operator "%s" is invalid!', $operator));
        }
    }
}
