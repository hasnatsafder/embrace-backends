<?php
declare(strict_types=1);

namespace App\Model\Rule;

use Cake\ORM\Entity;

/**
 * Class NotChangeableRule
 *
 * @package App\Model\Rule
 */
class NotChangeableRule
{
    /**
     * The list of fields to check
     *
     * @var array
     */
    protected $_fields;

    /**
     * Constructor.
     *
     * @param string|array $fields The field or fields to check existence as primary key.
     */
    public function __construct($fields)
    {
        $this->_fields = (array)$fields;
    }

    public function __invoke(Entity $entity, array $options)
    {
        foreach ($this->_fields as $i => $field) {
            if ($entity->isNew() || !$entity->isDirty($field)) {
                return true;
            }

            if ($entity->getOriginal($field) !== null) {
                return $entity->get($field) === $entity->getOriginal($field);
            }
        }

        return true;
    }
}
