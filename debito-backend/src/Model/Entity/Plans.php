<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Tools\Model\Entity\EnumTrait;

/**
 * AccountsUser Entity
 *
 * @property string $id
 * @property string $name
 * @property int $price
 * @property string $billing_interval
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Plan extends Entity
{

    use EnumTrait;
    public const BILLING_INTERVAL_MONTHLY= 'MONTHLY';
    public const BILLING_INTERVAL_YEARLY = 'YEARLY';
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'price' => true,
        'billing_interval' => true,
    ];

    /**
     * @param null $value
     * @return array|string
     */
    public static function billingIntervals($value = null)
    {
        $options = [
            static::BILLING_INTERVAL_MONTHLY => __('Monthly'),
            static::BILLING_INTERVAL_YEARLY => __('Yearly'),
        ];

        return static::enum($value, $options);
    }
}
