<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimPayment Entity
 *
 * @property string $id
 * @property string $claim_id
 * @property string $collector_file_id
 * @property string|null $currency_id
 * @property \Cake\I18n\FrozenDate $payment_date
 * @property int|null $paid_fee_without_vat
 * @property int|null $paid_fee_with_vat
 * @property int|null $vat_of_fees
 * @property int|null $vat_of_commision
 * @property int|null $commision
 * @property int|null $gross_paid
 * @property int|null $paid_to_creditor
 * @property int|null $paid_principal
 * @property int|null $paid_fees
 * @property int|null $paid_interest
 * @property int|null $remaining_principal
 * @property int|null $remaining_total
 * @property string|null $raw_content
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\CollectorFile $collector_file
 * @property \App\Model\Entity\Currency $currency
 */
class ClaimPayment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'collector_file_id' => true,
        'currency_id' => true,
        'payment_date' => true,
        'paid_fee_without_vat' => true,
        'paid_fee_with_vat' => true,
        'vat_of_fees' => true,
        'vat_of_commision' => true,
        'commision' => true,
        'gross_paid' => true,
        'paid_to_creditor' => true,
        'paid_principal' => true,
        'paid_fees' => true,
        'paid_interest' => true,
        'remaining_principal' => true,
        'remaining_total' => true,
        'raw_content' => true,
    ];
}
