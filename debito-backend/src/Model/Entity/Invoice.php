<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity
 *
 * @property string $id
 * @property string|null $created_by_id
 * @property string|null $modified_by_id
 * @property string $account_id
 * @property string $invoice_status_id
 * @property string $currency_id
 * @property string|null $erp_integration_id
 * @property string|null $erp_integration_foreign_key
 * @property string $debtor_id
 * @property int $invoice_number
 * @property \Cake\I18n\FrozenDate $issue_date
 * @property \Cake\I18n\FrozenDate $due_date
 * @property \Cake\I18n\FrozenTime|null $hidden_until
 * @property float $net_amount
 * @property float $gross_amount
 * @property string $pdf_file_path
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User|null $created_by
 * @property \App\Model\Entity\User|null $modified_by
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\InvoiceStatus $invoice_status
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\ErpIntegration|null $erp_integration
 * @property \App\Model\Entity\Debtor $debtor
 * @property bool $converted_to_claim
 * @property bool $snoozed
 * @property \App\Model\Entity\ClaimDunning[] $claim_dunnings
 * @property \App\Model\Entity\ClaimLine[] $claim_lines
 */
class Invoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'invoice_status_id' => true,
        'currency_id' => true,
        'erp_integration_id' => true,
        'erp_integration_foreign_key' => true,
        'debtor_id' => true,
        'invoice_number' => true,
        'issue_date' => true,
        'due_date' => true,
        'hidden_until' => true,
        'net_amount' => true,
        'gross_amount' => true,
        'pdf_file_path' => true,
        'snoozed' => true,
        'converted_to_claim' => true,
        'paid' => true
    ];
}
