<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AccountsCollector Entity
 *
 * @property string $id
 * @property string $account_id
 * @property string $collector_id
 * @property string|null $collector_identifier
 * @property \Cake\I18n\FrozenTime|null $requested_at
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Collector $collector
 */
class AccountsCollectors extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'collector_id' => true,
        'collector_identifier' => true,
        'requested_at' => true,
    ];
}
