<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use EconomyPie\Auth\Adapter\DineroAdapter;
use RuntimeException;
use Tools\Model\Entity\EnumTrait;

/**
 * ErpIntegration Entity
 *
 * @property string $id
 * @property string $created_by_id
 * @property string $modified_by_id
 * @property string $account_id
 * @property string $vendor_identifier
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $created_by
 * @property \App\Model\Entity\User $modified_by
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\ErpIntegrationData[] $erp_integration_data
 */
class ErpIntegration extends Entity
{

    use EnumTrait;
    public const STAGE_NOT_SYNCED= 'NOT_SYNCED';
    public const STAGE_UNPAID_SYNCED = 'UNPAID_SYNCED';
    public const STAGE_PAID_SYNCED = 'PAID_SYNCED';
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created_by_id' => true,
        'modified_by_id' => true,
        'account_id' => true,
        'vendor_identifier' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'created_by' => true,
        'modified_by' => true,
        'account' => true,
        'erp_integration_data' => true,
        'stage' => true,
    ];

    public function getAuthData(): array
    {
        /** @var \App\Model\Table\ErpIntegrationDataTable $erpIntegrationDataTable */
        $erpIntegrationDataTable = TableRegistry::getTableLocator()->get('ErpIntegrationData');
        $result = [];

        switch ($this->vendor_identifier) {
            case 'dinero':
                $authToken = $erpIntegrationDataTable->find()
                    ->where([
                        'ErpIntegrationData.erp_integration_id' => $this->id,
                        'ErpIntegrationData.data_key' => 'auth_token',
                    ])
                    ->first();

                if (empty($authToken)) {
                    throw new RuntimeException(
                        'Account is not authorized with ERP integration: ' . $this->vendor_identifier
                    );
                }
                // We need an organization ID for Dinero
                if (empty($authToken->org_id)) {
                    throw new RuntimeException(
                        'No Organization ID found for this integration : ' . $this->vendor_identifier
                    );
                }

                // For Dinero we need to fetch a new token everytime
                $DineroAuth = new DineroAdapter([
                    'api_key' => $authToken->data_value,
                ]);

                $token = $DineroAuth->getToken();

                $result['auth_token'] = $token;
                $result['org_id'] = $authToken->org_id;
                break;
            case 'billy':
            case 'economic':
                $authToken = $erpIntegrationDataTable->find()
                    ->where([
                        'ErpIntegrationData.erp_integration_id' => $this->id,
                        'ErpIntegrationData.data_key' => 'auth_token',
                    ])
                    ->first();

                if (empty($authToken)) {
                    throw new RuntimeException(
                        'Account is not authorized with ERP integration: ' . $this->vendor_identifier
                    );
                }

                $result['auth_token'] = $authToken->data_value;
                break;
            default:
                throw new RuntimeException(
                    'Cannot create authentication object for ' . $this->vendor_identifier
                );
        }

        return $result;
    }

    /**
     * @param null $value
     * @return array|string
     */
    public static function stages($value = null)
    {
        $options = [
            static::STAGE_NOT_SYNCED => __('Not_synced'),
            static::STAGE_UNPAID_SYNCED => __('unpaid_synced'),
            static::STAGE_PAID_SYNCED => __('paid_synced'),
        ];

        return static::enum($value, $options);
    }
}
