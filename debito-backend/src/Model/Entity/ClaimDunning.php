<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimLine Entity
 *
 * @property string $id
 * @property string $invoice_id
 * @property \Cake\I18n\FrozenDate|null $warning_1
 * @property \Cake\I18n\FrozenDate|null $warning_2
 * @property \Cake\I18n\FrozenDate|null $warning_3
 * @property \Cake\I18n\FrozenDate $warning_1_sent
 * @property \Cake\I18n\FrozenDate $warning_2_sent
 * @property \Cake\I18n\FrozenDate $warning_3_sent
 *
 * @property \App\Model\Entity\Invoice $invoice
 * @property \App\Model\Entity\ClaimDunning $claim_dunning
 * @property bool $warning_1_amount
 * @property bool $warning_2_amount
 * @property bool $warning_3_amount
 * @property string|null $warning_1_file_path
 * @property string|null $warning_2_file_path
 * @property string|null $warning_3_file_path
 * @property string|null $email_text
 * @property string|null $cc_email
 * @property string|null $bcc_email
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ClaimDunning extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'invoice_id' => true,
        'warning_1' => true,
        'warning_2' => true,
        'warning_3' => true,
        'warning_1_file_path' => true,
        'warning_2_file_path' => true,
        'warning_3_file_path' => true,
        'warning_1_amount' => true,
        'warning_2_amount' => true,
        'warning_3_amount' => true,
        'email_text' => true,
        'cc_email' => true,
        'bcc_email' => true,
    ];
}
