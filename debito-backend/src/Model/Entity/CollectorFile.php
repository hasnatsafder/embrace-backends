<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CollectorFile Entity
 *
 * @property string $id
 * @property string $collector_file_type_id
 * @property string $name
 * @property string $extension
 * @property string $mime_type
 * @property int $size
 * @property string $dir
 * @property \Cake\I18n\FrozenTime|null $completed
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\CollectorFileType $collector_file_type
 * @property \App\Model\Entity\ClaimAction[] $claim_actions
 * @property \App\Model\Entity\ClaimFinancial[] $claim_financials
 */
class CollectorFile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'collector_file_type_id' => true,
        'name' => true,
        'extension' => true,
        'mime_type' => true,
        'size' => true,
        'dir' => true,
        'completed' => true,
    ];
}
