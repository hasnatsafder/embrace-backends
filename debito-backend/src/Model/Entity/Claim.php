<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Claim Entity
 *
 * @property string $id
 * @property string $created_by_id
 * @property string|null $modified_by_id
 * @property string|null $account_id
 * @property string|null $debtor_id
 * @property string|null $claim_phase_id
 * @property string|null $collector_id
 * @property string|null $dunning_configuration_id
 * @property string|null $customer_reference
 * @property string|null $collector_reference
 * @property integer|null $fi_payment_reference
 * @property string|null $fi_check_digit
 * @property string|null $dispute
 * @property \Cake\I18n\FrozenTime|null $completed
 * @property \Cake\I18n\FrozenTime|null $synced
 * @property \Cake\I18n\FrozenTime|null $ended_at
 * @property \Cake\I18n\FrozenTime|null $frozen_until
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $created_by
 * @property \App\Model\Entity\User|null $modified_by
 * @property \App\Model\Entity\Account|null $account
 * @property \App\Model\Entity\Debtor|null $debtor
 * @property \App\Model\Entity\ClaimPhase|null $claim_phase
 * @property \App\Model\Entity\Collector|null $collector
 * @property \App\Model\Entity\ClaimAction[] $claim_actions
 * @property \App\Model\Entity\ClaimFinancial[] $claim_financials
 * @property \App\Model\Entity\ClaimFreeze[] $claim_freezes
 * @property \App\Model\Entity\ClaimLine[] $claim_lines
 * @property \Cake\I18n\FrozenTime|null $approved
 * @property string|null $currency_id
 * @property bool $on_hold
 * @property string|null $old_id
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Currency|null $currency
 * @property bool|null $rejected_is_public
 * @property string|null $rejected_reason
 * @property \App\Model\Entity\DebtorLawyer|null $debtor_lawyer
 * @property \App\Model\Entity\ClaimTransaction[] $claim_transactions
 */
class Claim extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'debtor_id' => true,
        'created_by_id' => true,
        'account_id' => true,
        'claim_phase_id' => true,
        'dunning_configuration_id' => true,
        'customer_reference' => true,
        'collector_reference' => true,
        'collector_id' => true,
        'dispute' => true,
        'completed' => true,
        'synced' => true,
        'created' => true,
        'ended_at' => true,
        'frozen_until' => false,
        'rejected_is_public' => true,
        'rejected_reason' => true,
        'on_hold' => true,
        'old_id' => true,
        'currency_id' => true,
    ];

    /**
     * Checks whether there is already a specific claim action type added to this claim
     *
     * @param string $claimActionType
     * @return bool
     */
    public function hasClaimActionOfType(string $claimActionType): bool
    {
        $claimActionsTable = $this->getTableLocator()->get('ClaimActions');

        $claimLineType = $claimActionsTable->ClaimActionTypes->getFromIdentifier($claimActionType);

        return $claimActionsTable->exists([
            'claim_id' => $this->id,
            'claim_action_type_id' => $claimLineType->id,
        ]);
    }

    /**
     * @return string
     */
    public function getOrGeneratePaymentFiCode(): string
    {
        if ($this->fi_payment_reference !== null) {
            if ($this->fi_check_digit === null) {
                throw new \RuntimeException('Something went wrong here...');
            }
            return $this->generatedFullFiCode();
        }

        /** @var \App\Model\Table\ClaimsTable $claimsTable */
        $claimsTable = $this->getTableLocator()->get('Claims');
        $this->fi_payment_reference = $claimsTable->getNextFiPaymentReference();
        $paddedPaymentReference = sprintf('%014d', $this->fi_payment_reference);
        $this->fi_check_digit = $claimsTable->generateFiCheckCipher($paddedPaymentReference);
        $claimsTable->saveOrFail($this);
        return sprintf('%s%s', $paddedPaymentReference, $this->fi_check_digit);
    }

    /**
     * public function generate fullPaymentCode
     */
    public function generatedFullFiCode() {
        $paddedPaymentReference = sprintf('%014d', $this->fi_payment_reference);
        return "+71<" . sprintf('%s%s', $paddedPaymentReference, $this->fi_check_digit) . " + 84316865";
    }
}
