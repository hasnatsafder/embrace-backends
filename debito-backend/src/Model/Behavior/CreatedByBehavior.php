<?php
declare(strict_types=1);

namespace App\Model\Behavior;

use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\RulesChecker;

/**
 * CreatedBy behavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior|null
 */
class CreatedByBehavior extends Behavior
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        if ($this->getTable()->hasField('modified_by_id')) {
            $this->getTable()->belongsTo('ModifiedBy', [
                'foreignKey' => 'modified_by_id',
                'joinType' => $this->getTable()->getSchema()->isNullable('modified_by_id') ? 'LEFT' : 'INNER',
                'className' => 'Users',
                'finder' => 'withTrashed',
            ]);
        }

        if ($this->getTable()->hasField('account_id') && !$this->getTable()->hasAssociation('Accounts')) {
            $this->getTable()->belongsTo('Accounts', [
                'foreignKey' => 'account_id',
                'joinType' => $this->getTable()->getSchema()->isNullable('account_id') ? 'LEFT' : 'INNER',
                'finder' => 'withTrashed',
            ]);
        }

        $this->getTable()->belongsTo('CreatedBy', [
            'foreignKey' => 'created_by_id',
            'joinType' => $this->getTable()->getSchema()->isNullable('created_by_id') ? 'LEFT' : 'INNER',
            'className' => 'Users',
            'finder' => 'withTrashed',
        ]);

        //We can use this to manually set settings - otherwise we have sane defaults
        if (!empty($config['footprint'])) {
            $this->getTable()->addBehavior('Muffin/Footprint.Footprint', $config['footprint']);

            return;
        }

        $footprintEvents = [];
        $footprintPropertiesMap = [];

        if ($this->getTable()->hasField('created_by_id')) {
            $footprintEvents['Model.beforeSave']['created_by_id'] = 'new';
            $footprintPropertiesMap['modified_by_id'] = '_footprint.id';
        }

        if ($this->getTable()->hasField('modified_by_id')) {
            $footprintEvents['Model.beforeSave']['modified_by_id'] = 'always';
            $footprintPropertiesMap['modified_by_id'] = '_footprint.id';
        }

        if ($this->getTable()->hasField('account_id')) {
            if ($this->getConfig('setAccountIdOnNew') === true) {
                $footprintEvents['Model.beforeSave']['account_id'] = 'new';
                $footprintPropertiesMap['account_id'] = '_footprint.active_account_id';
            }

            if ($this->getConfig('setAccountIdOnAlways') === true) {
                $footprintEvents['Model.beforeSave']['account_id'] = 'always';
                $footprintPropertiesMap['account_id'] = '_footprint.active_account_id';
            }
        }

        if (!empty($footprintEvents) || $footprintPropertiesMap) {
            $this->getTable()->addBehavior('Muffin/Footprint.Footprint', [
                'events' => $footprintEvents,
                'propertiesMap' => $footprintPropertiesMap,
            ]);
        }
    }

    public function buildRules(Event $event, RulesChecker $rules): RulesChecker
    {
        if ($this->getTable()->hasField('modified_by_id')) {
            $rules->add($rules->existsIn(['modified_by_id'], 'ModifiedBy'));
        }
        $rules->add($rules->existsIn(['created_by_id'], 'CreatedBy'));

        return $rules;
    }
}
