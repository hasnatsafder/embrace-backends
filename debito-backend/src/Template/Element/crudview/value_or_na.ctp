<?php
/**
 * @var \App\View\AppView $this
 * @var string $value
 */
declare(strict_types=1);
?>

<?php if (!empty($value)): ?>
    <?= $value ?>
<?php else: ?>
    <span class="label label-default"><?= __('N/A') ?></span>
<?php endif; ?>
