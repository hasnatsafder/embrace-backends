<?php

declare(strict_types=1);

namespace App\Utility;

use App\Model\Entity\Account;
use App\Model\Entity\Currency;
use App\Model\Entity\Debtor;
use App\Model\Entity\ErpIntegration;
use App\Model\Entity\InvoiceStatus;
use App\Model\Table\ErpIntegrationsTable;
use App\Model\Table\InvoicesTable;
use Cake\I18n\FrozenDate;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use EconomyPie\Debtors\DebtorData;
use EconomyPie\Debtors\DebtorsAdapterFactory;
use EconomyPie\Invoices\InvoiceAdapterInterface;
use EconomyPie\Invoices\InvoiceData;
use EconomyPie\Invoices\InvoicesAdapterFactory;
use RuntimeException;
use WyriHaximus\FlyPie\FilesystemRegistry;
use App\Utility\NotifyUser;

class ErpIntegrations
{
    /**
     * @param string $erpIntegrationId
     * @throws \Exception
     */
    public static function syncErpInvoices(string $erpIntegrationId): bool
    {
        /** @var \App\Model\Table\ErpIntegrationsTable $erpIntegrationsTable */
        $erpIntegrationsTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
        $erpIntegration = $erpIntegrationsTable->get($erpIntegrationId);

        assert($erpIntegrationsTable instanceof ErpIntegrationsTable);
        assert($erpIntegration instanceof ErpIntegration);

        $authData = $erpIntegration->getAuthData();
        $invoicesAdapter = (new InvoicesAdapterFactory())->build($erpIntegration->vendor_identifier, $authData);
        if ($erpIntegration->stage == "" || $erpIntegration->stage == null || $erpIntegration->stage == "NOT_SYNCED"){
            // get invoices for just last 3 years
            $date = FrozenDate::today();
            $date = $date->subYears(3);
            $invoices = $invoicesAdapter->getAll(['unpaid' => true], $date);

            // delete ERP integration if error found
            if (isset($invoices['error'])) {
                return false;
            }
            static::createInvoices($invoices, $erpIntegration->account_id, $erpIntegration->id, $invoicesAdapter, []);
            $erpIntegration->stage = 'UNPAID_SYNCED';
            $erpIntegration->modified_by_id = $erpIntegration->modified_by_id;
            $erpIntegrationsTable->save($erpIntegration);
            TableRegistry::getTableLocator()->get('Queue.QueuedJobs')
                ->createJob('Erp', ['erpIntegration_id' => $erpIntegration->id],
            ['reference' => $erpIntegration->id]);

            // send email to customers only this time that invoices are synced
            $account = TableRegistry::getTableLocator()->get('Accounts')->get($erpIntegration->account_id);
            NotifyUser::sendIntegrationConfirmationEmail($account);
        }
        else if ($erpIntegration->stage == "UNPAID_SYNCED") {
            // get invoices for just last 3 years
            $date = FrozenDate::today();
            $date = $date->subMonths(18);
            $paidInvoices = $invoicesAdapter->getAll(['paid' => true], $date);
            // delete ERP integration if error found
            if (isset($paidInvoices['error'])) {
                return false;
            }
            static::createInvoices([], $erpIntegration->account_id, $erpIntegration->id, $invoicesAdapter, $paidInvoices);
            $erpIntegration->stage = 'PAID_SYNCED';
            $erpIntegration->modified_by_id = $erpIntegration->modified_by_id;
            $erpIntegrationsTable->save($erpIntegration);
        }
        else if ($erpIntegration->stage == "PAID_SYNCED") {

            // get last synced invoices date
            $last_invoice = TableRegistry::getTableLocator()->get('Invoices')->find('all')
                    ->where(['erp_integration_id' => $erpIntegration->id])
                    ->orderDesc('issue_date')
                    ->first();
            if ($last_invoice){
                $last_invoice_date = new FrozenDate($last_invoice->issue_date);
                $last_invoice_date = $last_invoice_date->subYear();
            }
            else{
                // get invoices for just last 2 years
                $last_invoice_date = FrozenDate::today();
                $last_invoice_date = $last_invoice_date->subYears(10);
            }
            $invoices = $invoicesAdapter->getAll(['unpaid' => true], $last_invoice_date);
            $paidInvoices = $invoicesAdapter->getAll(['paid' => true], $last_invoice_date);
            // delete ERP integration if error found
            if (isset($paidInvoices['error'])) {
                return false;
            }

            static::createInvoices($invoices, $erpIntegration->account_id, $erpIntegration->id, $invoicesAdapter, $paidInvoices);
        }

        // update status of invoices
        //static::updateInvoicesStatus($paidInvoices, $erpIntegration->account_id, $erpIntegration->id, $invoicesAdapter);
        return true;
    }

    /**
     * Saves invoices from ERP integration(s)' data
     *
     * @param array $invoices
     * @param string $accountId
     * @param string $erpIntegrationId
     * @return \App\Model\Entity\Invoice[]|bool
     * @throws \Exception
     */
    private static function createInvoices(array $invoices, string $accountId, string $erpIntegrationId, InvoiceAdapterInterface $invoiceAdapter, Array $paidInvoices)
    {
        if (!is_array($invoices)) {
            $invoices = [$invoices];
        }
        // if only unpaid invoices given
        if ($invoices == []){
            $invoices = $paidInvoices;
        }
        // if both proved
        if ($invoices != [] && $paidInvoices != []){
            $invoices = array_merge($invoices, $paidInvoices);
        }
        $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
        $paidInvoicesTable = TableRegistry::getTableLocator()->get('PaidInvoices');
        assert($invoicesTable instanceof InvoicesTable);
        $invoicesData = [];

        $account = static::_getAccount($accountId);
        // mark all paid invoices as paid in database
        static::updateInvoicesStatus($paidInvoices, $account->id, $erpIntegrationId);

        foreach ($invoices as $invoice) {
            assert($invoice instanceof InvoiceData);
            if (static::_invoiceExists($erpIntegrationId, $invoice->id, $account)) {
                Log::debug('Invoice already exists: ' . $invoice->id);
                continue;
            }
            if (static::_paidInvoiceExists($erpIntegrationId, $invoice->id, $account)) {
                Log::debug('Paid Invoice already exists: ' . $invoice->id);
                continue;
            }

            // if invoice is paid then dont download it, else download
            $paid = false;
            foreach ($paidInvoices as $paidInvoice) {
                if ($paidInvoice->invoiceNumber == $invoice->invoiceNumber) {
                    $paid = true;
                }
            }

            $debtor = static::_getDebtor($erpIntegrationId, $invoice->debtorNumber, $account);
            // if paid get paid status, else get pending
            $placement = $paid ? 4 : 0;
            $invoiceStatus = static::_getInvoiceStatus($placement);
            $currency = static::_getCurrency($invoice);
            $invoicesData = $paid ? $paidInvoicesTable->newEntity() : $invoicesTable->newEntity();
            $invoicesData->account_id = $account->id;
            $invoicesData->invoice_status_id = $invoiceStatus->id;
            $invoicesData->currency_id = $currency->id;
            $invoicesData->erp_integration_id = $erpIntegrationId;
            $invoicesData->customer_name = $debtor->_getFullName();
            $invoicesData->erp_integration_foreign_key = $invoice->id;
            $invoicesData->debtor_id = $debtor->id;
            $invoicesData->invoice_number = $invoice->invoiceNumber;
            $invoicesData->issue_date = new FrozenDate($invoice->issueDate);
            $invoicesData->due_date = new FrozenDate($invoice->dueDate);
            $invoicesData->net_amount = $invoice->netAmount;
            $invoicesData->gross_amount = $invoice->grossAmount;
            $invoicesData->pdf_file_path = $paid ? "" : static::saveInvoiceFile($invoice, $invoiceAdapter);
            $paid ? $paidInvoicesTable->save($invoicesData) : $invoicesTable->save($invoicesData);
        }
        return $invoices;
    }

    /**
     * @param $accountId
     * @return \App\Model\Entity\Account
     */
    private static function _getAccount($accountId): Account
    {
        return TableRegistry::getTableLocator()->get('Accounts')
            ->find()
            ->enableBufferedResults(false)
            ->where(['Accounts.id' => $accountId])
            ->firstOrFail();
    }

    private static function _invoiceExists(string $erpIntegrationId, string $invoiceId, Account $account): bool
    {
        return !TableRegistry::getTableLocator()->get('Invoices')->find()
            ->select(['Invoices.id'])
            // ->enableHydration(false)
            ->where([
                'Invoices.account_id' => $account->id,
                'Invoices.erp_integration_id' => $erpIntegrationId,
                'Invoices.erp_integration_foreign_key' => $invoiceId,
            ])
            ->isEmpty();
    }

    private static function _paidInvoiceExists(string $erpIntegrationId, string $invoiceId, Account $account): bool
    {
        return !TableRegistry::getTableLocator()->get('PaidInvoices')->find()
            ->select(['PaidInvoices.id'])
            // ->enableHydration(false)
            ->where([
                'PaidInvoices.account_id' => $account->id,
                'PaidInvoices.erp_integration_id' => $erpIntegrationId,
                'PaidInvoices.erp_integration_foreign_key' => $invoiceId,
            ])
            ->isEmpty();
    }

    /**
     * @param string $erpIntegrationId
     * @param string $debtorId
     * @param \App\Model\Entity\Account $account
     * @return \App\Model\Entity\Debtor
     */
    private static function _getDebtor(string $erpIntegrationId, string $debtorId, Account $account): Debtor
    {
        return TableRegistry::getTableLocator()->get('Debtors')->findOrCreate([
            'erp_integration_id' => $erpIntegrationId,
            'erp_integration_foreign_key' => $debtorId,
            'account_id' => $account->id,
        ], function (Debtor $debtor) use ($erpIntegrationId, $debtorId) {
            $debtorData = static::getDebtorErpData($erpIntegrationId, (string) $debtorId);

            if (!empty($debtorData->vatNumber)) {
                $company = $debtorData->name;
                $first_name = null;
                $last_name = null;
                $vatNumber = $debtorData->vatNumber;
                $isCompany = true;
            } else {
                $company = null;
                $vatNumber = null;
                // get the last name after space and store that as last name
                $arr = explode(" ", $debtorData->name);
                if (count($arr) > 1) {
                    $first_name = str_replace(strval(end($arr)), "", $debtorData->name);
                    $last_name = end($arr);
                } else {
                    $first_name = $debtorData->name;
                    $last_name = "";
                }

                $isCompany = false;
            }

            // set Debtor country
            if (isset($debtorData->country) && $debtorData->country != null) {
                $debtorCountry = TableRegistry::getTableLocator()->get('Countries')->find()->where(['name' => $debtorData->country])->orWhere(['iso_code' => $debtorData->country])->first();
            }

            $debtorCountryId = null;
            if (isset($debtorCountry)) {
                $debtorCountryId = $debtorCountry->id;
            }

            $debtor->set([
                'erp_integration_id' => $erpIntegrationId,
                'vat_number' => $vatNumber,
                'company_name' => $company,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'address' => $debtorData->streetName,
                'zip_code' => $debtorData->zipCode,
                'city' => $debtorData->city,
                'phone' => $debtorData->phone,
                'email' => $debtorData->email,
                'is_company' => $isCompany,
                'country_id' => $debtorCountryId,
            ]);
        });
    }

    /**
     * @param $erpIntegrationId
     * @param string $erpId
     * @return \EconomyPie\Debtors\DebtorData
     */
    private static function getDebtorErpData($erpIntegrationId, string $erpId): DebtorData
    {
        /** @var \App\Model\Table\ErpIntegrationsTable $erpIntegrationsTable */
        $erpIntegrationsTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
        $erpIntegration = $erpIntegrationsTable->get($erpIntegrationId);

        $authData = $erpIntegration->getAuthData();
        $debtorAdapter = (new DebtorsAdapterFactory())->build($erpIntegration->vendor_identifier, $authData);

        return $debtorAdapter->get($erpId);
    }

    /**
     * @param \EconomyPie\Invoices\InvoiceData $invoice
     * @return \App\Model\Entity\InvoiceStatus
     */
    private static function _getInvoiceStatus($placement = 0): InvoiceStatus
    {
        return TableRegistry::getTableLocator()->get('InvoiceStatuses')->find()
            ->enableBufferedResults(false)
            ->where(['InvoiceStatuses.placement' => $placement])
            ->firstOrFail();
    }

    /**
     * @param \EconomyPie\Invoices\InvoiceData $invoice
     * @return \App\Model\Entity\Currency
     */
    private static function _getCurrency(InvoiceData $invoice): Currency
    {
        $currency = TableRegistry::getTableLocator()->get('Currencies')
            ->find()
            ->where(['Currencies.iso_code' => $invoice->currency])
            ->cache(function (Query $q) {
                return 'currencies-' . md5(serialize($q->clause('where')));
            })
            ->first();

        if ($currency === null) {
            throw new RuntimeException('Currency not found: ' . $invoice->currency);
        }

        return $currency;
    }

    /**
     * Saves the file from the invoice to filesystem from ERP API
     *
     * @param \EconomyPie\Invoices\InvoiceData $invoiceData
     * @param \EconomyPie\Invoices\InvoiceAdapterInterface $invoiceAdapter
     * @return string Filename
     * @throws \League\Flysystem\FileExistsException
     */
    private static function saveInvoiceFile(InvoiceData $invoiceData, InvoiceAdapterInterface $invoiceAdapter): string
    {
        $stream = $invoiceAdapter->getPdf($invoiceData->pdfLink);
        $filesystem = FilesystemRegistry::retrieve('erp_invoices');
        $fileName = Text::uuid() . '.pdf';

        if (!$filesystem->writeStream($fileName, $stream)) {
            throw new RuntimeException('Could not save file: ' . $fileName);
        }

        is_resource($stream) && fclose($stream);

        return $fileName;
    }

    /**
     * @param string $erpIntegrationId
     * @throws \Exception
     */
    public static function getCount(string $erpIntegrationId)
    {
        /** @var \App\Model\Table\ErpIntegrationsTable $erpIntegrationsTable */
        $erpIntegrationsTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
        $erpIntegration = $erpIntegrationsTable->get($erpIntegrationId);

        assert($erpIntegrationsTable instanceof ErpIntegrationsTable);
        assert($erpIntegration instanceof ErpIntegration);

        $authData = $erpIntegration->getAuthData();
        $invoicesAdapter = (new InvoicesAdapterFactory())->build($erpIntegration->vendor_identifier, $authData);
        $invoicesCount = $invoicesAdapter->getCount();

        // return false for count
        if ($invoicesCount['error']) {
            return false;
        }

        return $invoicesCount['total'];
    }

    /**
     * Updates the paid stauts of invoices from ERP integration(s)' data
     *
     * @param array $invoices
     * @param string $accountId
     * @param string $erpIntegrationId
     * @return \App\Model\Entity\Invoice[]|bool
     * @throws \Exception
     */
    private static function updateInvoicesStatus(array $invoicesPaid, string $accountId, string $erpIntegrationId)
    {
        $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
        $paidInvoicesTable = TableRegistry::getTableLocator()->get('PaidInvoices');
        $invoicePaidStatus = TableRegistry::getTableLocator()->get('InvoiceStatuses')->find()
            ->where(['identifier' => 'paid'])
            ->firstOrFail();
        foreach ($invoicesPaid as $invoiceErp) {
            $paid_invoice = $invoicesTable->find('all')
                ->where([
                    'erp_integration_id' => $erpIntegrationId,
                    'account_id' => $accountId,
                    'erp_integration_foreign_key' => $invoiceErp->id
                ])
                ->first();
            if ($paid_invoice) {
                $newPaidInvoice = $paidInvoicesTable->newEntity();
                $newPaidInvoice->account_id = $paid_invoice->account_id;
                $newPaidInvoice->invoice_status_id = $invoicePaidStatus->id;
                $newPaidInvoice->currency_id = $paid_invoice->currency_id;
                $newPaidInvoice->erp_integration_id = $paid_invoice->erp_integration_id;
                $newPaidInvoice->erp_integration_foreign_key = $paid_invoice->erp_integration_foreign_key;
                $newPaidInvoice->debtor_id = $paid_invoice->debtor_id;
                $newPaidInvoice->invoice_number = $paid_invoice->invoice_number;
                $newPaidInvoice->issue_date = $paid_invoice->issue_date;
                $newPaidInvoice->due_date = $paid_invoice->due_date;
                $newPaidInvoice->hidden_until = $paid_invoice->hidden_until;
                $newPaidInvoice->net_amount = $paid_invoice->net_amount;
                $newPaidInvoice->gross_amount = $paid_invoice->gross_amount;
                $newPaidInvoice->pdf_file_path = $paid_invoice->pdf_file_path;
                $newPaidInvoice->converted_to_claim = $paid_invoice->converted_to_claim;
                $newPaidInvoice->snoozed = $paid_invoice->snoozed;
                $newPaidInvoice->created = $paid_invoice->created;
                $newPaidInvoice->modified = $paid_invoice->modified;
                $newPaidInvoice->deleted = $paid_invoice->deleted;
                $paidInvoicesTable->save($newPaidInvoice);
                if ($paidInvoicesTable->save($newPaidInvoice)) {
                    Log::info('Invoice marked paid '. $paid_invoice->id);
                }
                // delete paid invoice from invoice table
                if ($invoicesTable->delete($paid_invoice)) {
                    Log::info('Invoice is '. $paid_invoice->id);
                }
                else {
                    Log::error("Invoice " . $paid_invoice->id . " could not be updated");
                }
            }
        }
        return true;
    }
}
