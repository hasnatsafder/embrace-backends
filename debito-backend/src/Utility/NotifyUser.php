<?php
declare(strict_types=1);

namespace App\Utility;

use App\Model\Entity\Account;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class NotifyUser
{

    private $email;
    private $subject;
    private $body;
    private $account_id;
    private $claim;
    private $viewVars;
    private $template;
    private $sendBCC;

    public function __construct()
    {
        $this->email = new Email('sparkpost');
        $this->subject = "";
        $this->body = "";
        $this->account_id = "";
        $this->viewVars = "";
        $this->template = "";
        $this->sendBCC = false;
    }

    /**
     * Notify user for rejected claims
     *
     * @param String $rejected_reason
     * @return bool
     */

    public static function sendRejected($claim, $subject, $body)
    {
        $instance = new NotifyUser();
        $instance->body = $body;
        $instance->account_id = $claim->account_id;
        $instance->subject = $subject;
        $instance->sendBCC = true;

        // get currency symbol
        $currencyTable = TableRegistry::getTableLocator()->get('Currencies');
        $currency = $currencyTable->get($claim->currency_id);
        $total = HelperFunctions::getTotalClaimSum($claim);

        $instance->viewVars = [
            'claim' => $claim,
            'data' => $body,
            'total' => HelperFunctions::convertToDanish($total),
            'currency' => $currency,
            'frontendUrl' => Configure::read('Frontendurl'),
        ];
        $instance->template = 'debtor_bankrupt';
        $instance->sendEmail();
    }

    /**
     * Send the email to user and account if provided
     *
     * @param $message
     */
    private function sendEmail()
    {
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');

        $account = $accountsTable->get($this->account_id);
        $user = $usersTable->get($account->created_by_id);

        $this->email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to($user->email)
            ->subject($this->subject)
            ->viewVars($this->viewVars)
            ->template($this->template)
            ->emailFormat('html');
        $this->sendBCC ? $this->email->bcc(Configure::read('Adminemail')) : false;
        if ($account->email && $account->email !== $user->email) {
            $this->email->cc($account->email);
        }
        $this->email->send();
    }

    /**
     * Send email to account user that scripts are fetch now.
     * @param $account
     * @return bool
     */
    public static function sendIntegrationConfirmationEmail($account): bool
    {
        $instance = new NotifyUser();
        $instance->account_id = $account->id;
        $instance->subject = "Din konto er klar til brug!";

        $instance->template = 'integration_confirmation';
        $instance->sendEmail();

        return true;
    }
}
