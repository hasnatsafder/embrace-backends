<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Datasource\Exception\MissingModelException;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\View\View;
use Crud\Controller\ControllerTrait;
use Muffin\Footprint\Auth\FootprintAwareTrait;
use Search\Model\Behavior\SearchBehavior;
use UnexpectedValueException;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @property \Crud\Controller\Component\CrudComponent $Crud
 * @property \Cake\Controller\Component\RequestHandlerComponent $RequestHandler
 * @property \Cake\Controller\Component\FlashComponent $Flash
 * @property \Cake\Controller\Component\AuthComponent $Auth
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    use ControllerTrait;
    use FootprintAwareTrait;

    /**
     * Whether or not to treat a controller as
     * if it were an admin controller or not.
     *
     * Used to turn CrudView on and off at a class-level
     *
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * A list of actions where the CrudView.View
     * listener should be enabled. If an action is
     * in this list but `isAdmin` is false, the
     * action will still be rendered via CrudView.View
     *
     * @var array
     */
    protected $adminActions = [];

    /**
     * A list of actions that should be allowed for
     * authenticated users
     *
     * @var array
     */
    protected $allowedActions = [];

    /**
     * A list of actions where the Crud.SearchListener
     * and Search.PrgComponent should be enabled
     *
     * @var array
     */
    protected $searchActions = ['index', 'lookup'];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.Add',
                'Crud.Edit',
                'Crud.View',
                'Crud.Delete',
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog',
                'Crud.RelatedModels',
                'Crud.Redirect',
                'CrudJsonApi.JsonApi',
            ],
        ]);

        if ($this->isAdmin || in_array($this->getRequest()->getParam('action'), $this->adminActions)) {
            $this->Crud->addListener('CrudView.View');
        }

        if (in_array($this->getRequest()->getParam('action'), $this->searchActions) && $this->modelClass !== null) {
            [$plugin, $tableClass] = pluginSplit($this->modelClass);
            try {
                if ($this->{$tableClass}->behaviors()->get('Search') instanceof SearchBehavior) {
                    $this->Crud->addListener('Crud.Search');
                    $this->loadComponent('Search.Prg', [
                        'actions' => $this->searchActions,
                    ]);
                }
            } catch (MissingModelException $e) {
            } catch (UnexpectedValueException $e) {
            }
        }
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

//        $this->Crud->on('beforePaginate', function (Event $event) {
//            $repository = $event->subject()->query->repository();
//            $primaryKey = $repository->primaryKey();
//
//            if (!is_array($primaryKey)) {
//                $this->paginate['order'] = [
//                    sprintf('%s.%s', $repository->alias(), $primaryKey) => 'asc',
//                ];
//            }
//        });

        if ($this->Crud->isActionMapped()) {
            $this->Crud->action()->setConfig('scaffold.sidebar_navigation', false);
            $this->Crud->action()->setConfig('scaffold.brand', Configure::read('App.name'));
            $this->Crud->action()->setConfig('scaffold.site_title', Configure::read('App.name'));
            if (method_exists($this, 'getUtilityNavigation')) {
                $this->Crud->action()->setConfig('scaffold.utility_navigation', $this->getUtilityNavigation());
            }
        }

        $isRest = in_array($this->response->getType(), ['application/json', 'application/xml']);
        $isAdmin = $this->isAdmin || in_array($this->getRequest()->getParam('action'), $this->adminActions);

        if (!$isRest && $isAdmin && empty($this->getRequest()->getParam('_ext'))) {
            $this->viewBuilder()->setClassName('CrudView\View\CrudView');
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        $isRest = in_array($this->getResponse()->getType(), ['application/json', 'application/xml']);

        if (!array_key_exists('_serialize', $this->viewVars) && $isRest) {
            $this->set('_serialize', true);
        }

        // In controller action or preferably in beforeRender()
        $this->viewBuilder()->setHelpers([
            'CrudView.CrudView' => [
                'fieldFormatters' => [
                    'datetime' => function (string $name, $value, Entity $entity, array $options, View $View) {
                        return !empty($value) ? $View->Time->i18nFormat($value) : '<span class="label label-info">N/A</span>';
                    },
                ],
            ],
        ]);
    }

    /**
     * Check if the provided user is authorized for the request.
     *
     * @param array|\ArrayAccess|null $user The user to check the authorization of.
     *   If empty the user fetched from storage will be used.
     * @return bool True if $user is authorized, otherwise false
     */
    public function isAuthorized($user = null)
    {
        $action = $this->getRequest()->getParam('action');
        if (in_array($action, $this->allowedActions)) {
            return true;
        }

        return false;
    }
}
