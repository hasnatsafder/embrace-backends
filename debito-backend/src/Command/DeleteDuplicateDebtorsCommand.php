<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;

/**
 * CreateUser command.
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class DeleteDuplicateDebtorsCommand extends Command
{
    public $modelClass = 'App.Debtors';
    protected $claimsTable;
    protected $accountsTable;
    protected $debtorsTable;

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    public function __construct()
    {
        $this->claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        libxml_use_internal_errors(true);
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        Log::notice(" ======== Starting deletion of same debtors ======== ");
        $accounts = $this->accountsTable->find('all');

        foreach ($accounts as $account) {
          Log::notice("Working on account " . $account->name);
          $debtors = $this->debtorsTable->find('all')->where(['account_id' => $account->id]);
          foreach ($debtors as $debtor) {
            // get debtor if not found it implies its been deleted already
            $debtor_get = $this->debtorsTable->find('all')->where(['id' => $debtor->id])->first();
            if (!$debtor_get){
              Log::error("Debtor not found");
              continue;
            }

            if ($debtor->vat_number && $debtor->vat_number == "36937378") {
              echo "found";
            }

            // 1 find all debtors with same vat_number as this 
            if ($debtor->vat_number) {
              $same_debtors = $this->debtorsTable->find('all')
              ->where(['account_id' => $account->id, 'id <>' => $debtor->id, 'vat_number' => $debtor->vat_number]);
            }
            else {
              $same_debtors = $this->debtorsTable->find('all')
              ->where(['account_id' => $account->id, 'id <>' => $debtor->id, 'first_name' => $debtor->first_name, 'last_name' => $debtor->last_name]);
            }

            // delete same debtor if no claim found
            foreach ($same_debtors as $same_debtor) {
              $claims = $this->claimsTable->find('all')->where(['debtor_id' => $same_debtor->id]);
              if ($claims->count() == 0) {
                Log::error("deleting debtor " . $same_debtor->id);
                $this->debtorsTable->delete($same_debtor);
              }
            }
          }
        }

        Log::notice("===== Ending debtor deletion =====");
        return static::CODE_SUCCESS;
    }
}
