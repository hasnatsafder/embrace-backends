<?php

namespace App\Command;

use App\Model\Entity\Account;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\Query;
use Psr\Log\LogLevel;

/**
 * CreateNotificationSummaries command.
 *
 * @property \App\Model\Table\NotificationSummariesTable $NotificationSummaries
 */
class CreateNotificationSummariesCommand extends Command
{
    use MailerAwareTrait;

    public $modelClass = 'App.NotificationSummaries';

    protected $validNotificationIntervals = [
        'instantly',
        'daily',
        'weekly',
        'monthly',
    ];

    protected $notificationsHandled = 0;

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        $parser->addArgument('notification_interval', [
            'help' => 'What notification interval do you want to trigger here - should be one of: "instant", "daily", "weekly" or "monthly"',
        ]);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io): ?int
    {
        $notificationInterval = $args->getArgument('notification_interval');

        if (empty($notificationInterval) || !in_array($notificationInterval, $this->validNotificationIntervals)) {
            throw new \InvalidArgumentException(
                sprintf('Notification interval is not found in allowed range: "%s"', $notificationInterval)
            );
        }

        $notificationIntervalToUse = $this->getAccountNotificationInterval($notificationInterval);

        $notifications = $this->NotificationSummaries->Notifications->find('NotSent')
            ->select(['Notifications.user_id'])
            ->innerJoinWith('Users.Accounts', function (Query $q) use ($notificationIntervalToUse) {
                return $q->where(['Accounts.notification_summary_interval' => $notificationIntervalToUse]);
            })
            ->group(['Notifications.user_id'])
            ->disableHydration()
            ->extract('user_id');

        if ($notifications->count() === 0) {
            $this->log(
                'No notifications to process',
                LogLevel::DEBUG
            );

            return null;
        }

        $this->log(
            sprintf('Processing %d users\' notifications now', $notifications->count()),
            LogLevel::DEBUG
        );

        $notifications->each(function (string $notificationUserId) {
            $this->handleNotificationsForUser($notificationUserId);
        });

        return null;
    }

    /**
     * @param string $notificationInterval
     * @return string
     */
    private function getAccountNotificationInterval(string $notificationInterval): string
    {
        switch ($notificationInterval) {
            case 'instantly':
                return Account::NOTIFICATION_SUMMARY_INTERVAL_INSTANT;
            case 'daily':
                return Account::NOTIFICATION_SUMMARY_INTERVAL_DAILY;
            case 'weekly':
                return Account::NOTIFICATION_SUMMARY_INTERVAL_WEEKLY;
            case 'monthly':
                return Account::NOTIFICATION_SUMMARY_INTERVAL_MONTHLY;
        }
    }

    protected function handleNotificationsForUser(string $notificationUserId)
    {
        $notifications = $this->NotificationSummaries->Notifications->find('NotSent')
            ->where(['Notifications.user_id' => $notificationUserId])
            ->all();

        if ($notifications->isEmpty()) {
            $this->log('There was suddenly no notification for user', LogLevel::WARNING);

            return;
        }

        $this->notificationsHandled += $notifications->count();

        $notificationSummary = $this->NotificationSummaries->newEntity([
            'user_id' => $notificationUserId,
        ]);

        $this->NotificationSummaries->saveOrFail($notificationSummary);

        $this->NotificationSummaries->Notifications->updateAll(
            ['notification_summary_id' => $notificationSummary->id],
            ['id IN' => $notifications->extract('id')->toList()]
        );

        return true;


    }
}
