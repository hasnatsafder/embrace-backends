<?php

declare(strict_types=1);

namespace App\Command;

use App\Utility\HelperFunctions;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use CakePdf\Pdf\CakePdf;

/**
 * Class SendDunningCommand
 *
 * @package App\Command
 * @property \App\Model\Table\ClaimsTable Claims
 * @property \League\Flysystem\AdapterInterface casesFileSystem
 */
class SendDunningCommand extends Command
{
    private $claimDunningsTable;
    private $invoicesTable;
    private $debtorsTable;
    private $accountsTable;
    private $dateFormat;

    public function initialize()
    {
        parent::initialize();
        $this->invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
        $this->claimDunningsTable = TableRegistry::getTableLocator()->get('ClaimDunnings');
        $this->debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->dateFormat = "dd-MM-yyyy";
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {

        for ($i = 1; $i <= 3; $i++) {
            $claim_dunning_today = $this->claimDunningsTable->find(
                'all',
                [
                    'conditions' => [
                        'warning_' . $i => date('Y-m-d'),
                        'warning_' . $i . "_file_path is" => null,

                    ],
                ]
            )->all();
            foreach ($claim_dunning_today as $claim_dunning) {
                $charge = false;
                if ($i == 1) {
                    $charge = $claim_dunning->warning_1_amount;
                }
                if ($i == 2) {
                    $charge = $claim_dunning->warning_2_amount;
                }
                if ($i == 3) {
                    $charge = $claim_dunning->warning_3_amount;
                }
                $data = $this->generateInvoiceData($claim_dunning, $charge, $i);
                $data['warning_number'] = $i;
                $file = $this->generatePDF($data);

                // update the file path
                if ($i == 1) {
                    $claim_dunning->warning_1_file_path = $file;
                }
                if ($i == 2) {
                    $claim_dunning->warning_2_file_path = $file;
                }
                if ($i == 3) {
                    $claim_dunning->warning_3_file_path = $file;
                }
                if ($this->claimDunningsTable->save($claim_dunning)) {
                    $this->sendEmailToDebtor($claim_dunning, $file, $data);
                }
            }
        }
    }

    /**
     * Generate a new claimLine if we have check of send DKK100 else get invoice claim line number
     */
    private function generateInvoiceData($claim_dunning, $charge, $number)
    {
        $dataArray = [
            'data' => [], // the row data
            'total' => 0, // total amount of money
            'debtor_data' => [],
            'account_data',
        ];
        $invoice = $this->invoicesTable->get($claim_dunning->invoice_id);

        // build the data to be send in rows
        array_push($dataArray['data'], $this->buildRows(
            $invoice->invoice_number,
            $invoice->issue_date->i18nFormat($this->dateFormat),
            $invoice->due_date->i18nFormat($this->dateFormat),
            "Faktura",
            $invoice->gross_amount
        ));

        // get the status of previous warnings for which we charged 100 Kr
        $previous_warnings = $this->getPreviousPaidWarnings($claim_dunning);

        // set the data of previous invoices
        // fetch first warning
        if ($previous_warnings['warning_1']) {
            // build the data to be send in rows
            array_push($dataArray['data'], $this->buildRows(
                $invoice->invoice_number . "-1",
                $claim_dunning->warning_1->i18nFormat($this->dateFormat),
                $claim_dunning->warning_1->i18nFormat($this->dateFormat),
                "Rykkergebyr",
                100.00
            ));
        }
        // fetch second warning
        if ($previous_warnings['warning_2']) {
            // build the data to be send in rows
            array_push($dataArray['data'], $this->buildRows(
                $invoice->invoice_number . "-2",
                $claim_dunning->warning_2->i18nFormat($this->dateFormat),
                $claim_dunning->warning_2->i18nFormat($this->dateFormat),
                "Rykkergebyr",
                100.00
            ));
        }

        //set current warning
        if ($charge) {
            // build the data to be send in rows
            array_push($dataArray['data'], $this->buildRows(
                $invoice->invoice_number . "-" . $number,
                FrozenTime::now()->i18nFormat($this->dateFormat),
                FrozenTime::now()->i18nFormat($this->dateFormat),
                "Rykkergebyr",
                100.00
            ));
        }

        // 2- calculate total
        foreach ($dataArray['data'] as $invoiceData) {
            $dataArray['total'] += $invoiceData['amount'];
        }
        $dataArray['total'] *= 100;
        // convert to Danish formatting
        foreach ($dataArray['data'] as $key => $invoiceData) {
            //dd($dataArray['data'][$key]['amount']);
            $dataArray['data'][$key]['amount'] = HelperFunctions::convertToDanishCurrency(strval($dataArray['data'][$key]['amount']));
            $dataArray['data'][$key]['amount'] = HelperFunctions::convertToDanish(strval($dataArray['data'][$key]['amount']));
        }

        // 3- Get Debtors Data
        $dataArray['debtor_data'] = $this->debtorsTable->get($invoice->debtor_id);

        // 4- Get Accounts Data
        $dataArray['account_data'] = $this->accountsTable->get($invoice->account_id);

        return $dataArray;
    }

    /**
     * Build the rows needed
     *
     * @param String $nr
     * @param String $invoice_date
     * @param String $due_date
     * @param String $text
     * @param Float $amount
     * @return Array $rowArray
     */
    private function buildRows($nr, $invoice_date, $due_date, $text, $amount)
    {
        $rowArray = [];
        $rowArray['nr'] = $nr;
        $rowArray['issue_date'] = $invoice_date;
        $rowArray['due_date'] = $due_date;
        $rowArray['text'] = $text;
        $rowArray['amount'] = $amount;

        return $rowArray;
    }

    /**
     * Get previous warnings with 100 Kr
     *
     * @param ClaimDunning $claimdunning
     * @return Array $previous_warnings
     */
    private function getPreviousPaidWarnings($claim_dunning)
    {
        $previous_warnings = [
            'warning_1' => false,
            'warning_2' => false,
        ];
        if ($claim_dunning->warning_1_file_path && $claim_dunning->warning_1_amount) {
            $previous_warnings['warning_1'] = true;
        }
        if ($claim_dunning->warning_2_file_path && $claim_dunning->warning_2_amount) {
            $previous_warnings['warning_2'] = true;
        }

        return $previous_warnings;
    }

    /**
     * GeneratePDF and save it
     */
    private function generatePDF($invoiceData)
    {
        Configure::write('CakePdf', [
            'engine' => [
                'className' => 'CakePdf.WkHtmlToPdf',
                'binary' => '/usr/local/bin/wkhtmltopdf',
                'options' => [
                    'print-media-type' => false,
                    'outline' => true,
                    'dpi' => 96,
                ],
            ],
        ]);
        $CakePdf = new CakePdf();
        $CakePdf->template('reminder', 'default');

        $CakePdf->viewVars([
            "data" => $invoiceData['data'],
            "total" => HelperFunctions::convertToDanish($invoiceData['total']),
            "debtor" => $invoiceData['debtor_data'],
            'account' => $invoiceData['account_data'],
            'warning_number' => $invoiceData['warning_number'],
            "date" => FrozenTime::now()->i18nFormat($this->dateFormat),
        ]);
        $file_name = Text::uuid() . ".pdf";
        // Or write it to file directly
        $pdf = $CakePdf->write(WWW_ROOT . 'files' . DS . "EconomyPie" . DS . "invoices" . DS . $file_name);

        return $file_name;
    }

    /**
     * Send email to debtor with attachment
     *
     * @param ClaimDunning $claim_dunning
     * @param String $file_path
     * @param Array $dataArray
     * @return Boolean
     */
    private function sendEmailToDebtor($claim_dunning, $file_path, $dataArray)
    {
        $email = new Email('sparkpost');
        $email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to($dataArray['debtor_data']->email)
            ->subject("You have a new warning")
            ->emailFormat('html')
            ->attachments(WWW_ROOT . 'files' . DS . "EconomyPie" . DS . "invoices" . DS . $file_path);
        if ($claim_dunning->cc_email) {
            $email->cc($claim_dunning->cc_email);
        }
        if ($claim_dunning->bcc_email) {
            $email->bcc($claim_dunning->bcc_email);
        }
        $email->send($claim_dunning->email_text);
    }
}
