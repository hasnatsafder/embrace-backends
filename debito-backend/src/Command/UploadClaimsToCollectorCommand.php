<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ResultSetInterface;
use Cake\I18n\FrozenTime;
use CollectorFiles\UploadFileHandler\CaseUploaderFactory;

/**
 * Class SendClaimsToCollectorCommand
 *
 * @package App\Command
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \League\Flysystem\AdapterInterface $casesFileSystem
 */
class UploadClaimsToCollectorCommand extends Command
{

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Claims');
    }

    /**
     * @param \Cake\Console\ConsoleOptionParser $parser
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->addOptions([
            'collector' => [
                'help' => __('Collector to use'),
                'short' => 'c',
                'default' => null,
                'choices' => ['mortang', 'lindorff', 'collectia'],
                'required' => true,
            ],
        ]);

        return $parser;
    }

    /**
     * @param \Cake\Console\Arguments $args
     * @param \Cake\Console\ConsoleIo $io
     * @return int|null|void
     * @throws \League\Csv\CannotInsertRecord
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->verbose('Starting at: ' . (new FrozenTime())->i18nFormat(null, 'Europe/Copenhagen'));
        $collector = $args->getOption('collector');

        $caseUploader = CaseUploaderFactory::build($collector);

        //Fetch the claims
        $claims = $this->getClaimsToBeUploaded($collector);
        $amountOfClaims = $claims->count();

        if ($amountOfClaims === 0) {
            $io->error('No claims found. Aborting...');

            return false;
        }

        $io->verbose(sprintf('Processing %d claims', $amountOfClaims));
        $io->out();
        /** @var \Cake\Shell\Helper\ProgressHelper $progressBar */
        $progressBar = $io->helper('Progress');
        $progressBar->init([
            'total' => $amountOfClaims,
        ]);

        $caseUploader->uploadClaims($claims);

        $io->out();

        $io->success('Finished at: ' . (new FrozenTime())->i18nFormat(null, 'Europe/Copenhagen'));
    }

    protected function getClaimsToBeUploaded($collectorIdentifier): ResultSetInterface
    {
        $collector = $this->Claims->Collectors->find()
            ->where(['Collectors.identifier' => $collectorIdentifier])
            ->firstOrFail();

        return $this->Claims->find()
            ->where([
                'Claims.collector_id' => $collector->id,
                'Claims.synced IS' => null,
                'Claims.ended_at IS' => null,
                'Claims.approved IS NOT' => null,
                'Claims.completed IS NOT' => null,
            ])
            ->all();
    }
}
