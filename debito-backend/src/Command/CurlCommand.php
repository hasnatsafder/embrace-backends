<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use DOMDocument;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

/**
 * Class SendClaimsToCollectorCommand
 *
 * @package App\Command
 * @property \App\Model\Table\ClaimsTable Claims
 * @property \League\Flysystem\AdapterInterface casesFileSystem
 */
class CurlCommand extends Command
{
    protected $client;
    // file to store cookie data
    protected $cookieFile = 'cookie_jar.txt';
    protected $cookieJar;

    protected $claimsTable;
    protected $claimActionsTable;
    protected $claimFinancialsTable;
    protected $collectorsTable;
    protected $claimActionTypesTable;

    protected $mortang;
    protected $sagAfsluttet;

    protected $globalResponse;

    public function __construct()
    {
        $this->client = new Client();
        $this->cookieJar = new FileCookieJar($this->cookieFile, true);
        $this->claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions');
        $this->claimActionTypesTable = TableRegistry::getTableLocator()->get('ClaimActionTypes');
        $this->claimPhasesTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $this->claimFinancialsTable = TableRegistry::getTableLocator()->get('ClaimFinancials');
        $this->collectorsTable = TableRegistry::getTableLocator()->get('Collectors');

        libxml_use_internal_errors(true);
    }

    public function initialize()
    {
        parent::initialize();
        $this->mortang = $this->collectorsTable->find()->where(['identifier' => "mortang"])->first();
        $this->sagAfsluttet = $this->claimActionTypesTable->find()->where(['identifier' => 'Sag afsluttet'])->first();
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {

        $claims = $this->getClaims();
        $counter = 0;
        Log::notice(" ======== Start scraping data from mortang ======== ");
        $this->log(" ======== Start scraping data from mortang ======== ");
        foreach ($claims as $claim) {
            sleep(1);
            if ($counter % 10 === 0) {
                $time_lapse_seconds = rand(5, 15);
                echo $time_lapse_seconds . " sec gap and logging in" . PHP_EOL;
                echo $counter . " done, left are " . ($claims->count() - $counter) . PHP_EOL;
                sleep($time_lapse_seconds);
                if (!$this->login()) {
                    echo "Problem with Login - EXITING" . PHP_EOL;

                    return;
                }
            }
            $counter++;
            Log::notice("Working on " . $claim->customer_reference);

            try {
                $this->claimActionsTable->deleteAll(['claim_id' => $claim->id]);

                $statusArray = $this->scrapActions($claim->collector_reference);
                if (!empty($statusArray)) {
                    $this->instertNewActions($claim, array_reverse($statusArray, false));

                    $statusArray = $this->scrapActionsFinancials($claim);
                    $this->instertNewActions($claim, $statusArray);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
        Log::notice(" ======== End scraping data ======== ");
    }

    /**
     * To get all claims with order 4,5,6
     *
     * @return Array of Claims
     */

    public function getClaims()
    {

        $claimActionFourth = $this->claimPhasesTable->find()->where(['identifier' => 'reminder'])->first();
        $claimActionFifth = $this->claimPhasesTable->find()->where(['identifier' => 'debt_collection'])->first();

        $claims = $this->claimsTable->find("all")->
        where([
            'claim_phase_id IN' => [$claimActionFourth->id, $claimActionFifth->id],
            'collector_id' => $this->mortang->id,
        ]);

        return $claims;
    }

    /**
     * To attempt login into mortang system
     *
     * @return Bool
     */
    public function login()
    {

        $success = false;
        $numTries = 0;
        while (!$success) {
            try {
                $response = $this->client->request('POST', 'https://inkasso.mortang.dk/inkassoonline/IO_Login.asp?langid=0', [
                    'form_params' => [
                        'userpwd' => 'DEB2018',
                        'username' => 'D000',
                        'login' => 'true',
                    ],
                    'cookies' => $this->cookieJar,
                ]);
                $success = true;

                return true;
            } catch (RequestException $e) {
                $numTries++;
                if ($numTries > 3) {
                    if ($e->hasResponse()) {
                        $this->log(Psr7\str($e->getResponse()));
                    }
                    $this->sendFailedLoginEmail(Psr7\str($e->getResponse()));

                    return false;
                } else {
                    $this->log("Cound not login - Retrying in 10");
                    sleep(10);
                }
            }
        }
    }

    private function sendFailedLoginEmail($errorString)
    {
        $email = new Email('sparkpost');
        $myemail = $email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to(Configure::read('Adminemail'))
            ->cc('ihasnat@yandex.com')
            ->subject("Login Failed at mortang")
            ->emailFormat('html')
            ->send("<h1> Login for mortang failed <h2> <br> <p>" . $errorString . "</p>");
    }

    /**
     * To get all actions from the page, paginate using offsets, until no more actions left, log and return error if page was not found. in case of success return actions and set
     * global response so we can use HTML to scrap all financial information in scrapActionsFinancials
     *
     * @param String collector_reference
     * @return array
     */

    protected function scrapActions($collector_reference)
    {

        $collector_reference = str_replace(".", "-", $collector_reference);
        $statusArray = [];

        $offset = 0;
        $statusCounter = 0;
        $actionsCount = 10;
        while ($actionsCount >= 10) {
            try {
                $response = $this->client->request(
                    'GET',
                    'https://inkasso.mortang.dk/inkassoonline/IO_Sag.asp?qsID=' . $collector_reference . "&offset=" . $offset,
                    [
                        'cookies' => $this->cookieJar,
                    ]
                );
            } catch (RequestException $e) {
                return [];
            }
            //set global response
            $this->globalResponse = $response;
            $actionsCount = 0;
            $offset += 10;

            //Get main HTML Body
            $body = $response->getBody();
            $DOM = new DOMDocument();
            $DOM->loadHTML(strval($body));

            //Table where statues are
            $innerHTML = '';
            $elem = $DOM->getElementById('fstable');
            $children = $elem->childNodes;
            foreach ($children as $child) {
                $tmp_doc = new DOMDocument();
                $tmp_doc->appendChild($tmp_doc->importNode($child, true));
                $innerHTML .= $tmp_doc->saveHTML();
            }

            //Table rows of actions table
            $actionTr = new DOMDocument();
            $actionTr->loadHTML(strval($innerHTML));
            $elems = $actionTr->getElementsByTagName('tr');
            $innerHTML = '';
            $counter = 0;
            foreach ($elems as $elem) {
                $counter++;
                if ($counter < 4) {
                    continue;
                }

                $tmp_doc = new DOMDocument();
                $tmp_doc->appendChild($tmp_doc->importNode($elem, true));

                //Table columns of actions table
                $innerHTML = $tmp_doc->saveHTML();
                $actionTd = new DOMDocument();
                $actionTd->loadHTML(strval($innerHTML));
                $elemstds = $actionTd->getElementsByTagName('td');
                $tdCounter = 0;
                foreach ($elemstds as $elemstd) {
                    if ($tdCounter == 1) {
                        $statusArray[$statusCounter]['name'] = $elemstd->nodeValue;
                    } else {
                        $statusArray[$statusCounter]['date'] = $elemstd->nodeValue;
                    }
                    $tdCounter++;
                }
                $actionsCount++;
                $statusCounter++;
            }
        }

        return $statusArray;
    }

    /**
     * To delete all existing claim actions and insert new ones
     *
     * @param Object Claim
     * @param Object Actions
     * @return bool
     */

    public function instertNewActions($claim, $statusArray)
    {

        //insert new actions
        for ($i = 0; $i < sizeof($statusArray); $i++) {
            $date = date_create_from_format('d-m-Y', str_replace(" ", "", $statusArray[$i]['date']));
            $claimAction = $this->claimActionsTable->newEntity();
            $claimAction->claim_id = $claim->id;
            $claimAction->claim_action_type_id = $this->getActionTypeID($statusArray[$i]['name'], $claim);
            $claimAction->date = date_format($date, 'Y-m-d');
            if (array_key_exists("paid", $statusArray[$i])) {
                $claimAction->paid = $statusArray[$i]['paid'];
            } else {
                $claimAction->paid = 0;
            }
            $save = $this->claimActionsTable->save($claimAction);
        }
    }

    public function getActionTypeID($claimActionName, $claim)
    {
        $claimActionTypesTable = TableRegistry::getTableLocator()->get('ClaimActionTypes');

        if (strpos($claimActionName, 'Faktura nr.') !== false) {
            return $this->sagAfsluttet->id;
        }

        //search action type of claim in action Types array
        $actionTypeSearch = $claimActionTypesTable->find(
            'all',
            ['conditions' => ['collector_identifier' => $claimActionName]]
        )->first();

        //if not found then insert action type for mortang
        if (!$actionTypeSearch) {
            $newClaimActionType = $claimActionTypesTable->newEntity();
            $newClaimActionType->collector_identifier = $claimActionName;
            $newClaimActionType->identifier = $claimActionName;
            $newClaimActionType->name = $claimActionName;
            $newClaimActionType->collector_id = $this->mortang->id;
            $newClaimActionType->is_public = 0;
            $save = $claimActionTypesTable->save($newClaimActionType);
            echo "New Action Added named " . $claimActionName . " for " . $claim->collector_reference . PHP_EOL;
            $this->log("New Action Added named " . $claimActionName . " for " . $claim->collector_reference);

            return $save->id;
        } else {
            if ($actionTypeSearch->claim_phase_id) {
                //update claim and set phase to match phase of claim phase order
                $claim->claim_phase_id = $actionTypeSearch->claim_phase_id;
                $saveClaim = $this->claimsTable->save($claim);
            }

            return $actionTypeSearch->id;
        }
    }

    protected function scrapActionsFinancials($claim)
    {
        $statusArray = [];
        $this->setGeneralFinance($claim->id);
        $statusCounter = 0;

        //Get main HTML Body
        $body = $this->globalResponse->getBody();
        $DOM = new DOMDocument();
        $DOM->loadHTML(strval($body));

        //Table where statues are
        $innerHTML = '';
        $elem = $DOM->getElementById('fstable2');
        if (!$elem) {
            return [];
        }
        $children = $elem->childNodes;
        foreach ($children as $child) {
            $tmp_doc = new DOMDocument();
            $tmp_doc->appendChild($tmp_doc->importNode($child, true));
            $innerHTML .= $tmp_doc->saveHTML();
        }

        //Table rows of actions table
        $actionTr = new DOMDocument();
        $actionTr->loadHTML(strval($innerHTML));
        $elems = $actionTr->getElementsByTagName('tr');
        $innerHTML = '';
        $counter = 0;
        foreach ($elems as $elem) {
            $counter++;
            if ($counter < 4) {
                continue;
            }

            $tmp_doc = new DOMDocument();
            $tmp_doc->appendChild($tmp_doc->importNode($elem, true));

            //Table columns of actions table
            $innerHTML = $tmp_doc->saveHTML();
            $actionTd = new DOMDocument();
            $actionTd->loadHTML(strval($innerHTML));
            $elemstds = $actionTd->getElementsByTagName('td');
            $tdCounter = 0;
            foreach ($elemstds as $elemstd) {
                if ($tdCounter == 1) {
                    $statusArray[$statusCounter]['name'] = $elemstd->nodeValue;
                } else {
                    if ($tdCounter == 2) {
                        $statusArray[$statusCounter]['paid'] = $this->formatCurrency($elemstd->nodeValue);
                    } else {
                        $statusArray[$statusCounter]['date'] = $elemstd->nodeValue;
                    }
                }
                $tdCounter++;
            }
            $statusCounter++;
        }

        return $statusArray;
    }

    protected function setGeneralFinance($claim_id)
    {

        $financeArray = [];

        //Get main HTML Body
        $body = $this->globalResponse->getBody();
        $DOM = new DOMDocument();
        $DOM->loadHTML(strval($body));

        //Table where statues are
        $innerHTML = '';
        $elem = $DOM->getElementsByTagName('table');
        $third_table = $elem->item(2);
        $children = $third_table->childNodes;
        foreach ($children as $child) {
            $tmp_doc = new DOMDocument();
            $tmp_doc->appendChild($tmp_doc->importNode($child, true));
            $innerHTML .= $tmp_doc->saveHTML();
        }
        //Table rows of actions table
        $actionTr = new DOMDocument();
        $actionTr->loadHTML(strval($innerHTML));
        $elems = $actionTr->getElementsByTagName('tr');
        $innerHTML = '';
        $counter = 0;
        $arrCounter = 0;
        foreach ($elems as $elem) {
            $counter++;
            if ($counter < 3) {
                continue;
            }
            $arrCounter++;
            $tmp_doc = new DOMDocument();
            $tmp_doc->appendChild($tmp_doc->importNode($elem, true));

            //Table columns of actions table
            $innerHTML = $tmp_doc->saveHTML();
            $actionTd = new DOMDocument();
            $actionTd->loadHTML(strval($innerHTML));
            $elemstds = $actionTd->getElementsByTagName('td');
            $third_td = $elemstds->item(2);
            $fourth_td = $elemstds->item(3);
            if ($third_td && $fourth_td != "" && !empty($fourth_td)) {
                array_push($financeArray, $this->formatCurrency($fourth_td->nodeValue));
            }
        }

        //insert data Into database
        $claim_financial = $this->claimFinancialsTable->find()->where(['claim_id' => $claim_id])->first();
        if (!$claim_financial) {
            $claim_financial = $this->claimFinancialsTable->newEntity();
        }
        $claim_financial->hovedstol = $financeArray[0];
        $claim_financial->rykkergebyrer = $financeArray[1];
        $claim_financial->ovrige_gebyrer = $financeArray[2];
        $claim_financial->renter = $financeArray[3];
        $claim_financial->retsgebyrer = $financeArray[4];
        $claim_financial->inkassoomkostninger = $financeArray[5];
        $claim_financial->tilkendt_modesalar = $financeArray[6];
        $claim_financial->indbetalt = $financeArray[7];
        $claim_financial->restgald = $financeArray[9];
        $claim_financial->claim_id = $claim_id;
        $this->claimFinancialsTable->save($claim_financial);
    }

    protected function formatCurrency($amount)
    {
        return preg_replace('/[^0-9]+/', '', $amount);
    }
}
