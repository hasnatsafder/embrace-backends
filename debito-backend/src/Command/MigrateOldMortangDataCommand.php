<?php

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use League\Csv\Reader;

class MigrateOldMortangDataCommand extends Command
{
    protected $dbConnection;
    private $mortangId;
    private $denmarkId;
    private $endedClaimActionID;
    private $accountOwnerRoleId;
    private $endedClaimPhase;
    private $debtCollectionPhase;
    private $kroneId;

    public function initialize()
    {
        parent::initialize();

        $this->mortangId = "6b296e78-3234-49bc-ba6d-4a608c2a607c";
        $this->endedClaimActionID = "962cfe6d-614f-4b62-9789-6ec8fa79ba37";
        $this->denmarkId = "03bfcf66-043a-11e8-9a3c-080027548b1b";
        $this->accountOwnerRoleId = "ee7a94f3-646a-4e95-8f5f-17b1ad59c3ec";
        $this->endedClaimPhase = "e003063d-518c-4903-b45b-7df2eddfbf5b";
        $this->debtCollectionPhase = "26c4efbd-2bfa-4252-987c-8deffe38271a";
        $this->kroneId = "e5243bf3-0a70-5924-94f6-af1d88c951c4";
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Starting Data Loading.');
        $this->loadData();
        //$this->addFinanceData();
        //$this->deldup();
    }

    private function loadData()
    {
        $csv = Reader::createFromPath('./webroot/files/data.csv', 'r');
        //$csv = Reader::createFromPath('./webroot/files/Test.csv', 'r');
        $headerArray = [];
        $bodyArray = [];
        $dataArray = [];
        $counter = 0;
        foreach ($csv as $contentKey => $contentData) {
            // if empty row of csv file
            if (!$contentData[0]) {
                continue;
            } // if header of file
            else {
                if ($contentKey === 0) { // first row of lindroff is column header
                    $headerArray = $contentData;
                    continue;
                } //if body
                else {
                    if ($contentKey != 0) {
                        $bodyArray = $contentData;

                        //for each value in body
                        for ($i = 0; $i < sizeof($headerArray); $i++) {
                            $dataArray[$counter][$headerArray[$i]] = $bodyArray[$i];
                        }
                        $counter++;
                    }
                }
            }
        }
        $old_claim_jnr = ""; // to make sure we add claim only for new jnrs
        $claim_id = ""; // to use this in adding the claim lines
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $count = 0;
        for ($i = 0; $i < $counter; $i++) {
            //if claim already found with customer_reference
            $data = $dataArray[$i];
            $query = $claimsTable->find();
            $claimOld = $query->select(['customer_reference', 'collector_reference', 'id', 'account_id', 'synced'])
                ->where([
                    'or' => [
                        "REPLACE(customer_reference,'-','')" => $this->filter_reference($dataArray[$i]['klientjnr']),
                        'collector_reference' => $dataArray[$i]['jnr'],
                    ],
                ])
                ->first();
            if (!$claimOld) {
                //echo "not found " . $data['jnr'] . " on date " . $data['sag_oprettet_dato'] . PHP_EOL;
            }
            //continue;
            if ($claimOld) {
                echo "Claim Found for " . $dataArray[$i]['klientjnr'] . " on date " . $dataArray[$i]['sag_oprettet_dato'] . PHP_EOL;
                //Also update collector-reference and check if we need to add account-collector reference
                $claimOld->collector_reference = $dataArray[$i]['jnr'];
                if (!$claimOld->synced) {
                    $date = date_create_from_format('m/d/Y', $dataArray[$i]['sag_oprettet_dato']);
                    $claimOld->synced = $date;
                    $claimOld->approved = $date;
                    $claimOld->completed = $date;
                }
                $claimsTable->save($claimOld);
                $this->handleAccountCollectors($dataArray[$i]['jnr'], $claimOld->account_id);
                continue;
            }
            if ($old_claim_jnr == "" || $old_claim_jnr != $dataArray[$i]['jnr']) {
                $account = $this->handleAccount($dataArray[$i]);
                $debtor_id = $this->handleDebtors($dataArray[$i], $account['account_id'], $account['user_id']);
                $claim_id = $this->handleClaims($dataArray[$i], $debtor_id, $account['account_id'], $account['user_id']);
                //get and set claims
            } else {
                $dd = $dataArray[$i];
                $this->handleClaimLines($dataArray[$i], $claim_id, $dataArray[$i]["beløb"]);
            }
            $old_claim_jnr = $dataArray[$i]['jnr'];
        }
    }

    private function filter_reference($ref)
    {
        $ref = str_replace("-", "", $ref);
        $ref = str_replace(".", "", $ref);
        $ref = str_replace(",", "", $ref);

        return $ref;
    }

    public function handleAccountCollectors($referenceClaim, $account_id)
    {

        $AccountsCollectorsTable = TableRegistry::getTableLocator()->get('AccountsCollectors');

        //if already found then just update
        $accountsCollector = $AccountsCollectorsTable->find()->where(['account_id' => $account_id, 'collector_id' => $this->mortangId])->first();
        if ($accountsCollector) {
            return;
        } else {
            $accountsCollector = $AccountsCollectorsTable->newEntity();
            $accountsCollector->setAccess('*', true);
        }

        //make reference
        $refArray = explode(".", $referenceClaim);

        $accountsCollector = $AccountsCollectorsTable->patchEntity($accountsCollector, [
            'account_id' => $account_id,
            'collector_id' => $this->mortangId,
            'collector_identifier' => $refArray[0],
        ]);

        return $AccountsCollectorsTable->save($accountsCollector)->id;
    }

    /**
     * handleAccount
     *
     * @param array of claim data
     * @return array of account_id and user_id
     */

    private function handleAccount($claim_array)
    {

        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');

        if ($claim_array['klient_cvr'] && $claim_array['klient_cvr'] !== "") {
            $account = $accountsTable->find(
                'all',
                ['conditions' => ["vat_number" => $claim_array['klient_cvr']]]
            )->last();
            // if account was found return its ID and user ID
            if ($account) {
                //echo "Account found but still adding in testing for " . $claim_array['klient_email']; //
                return ['account_id' => $account->id, 'user_id' => $account->created_by_id];
            }
        } else { // for private companies
            $account = $accountsTable->find(
                'all',
                ['conditions' => ["name" => $claim_array['klient_navn']]]
            )->last();
            // if account was found return its ID and user ID
            if ($account) {
                //echo "Account found but still adding in testing for " . $claim_array['klient_email']; //
                return ['account_id' => $account->id, 'user_id' => $account->created_by_id];
            }
        }

        $user_id = $this->handleUsers($claim_array);

        //preparing accounts data
        $account_is_company = $claim_array['klient_cvr'] ? 1 : 0;

        $account_reg_number = "";
        $account_account_number = "";
        if ($claim_array['klient_kontonr']) {
            $str = str_replace(" ", "", $claim_array['klient_kontonr']);
            $account_reg_number = substr($str, 0, 4);
            $account_account_number = substr($str, 4);
        }

        $account = $accountsTable->newEntity();
        $account->setAccess('*', true);
        $account = $accountsTable->patchEntity($account, [
            'created_by_id' => $user_id,
            'country_id' => $this->denmarkId,
            'name' => $claim_array['klient_navn'],
            'address' => $claim_array['klient_adr1'],
            'zip_code' => $claim_array['klient_postnr'],
            'city' => $claim_array['klient_bynavn'],
            'phone' => $claim_array['klient_telefon'],
            'vat_number' => $claim_array['klient_cvr'],
            'bank_reg_number' => $account_reg_number,
            'bank_account_number' => $account_account_number,
            'is_company' => $account_is_company,
            'email' => $claim_array['klient_email'],
            'users' => [
                [
                    'id' => $user_id,
                    '_joinData' => [
                        'role_id' => $this->accountOwnerRoleId,
                    ],
                ],
            ],
        ]);
        $account_id = $accountsTable->save($account, [
            'skipAutoCreateUser' => true,
        ])->id;
        if (!$account_id) {
            echo "account not saved for " . $claim_array['klient_email'];
        }

        $this->activateAccount($user_id, $account_id);

        //Add entry to Account Collectors Table
        $this->handleAccountCollectors($claim_array['jnr'], $account_id);

        return ['account_id' => $account_id, 'user_id' => $user_id];
    }

    /**
     * handleUsers
     *
     * @param array of claim data
     * @return string user_id
     */

    private function handleUsers($claim_array)
    {

        $usersTable = TableRegistry::getTableLocator()->get('Users');

        $user = $usersTable->find(
            'all',
            ['conditions' => ["email" => $claim_array['klient_email']]]
        )->last();

        if ($user) {
            echo "User Already found for: " . $claim_array['klient_email'] . " " . PHP_EOL;

            return $user->id;
        }

        $user = $usersTable->newEntity();
        $user->setAccess('*', true);

        $user = $usersTable->patchEntity($user, [
            'active_account_id' => null, // will add if after account is added
            'first_name' => $claim_array['klient_navn'],
            'email' => $claim_array['klient_email'],
            'password' => Text::uuid(),
            'is_confirmed' => false,
            'is_active' => true,
        ]);
        $u = $usersTable->save($user);
        if (!$u->id) {
            echo "error for " . $claim_array['klient_email'];
        }
        echo "User Created: " . $claim_array['klient_email'] . " " . PHP_EOL;

        return $u->id;
    }

    /**
     * activateAccount
     *
     * @param $user_id , $account_id
     * @return bool
     */

    private function activateAccount($user_id, $account_id)
    {
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $user = $usersTable->get($user_id);
        $user->active_account_id = $account_id;

        return $usersTable->save($user);
    }

    /**
     * handleDebtors
     *
     * @param $claim_array , $account_id, $user_id
     * @return int debtor_id
     */

    private function handleDebtors($claim_array, $account_id, $user_id)
    {
        $debtorsTable = TableRegistry::getTableLocator()->get('Debtors');

        if ($claim_array['navn'] && $claim_array['navn'] !== "") {
            $debtor = $debtorsTable->find(
                'all',
                ['conditions' => ["account_id" => $account_id, "OR" => ["company_name" => $claim_array['navn'], "first_name" => $claim_array['navn']]]]
            )->last();
            // if account was found return its ID and user ID
            if ($debtor) {
                //echo "Account found but still adding in testing for " . $claim_array['klient_email']; //
                return $debtor->id;
            }
        }

        $debtor = $debtorsTable->newEntity();
        $debtor->setAccess('*', true);

        $debtor_is_company = 0;
        $debtor_company_name = "";
        $debtor_first_name = $claim_array['navn'];
        if ($claim_array['cvrnr']) {
            $debtor_is_company = 1;
            $debtor_company_name = $claim_array['navn'];
            $debtor_first_name = "";
        }

        $debtor = $debtorsTable->patchEntity($debtor, [
            'created_by_id' => $user_id,
            'account_id' => $account_id,
            'country_id' => $this->denmarkId,
            'company_name' => $debtor_company_name,
            'first_name' => $debtor_first_name,
            'address' => $claim_array['adr1'],
            'zip_code' => $claim_array['postnr'],
            'city' => $claim_array['bynavn'],
            'vat_number' => $claim_array['cvrnr'],
            'phone' => $claim_array['telefon'],
            'email' => $claim_array['email'],
            'is_company' => $debtor_is_company,
        ]);

        $d = $debtorsTable->save($debtor);
        if (!$d) {
            echo "error";
        }

        return $d->id;
    }

    private function handleClaims($claim_array, $debtor_id, $account_id, $user_id)
    {
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $claim = $claimsTable->newEntity();
        $claim->setAccess('*', true);

        $claim_phase_id = $claim_array['sag_afsluttet_dato'] ? $this->endedClaimPhase : $this->debtCollectionPhase;
        $claim_ended_at = $claim_array['sag_afsluttet_dato'] ? $this->formatDateTime($claim_array['sag_afsluttet_dato']) : null;
        $created_claim_date = date_create_from_format('m/d/Y', $claim_array['sag_oprettet_dato']);

        $claim = $claimsTable->patchEntity($claim, [
            'created_by_id' => $user_id,
            'account_id' => $account_id,
            'debtor_id' => $debtor_id,
            'claim_phase_id' => $claim_phase_id,
            'collector_id' => $this->mortangId,
            'customer_reference' => $claim_array['klientjnr'],
            'collector_reference' => $claim_array['jnr'],
            'created' => $created_claim_date,
            'ended_at' => $claim_ended_at,
        ]);

        $claim_id = $claimsTable->save($claim)->id;

        $this->handleClaimLines($claim_array, $claim_id, $claim_array['beløb']);

        return $claim_id;
    }

    private function formatDateTime($date)
    {
        $dateNew = date_create($date);

        return date_format($dateNew, "Y-m-d H:i:s");
    }

    private function handleClaimLines($claim_array, $claim_id, $amount)
    {
        $claimsLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claimLine = $claimsLinesTable->newEntity();
        $claimLine->setAccess('*', true);

        //get claim Line Type ID
        $claimsLineTypesTable = TableRegistry::getTableLocator()->get('ClaimLineTypes');
        $claimLineType = $claimsLineTypesTable->find(
            'all',
            ['conditions' => ["name" => ucfirst($claim_array['bilagstype'])]]
        )->last();
        if (!$claimLineType->id) {
            echo "no";
        }
        $claimLine = $claimsLinesTable->patchEntity($claimLine, [
            'claim_id' => $claim_id,
            'claim_line_type_id' => $claimLineType->id,
            'currency_id' => $this->kroneId,
            'placement' => "01",
            "invoice_date" => $this->formatDates($claim_array['bilagsdato']),
            "maturity_date" => $this->formatDates($claim_array['forfaldsdato']),
            "amount" => $this->convertToDanishCurrency($amount), // Convert minus value to plus and to dkk
            "invoice_reference" => "-", // we dont have it
        ]);
        $saveClaimLine = $claimsLinesTable->save($claimLine);

        return $saveClaimLine->id;
    }

    private function formatDates($date)
    {
        $dateNew = date_create($date);

        return date_format($dateNew, "Y-m-d");
    }

    protected function convertToDanishCurrency($value)
    {
        if (strpos($value, '.') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, "."), 1)) == 1) {
                $float = number_format((float)$value, 2, '.', '');
                $integer = str_replace('.', '', $float);
                $integer = str_replace(',', '', $integer);
                $integer = str_replace(' ', '', $integer);
                $integer = str_replace('$', '', $integer);

                return abs($integer);
            }
            $integer = str_replace('.', '', $value);
            $integer = str_replace(',', '', $integer);
            $integer = str_replace(' ', '', $integer);
            $integer = str_replace('$', '', $integer);

            return abs($integer);
        }
        if (strpos($value, ',') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, ","), 1)) == 1) {
                $float = number_format((float)$value, 2, ',', '');
                $integer = str_replace(',', '', $float);
                $integer = str_replace('.', '', $integer);
                $integer = str_replace(' ', '', $integer);
                $integer = str_replace('$', '', $integer);

                return abs($integer);
            }
            $integer = str_replace(',', '', $value);
            $integer = str_replace('.', '', $integer);
            $integer = str_replace(' ', '', $integer);
            $integer = str_replace('$', '', $integer);

            return abs($integer);
        }
        $float = number_format((float)$value, 2, '.', '');
        $integer = str_replace('.', '', $float);

        return abs($integer);
    }

    public function handleClaimActions($claim_array, $claim_id)
    {
        $claimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions');
        $claimAction = $claimActionsTable->newEntity();
        $claimAction->setAccess('*', true);

        $claimAction = $claimActionsTable->patchEntity($claimAction, [
            'claim_id' => $claim_id,
            'claim_action_type_id' => $this->endedClaimActionID,
            'date' => $this->formatDates($claim_array['sag_afsluttet_dato']),
        ]);

        return $claimActionsTable->save($claimAction)->id;
    }

    private function addFinanceData()
    {
        $csv = Reader::createFromPath('./webroot/files/afregninger total 26-09-2018.csv', 'r');
        $headerArray = [];
        $bodyArray = [];
        $dataArray = [];
        $counter = 0;
        foreach ($csv as $contentKey => $contentData) {
            // if empty row of csv file
            if (!$contentData[0]) {
                continue;
            } // if header of file
            else {
                if ($contentKey === 0) { // first row of lindroff is column header
                    $headerArray = $contentData;
                    continue;
                } //if body
                else {
                    if ($contentKey != 0) {
                        $bodyArray = $contentData;

                        //for each value in body
                        for ($i = 0; $i < sizeof($headerArray); $i++) {
                            $dataArray[$counter][$headerArray[$i]] = $bodyArray[$i];
                        }
                        $counter++;
                    }
                }
            }
        }
        $old_claim_action['jnr'] = ""; // to make sure we add claim only for new jnrs
        $claim_id = ""; // to use this in adding the claim lines
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $claimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions');
        for ($i = 0; $i < $counter; $i++) {
            if ($dataArray[$i]['jnr'] == "D114.0119") {
                echo "yes";
            }

            $claim = $claimsTable->find(
                'all',
                ['conditions' => ["collector_reference" => $dataArray[$i]['jnr']]]
            )->last();

            if (!$claim) {
                echo "Claim not found for " . $dataArray[$i]['jnr'] . PHP_EOL;
                continue;
            }

            $date = date_create_from_format('m/j/Y', $dataArray[$i]['dato']);
            $date = date_format($date, "Y-m-d");

            $claimAction = $claimActionsTable->find(
                'all',
                ['conditions' => ["claim_id" => $claim->id, 'date' => $date]]
            )->last();

            if (!$claimAction) {
                $claimAction = $claimActionsTable->newEntity();
                $claimAction->setAccess('*', true);
            }

            //if the last was sags afsultet, then just update it
            $claimActionCheck = $claimActionsTable->find(
                'all',
                ['conditions' => ["claim_id" => $claim->id, "claim_action_type_id" => "2bcae665-f31a-41bc-b959-e09306fe519a", "actual_paid IS " => null]]
            )->last();

            if ($claimActionCheck && $claimActionCheck->claim_action_type_id == "2bcae665-f31a-41bc-b959-e09306fe519a" && $claimActionCheck->actual_paid === null) {
                $claimActionsTable->delete($claimActionCheck);
            }

            $claimAction->paid = $this->convertToDanishCurrency($dataArray[$i]['indbetalt']);
            $claimAction->debt_collection_fees = $this->convertToDanishCurrency($dataArray[$i]['salaer']) + $this->convertToDanishCurrency($dataArray[$i]['res_provision']);
            $claimAction->actual_paid = $this->convertToDanishCurrency($dataArray[$i]['sum']);
            $claimAction->claim_action_type_id = "2bcae665-f31a-41bc-b959-e09306fe519a";
            $claimAction->date = $date;
            $claimAction->claim_id = $claim->id;
            $claimActionsTable->save($claimAction);

            $claim->claim_phase_id = "e003063d-518c-4903-b45b-7df2eddfbf5b";
            $claimsTable->save($claim);
        }
    }

    private function deldup()
    {
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $claimLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions');

        $mortangClaims = $claimsTable->find()->where(['collector_id' => "6b296e78-3234-49bc-ba6d-4a608c2a607c"]);
        foreach ($mortangClaims as $value) {
            $claimline = $claimLinesTable->query();
            $claimline->delete()->where(['claim_id' => $value->id])->execute();

            $claimAction = $claimActionsTable->query();
            $claimAction->delete()->where(['claim_id' => $value->id])->execute();

            $claim = $claimsTable->query();
            $claim->delete()->where(['id' => $value->id])->execute();
        }
    }
}
