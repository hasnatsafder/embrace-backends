<?php
declare(strict_types=1);

namespace App\Command;

use App\Model\Entity\Claim;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Psr\Log\LogLevel;

/**
 * SetFiPaymentReferenceToClaims command.
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 */
class SetFiPaymentReferenceToClaimsCommand extends Command
{
    public $modelClass = 'App.Claims';

    public function execute(Arguments $args, ConsoleIo $io)
    {
        $progress = $io->helper('Progress');
        $claims = $this->Claims->find()
            ->select(['Claims.id'])
            ->where(['Claims.fi_payment_reference IS' => null]);

        $totalClaims = $claims->count();

        if ($totalClaims === 0) {
            $io->warning('No claims need FI codes!');

            return;
        }

        $progress->init([
            'total' => $totalClaims,
        ]);

        $io->info(sprintf('Processing %d claims', $totalClaims));

        $claims->each(function (Claim $claim) use ($progress) {
            $this->setFiReference($claim);
            $progress->increment();
            $progress->draw();
        });

        $io->success('Finished');
    }

    private function setFiReference(Claim $claim)
    {
        if ($claim->fi_payment_reference !== null) {
            throw new \RuntimeException('"fi_payment_reference" should be empty');
        }

        $fiCode = $claim->getOrGeneratePaymentFiCode();

        $this->log(
            sprintf('FI code for claim: %s : %s', $claim->id, $fiCode),
            LogLevel::DEBUG
        );
    }
}
