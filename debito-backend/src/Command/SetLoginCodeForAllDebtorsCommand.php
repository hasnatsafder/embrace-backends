<?php
declare(strict_types=1);

namespace App\Command;

use App\Model\Entity\Debtor;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;

/**
 * SetLoginCodeForAllDebtors command.
 *
 * @property \App\Model\Table\DebtorsTable $Debtors
 */
class SetLoginCodeForAllDebtorsCommand extends Command
{
    public $modelClass = 'App.Debtors';

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        /** @var \Cake\Shell\Helper\ProgressHelper $progress */
        $progress = $io->helper('Progress');

        $debtors = $this->Debtors->find()
            ->where(['Debtors.login_code' => '']);

        $progress->init([
            'total' => $debtors->count(),
        ]);

        $debtors->each(function (Debtor $debtor) use ($progress) {
            $loginCode = '';
            for ($i = 0; $i < 6; $i++) {
                $loginCode .= mt_rand(0, 9);
            }
            $debtor->login_code = $loginCode;
            $this->Debtors->saveOrFail($debtor);
            $progress->increment();
            $progress->draw();
        });
    }
}
