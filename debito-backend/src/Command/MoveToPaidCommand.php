<?php


namespace App\Command;


use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

class MoveToPaidCommand extends Command
{
    private $invoicesTable;
    private $paidInvoicesTable;
    private $invoicesStatusTable;
    private $debtorsTable;
    private $erpIntegrationTable;
    private $io;

    public function initialize()
    {
        parent::initialize();
        $this->invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
        $this->paidInvoicesTable = TableRegistry::getTableLocator()->get('PaidInvoices');
        $this->invoicesStatusTable = TableRegistry::getTableLocator()->get('InvoiceStatuses');
        $this->debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        $this->erpIntegrationTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
        $this->io = new ConsoleIo;
    }
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->moveToPaid();
        $this->addCustomerNameToInvoices();
    }

    private function moveToPaid() {
        $paid_invoice_status = $this->invoicesStatusTable->find()->where(['identifier' => 'paid'])->first();
        $paid_invoices = $this->invoicesTable->find('all')->where(['invoice_status_id' => $paid_invoice_status->id]);
        foreach ($paid_invoices as $paid_invoice) {
            $newPaidInvoice = $this->paidInvoicesTable->newEntity();
            $newPaidInvoice->account_id = $paid_invoice->account_id;
            $newPaidInvoice->invoice_status_id = $paid_invoice->invoice_status_id;
            $newPaidInvoice->currency_id = $paid_invoice->currency_id;
            $newPaidInvoice->erp_integration_id = $paid_invoice->erp_integration_id;
            $newPaidInvoice->erp_integration_foreign_key = $paid_invoice->erp_integration_foreign_key;
            $newPaidInvoice->debtor_id = $paid_invoice->debtor_id;
            $newPaidInvoice->invoice_number = $paid_invoice->invoice_number;
            $newPaidInvoice->issue_date = $paid_invoice->issue_date;
            $newPaidInvoice->due_date = $paid_invoice->due_date;
            $newPaidInvoice->hidden_until = $paid_invoice->hidden_until;
            $newPaidInvoice->net_amount = $paid_invoice->net_amount;
            $newPaidInvoice->gross_amount = $paid_invoice->gross_amount;
            $newPaidInvoice->pdf_file_path = $paid_invoice->pdf_file_path;
            $newPaidInvoice->converted_to_claim = $paid_invoice->converted_to_claim;
            $newPaidInvoice->snoozed = $paid_invoice->snoozed;
            $newPaidInvoice->created = $paid_invoice->created;
            $newPaidInvoice->modified = $paid_invoice->modified;
            $newPaidInvoice->deleted = $paid_invoice->deleted;
            $this->paidInvoicesTable->save($newPaidInvoice);

            // delete paid invoice from invoice table
            if ($this->invoicesTable->delete($paid_invoice)) {
                $this->io->out('Invoice is '. $paid_invoice->id);
            }
            else {
                var_dump($this->invoicesTable->delete($paid_invoice));
            }
        }
    }

    /**
     * Add debtor to invoices
     */
    public function addCustomerNameToInvoices() {
        $invoices = $this->invoicesTable->find('all');
        foreach ($invoices as $invoice) {
            $this->io->out('Invoice is '. $invoice->id);
            $debtor = $this->debtorsTable->find()->where(['id' => $invoice->debtor_id])->first();
            if (isset($debtor->company_name)) {
                $name = $debtor->company_name;
            }
            else {
                $name = $debtor->first_name;
                if ($debtor->last_name) {
                    $name .= " " . $debtor->last_name;
                }
            }
            $invoice->customer_name = $name;
            $this->invoicesTable->save($invoice);
        }
    }

}
