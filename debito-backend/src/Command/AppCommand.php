<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Command;

/**
 * AppCommand command.
 */
class AppCommand extends Command
{
}
