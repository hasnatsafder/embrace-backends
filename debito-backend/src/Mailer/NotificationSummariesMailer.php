<?php
declare(strict_types=1);

namespace App\Mailer;

use App\Model\Entity\NotificationSummary;
use Cake\Log\LogTrait;
use Cake\Mailer\Mailer;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\TableRegistry;
use Psr\Log\LogLevel;

/**
 * Class NotificationSummariesMailer
 *
 * @package App\Mailer
 */
class NotificationSummariesMailer extends Mailer
{
    use LocatorAwareTrait;
    use LogTrait;

    public function notification_summary(NotificationSummary $notificationSummary)
    {
        $notificationSummariesTable = $this->getTableLocator()->get('NotificationSummaries');
        $user = $notificationSummariesTable->Users->get($notificationSummary->user_id, ['finder' => 'withTrashed']);

        /** @var NotificationSummary $notificationSummary */
        $notificationSummary = $notificationSummariesTable->loadInto($notificationSummary, ['Notifications']);
        if (empty($notificationSummary->notifications)) {
            $this->log(
                'There was no notification found for NotificationSummary',
                LogLevel::WARNING,
                ['notificationSummary' => $notificationSummary]
            );
            return false;
        }
        $claimNotificationSummaries = $this->prepareClaimNotificationSummaries($notificationSummary->notifications);
        $email = $this
            ->setTransport('sparkpost')
            ->setFrom(['kommunikation@debito.dk' => 'Debito'])
            ->setTo($user->email, "Hasnat")
            ->setSubject(__('Notification summary from Debito'))
            ->emailFormat('html')
            ->set([
                'user' => $user,
                'notifications' => $claimNotificationSummaries,
            ]);

        $email->viewBuilder()->setTemplate('notification_summaries/notification_summary');
    }

    /**
     * Prepare claim wise notification summaries
     * @param array $notificationSummaries
     * @return array
     */
    private function prepareClaimNotificationSummaries(Array $notificationSummaries) : array {
        $claim_array = [];
        $claimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions');
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');

        foreach ($notificationSummaries as $summary) {
            if ($summary->data_values) {
                $data_array = json_decode($summary->data_values);
                $action_id = $data_array->claimActionId;
                if ($action_id) {
                    $claim_action = $claimActionsTable->find()->where(['ClaimActions.id' => $action_id])->contain(['ClaimActionTypes'])->firstOrFail();
                    $claim = $claimsTable->get($claim_action->claim_id);
                    // if key of claim was not found in claims array then insert it
                    if (!isset($claim_array[$claim->id])) {
                        $claim_array[$claim->id]['claim'] = $claim;
                        $claim_array[$claim->id]['actions'] = [];
                    }
                    array_push($claim_array[$claim->id]['actions'],$claim_action);
                }
            }
        }
        foreach ($claim_array as $claim) {
            // sort claim actions by when done
            usort($claim['actions'], function ($first, $second) {
                return $first->done <=> $second->done;
            });
        }
        return $claim_array;
    }

}
