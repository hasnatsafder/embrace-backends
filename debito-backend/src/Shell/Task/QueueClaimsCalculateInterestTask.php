<?php
declare(strict_types=1);

namespace App\Shell\Task;

use DebtCollectionTools\Claims\InterestCalculator;
use Queue\Model\QueueException;
use Queue\Shell\Task\QueueTask;

/**
 * Class QueueClaimActionCreatedTask
 *
 * @package App\Shell\Task
 * @property \App\Model\Table\ClaimsTable $Claims
 */
class QueueClaimsCalculateInterestTask extends QueueTask
{
    public $modelClass = 'App.Claims';

    /**
     * Main execution of the task.
     *
     * @param array $data The array passed to QueuedJobsTable::createJob()
     * @param int $jobId The id of the QueuedJob entity
     */
    public function run(array $data, $jobId): void
    {
        if (empty($data['claimId'])) {
            throw new QueueException('"claimId" is required');
        }

        $claim = $this->Claims->get($data['claimId']);

        (new InterestCalculator($claim))->addAllMissingInterest();
    }
}