<?php

namespace App\Shell\Task;

use App\Utility\ErpIntegrations;
use Psr\Log\LogLevel;
use Queue\Shell\Task\QueueTask;
use Queue\Shell\Task\QueueTaskInterface;

/**
 * @property \App\Model\Table\ErpIntegrationsTable $ErpIntegrations
 */
class QueueErpTask extends QueueTask implements QueueTaskInterface
{

    public $modelClass = 'App.ErpIntegrations';

    /**
     * @param array $data The array passed to QueuedJobsTable::createJob()
     * @param int $jobId The id of the QueuedJob entity
     * @return void
     */
    public function run(array $data, $jobId)
    {
        // check if integration exists
        $erpData = $this->ErpIntegrations->get($data['erpIntegration_id'], ['finder' => 'withTrashed']);
        $this->log(sprintf('Starting ERP sync: %s', $data['erpIntegration_id']), LogLevel::DEBUG);
        if ($erpData) {
            ErpIntegrations::syncErpInvoices($data['erpIntegration_id']);
            $this->log(sprintf('Finished ERP sync: %s', $data['erpIntegration_id']), LogLevel::DEBUG);
        } else {
            $this->log(sprintf('Count not find ERP data for: %s', $data['erpIntegration_id']), LogLevel::DEBUG);
        }
    }
}
