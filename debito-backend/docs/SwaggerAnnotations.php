<?php
    /**
     * @SWG\Info(title="Debito APIs", description="APIs used by Debito frontend", version="1.0.0")
     */

    //Auth
    /**
     * @SWG\Post(
     *     path="/api/v1/auth/register",
     *     summary="Register User",
     *     tags={"Auth"},
     *
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *                  example="test@test.com"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *
     *                  minimum=6,
     *                  example="nopass"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="422", description="Password length must be greater than 6 / Validation error(s) occurred. (Email invalid or already exists, missing required fields i.e. email and password)"),
     * )
     */

    /**
     * @SWG\Post(
     *     path="/api/v1/auth/token",
     *     summary="Login user",
     *     tags={"Auth"},
     *
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *                  example="test@test.com"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *
     *                  minimum=6,
     *                  example="nopass"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="400", description="Bad Request"),
     *     @SWG\Response(response="401", description="Invalid username or password")
     * )
     */

    /**
     * @SWG\Post(
     *     path="/api/v1/auth/forgotpassword",
     *     summary="Reset user password",
     *     tags={"Auth"},
     *
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *                  example="test@test.com"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful / Email account not found"),
     *     @SWG\Response(response="500", description="Unknown email configuration")
     * )
     */


    //Accounts
    /**
     * @SWG\Get(
     *     path="/api/v1/accounts",
     *     summary="List user accounts",
     *     tags={"Accounts"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="300"
     *     ),
     *     @SWG\Parameter(
     *         name="showCountries",
     *         in="query",
     *         description="Include countries in response or not",
     *
     *         type="string",
     *         enum={"0", "1"},
     *
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/accounts/{accountId}",
     *     summary="Get user claim detail",
     *     tags={"Accounts"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         description="Account ID",
     *         required=true,
     *         type="string",
     *         default="d5169733-b5be-4aab-b332-29a765d207e4",
     *     ),
     *     @SWG\Parameter(
     *         name="showCountries",
     *         in="query",
     *         description="Include countries in response or not",
     *
     *         type="string",
     *         enum={"0", "1"}
     *
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Put(
     *     path="/api/v1/accounts/{accountId}",
     *     summary="Update Account data",
     *     tags={"Accounts"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         required=true,
     *         description="ID of the account",
     *         type="string",
     *         default="d5169733-b5be-4aab-b332-29a765d207e4"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="name",
     *                  type="string",
     *
     *                  description="Full name of the account",
     *                  example="John Doe"
     *              ),@SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *                  example="test@test.com"
     *              ),
     *              @SWG\Property(
     *                  property="address",
     *                  type="string",
     *
     *                  example="test address"
     *              ),
     *              @SWG\Property(
     *                  property="phone",
     *                  type="string",
     *
     *                  example="12345678"
     *              ),
     *              @SWG\Property(
     *                  property="zip_code",
     *                  type="string",
     *
     *                  example="1254"
     *              ),
     *              @SWG\Property(
     *                  property="city",
     *                  type="string",
     *
     *                  example="test city"
     *              ),
     *              @SWG\Property(
     *                  property="bank_account_number",
     *                  type="string",
     *
     *                  example="0987654321"
     *              ),
     *              @SWG\Property(
     *                  property="bank_reg_number",
     *                  type="string",
     *
     *                  example="1324"
     *              ),
     *              @SWG\Property(
     *                  property="vat_number",
     *                  type="string",
     *
     *                  example="www.website.com",
     *                  description="Required if account is a company"
     *              ),
     *              @SWG\Property(
     *                  property="ean_number",
     *                  type="string",
     *
     *                  description="Can be passed if account is a company"
     *              ),
     *              @SWG\Property(
     *                  property="website",
     *                  type="string",
     *
     *                  example="www.website.com"
     *              ),
     *              @SWG\Property(
     *                  property="is_company",
     *                  type="string",
     *
     *                  enum={"0","1"},
     *                  default="0",
     *                  description="If account is private or a company"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="404", description="Account could not be found, or is not accessible"),
     *     @SWG\Response(response="422", description="This value does not exist")
     * )
     */

    /**
     * @SWG\Delete(
     *     path="/api/v1/accounts/{accountId}",
     *     summary="Delete account",
     *     tags={"Accounts"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         required=true,
     *         description="ID of the account",
     *         type="string",
     *         default="d5169733-b5be-4aab-b332-29a765d207e4"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="404", description="Account could not be found, or is not accessible"),
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/accounts/cvrsearch",
     *     summary="List user accounts",
     *     tags={"Accounts"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="type",
     *                  type="string",
     *
     *                  enum={"any", "vat", "name"},
     *                  example="any"
     *              ),
     *              @SWG\Property(
     *                  property="term",
     *                  type="string",
     *
     *                  description="Search word",
     *                  example="company"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="504", description="Gateway Timeout")
     * )
     */


    //Claims
    /**
     * @SWG\Get(
     *     path="/api/v1/claims",
     *     summary="List user claims",
     *     tags={"Claims"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="count",
     *         in="query",
     *         description="Return user claims count only",
     *
     *         type="string",
     *         enum={"0", "1"},
     *
     *     ),
     *     @SWG\Parameter(
     *         name="includeClaimLineSum",
     *         in="query",
     *         description="Include claim line sum in response or not",
     *
     *         type="string",
     *         enum={"0", "1"},
     *
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Post(
     *     path="/api/v1/claims",
     *     summary="Add a claim for logged in user",
     *     tags={"Claims"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="debtor_id",
     *                  type="string",
     *
     *                  description="Debtor ID for the claim",
     *                  example="d3f9b622-fa75-473d-a96f-8a8613db17dc"
     *              ),
     *              @SWG\Property(
     *                  property="currency_id",
     *                  type="string",
     *
     *                  description="Currency ID for the claim",
     *                  example="e5243bf3-0a70-5924-94f6-af1d88c951c4"
     *              ),
     *              @SWG\Property(
     *                  property="dispute",
     *                  type="string",
     *
     *                  description="Dispute text, if any",
     *                  example="disputed claim bla bla bla"
     *              ),
     *
     *          )
     *     ),
     *     @SWG\Response(response="201", description="Created successfully")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/claims/{claimId}",
     *     summary="Get user claim detail",
     *     tags={"Claims"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604",
     *     ),
     *     @SWG\Parameter(
     *         name="showNames",
     *         in="query",
     *         description="Show details of debtors, claims phase and currency along with their IDs",
     *
     *         type="string",
     *         enum={"0", "1"}
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Delete(
     *     path="/api/v1/claims/{claimId}",
     *     summary="Delete claim",
     *     tags={"Claims"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         required=true,
     *         description="ID of the claim",
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     * )
     */


    //ClaimActions

    /**
     * @SWG\Get(
     *     path="/api/v1/claims/{claimId}/claim-actions",
     *     summary="Get claim actions",
     *     tags={"ClaimActions"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604",
     *     ),
     *     @SWG\Parameter(
     *         name="showNames",
     *         in="query",
     *         description="Show details of claim actions along with their IDs",
     *
     *         type="string",
     *         enum={"0", "1"}
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort results by field",
     *
     *         type="string",
     *         default="name"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="500"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Claim Financials
    /**
     * @SWG\Get(
     *     path="/api/v1/claims/{claimId}/claim-financials",
     *     summary="Get financials of a claim",
     *     tags={"ClaimFinancials"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604",
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Claim Lines
    /**
     * @SWG\Get(
     *     path="/api/v1/claims/{claimId}/claim-lines",
     *     summary="Get claim lines",
     *     tags={"ClaimLines"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *      @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604",
     *     ),
     *     @SWG\Parameter(
     *         name="showNames",
     *         in="query",
     *         description="Show details of claim lines along with their IDs",
     *
     *         type="string",
     *         enum={"0", "1"}
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Post(
     *     path="/api/v1/claims/{claimId}/claim-lines",
     *     tags={"ClaimLines"},
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     description="Create a claim line",
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimId",
     *         in="path",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604"
     *     ),
     *     @SWG\Parameter(
     *         name="claim_line_type_id",
     *         in="formData",
     *         description="Claim line type ID",
     *         required=true,
     *         type="string",
     *         default="01c44a06-0d43-494c-a998-de40ec41e546"
     *     ),
     *     @SWG\Parameter(
     *         name="currency_id",
     *         in="formData",
     *         description="Selected currency ID of claim",
     *         required=true,
     *         type="string",
     *         default="e5243bf3-0a70-5924-94f6-af1d88c951c4"
     *     ),
     *     @SWG\Parameter(
     *         name="invoice_date",
     *         in="formData",
     *         description="Invoice creation date",
     *         required=true,
     *         type="string",
     *         format="date",
     *         default="2019-10-02"
     *     ),
     *     @SWG\Parameter(
     *         name="maturity_date",
     *         in="formData",
     *         description="Invoice due date",
     *         required=true,
     *         type="string",
     *         format="date",
     *         default="2019-10-25"
     *     ),
     *     @SWG\Parameter(
     *         name="amount",
     *         in="formData",
     *         description="Claim amount",
     *         required=true,
     *         type="string",
     *         default="10000"
     *     ),
     *     @SWG\Parameter(
     *         name="invoice_reference",
     *         in="formData",
     *         description="Claim invoice number",
     *         required=true,
     *         type="string",
     *         default="invoice-1233"
     *     ),
     *     @SWG\Parameter(
     *         name="placement",
     *         in="formData",
     *         description="placement",
     *         required=true,
     *         type="string",
     *         default="0"
     *     ),
     *     @SWG\Parameter(
     *         name="claim_id",
     *         in="formData",
     *         description="Claim ID",
     *         required=true,
     *         type="string",
     *         default="ccfdbf43-00e2-4707-b1ed-c6237b61e604"
     *     ),
     *     @SWG\Parameter(
     *          name="file_name",
     *          in="formData",
     *          required=true,
     *          description="Claim file",
     *          type="file"
     *     ),
     *     @SWG\Response( response=200, description="Operation successful"),
     *     @SWG\Response( response=500, description="Internal server error")
     * )
     *
     */


    //ClaimLineTypes
    /**
     * @SWG\Get(
     *     path="/api/v1/claim-line-types",
     *     summary="Get list of all claim line types in system",
     *     tags={"ClaimLineTypes"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="300"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/claim-line-types/{claimLineTypeId}",
     *     summary="Get claim line type by ID",
     *     tags={"ClaimLineTypes"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="claimLineTypeId",
     *         in="path",
     *         description="claim line type ID",
     *         required=true,
     *         type="integer",
     *         default="01c44a06-0d43-494c-a998-de40ec41e546"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Countries
    /**
     * @SWG\Get(
     *     path="/api/v1/countries",
     *     summary="Get list of all countries in system",
     *     tags={"Countries"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *    @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort results by field",
     *
     *         type="string",
     *         default="name"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="300"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/countries/{countryId}",
     *     summary="Get country by ID",
     *     tags={"Countries"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="countryId",
     *         in="path",
     *         description="country ID",
     *         required=true,
     *         type="integer",
     *         default="03c0def7-043a-11e8-9a3c-080027548b1b"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Currencies
    /**
     * @SWG\Get(
     *     path="/api/v1/currencies",
     *     summary="Get list of all currencies in system",
     *     tags={"Currencies"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort results by field",
     *
     *         type="string",
     *         default="name"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="300"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/currencies/{currencyId}",
     *     summary="Get currency by ID",
     *     tags={"Currencies"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="currencyId",
     *         in="path",
     *         description="currency ID",
     *         required=true,
     *         type="integer",
     *         default="6270cd5a-15fb-96fe-0307-dccf4b4d0a7e"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Debtors
    /**
     * @SWG\Get(
     *     path="/api/v1/debtors",
     *     summary="Get logged in user's debtors",
     *     tags={"Debtors"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="How many items to return at one time",
     *
     *         type="integer",
     *         default="300"
     *     ),
     *      @SWG\Parameter(
     *         name="showCountries",
     *         in="query",
     *         description="Include countries in response or not",
     *
     *         type="string",
     *         enum={"0", "1"}
     *
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Post(
     *     path="/api/v1/debtors",
     *     summary="Add a debtor for logged in user",
     *     tags={"Debtors"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="account_id",
     *                  type="string",
     *
     *                  description="Account ID for the user",
     *                  example="d5169733-b5be-4aab-b332-29a765d207e4"
     *              ),
     *              @SWG\Property(
     *                  property="address",
     *                  type="string",
     *
     *                  example="test address"
     *              ),
     *              @SWG\Property(
     *                  property="city",
     *                  type="string",
     *
     *                  example="test city"
     *              ),
     *              @SWG\Property(
     *                  property="company_name",
     *                  type="string",
     *
     *                  example="",
     *                  description="Required if debtor is a company"
     *              ),
     *              @SWG\Property(
     *                  property="country_id",
     *                  type="string",
     *
     *                  example="03bfcf66-043a-11e8-9a3c-080027548b1b",
     *                  description="Selected country's ID. Get from /api/v1/countries/  "
     *              ),
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *
     *              ),
     *              @SWG\Property(
     *                  property="first_name",
     *                  type="string",
     *
     *                  example="private debtor first name",
     *                  description="Required if debtor is not a company"
     *              ),
     *              @SWG\Property(
     *                  property="last_name",
     *                  type="string",
     *
     *                  example="private debtor last name",
     *                  description="Required if debtor is not a company"
     *              ),
     *              @SWG\Property(
     *                  property="phone",
     *                  type="string",
     *
     *              ),
     *              @SWG\Property(
     *                  property="vat_number",
     *                  type="string",
     *
     *              ),
     *              @SWG\Property(
     *                  property="zip_code",
     *                  type="string",
     *
     *              ),
     *              @SWG\Property(
     *                  property="is_company",
     *                  type="string",
     *
     *                  enum={"1", "0"},
     *                  example="1",
     *                  description="Flag if debtor being saved is company or private"
     *              )
     *
     *          )
     *     ),
     *     @SWG\Response(response="201", description="Created successfully")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/debtors/{debtorId}",
     *     summary="Get debtor by ID",
     *     tags={"Debtors"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="debtorId",
     *         in="path",
     *         description="debtor ID",
     *         required=true,
     *         type="integer",
     *         default="078ef0cd-e2e1-4cf0-9a13-749a35b34b87"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Put(
     *     path="/api/v1/debtors/{debtorId}",
     *     summary="Update Debtor's data",
     *     tags={"Debtors"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="accountId",
     *         in="path",
     *         required=true,
     *         description="ID of the account",
     *         type="string",
     *         default="d5169733-b5be-4aab-b332-29a765d207e4"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="name",
     *                  type="string",
     *
     *                  description="Full name of the account",
     *                  example="John Doe"
     *              ),@SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *                  example="test@test.com"
     *              ),
     *              @SWG\Property(
     *                  property="address",
     *                  type="string",
     *
     *                  example="test address"
     *              ),
     *              @SWG\Property(
     *                  property="phone",
     *                  type="string",
     *
     *                  example="12345678"
     *              ),
     *              @SWG\Property(
     *                  property="zip_code",
     *                  type="string",
     *
     *                  example="1254"
     *              ),
     *              @SWG\Property(
     *                  property="city",
     *                  type="string",
     *
     *                  example="test city"
     *              ),
     *              @SWG\Property(
     *                  property="bank_account_number",
     *                  type="string",
     *
     *                  example="0987654321"
     *              ),
     *              @SWG\Property(
     *                  property="bank_reg_number",
     *                  type="string",
     *
     *                  example="1324"
     *              ),
     *              @SWG\Property(
     *                  property="vat_number",
     *                  type="string",
     *
     *                  example="www.website.com",
     *                  description="Required if account is a company"
     *              ),
     *              @SWG\Property(
     *                  property="ean_number",
     *                  type="string",
     *
     *                  description="Can be passed if account is a company"
     *              ),
     *              @SWG\Property(
     *                  property="website",
     *                  type="string",
     *
     *                  example="www.website.com"
     *              ),
     *              @SWG\Property(
     *                  property="is_company",
     *                  type="string",
     *
     *                  enum={"0","1"},
     *                  default="0",
     *                  description="If account is private or a company"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="404", description="Debtor could not be found, or is not accessible"),
     *     @SWG\Response(response="422", description="This value does not exist")
     * )
     */

    /**
     * @SWG\Delete(
     *     path="/api/v1/debtors/{debtorId}",
     *     summary="Delete debtor by ID",
     *     tags={"Debtors"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="debtorId",
     *         in="path",
     *         description="debtor ID",
     *         required=true,
     *         type="integer",
     *         default="078ef0cd-e2e1-4cf0-9a13-749a35b34b87"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //DebtorLawyers
    /**
     * @SWG\Post(
     *     path="/api/v1/debtor-lawyers",
     *     summary="Add a debtor for logged in user",
     *     tags={"DebtorLawyers"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="claim_id",
     *                  type="string",
     *
     *                  description="Claim ID",
     *                  example="ccfdbf43-00e2-4707-b1ed-c6237b61e604"
     *              ),
     *              @SWG\Property(
     *                  property="name",
     *                  type="string",
     *
     *                  example="Debtor liar lawyer"
     *              ),
     *              @SWG\Property(
     *                  property="city",
     *                  type="string",
     *
     *                  example="liar lawyer city",
     *              ),
     *              @SWG\Property(
     *                  property="zip_code",
     *                  type="string",
     *
     *                  example="1234",
     *              ),
     *              @SWG\Property(
     *                  property="address",
     *                  type="string",
     *
     *                  example="liar lawyer address",
     *              ),
     *              @SWG\Property(
     *                  property="email",
     *                  type="string",
     *
     *              ),
     *              @SWG\Property(
     *                  property="phone",
     *                  type="string",
     *
     *              ),
     *              @SWG\Property(
     *                  property="vat_number",
     *                  type="string",
     *
     *              )
     *
     *          )
     *     ),
     *     @SWG\Response(response="201", description="Created successfully")
     * )
     */


    //Integrations
    /**
     * @SWG\Get(
     *     path="/api/v1/integrations/economic/status",
     *     summary="Get status of economic integration",
     *     tags={"Integrations"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */


    //Users
    /**
     * @SWG\Get(
     *     path="/api/v1/users",
     *     summary="Get logged in user data",
     *     tags={"Users"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Get(
     *     path="/api/v1/users/{userId}",
     *     summary="Get user by ID",
     *     tags={"Users"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="path",
     *         required=true,
     *         description="ID of the user",
     *         type="string",
     *         default="0159f5d4-8d88-476e-a729-be3f2dd170b5"
     *     ),
     *     @SWG\Response(response="200", description="Operation successful")
     * )
     */

    /**
     * @SWG\Put(
     *     path="/api/v1/users/{userId}",
     *     summary="Update user data",
     *     tags={"Users"},
     *
     *     @SWG\Parameter(
     *         name="Authorization",
     *         in="header",
     *         description="Authorization Bearer token",
     *         required=true,
     *         type="string",
     *         default="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwMTU5ZjVkNC04ZDg4LTQ3NmUtYTcyOS1iZTNmMmRkMTcwYjUiLCJleHAiOjE1NzI1MjM3ODksImlhdCI6MTU3MTkxODk4OX0.I3iN0lhL71sw2-Qbg84Xr6TV26K8hg2aD0OJZi3iWcs"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="path",
     *         required=true,
     *         description="The id of the user to whose account to set active",
     *         type="string",
     *         default="0159f5d4-8d88-476e-a729-be3f2dd170b5"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="active_account_id",
     *                  type="string",
     *
     *                  description="Account ID to set active for the user",
     *                  example="d5169733-b5be-4aab-b332-29a765d207e4"
     *              ),
     *              @SWG\Property(
     *                  property="first_name",
     *                  type="string",
     *
     *                  description="First name of the user",
     *                  example="John"
     *              ),
     *              @SWG\Property(
     *                  property="last_name",
     *                  type="string",
     *
     *                  description="Last name of the user",
     *                  example="Doe"
     *              ),
     *              @SWG\Property(
     *                  property="phone",
     *                  type="string",
     *
     *                  description="Phone# of the user",
     *                  example="12345678"
     *              ),
     *              @SWG\Property(
     *                  property="old_password",
     *                  type="string",
     *                  format="password",
     *
     *                  description="Current password of the user",
     *                  example="15sds678"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string",
     *                  format="password",
     *
     *                  description="New password to set for the user",
     *                  example="sda33ssds"
     *              )
     *          )
     *     ),
     *     @SWG\Response(response="200", description="Operation successful"),
     *     @SWG\Response(response="404", description="User could not be found, or is not accessible"),
     *     @SWG\Response(response="422", description="This value does not exist")
     * )
     */


