<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddLoginCodeToDebtors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('debtors');

        $table->addColumn('login_code', 'string', [
            'default' => null,
            'limit' => 6,
            'null' => false,
            'after' => 'erp_integration_foreign_key',
        ]);

        $table->update();
    }
}
