<?php
use Migrations\AbstractMigration;

class AddPlansData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('plans')
        ->insert([
            [
                'id' => '5768a0c5-34b7-776b-9257-77411c692b06',
                'name' => 'Trial',
                'price' => '0',
                'billing_interval' => 'MONTHLY',
                'created' => '2020-04-18 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ],
            [
                'id' => 'e003063d-8808-4252-88a9-eaa1eeac53a8',
                'name' => 'Professional',
                'price' => '599',
                'billing_interval' => 'MONTHLY',
                'created' => '2020-04-19 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ],
            [
                'id' => '04fbe865-4252-776b-9257-8deffe38271a',
                'name' => 'Premium',
                'price' => '1499',
                'billing_interval' => 'MONTHLY',
                'created' => '2020-04-20 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ],
            [
                'id' => 'e006373d-4252-4252-88a9-eaapl7ac53a8',
                'name' => 'Professional',
                'price' => '7188',
                'billing_interval' => 'YEARLY',
                'created' => '2020-04-21 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ],
            [
                'id' => '04fb0961-4252-8808-9257-8depi938271a',
                'name' => 'Premium',
                'price' => '17988',
                'billing_interval' => 'YEARLY',
                'created' => '2020-04-22 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ],
            [
                'id' => '555c21f9-8808-4252-9257-eaa1eeac53a8',
                'name' => 'Free subscription',
                'price' => '0',
                'billing_interval' => 'MONTHLY',
                'created' => '2020-04-23 16:21:06',
                'modified' => '2020-04-28 16:21:06'
            ]
        ])
        ->save();
    }
}
