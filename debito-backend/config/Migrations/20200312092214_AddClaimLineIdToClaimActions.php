<?php
use Migrations\AbstractMigration;

class AddClaimLineIdToClaimActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');
        $table->addColumn('claim_line_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'claim_id',
        ]);
        $table->update();
    }
}
