<?php
use Migrations\AbstractMigration;

class AddSoftwarningToClaimLineTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
        INSERT INTO `claim_line_types` (`id`, `identifier`, `name`, `description`, `created`, `modified`, `deleted`)
        VALUES
            ('b34c5777-3b86-4649-9f27-de40ec41e546', 'soft_warning', 'Venlig påmindelse', NULL, '2020-06-18 00:12:49', '2020-06-18 00:12:49', NULL);
        SQL;
        $this->execute($sql);
    }
}
