<?php

use Migrations\AbstractMigration;

class AddInvoiceIdToClaimLines extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_lines');
        $table->addColumn('invoice_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'currency_id',
        ]);
        $table->addIndex([
            'invoice_id',
        ], [
            'name' => 'BY_INVOICE_ID',
            'unique' => false,
        ]);
        $table->update();
    }
}
