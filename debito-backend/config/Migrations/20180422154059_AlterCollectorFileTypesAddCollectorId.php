<?php

use Migrations\AbstractMigration;

class AlterCollectorFileTypesAddCollectorId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('collector_file_types');

        $table->addColumn('collector_id', 'uuid', [
            'default' => null,
            'null' => false,
            'after' => 'id',
        ]);
        $table->addIndex([
            'collector_id',
        ], [
            'name' => 'BY_COLLECTOR_ID',
            'unique' => false,
        ]);

        $table->update();
    }
}
