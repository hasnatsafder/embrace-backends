<?php

use Migrations\AbstractMigration;

class CreateClaimPayments extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_payments');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('collector_file_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('currency_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('payment_date', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('paid_fee_without_vat', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('paid_fee_with_vat', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('vat_of_fees', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('vat_of_commision', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('commision', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('gross_paid', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('paid_to_creditor', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('paid_principal', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('paid_fees', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('paid_interest', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('remaining_principal', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('remaining_total', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('raw_content', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'claim_id',
        ], [
            'name' => 'BY_CLAIM_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'collector_file_id',
        ], [
            'name' => 'BY_COLLECTOR_FILE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'payment_date',
        ], [
            'name' => 'BY_PAYMENT_DATE',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
