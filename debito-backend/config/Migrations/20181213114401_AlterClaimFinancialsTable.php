<?php

use Migrations\AbstractMigration;

class AlterClaimFinancialsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_financials');

        $table->removeColumn('collector_file_id');
        $table->removeColumn('date');
        $table->removeColumn('content');
        $table->removeColumn('deleted');

        $table->addColumn('hovedstol', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('rykkergebyrer', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('ovrige_gebyrer', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('renter', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('retsgebyrer', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('inkassoomkostninger', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('tilkendt_modesalar', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('indbetalt', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('restgald', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
