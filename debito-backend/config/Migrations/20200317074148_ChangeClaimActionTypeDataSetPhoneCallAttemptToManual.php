<?php
use Migrations\AbstractMigration;

class ChangeClaimActionTypeDataSetPhoneCallAttemptToManual extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute(<<<SQL
UPDATE `claim_action_types` SET `is_automated` = '0' WHERE `id` = '05b95bf3-0b5e-4884-af1b-baf2621dcb12';
SQL
);
    }
}
