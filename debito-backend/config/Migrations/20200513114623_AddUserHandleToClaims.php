<?php
use Migrations\AbstractMigration;

class AddUserHandleToClaims extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table = $table->removeColumn('old_id');
        $table->addColumn('user_handle', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'modified_by_id',
        ]);
        $table->update();
    }
}
