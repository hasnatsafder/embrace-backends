<?php

use Migrations\AbstractMigration;

class UpdateClaimPhaseActionTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_action_types');
        $table->changeColumn('claim_phase_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
