<?php

use Migrations\AbstractMigration;

class AddPdfPathToInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');
        $table->addColumn('pdf_file_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
            'after' => 'gross_amount',
        ]);
        $table->update();
    }
}
