<?php

use Migrations\AbstractMigration;

class AddCollectorsData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `collectors` (`id`, `name`, `identifier`, `is_active`, `created`, `modified`, `deleted`, `email`)
VALUES
	('5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Lindorff', 'lindorff', 1, '2018-04-09 15:03:01', '2018-04-09 15:03:01', NULL, "kundesupport.dk@lindorff.com"),
	('6b296e78-3234-49bc-ba6d-4a608c2a607c', 'Mortang', 'mortang', 1, '2018-04-09 15:03:12', '2018-04-09 15:03:12', NULL, "debito@mortang.dk"),
    ('7d296e78-3234-49bc-ba6d-4a608c2a6081', 'Collectia', 'collectia', 1, '2019-04-09 15:03:12', '2019-04-09 15:03:12', NULL, NULL);
SQL;

        $this->execute($sql);
    }
}
