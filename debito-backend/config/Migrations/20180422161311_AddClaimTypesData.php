<?php

use Migrations\AbstractMigration;

class AddClaimTypesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `claim_types` (`id`, `name`, `identifier`, `created`, `modified`, `deleted`)
VALUES
	('254652c4-de20-44b8-888e-5d4c0b60cfcd', 'Rykkerservice', 'reminder_service', '2018-04-22 16:13:02', '2018-04-22 16:13:02', NULL),
	('6a17bcd0-6c64-4b66-bd13-3738abf4c1c3', 'Default Debito Case Type', 'default_case_type', '2018-04-22 16:12:49', '2018-04-22 16:12:49', NULL),
	('945bee35-f20e-4199-87df-8df1bcc4fbb2', 'Inkasso', 'debt_collection', '2018-04-22 16:12:23', '2018-04-22 16:12:23', NULL);
SQL;
        $this->execute($sql);
    }
}
