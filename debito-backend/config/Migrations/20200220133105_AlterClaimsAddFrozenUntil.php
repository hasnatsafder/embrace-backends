<?php

use Migrations\AbstractMigration;

class AlterClaimsAddFrozenUntil extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');

        $table->addColumn('frozen_until', 'datetime', [
            'default' => null,
            'null' => true,
            'after' => 'ended_at',
        ]);

        $table->update();
    }
}
