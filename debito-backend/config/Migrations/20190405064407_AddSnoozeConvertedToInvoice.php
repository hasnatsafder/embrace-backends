<?php

use Migrations\AbstractMigration;

class AddSnoozeConvertedToInvoice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');
        $table->addColumn(
            'snoozed',
            'boolean',
            [
                'after' => 'pdf_file_path',
                'default' => 0,
            ]
        );
        $table->addColumn(
            'converted_to_claim',
            'boolean',
            [
                'after' => 'pdf_file_path',
                'default' => 0,
            ]
        );
        $table->update();
    }
}
