<?php

use Migrations\AbstractMigration;

class AddClaimTransactionTypesData extends AbstractMigration
{
    public function down()
    {

    }
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function up()
    {
        $this->table('claim_transaction_types')
            ->insert([
                [
                    'id' => 'f99c7676-e4ea-44ef-b7ea-dfd100e4754f',
                    'identifier' => 'warning_one',
                    'name' => 'Warning 1',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'e35383d6-ff72-441f-a4f5-d365a350e4d2',
                    'identifier' => 'warning_two',
                    'name' => 'Warning 2',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '0eb0237c-4ee0-4ac8-ae0f-fd591dc30fee',
                    'identifier' => 'warning_three',
                    'name' => 'Warning 3',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '380ebb1c-c07f-424f-9457-e14ef8d5045c',
                    'identifier' => 'interest_8_05',
                    'name' => 'Interest (8,05%)',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'c689820e-3c14-4dcc-b87b-7d464a826887',
                    'identifier' => 'debt_collection_commission',
                    'name' => 'Debt collection commission',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '214b8619-63b6-4ee0-b755-5167f5a3c596',
                    'identifier' => 'debt_collection_compensation_fee',
                    'name' => 'Debt collection compensation fee',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '7336b94f-25eb-46c4-9ad4-65a7aa5c5e9e',
                    'identifier' => 'payment_fi',
                    'name' => 'Indbetaling (FI)',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'd9c6ef4d-d9b7-4ba5-b3f0-601c51f63ff9',
                    'identifier' => 'payment_manual',
                    'name' => 'Indbetaling (manual)',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '95bde242-c9cc-48ab-8fc4-7d4c21dc2211',
                    'identifier' => 'end_of_claim_rounding',
                    'name' => 'Rounding when claim is ending',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
                [
                    'id' => '826fccb0-bf1f-4b3b-8dda-ee75949e898d',
                    'identifier' => 'principal',
                    'name' => 'Principal',
                    'description' => null,
                    'created' => '2020-02-26 12:50:00',
                    'modified' => '2020-02-26 12:50:00',
                    'deleted' => null,
                ],
            ])
            ->save();
    }
}
