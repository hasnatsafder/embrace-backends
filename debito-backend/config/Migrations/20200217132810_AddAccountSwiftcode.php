<?php
use Migrations\AbstractMigration;

class AddAccountSwiftcode extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('accounts');
        $table->addColumn('swift_code', 'string', [
            'after' => 'iban',
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
