<?php
use Migrations\AbstractMigration;

class AddDebitoAsCollector extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute(<<<SQL
INSERT INTO `collectors` (`id`, `name`, `identifier`, `is_active`, `created`, `modified`, `deleted`, `email`)
VALUES
	(X'65613463353031392D383636612D343831392D383237302D326566623465333036303462', X'44656269746F20417053', X'64656269746F', 1, '2020-02-17 08:59:20', '2020-02-17 08:59:20', NULL, NULL);
SQL
);
    }
}
