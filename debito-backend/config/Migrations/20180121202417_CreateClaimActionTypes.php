<?php

use Migrations\AbstractMigration;

class CreateClaimActionTypes extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_action_types');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_phase_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('collector_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('identifier', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('collector_identifier', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('description', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('is_public', 'boolean', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'claim_phase_id',
        ], [
            'name' => 'BY_CLAIM_PHASE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'collector_id',
        ], [
            'name' => 'BY_COLLECTOR_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'identifier',
        ], [
            'name' => 'index',
            'unique' => true,
        ]);
        $table->addIndex([
            'collector_identifier',
        ], [
            'name' => 'BY_COLLECTOR_IDENTIFIER',
            'unique' => false,
        ]);
        $table->addIndex([
            'is_public',
        ], [
            'name' => 'BY_IS_PUBLIC',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
