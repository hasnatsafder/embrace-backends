<?php

use Migrations\AbstractMigration;

class AlterErpIntegrationDataChangeValueColumnType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('erp_integration_data');

        $table->changeColumn('value', 'text', [
            'default' => null,
            'null' => false,
        ]);

        $table->update();
    }
}
