<?php

use Migrations\AbstractMigration;

class AddRoleData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $this->execute(<<<SQL
INSERT INTO `roles` (`id`, `name`, `identifier`, `created`, `modified`, `deleted`)
VALUES
	('31cd8a5f-7645-4d56-b180-c6c633c5e32f', 'Accountant', 'accountant', '2018-01-28 14:10:23', '2018-01-28 14:10:23', NULL),
	('ee7a94f3-646a-4e95-8f5f-17b1ad59c3ec', 'Owner', 'owner', '2018-01-28 13:56:49', '2018-01-28 13:56:49', NULL),
	('a2938a72-899a-453b-8f06-c432314192ae', 'Root', 'root', '2018-01-28 14:16:20', '2018-01-28 14:16:20', NULL),
  ('a2938a72-899a-453b-8f06-c432318987er', 'Consultant', 'consultant', '2018-01-28 14:16:20', '2018-01-28 14:16:20', NULL),
	('ae7800b8-9b0f-4ef4-9a42-d0e49a9466c5', 'User', 'user', '2018-01-28 14:10:09', '2018-01-28 14:10:09', NULL);
SQL
        );
    }
}
