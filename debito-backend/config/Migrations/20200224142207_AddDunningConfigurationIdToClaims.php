<?php

use Migrations\AbstractMigration;

class AddDunningConfigurationIdToClaims extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('dunning_configuration_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'collector_id',
        ]);
        $table->update();
    }
}
