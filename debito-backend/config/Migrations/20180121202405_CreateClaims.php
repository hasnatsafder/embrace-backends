<?php

use Migrations\AbstractMigration;

class CreateClaims extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created_by_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('account_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('debtor_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('claim_phase_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('customer_reference', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('collector_reference', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('dispute', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('completed', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('synced', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('ended_at', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('rejected_reason', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'created_by_id',
        ], [
            'name' => 'BY_CREATED_BY_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'account_id',
        ], [
            'name' => 'BY_ACCOUNT_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'debtor_id',
        ], [
            'name' => 'BY_DEBTOR_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'claim_phase_id',
        ], [
            'name' => 'BY_CLAIM_PHASE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'customer_reference',
        ], [
            'name' => 'BY_CUSTOMER_REFERENCE',
            'unique' => false,
        ]);
        $table->addIndex([
            'collector_reference',
        ], [
            'name' => 'BY_COLLECTOR_REFERENCE',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
