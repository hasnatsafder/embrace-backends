<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddDoneToClaimActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');
        $table->addColumn('done', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
            'null' => true,
            'after' => 'date',
        ]);
        $table->update();

        //Make sure all existing data also has kinda valid date in this new column
        $this->execute(<<<SQL
UPDATE `claim_actions`
SET `done` = `date` + INTERVAL 12 HOUR
SQL
);
    }
}
