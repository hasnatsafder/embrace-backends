<?php
use Migrations\AbstractMigration;

class CreateNotifications extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('notifications');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created_by_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('account_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('user_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('notification_summary_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('template', 'smallinteger', [
            'default' => null,
            'signed' => false,
            'limit' => 2,
            'null' => false,
        ]);
        $table->addColumn('data_values', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'account_id',
        ], [
            'name' => 'BY_ACCOUNT_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'user_id',
            'notification_summary_id',
        ], [
            'name' => 'BY_USER_ID_NOTIFICATION_SUMMARY_ID',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
