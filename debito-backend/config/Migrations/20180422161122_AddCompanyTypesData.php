<?php

use Migrations\AbstractMigration;

class AddCompanyTypesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `company_types` (`id`, `name`, `code`, `created`, `modified`, `deleted`)
VALUES
	('2e09f5f5-4647-11e8-9a55-080027ee1df7', 'Enkeltmandsvirksomhed', '10', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f6c0-4647-11e8-9a55-080027ee1df7', 'Interessentselskab (I/S)', '30', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f6e1-4647-11e8-9a55-080027ee1df7', 'Iværksætterselskab (IVS)', '81', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f6f6-4647-11e8-9a55-080027ee1df7', 'Anpartsselskab (ApS)', '80', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f70a-4647-11e8-9a55-080027ee1df7', 'Aktieselskab (A/S)', '60', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f71c-4647-11e8-9a55-080027ee1df7', 'Kommanditselskab (K/S)', '40', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f730-4647-11e8-9a55-080027ee1df7', 'Andelsselskab med begrænset ansvar (a.m.b.a.)', '140', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f744-4647-11e8-9a55-080027ee1df7', 'Forening med begrænset ansvar (f.m.b.a.)', '152', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f756-4647-11e8-9a55-080027ee1df7', 'Selskab med begrænset ansvar (s.m.b.a.)', '151', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f76a-4647-11e8-9a55-080027ee1df7', 'Interessentskab', '20', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f77c-4647-11e8-9a55-080027ee1df7', 'Anden udenlandsk virksomhed', '210', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f78f-4647-11e8-9a55-080027ee1df7', 'Fonde og andre selvejende institutioner', '90', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7a2-4647-11e8-9a55-080027ee1df7', 'Erhvervsdrivende fond', '100', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7b4-4647-11e8-9a55-080027ee1df7', 'Frivillig forening', '115', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7c6-4647-11e8-9a55-080027ee1df7', 'Kommanditaktieselskab/Partnerselskab', '70', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7d8-4647-11e8-9a55-080027ee1df7', 'Forening', '110', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7eb-4647-11e8-9a55-080027ee1df7', 'Personlig Mindre Virksomhed', '15', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f7fd-4647-11e8-9a55-080027ee1df7', 'Enhed under oprettelse i Erhvervsstyrelsen', '270', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f80f-4647-11e8-9a55-080027ee1df7', 'Statslig administrativ enhed', '230', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f826-4647-11e8-9a55-080027ee1df7', 'Filial af udenlandsk aktieselskab, kommanditakties', '170', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f839-4647-11e8-9a55-080027ee1df7', 'Aktieselskab', '0', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f84b-4647-11e8-9a55-080027ee1df7', 'Iværksætterselskab', '0', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f85e-4647-11e8-9a55-080027ee1df7', 'Anpartsselskab', '0', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f870-4647-11e8-9a55-080027ee1df7', 'Primærkommune', '0', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f883-4647-11e8-9a55-080027ee1df7', 'Personligt ejet Mindre Virksomhed', '0', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL),
	('2e09f895-4647-11e8-9a55-080027ee1df7', 'Øvrige virksomhedsformer', '280', '2018-04-22 15:54:41', '2018-04-22 15:54:46', NULL);
SQL;
        $this->execute($sql);
    }
}
