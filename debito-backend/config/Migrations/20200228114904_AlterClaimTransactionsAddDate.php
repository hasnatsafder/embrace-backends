<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AlterClaimTransactionsAddDate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function up()
    {
        $this->execute(<<<SQL
ALTER TABLE `claim_transactions`
ADD `date` DATE
AS (DATE(`timestamp`)) PERSISTENT
AFTER `timestamp`
SQL
        );

        $table = $this->table('claim_transactions');

        $table->addIndex([
            'claim_id',
            'claim_transaction_type_id',
            'date',
        ], [
            'name' => 'BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID_DATE',
            'unique' => false,
        ]);

        if ($table->hasIndexByName('BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID')) {
            $table->removeIndexByName('BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID');
        }


        $table->save();
    }

    public function down()
    {
        $table = $this->table('claim_transactions');

        $table->removeColumn('date');

        if ($table->hasIndexByName('BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID_DATE')) {
            $table->removeIndexByName('BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID_DATE');
        }

        $table->save();
    }
}
