<?php
use Migrations\AbstractMigration;

class AddClaimRejectedIsPublic extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');

        $table->addColumn('rejected_is_public', 'boolean', [
            'default' => false,
            'null' => false,
            'after' => 'frozen_until',
        ]);
        $table->update();
    }
}
