<?php
use Migrations\AbstractMigration;

class AddClaimAcionTypeForDebtorMessage extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $debitoCollectorId = 'ea4c5019-866a-4819-8270-2efb4e30604b';

        $this->table('claim_action_types')
            ->insert([
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43ce',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'debtor_message',
                    'collector_identifier' => 'debtor_message',
                    'name' => 'Message from debtor',
                    'description' => 'Messages written from debtors',
                    'is_public' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
            ])
            ->save();
    }
}
