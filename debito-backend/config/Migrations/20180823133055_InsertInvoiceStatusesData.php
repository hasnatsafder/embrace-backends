<?php

use Migrations\AbstractMigration;

class InsertInvoiceStatusesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function up()
    {
        $this->table('invoice_statuses')
            ->insert([
                [
                    'id' => 'c989f40b-3830-4b91-92ca-4f983bfbc82e',
                    'identifier' => 'pending',
                    'name' => 'Pending',
                    'description' => 'No action has been taken on this invoice yet',
                    'placement' => '0',
                    'created' => '2018-08-23 13:30:33',
                    'modified' => '2018-08-23 13:30:33',
                    'deleted' => null,
                ],
                [
                    'id' => '393a0457-68a2-4e97-822e-9a90c7a756f9',
                    'identifier' => 'paid',
                    'name' => 'Paid',
                    'description' => 'This invoice has been paid',
                    'placement' => '4',
                    'created' => '2018-08-23 13:30:33',
                    'modified' => '2018-08-23 13:30:33',
                    'deleted' => null,
                ],
                [
                    'id' => '740a2bbf-7f1b-4060-a1e8-544bb286e9fd',
                    'identifier' => 'in_progress',
                    'name' => 'In Progress',
                    'description' => 'This invoice has a claim running currently',
                    'placement' => '3',
                    'created' => '2018-08-23 13:30:33',
                    'modified' => '2018-08-23 13:30:33',
                    'deleted' => null,
                ],
                [
                    'id' => '973ef2dd-2157-4532-aed8-229bbf315249',
                    'identifier' => 'ignored',
                    'name' => 'Ignored',
                    'description' => 'This invoice is being ignored for now',
                    'placement' => '2',
                    'created' => '2018-08-23 13:30:33',
                    'modified' => '2018-08-23 13:30:33',
                    'deleted' => null,
                ],
                [
                    'id' => 'c89f88bb-28d6-4397-9cc5-d55438f4718d',
                    'identifier' => 'watching',
                    'name' => 'Watching',
                    'description' => 'This invoice is being watched',
                    'placement' => '1',
                    'created' => '2018-08-23 13:30:33',
                    'modified' => '2018-08-23 13:30:33',
                    'deleted' => null,
                ],
            ])
            ->update();
    }

    public function down()
    {
        $this->execute("DELETE FROM `invoice_statuses` WHERE `id` IN ('393a0457-68a2-4e97-822e-9a90c7a756f9','740a2bbf-7f1b-4060-a1e8-544bb286e9fd','973ef2dd-2157-4532-aed8-229bbf315249','c89f88bb-28d6-4397-9cc5-d55438f4718d');");
    }
}
