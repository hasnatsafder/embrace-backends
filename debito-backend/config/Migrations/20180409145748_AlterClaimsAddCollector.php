<?php

use Migrations\AbstractMigration;

class AlterClaimsAddCollector extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('collector_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'claim_phase_id',
        ]);
        $table->addIndex([
            'collector_id',
        ], [
            'name' => 'BY_COLLECTOR_ID',
            'unique' => false,
        ]);
        $table->update();
    }
}
