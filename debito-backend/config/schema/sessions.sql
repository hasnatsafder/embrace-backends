# Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
#
# Licensed under The MIT License
# For full copyright and license information, please see the LICENSE.txt
# Redistributions of files must retain the above copyright notice.
# MIT License (https://opensource.org/licenses/mit-license.php)

CREATE TABLE `sessions` (
    `id`       CHAR(40) CHARACTER SET ascii
    COLLATE ascii_bin NOT NULL,
    `created`  DATETIME         DEFAULT CURRENT_TIMESTAMP, -- optional, requires MySQL 5.6.5+
    `modified` DATETIME         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, -- optional, requires MySQL 5.6.5+
    `data`     BLOB             DEFAULT NULL, -- for PostgreSQL use bytea instead of blob
    `expires`  INT(10) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;
