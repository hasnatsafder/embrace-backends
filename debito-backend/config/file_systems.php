<?php
declare(strict_types=1);

use Cake\Core\Configure;
use League\Flysystem\Adapter\Local;

return [
    'WyriHaximus' => [
        'FlyPie' => [
            'generic' => [
                'adapter' => Local::class,
                'vars' => [
                    'path' => ROOT,
                ],
            ],
            'internal_case_files' => [
                'adapter' => Local::class,
                'vars' => [
                    'path' => WWW_ROOT . 'files' . DS . 'internal_case_files' . DS,
                ],
            ],
            'erp_invoices' => [
                'adapter' => Local::class,
                'vars' => [
                    'path' => WWW_ROOT . 'files' . DS . 'EconomyPie' . DS . 'invoices' . DS,
                ],
            ],
            'local_tmp' => [
                'adapter' => Local::class,
                'vars' => [
                    'path' => TMP . 'files' . DS,
                ],
            ],
            'sys_temp' => [
                'adapter' => Local::class,
                'vars' => [
                    'path' => sys_get_temp_dir() . DS,
                ],
            ],
            'mortang' => [
                'adapter' => !empty(Configure::read('WyriHaximus.FlyPie.mortang.adapter')) ?
                    Configure::read('WyriHaximus.FlyPie.mortang.adapter') :
                    'Local',
                'vars' => !empty(Configure::read('WyriHaximus.FlyPie.mortang.vars')) ?
                    Configure::read('WyriHaximus.FlyPie.mortang.vars') :
                    ['path' => WWW_ROOT . 'files' . DS . 'remote_mock' . DS . 'mortang' . DS],
            ],
        ],
    ],
];
