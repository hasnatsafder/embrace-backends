<?php

use Cake\Core\Configure;

return [
    'Swagger' => [
        'ui' => [
            'title' => 'Debito Swagger',
            'validator' => false,
            'api_selector' => true,
            'route' => '/alt3/swagger/',
            'schemes' => [Configure::read('http_type', 'https')],
        ],
        'docs' => [
            'crawl' => Configure::read('debug'),
            'route' => '/alt3/swagger/docs/',
            'cors' => [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST',
                'Access-Control-Allow-Headers' => 'X-Requested-With',
            ],
        ],
        'library' => [
            'api' => [
                'include' => ROOT . DS . 'docs',
                'exclude' => [
                    '/Editor/',
                ],
            ],
            'editor' => [
                'include' => [
                    ROOT . DS . 'plugins' . DS . 'API' . DS . 'src' . DS . 'Controller' . DS . 'AppController.php',
                    ROOT . DS . 'src' . DS . 'Controller' . DS . 'AppController.php',
                    ROOT . DS . 'src' . DS . 'Controller' . DS . 'Editor',
                    ROOT . DS . 'src' . DS . 'Model',
                ],
            ],
        ],
    ],
];
