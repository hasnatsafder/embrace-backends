<?php
declare(strict_types=1);

return [
    'Integrations' => [
        'erp' => [
            'economic' => [
                'authUrl' => 'https://secure.e-conomic.com/secure/api1/requestaccess.aspx',
            ],
        ],
    ],
];
