<?php
declare(strict_types=1);

return [
    'DebtCollection' =>
        [
            'FiPayments' => [
                'CreditorCode' => '84316865',
            ],
            'InterestRateForClaims' => 8.05,
            'DaysInDebtCollectionYear' => 360,
            'DunningFees' => [
                'DK' => [
                    'WarningFee' => 100,
                    'CompensationFee' => 310,
                ],
            ],
        ],
];
