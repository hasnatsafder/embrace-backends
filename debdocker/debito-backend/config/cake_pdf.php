<?php
declare(strict_types=1);

return [
    'CakePdf' => [
        'engine' => [
            'className' => 'CakePdf.WkHtmlToPdf',
            'binary' => '/usr/local/bin/wkhtmltopdf',
            'options' => [
                'print-media-type' => false,
                'outline' => true,
                'dpi' => 96,
            ],
        ],
    ],
];
