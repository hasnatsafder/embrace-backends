<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddMessageToClaimActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');

        $table->addColumn('message', 'text', [
            'default' => null,
            'null' => true,
            'after' => 'date',
            'collation' => 'utf8mb4_bin',
            'encoding' => 'utf8mb4',
        ]);

        $table->update();
    }
}
