<?php

use Migrations\AbstractMigration;

class AddEmailCollectors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('collectors');
        $table->addColumn('email', 'string', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
