<?php
use Migrations\AbstractMigration;

class AddInvoicePaidTable extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('paid_invoices');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created_by_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified_by_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('account_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('invoice_status_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('currency_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('erp_integration_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('erp_integration_foreign_key', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('debtor_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('invoice_number', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('issue_date', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('due_date', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('net_amount', 'decimal', [
            'default' => null,
            'precision' => 12,
            'scale' => 3,
            'null' => false,
        ]);
        $table->addColumn('gross_amount', 'decimal', [
            'default' => null,
            'precision' => 12,
            'scale' => 3,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'created_by_id',
        ], [
            'name' => 'BY_CREATED_BY_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'modified_by_id',
        ], [
            'name' => 'BY_MODIFIED_BY_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'account_id',
        ], [
            'name' => 'BY_ACCOUNT_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'invoice_status_id',
        ], [
            'name' => 'BY_INVOICE_STATUS_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'debtor_id',
        ], [
            'name' => 'BY_DEBTOR_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'invoice_number',
        ], [
            'name' => 'BY_INVOICE_NUMBER',
            'unique' => false,
        ]);
        $table->addIndex([
            'issue_date',
        ], [
            'name' => 'BY_ISSUE_DATE',
            'unique' => false,
        ]);
        $table->addIndex([
            'due_date',
        ], [
            'name' => 'BY_DUE_DATE',
            'unique' => false,
        ]);
        $table->addIndex([
            'erp_integration_id',
        ], [
            'name' => 'BY_ERP_INTEGRATION_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'erp_integration_foreign_key',
        ], [
            'name' => 'BY_ERP_INTEGRATION_FOREIGN_KEY',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
