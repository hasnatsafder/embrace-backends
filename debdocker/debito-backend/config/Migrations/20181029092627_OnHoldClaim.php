<?php

use Migrations\AbstractMigration;

class OnHoldClaim extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn(
            'on_hold',
            'boolean',
            [
                'after' => 'ended_at',
                'default' => 0,
            ]
        );
        $table->update();
    }
}
