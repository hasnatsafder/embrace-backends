<?php
use Migrations\AbstractMigration;

class AddFreezeClaimActionType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $debitoCollectorId = 'ea4c5019-866a-4819-8270-2efb4e30604b';

        $this->table('claim_action_types')
            ->insert([
                [
                    'id' => '1ff6d610-b608-41b2-ac9f-7c697bce12ba',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'claim_freeze',
                    'collector_identifier' => 'claim_freeze',
                    'name' => 'Freeze the claim',
                    'description' => 'Do nothing until the freeze period ends',
                    'is_public' => 1,
                    'created' => '2020-02-21 10:27:00',
                    'modified' => '2020-02-21 10:27:00',
                    'deleted' => null,
                ],
            ])
            ->save();
    }
}
