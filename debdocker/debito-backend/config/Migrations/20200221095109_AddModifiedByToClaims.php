<?php

use Migrations\AbstractMigration;

class AddModifiedByToClaims extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('modified_by_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'created_by_id',
        ]);
        $table->update();
    }
}
