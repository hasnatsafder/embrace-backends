<?php

use Migrations\AbstractMigration;

class AddInvalidAccountCollectors extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('accounts_collectors');
        $table->addColumn(
            'invalid',
            'boolean',
            [
                'after' => 'collector_identifier',
                'default' => 0,
            ]
        );
        $table->update();
    }
}
