<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddUserIdToClaimActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');
        $table->addColumn('user_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'modified_by_id',
        ]);
        $table->update();
    }
}
