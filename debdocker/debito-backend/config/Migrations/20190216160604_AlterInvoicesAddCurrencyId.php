<?php

use Migrations\AbstractMigration;

class AlterInvoicesAddCurrencyId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');
        $table->addColumn('currency_id', 'uuid', [
            'default' => null,
            'null' => false,
            'after' => 'invoice_status_id',
        ]);
        $table->update();
    }
}
