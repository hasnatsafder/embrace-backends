<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateSessions extends AbstractMigration
{
    public function up()
    {
        if (!$this->hasTable('sessions')) {
            $sql = <<<SQL
CREATE TABLE `sessions` (
    `id`       CHAR(40) CHARACTER SET ascii
    COLLATE ascii_bin NOT NULL,
    `created`  DATETIME         DEFAULT CURRENT_TIMESTAMP, -- optional, requires MySQL 5.6.5+
    `modified` DATETIME         DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, -- optional, requires MySQL 5.6.5+
    `data`     BLOB             DEFAULT NULL, -- for PostgreSQL use bytea instead of blob
    `expires`  INT(10) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;

SQL;
            $this->execute($sql);
        }
    }

    public function down()
    {
        if ($this->hasTable('sessions')) {
            $this->table('sessions')->drop()->save();
        }
    }
}
