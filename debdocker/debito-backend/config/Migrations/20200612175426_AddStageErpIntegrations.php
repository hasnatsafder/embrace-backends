<?php
use Migrations\AbstractMigration;

class AddStageErpIntegrations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('erp_integrations');
        $table->addColumn('stage', 'string', [
            'default' => 'NOT_SYNCED',
            'limit' => 255,
            'null' => true,
            'after' =>  'vendor_identifier'
        ]);
        $table->update();
    }
}
