<?php

use Migrations\AbstractMigration;

class AddClaimPhasesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `claim_phases` (`id`, `identifier`, `name`, `description`, `order`, `created`, `modified`, `deleted`)
VALUES
	('5768a0a5-34b7-4b44-9257-feae964054c5', 'case_received', 'Received', 'Case received (from frontend or dashboard)', 1, '2018-04-22 16:21:06', '2018-04-22 16:21:06', NULL),
    ('254652c4-de20-44b8-888e-5d4c0b60cfcd', 'case_created', 'Created', 'Case created by debito', 2, '2018-04-22 16:23:00', '2018-04-22 16:23:00', NULL),
    ('555c21f9-8808-46fe-88a9-eaa1eeac53a8', 'case_sent', 'Sent', 'Case sent to external partner', 3, '2018-04-22 16:23:00', '2018-04-22 16:23:00', NULL),
	('04fbe865-776b-4f74-8546-77411c692b06', 'reminder', 'Reminder', 'Case received at external partner + first letter sent (rykker)', 4, '2018-04-22 16:22:25', '2018-04-22 16:22:25', NULL),
	('26c4efbd-2bfa-4252-987c-8deffe38271a', 'debt_collection', 'Debt collection', 'A lot of actions will happen here (received from the files)', 5, '2018-04-22 16:22:57', '2018-04-22 16:22:57', NULL),
	('e003063d-518c-4903-b45b-7df2eddfbf5b', 'ended', 'Ended', 'Either with or without succes', 6, '2018-04-22 16:26:22', '2018-04-22 16:26:22', NULL),
    ('555c21f9-2bfa-44b8-b45b-8deffe38271a', 'rejected', 'Rejected', 'Claim was rejected by collector', -1, '2018-04-22 16:26:22', '2018-04-22 16:26:22', NULL);

SQL;
        $this->execute($sql);
    }
}
