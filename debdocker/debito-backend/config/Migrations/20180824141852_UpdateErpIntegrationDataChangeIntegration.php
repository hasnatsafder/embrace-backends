<?php

use Migrations\AbstractMigration;

class UpdateErpIntegrationDataChangeIntegration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('erp_integration_data');

        if ($table->hasColumn('integration')) {
            $table->removeColumn('integration');
        }

        $table->addColumn('erp_integration_id', 'uuid', [
            'default' => null,
            'null' => false,
            'after' => 'modified_by_id',
        ]);

        $table->addIndex([
            'erp_integration_id',
        ], [
            'name' => 'BY_ERP_INTEGRATION_ID',
            'unique' => false,
        ]);

        $table->save();
    }
}
