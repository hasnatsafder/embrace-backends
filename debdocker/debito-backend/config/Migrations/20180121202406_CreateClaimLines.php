<?php

use Migrations\AbstractMigration;

class CreateClaimLines extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_lines');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_line_type_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('currency_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('placement', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('invoice_date', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('maturity_date', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('amount', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addColumn('invoice_reference', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('file_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('file_extension', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('file_mime_type', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('file_size', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('file_dir', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'claim_id',
        ], [
            'name' => 'BY_CLAIM_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'claim_line_type_id',
        ], [
            'name' => 'BY_CLAIM_LINE_TYPE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'currency_id',
        ], [
            'name' => 'BY_CURRENCY_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'placement',
        ], [
            'name' => 'BY_PLACEMENT',
            'unique' => false,
        ]);
        $table->addIndex([
            'invoice_date',
        ], [
            'name' => 'BY_INVOICE_DATE',
            'unique' => false,
        ]);
        $table->addIndex([
            'maturity_date',
        ], [
            'name' => 'BY_MATURITY_DATE',
            'unique' => false,
        ]);
        $table->addIndex([
            'amount',
        ], [
            'name' => 'BY_AMOUNT',
            'unique' => false,
        ]);
        $table->addIndex([
            'invoice_reference',
        ], [
            'name' => 'BY_INVOICE_REFERENCE',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
