<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddIsAutomatedToClaimActionTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_action_types');

        $table->addColumn('is_automated', 'boolean', [
            'default' => false,
            'null' => false,
            'after' => 'is_public',
        ]);

        $table->update();

        if ($this->isMigratingUp()) {
            $debitoCollectorId = 'ea4c5019-866a-4819-8270-2efb4e30604b';
            $claimPhaseDebtCollectionId = '26c4efbd-2bfa-4252-987c-8deffe38271a';

            $table
                ->insert([
                    [
                        'id' => '05b95bf3-0b5e-4884-af1b-baf2621dcb12',
                        'claim_phase_id' => $claimPhaseDebtCollectionId,
                        'collector_id' => $debitoCollectorId,
                        'identifier' => 'phone_call_attempt',
                        'collector_identifier' => 'phone_call_attempt',
                        'name' => 'Phone call attempt',
                        'description' => null,
                        'is_public' => 0,
                        'is_automated' => 1,
                        'created' => '2020-02-25 13:38:00',
                        'modified' => '2020-02-25 13:38:00',
                        'deleted' => null,
                    ],
                    [
                        'id' => 'fb5feac6-b302-4562-9c2e-f7959d657632',
                        'claim_phase_id' => $claimPhaseDebtCollectionId,
                        'collector_id' => $debitoCollectorId,
                        'identifier' => 'installment_plan_check',
                        'collector_identifier' => 'installment_plan_check',
                        'name' => 'Check if installment plan is maintained',
                        'description' => null,
                        'is_public' => 0,
                        'is_automated' => 1,
                        'created' => '2020-02-25 13:38:00',
                        'modified' => '2020-02-25 13:38:00',
                        'deleted' => null,
                    ],
                ])
                ->save();

            $this->execute(<<<SQL
UPDATE `claim_action_types`
SET `is_automated` = 1
WHERE `identifier` IN (
    'claim_started', 
    'send_soft_warning',
    'send_soft_warning_text_message',
    'add_warning_of_debt_collection',
    'send_warning_one',
    'send_warning_two',
    'send_warning_three',
    'send_warning_notification_text',
    'start_reminder_phase',
    'manual_debt_collection_starts',
    'installment_plan_not_maintained' 
)
SQL
            );
        }
    }
}
