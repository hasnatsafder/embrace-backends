<?php
use Migrations\AbstractMigration;

class AddDebtorNameToInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');
        $table->addColumn('customer_name', 'string', [
            'default' => null,
            'null' => true,
            'after' => 'erp_integration_id',
        ]);
        $table->update();
        $table = $this->table('paid_invoices');
        $table->addColumn('customer_name', 'string', [
            'default' => null,
            'null' => true,
            'after' => 'erp_integration_id',
        ]);
        $table->update();
    }
}
