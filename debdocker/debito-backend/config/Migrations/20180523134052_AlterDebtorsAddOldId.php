<?php

use Migrations\AbstractMigration;

class AlterDebtorsAddOldId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('debtors');

        $table->addColumn('old_id', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);

        $table->update();
    }
}
