<?php

use Migrations\AbstractMigration;

class AlterClaimLinesRemoveFileExtension extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_lines');

        $table->removeColumn('file_extension');

        $table->update();
    }
}
