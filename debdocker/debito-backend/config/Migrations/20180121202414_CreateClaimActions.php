<?php

use Migrations\AbstractMigration;

class CreateClaimActions extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('collector_file_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('claim_action_type_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('paid', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('interest_added', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('debt_collection_fees', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('actual_paid', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('date', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addIndex([
            'claim_id',
        ], [
            'name' => 'BY_CLAIM_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'collector_file_id',
        ], [
            'name' => 'BY_COLLECTOR_FILE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'claim_action_type_id',
        ], [
            'name' => 'BY_CLAIM_ACTION_TYPE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'date',
        ], [
            'name' => 'BY_DATE',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
