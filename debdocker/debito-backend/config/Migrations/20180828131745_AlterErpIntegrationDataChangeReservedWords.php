<?php

use Migrations\AbstractMigration;

class AlterErpIntegrationDataChangeReservedWords extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('erp_integration_data');

        $table->renameColumn('key', 'data_key');
        $table->renameColumn('value', 'data_value');

        $table->update();
    }
}
