<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddPrincipalSplitToAccounts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('accounts');

        $table->addColumn('principal_split', 'decimal', [
            'default' => 50.00,
            'null' => false,
            'precision' => 5,
            'scale' => 2,
            'after' => 'longitude',
            'signed' => false,
        ]);

        $table->update();
    }
}
