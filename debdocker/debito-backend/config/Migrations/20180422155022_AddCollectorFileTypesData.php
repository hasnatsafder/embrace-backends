<?php

use Migrations\AbstractMigration;

class AddCollectorFileTypesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `collector_file_types` (`id`, `collector_id`, `name`, `identifier`, `description`, `created`, `modified`, `deleted`)
VALUES
	('0e434c97-e9bf-4903-aa57-265a423b7ab2', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Legacy - indbetalingsfil', 'legacy_payment_file', 'Uses INB0DB*.txt format', '2018-04-22 15:44:13', '2018-04-22 15:44:13', NULL),
	('354661ae-57af-40a5-b141-58f8434618b3', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Afslutningsfil', 'completion_file', 'Uses AVSL0DB*.csv format', '2018-04-22 15:45:30', '2018-04-22 15:45:30', NULL),
	('4dd7e94f-b571-4603-8f01-f735aa72827c', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Indbetalingsfil', 'payment_file', 'Uses INB0DB*.csv format', '2018-04-22 15:45:01', '2018-04-22 15:45:01', NULL),
	('5bae97d0-352e-4379-9242-42771b7fea48', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'CRF fil (financial status)', 'crf_file', 'Uses Debito_20*.csv format', '2018-04-22 15:45:57', '2018-04-22 15:45:57', NULL),
	('e39ec1b7-3fde-4c06-a873-287fe1f8c8d7', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Legacy - seneste handlinger', 'legacy_latest_actions', 'Uses 3*.csv format', '2018-04-22 15:43:41', '2018-04-22 15:43:41', NULL),
	('e616cba8-d282-43ff-a74d-ed480952f95d', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Kvitteringsfil', 'received_recipient', 'Uses RPT0DB*.csv format', '2018-04-22 15:46:48', '2018-04-22 15:46:48', NULL),
	('e8ef1d15-42f6-43b1-9254-7ae8bc50bb70', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Legacy - alle handlinger', 'legacy_all_actions', 'Uses Debito_Alle_ha*.csv format', '2018-04-22 15:47:36', '2018-04-22 15:47:36', NULL),
	('f5989e21-8c84-40cc-a49e-c29a875f6658', '5dbdf878-1bd2-4fe6-aa53-7e0803f36539', 'Seneste handlinger', 'latest_actions', 'Uses Seneste_handlinger*.csv format', '2018-04-22 15:46:25', '2018-04-22 15:46:25', NULL);
SQL;
        $this->execute($sql);
    }
}
