<?php

use Migrations\AbstractMigration;

class AddSyncToClaimline extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_lines');
        $table->addColumn('synced', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->update();
    }
}
