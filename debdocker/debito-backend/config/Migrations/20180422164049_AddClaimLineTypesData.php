<?php

use Migrations\AbstractMigration;

class AddClaimLineTypesData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $sql = <<<SQL
INSERT INTO `claim_line_types` (`id`, `identifier`, `name`, `description`, `created`, `modified`, `deleted`)
VALUES
	('01c44a06-0d43-494c-a998-de40ec41e546', 'invoice', 'Faktura', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('0811c5a9-0267-43ac-899a-ff216ca6f704', 'document', 'Dokument', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('5da19e9a-3b86-4bc2-9f27-5b23ecba48ca', 'payment', 'Indbetaling', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('8da384b7-758e-492a-a419-24ea446a84e3', 'created_by_error', 'Fejloprettet', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('a0aea290-d8ad-4885-bec3-59e64b58bfb9', 'late_payment_fee', 'Rykkergebyr', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('b34c5777-38f4-4356-8508-61e38e7724e1', 'compensation_fee', 'Kompensationsgebyr', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL),
	('ce684c57-17bc-4649-8011-e0a66056f7a8', 'credit_note', 'Kreditnota', NULL, '2017-07-03 00:12:49', '2017-07-03 00:12:49', NULL);
SQL;
        $this->execute($sql);
    }
}
