<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateClaimTransactions extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_transactions');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created_by_id', 'uuid', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('claim_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('claim_transaction_type_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('timestamp', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('amount', 'decimal', [
            'default' => null,
            'null' => false,
            'precision' => 13,
            'scale' => 4,
        ]);
        $table->addColumn('total', 'decimal', [
            'default' => null,
            'null' => false,
            'precision' => 13,
            'scale' => 4,
        ]);
        $table->addColumn('subject', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('description', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('position', 'smallinteger', [
            'signed' => false,
            'default' => 1,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addIndex([
            'claim_id',
            'claim_transaction_type_id',
        ], [
            'name' => 'BY_CLAIM_ID_CLAIM_TRANSACTION_TYPE_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'claim_id',
            'timestamp',
        ], [
            'name' => 'BY_CLAIM_ID_TIMESTAMP',
            'unique' => false,
        ]);
        $table->addIndex([
            'claim_id',
            'position',
        ], [
            'name' => 'BY_CLAIM_ID_POSITION',
            'unique' => true,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
