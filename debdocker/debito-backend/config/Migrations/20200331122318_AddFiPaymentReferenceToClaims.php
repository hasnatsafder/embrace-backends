<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddFiPaymentReferenceToClaims extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('fi_payment_reference', 'biginteger', [
            'default' => null,
            'signed' => false,
            'limit' => 14,
            'null' => true,
            'after' => 'collector_reference',
        ]);

        $table->addColumn('fi_check_digit', 'string', [
            'default' => null,
            'limit' => 1,
            'null' => true,
            'after' => 'fi_payment_reference',
        ]);

        $table->addIndex([
            'fi_payment_reference',
        ], [
            'name' => 'BY_FI_PAYMENT_REFERENCE',
            'unique' => true,
        ]);
        $table->update();
    }
}
