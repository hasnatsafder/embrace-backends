<?php
use Migrations\AbstractMigration;

class AddClaimPhasesToClaimActionTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $debitoCollectorId = 'ea4c5019-866a-4819-8270-2efb4e30604b';

        $this->table('claim_action_types')
            ->insert([
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cf',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_case_received',
                    'collector_identifier' => 'transition_to_case_received',
                    'name' => 'Validating claim',
                    'description' => 'Case received',
                    'is_public' => 0,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cg',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_rejected',
                    'collector_identifier' => 'transition_to_case_rejected',
                    'name' => 'Claim rejected',
                    'description' => 'Case rejected',
                    'is_public' => 1,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43ch',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_case_created',
                    'collector_identifier' => 'transition_to_case_created',
                    'name' => 'Validation successful',
                    'description' => 'Case created',
                    'is_public' => 1,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43ci',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_case_sent',
                    'collector_identifier' => 'transition_to_case_sent',
                    'name' => 'Case sent',
                    'description' => 'Case sent',
                    'is_public' => 0,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cj',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_soft_warning',
                    'collector_identifier' => 'transition_to_soft_warning',
                    'name' => 'Soft warning sent',
                    'description' => 'A "soft" warning has been sent to the debtor, without fees or anything added yet',
                    'is_public' => 1,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43ck',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_reminder',
                    'collector_identifier' => 'transition_to_case_reminder',
                    'name' => 'Reminder activated',
                    'description' => 'Case reminder',
                    'is_public' => 0,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cl',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_debt_collection',
                    'collector_identifier' => 'transition_to_case_debt_collection',
                    'name' => 'Claim active',
                    'description' => 'Case debt collection',
                    'is_public' => 0,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cm',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_surveillance',
                    'collector_identifier' => 'transition_to_case_surveillance',
                    'name' => 'Surveillance',
                    'description' => 'Case surveillance',
                    'is_public' => 1,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'a2f1f258-5ef0-4951-b4a8-87187a3d43cn',
                    'claim_phase_id' => null,
                    'collector_id' => $debitoCollectorId,
                    'identifier' => 'transition_to_ended',
                    'collector_identifier' => 'transition_to_case_ended',
                    'name' => 'Claim ended',
                    'description' => 'Case ended',
                    'is_public' => 1,
                    'is_automated' => 1,
                    'created' => '2020-03-19 13:31:00',
                    'modified' => '2020-03-19 13:31:00',
                    'deleted' => null,
                ],
            ])
            ->save();
    }
}
