<?php

use Migrations\AbstractMigration;

class AddOrgIdErpIntegrationsData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('erp_integration_data');
        $table->addColumn(
            'org_id',
            'string',
            [
                'after' => 'data_value',
                'default' => null,
                'null' => true,
            ]
        );
        $table->update();
    }
}
