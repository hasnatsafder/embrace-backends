<?php
use Migrations\AbstractMigration;

class AddCreatedByIdToClaimActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_actions');
        $table->addColumn('created_by_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'id',
        ]);
        $table->update();
    }
}
