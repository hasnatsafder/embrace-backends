<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddClaimPhasesForInternalCollection extends AbstractMigration
{

    public function down()
    {
    }

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function up()
    {
        $this->table('claim_phases')
            ->insert([
                [
                    'id' => 'c761ed80-1d21-4e35-bb98-d49a4560f606',
                    'identifier' => 'soft_warning',
                    'name' => 'Soft warning',
                    'description' => 'A "soft" warning has been sent to the debtor, without fees or anything added yet',
                    'order' => 3,
                    'created' => '2020-02-19 09:47:00',
                    'modified' => '2020-02-19 09:47:00',
                    'deleted' => null,
                ],
                [
                    'id' => 'f571fcbf-8c59-4726-aeac-f05354b0811b',
                    'identifier' => 'surveillance',
                    'name' => 'Surveillance',
                    'description' => 'The claim has not been paid, but we are also not in the "debt collection" phase where we try on a regular basis to collect money. We will still try to collect the money',
                    'order' => 5,
                    'created' => '2020-02-19 09:47:00',
                    'modified' => '2020-02-19 09:47:00',
                    'deleted' => null,
                ],
            ])
            ->save();
    }
}
