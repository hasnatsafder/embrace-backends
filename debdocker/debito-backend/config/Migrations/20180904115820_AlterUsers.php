<?php

use Migrations\AbstractMigration;

class AlterUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('token', 'string', [
            'default' => "",
            'limit' => 255,
        ]);
        $table->addColumn('verified', 'boolean', [
            'default' => 0,
            'null' => false,
        ]);
        $table->update();
    }
}
