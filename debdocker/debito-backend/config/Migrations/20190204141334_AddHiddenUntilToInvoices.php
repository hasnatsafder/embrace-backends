<?php

use Migrations\AbstractMigration;

class AddHiddenUntilToInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');

        $table->addColumn('hidden_until', 'datetime', [
            'default' => null,
            'null' => true,
            'after' => 'due_date',
        ]);
        $table->addIndex([
            'hidden_until',
        ], [
            'name' => 'BY_HIDDEN_UNTIL',
            'unique' => false,
        ]);
        $table->update();
    }
}
