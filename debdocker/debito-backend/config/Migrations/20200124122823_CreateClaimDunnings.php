<?php

use Migrations\AbstractMigration;

class CreateClaimDunnings extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claim_dunnings');
        $table->addColumn('id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('invoice_id', 'uuid', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('warning_1', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('warning_2', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('warning_3', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('warning_1_amount', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->addColumn('warning_2_amount', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->addColumn('warning_3_amount', 'boolean', [
            'default' => false,
            'null' => false,
        ]);
        $table->addColumn('warning_1_file_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('warning_2_file_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('warning_3_file_path', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('email_text', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('cc_email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('bcc_email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addIndex([
            'invoice_id',
        ], [
            'name' => 'BY_INVOICE_ID',
            'unique' => false,
        ]);
        $table->addPrimaryKey([
            'id',
        ]);
        $table->create();
    }
}
