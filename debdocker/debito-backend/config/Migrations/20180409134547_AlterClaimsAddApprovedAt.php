<?php

use Migrations\AbstractMigration;

class AlterClaimsAddApprovedAt extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('claims');
        $table->addColumn('approved', 'datetime', [
            'default' => null,
            'after' => 'completed',
            'null' => true,
        ]);
        $table->addIndex([
            'approved',
        ], [
            'name' => 'BY_APPROVED',
            'unique' => false,
        ]);
        $table->update();
    }
}
