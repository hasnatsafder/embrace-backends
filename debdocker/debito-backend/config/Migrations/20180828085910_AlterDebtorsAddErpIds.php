<?php

use Migrations\AbstractMigration;

class AlterDebtorsAddErpIds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('debtors');

        $table->addColumn('erp_integration_id', 'uuid', [
            'default' => null,
            'null' => true,
            'after' => 'country_id',
        ]);
        $table->addColumn('erp_integration_foreign_key', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
            'after' => 'langitude',
        ]);
        $table->addIndex([
            'erp_integration_id',
        ], [
            'name' => 'BY_ERP_INTEGRATION_ID',
            'unique' => false,
        ]);
        $table->addIndex([
            'erp_integration_foreign_key',
        ], [
            'name' => 'BY_ERP_INTEGRATION_FOREIGN_KEY',
            'unique' => false,
        ]);
        $table->update();
    }
}
