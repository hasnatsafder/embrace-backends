<?php
use Migrations\AbstractMigration;

class AlterAccountsAddNotificationSummaryInterval extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('accounts');
        $table->addColumn('notification_summary_interval', 'string', [
            'default' => 'DAILY',
            'limit' => 255,
            'null' => true,
            'after' =>  'longitude'
        ]);
        $table->update();
    }
}
