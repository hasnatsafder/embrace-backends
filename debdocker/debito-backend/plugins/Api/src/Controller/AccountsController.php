<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\CountriesTable $Countries
 */
class AccountsController extends AppController
{
    public $modelClass = 'App.Accounts';

    protected $allowedActions = ['index', 'view', 'edit', 'add', 'delete', 'cvrsearch', 'restore'];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query = $this->authMatchUsers($event->getSubject()->query);
        });
        $this->Crud->on('afterSave', function (Event $event) {
            //setting the newly created query to be the active one
            if ($event->getSubject()->created) {
                //inside created method since only need to update on new entity save, not on edit
                $usersTable = TableRegistry::get('users');
                $user = $usersTable->get(['id' => $this->Auth->user('id')]);
                $user->active_account_id = $event->getSubject()->entity->id;
                $usersTable->save($user);
            }
        });

        //set Denmark as default country if none provided
        $this->Crud->on('beforeSave', function (Event $event) {
            //setting the newly created query to be the active one
            if (!isset($event->getSubject()->id)) {
                $event->getSubject()->saveOptions["skipAutoCreateUser"] = false;
                //inside created method since only need to update on new entity save, not on edit
                if (!isset($event->getSubject()->entity->country_id) || $event->getSubject()->entity->country_id == null || $event->getSubject()->entity->country_id == "") {
                    $this->loadModel('Countries');
                    $denmark = $this->Countries->find(
                        'all',
                        ['conditions' => ['iso_code' => 'DK']]
                    )->select('id')->first();
                    $ev = $event->getSubject()->entity->country_id = $denmark->id;
                }
            }
        });

        $this->Crud->on('beforePaginate', function (Event $event) {
            $event->getSubject()->query = $this->authMatchUsers($event->getSubject()->query);
        });
    }

    protected function authMatchUsers(Query $query): Query
    {
        return $query
            ->matching('Users', function (Query $query) {
                return $query->where(['Users.id' => $this->Auth->user('id')]);
            });
    }

    public function index(): Response
    {
        if ($this->request->getQuery('showCountries') === '1') {
            $this->Crud->on('beforePaginate', function (Event $event) {
                $event->getSubject()->query
                    ->select([
                        'Accounts.id',
                        'Accounts.address',
                        'Accounts.name',
                        'Accounts.zip_code',
                        'Accounts.city',
                        'Accounts.phone',
                        'Accounts.vat_number',
                        'Accounts.is_company',
                        'Accounts.notification_summary_interval',
                        'Countries.name',
                    ])
                    ->contain(['countries']);
            });
        }

        return $this->Crud->execute();
    }

    public function view(): Response
    {
        if ($this->request->getQuery('showCountries') === '1') {
            $this->Crud->on('beforeFind', function (Event $event) {
                $event->getSubject()->query
                    ->contain(['countries']);
            });
        }

        return $this->Crud->execute();
    }

    public function cvrsearch()
    {
        $searchTerm = $this->request->data['term'];

        $type = "search"; //default
        if (isset($this->request->data['type']) && ($this->request->data['type'] === "vat" || $this->request->data['type'] === "name")) {
            $type = $this->request->data['type'];
        }

        $searchTerm = str_replace(" ", "%20", $searchTerm);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://cvrapi.dk/api?' . $type . '=' . $searchTerm . '&country=dk');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = [];
        $headers[] = 'User-Agent: Debito-system - Christoffer Baadsgaard +45 22662206';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result);
        if (curl_errno($ch) || isset($result->error)) {
            $this->set('data', ["No Result found"]);
            $this->set('success', false);

            return;
        }
        curl_close($ch);

        $this->set('data', $result);
        $this->set('success', true);
    }

    /**
     * Soft delete account
     * @param String $account_id
     */
    public function delete($account_id)
    {
        $this->loadModel('Claims');
        $this->loadModel('Debtors');
        $this->loadModel('Users');

        // check1 if account has claims assigned to it.
        $claims = $this->Claims->find()->where(['account_id' => $account_id])->count();
        if ($claims !== 0) {
            $this->set('data', 'Cannot delete, Account has ' . $claims . " claim(s) assigned to it");
            $this->set('success', false);
            return;
        }

        // check2 if account is active user account
        $users = $this->Users->find()->where(['active_account_id' => $account_id])->count();
        if ($users !== 0) {
            $this->set('data', 'Cannot delete, Account is active');
            $this->set('success', false);
            return;
        }
        // delete all debtors of account
        $this->Debtors->trashAll(['account_id' => $account_id]);

        //delete account
        $account = $this->Accounts->get($account_id);
        if ($this->Accounts->trash($account)) {
            $this->set('data', "Successfully deleted account");
            $this->set('success', true);
        }
    }

    /**
     * restore deleted account
     * @param String $account_id
     */
    public function restore($account_id)
    {
        $this->loadModel('Debtors');

        // restore all debtors of account
         $debtors = $this->Debtors->find('withTrashed')->where(['account_id' => $account_id]);
         foreach ($debtors as $debtor) {
             $this->Debtors->restoreTrash($debtor);
         }

        //restore account
        $account = $this->Accounts->find('withTrashed')->where(['id' => $account_id])->first();
        if ($this->Accounts->restoreTrash($account)) {
            $this->set('data', "Successfully restored account");
            $this->set('success', true);
        }
    }
}
