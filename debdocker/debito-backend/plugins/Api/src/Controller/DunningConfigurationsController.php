<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * DunningConfigurations Controller
 *
 * @property \App\Model\Table\DunningConfigurationsTable $DunningConfigurations
 */
class DunningConfigurationsController extends AppController
{
    public $modelClass = 'App.DunningConfigurations';
    protected $allowedActions = ['index', 'add', 'view', 'edit', 'delete'];
    protected $requireActiveAccount = true;
    protected $filterByAccountId = true;
}
