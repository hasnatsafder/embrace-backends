<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Event\Event;
use App\Model\Entity\ClaimDunning;
use Cake\Network\Response;

/**
 * ClaimDunnings Controller
 *
 * @property \App\Model\Table\ClaimDunningsTable $ClaimDunnings
 * @property \App\Model\Table\InvoicesTable $Invoices
 * @property \App\Model\Table\DebtorsTable $Debtors
 */
class ClaimDunningsController extends AppController
{
    public $modelClass = 'App.ClaimDunnings';
    protected $allowedActions = ['add', 'edit'];

    public function add()
    {
        $invoice_id = $this->request->getData('invoice_id');

        // if record not found then add one
        $claim_dunning = $this->ClaimDunnings->findOrCreate([
            'invoice_id' => $invoice_id,
        ], function (ClaimDunning $claimDunning) use ($invoice_id) {

            $claimDunning->set([
                'id' => '', // just so we can return the id created
                'invoice_id' => $invoice_id,
                'warning_1' => null,
                'warning_2' => null,
                'warning_3' => null,
                'warning_1_amount' => true,
                'warning_2_amount' => true,
                'warning_3_amount' => true,
                'warning_1_claim_line_id' => null,
                'warning_2_claim_line_id' => null,
                'warning_3_claim_line_id' => null,
                'email_text' => null,
                'cc_email' => null,
                'bcc_email' => null,
            ]);
        });

        // get the debtor email and id too
        $claim_dunning->debtor_email = $this->getDebtor($invoice_id)->email;

        $this->set('data', $claim_dunning);
        $this->set('success', true);
    }

    /**
     * Private function to get detials of debtor
     *
     * @param String $claim_id
     * @return Debtor
     */
    private function getDebtor($invoice_id)
    {
        //get claim detail
        $this->LoadModel('Invoices');
        $invoice = $this->Invoices->find()
            ->where(['id' => $invoice_id])
            ->select(['id', 'debtor_id'])
            ->firstOrFail();

        $this->loadModel('Debtors');

        return $this->Debtors->find()
            ->where(['id' => $invoice->debtor_id])
            ->select(['id', 'email'])
            ->first();
    }

    /**
     * Make sure the claim_id cannot be changed when editing
     *
     * @param $id
     * @return \Cake\Network\Response
     * @throws \Exception
     */
    public function edit($id): \Cake\Http\Response
    {

        $this->ClaimDunnings->get($id);

        $debtorEmail = $this->getRequest()->getData('debtor_email');
        $invoice_id = $this->ClaimDunnings->get($id)->invoice_id;
        // update debtor and reply whether it was updated or not
        $saved = false;
        if (isset($debtorEmail)) {
            $saved = $this->updateDebtor($invoice_id, $debtorEmail);
        }

        // after saving data, add key for whether we update debtor email or not
        $this->Crud->on('afterSave', function (Event $event) use ($saved) {
            $this->set([
                'debtor_saved' => $saved,
                'success' => true,
                '_serialize' => ['debtor_saved', 'success'],
            ]);
        });

        return $this->Crud->execute();
    }

    /**
     * Update debtor if new email is provided
     *
     * @param String $claim_id
     * @param String $debtorEmail
     * @return bool
     */
    private function updateDebtor($invoice_id, $debtorEmail)
    {
        $debtor = $this->getDebtor($invoice_id);

        // if debtor email was same then dont overwrite it and return false
        if ($debtor->email == $debtorEmail) {
            return false;
        }
        $debtor->email = $debtorEmail;
        if ($this->Debtors->save($debtor)) {
            return true;
        } else {
            return false;
        }
    }
}
