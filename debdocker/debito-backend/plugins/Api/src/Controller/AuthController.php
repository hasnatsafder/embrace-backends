<?php
declare(strict_types=1);

namespace Api\Controller;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Exception\UnauthorizedException;
use App\Exception\InvalidPasswordLength;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Auth Controller
 *
 * @property \App\Model\Table\UsersTable Users
 */
class AuthController extends AppController
{
    public $modelClass = 'App.Users';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->mapAction('register', [
            'className' => 'CrudUsers.Register',
            'messages' => [
                'success' => [
                    'params' => ['class' => 'alert alert-success alert-dismissible'],
                ],
                'error' => [
                    'params' => ['class' => 'alert alert-danger alert-dismissible'],
                ],
            ],
        ]);

        $this->Crud->mapAction('forgotpassword', [
            'className' => 'CrudUsers.ForgotPassword',
            'messages' => [
                'success' => [
                    'params' => ['class' => 'alert alert-success alert-dismissible'],
                ],
                'error' => [
                    'params' => ['class' => 'alert alert-danger alert-dismissible'],
                ],
            ],
        ]);

        $this->Crud->mapAction('resetpassword', [
            'className' => 'CrudUsers.ResetPassword',
            'messages' => [
                'success' => [
                    'params' => ['class' => 'alert alert-success alert-dismissible'],
                ],
                'error' => [
                    'params' => ['class' => 'alert alert-danger alert-dismissible'],
                ],
            ],
        ]);

        $this->Crud->mapAction('verify', [
            'className' => 'CrudUsers.Verify',
            'messages' => [
                'success' => [
                    'params' => ['class' => 'alert alert-success alert-dismissible'],
                ],
                'error' => [
                    'params' => ['class' => 'alert alert-danger alert-dismissible'],
                ],
            ],
        ]);

        $this->Auth->allow(['register', 'token', 'forgotpassword', "resetpassword", "verify", "accountVerificationToken"]);
    }

    public function register()
    {
        // if length of password is less than 6 throw error
        if (isset($this->request->data['password'])) {
            $this->Crud->on('beforeRegister', function (Event $even) {
                if (strlen($this->request->data['password']) < 6) {
                    throw new InvalidPasswordLength();
                }
            });
        }

        $this->Crud->on('afterRegister', function (Event $event) {
            if ($event->subject->created) {
                $this->set('data', [
                    'id' => $event->subject->entity->id,
                    'token' => JWT::encode(
                        [
                            'sub' => $event->subject->entity->id,
                            'exp' => time() + 604800,
                        ],
                        Security::getSalt()
                    ),
                ]);
                $this->Crud->action()->setConfig('serialize.data', 'data');
            }
        });

        return $this->Crud->execute();
    }

    public function resetpassword()
    {

        $this->Crud->on('verifyToken', function (Event $event) {
            $event->getSubject()->verified = true;
            if ($event->getSubject()->entity) {
                $event->getSubject()->entity->token = "";
            }
        });

        $this->Crud->on('afterResetPassword', function (Event $event) {
            $this->Crud->action()->setConfig('serialize.data', 'data');
        });

        return $this->Crud->execute();
    }

    public function verify()
    {
        $this->Crud->on('verifyToken', function (Event $event) {
            $event->getSubject()->verified = true;
        });
        $this->Crud->on('afterVerify', function (Event $event) {
            //$this->set('data', [
            $this->Crud->action()->setConfig('serialize.data', 'data');
        });

        return $this->Crud->execute();
    }

    public function forgotpassword()
    {
        $this->Crud->on('afterForgotPassword', function (Event $event) {
            //
        });
        $this->Crud->on('afterSave', function (Event $event) {
            $url = Configure::read('Frontendurl') . "resetpassword?token=" . $event->getSubject()->entity->token;
            $email = new Email('sparkpost');
            $email->from(['kundeservice@debito.dk' => 'Debito'])
                ->to($event->getSubject()->entity->email)
                ->subject('Nulstil password')
                ->viewVars(['value' => $url])
                ->template('forgotpassword')
                ->emailFormat('html')
                ->bcc("christoffer@debito.dk")
                ->send();
        });

        return $this->Crud->execute();
    }

    public function token()
    {
        $this->getRequest()->allowMethod('POST');

        $user = $this->Auth->identify();

        if (!$user) {
            throw new UnauthorizedException('Invalid username or password');
        }

        $this->set([
            'success' => true,
            'data' => [
                'token' => JWT::encode(
                    [
                        'sub' => $user['id'],
                        'exp' => (int)(new FrozenTime())->addDays(7)->toUnixString(),
                        'iat' => time(),
                    ],
                    Security::getSalt()
                ),
            ],
            '_serialize' => ['success', 'data'],
        ]);
    }
    public function accountVerificationToken($token) {
        $accountVerificationTable = TableRegistry::getTableLocator()->get('AccountVerifications');
        $record = $accountVerificationTable->find()->where([
            'form_key' => $token,
            'form_key_expire >=' => FrozenTime::now()
        ])->enableBufferedResults(false)->firstOrFail();
        $this->set([
            'success' => true,
            'data' => $record
        ]);
    }
}
