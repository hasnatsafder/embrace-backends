<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 */
class NotificationsController extends AppController
{
    public $modelClass = 'App.Notifications';
    protected $allowedActions = ['index', 'view'];
    protected $filterByAccountId = true;
    protected $requireActiveAccount = false;
}
