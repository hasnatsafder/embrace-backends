<?php
declare(strict_types=1);

namespace Api\Controller;

use App\Exception\NoAccountSelected;
use Cake\Controller\Component\AuthComponent;
use Cake\Controller\Controller;
use Cake\Datasource\Exception\MissingModelException;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Inflector;
use Crud\Controller\Component\CrudComponent;
use Crud\Controller\ControllerTrait;
use Muffin\Footprint\Auth\FootprintAwareTrait;
use RuntimeException;
use Search\Model\Behavior\SearchBehavior;
use UnexpectedValueException;

/**
 * @property \Crud\Controller\Component\CrudComponent $Crud
 * @property \Cake\Controller\Component\RequestHandlerComponent $RequestHandler
 * @property \Cake\Controller\Component\FlashComponent $Flash
 * @property \Cake\Controller\Component\AuthComponent $Auth
 * @property \App\Controller\Component\RoleRequirerComponent $RoleRequirer
 */
class AppController extends Controller
{
    use ControllerTrait;
    use FootprintAwareTrait;

    public $paginate = [
        'page' => 1,
        'limit' => 5000,
        'maxLimit' => 11115,
    ];

    /**
     * A list of actions that should be allowed for authenticated users
     *
     * @var array
     */
    protected $allowedActions = [];

    /**
     * A list of actions where the Crud.SearchListener
     * and Search.PrgComponent should be enabled
     *
     * @var array
     */
    protected $searchActions = ['index', 'lookup'];

    /**
     * Defines whether the controller should check if the authenticated user has an account set as currently active.
     * Will throw exceptions if `true` but no account is set.
     *
     * @var bool
     */
    protected $requireActiveAccount = false;

    /**
     * Defines whether the controller should show only record with matching `account_id` to the currently authenticated
     * user
     *
     * @var bool
     */
    protected $filterByAccountId = false;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponents();

        if ($this->modelClass !== null && in_array($this->getRequest()->getParam('action'), $this->searchActions, true)) {
            [$plugin, $tableClass] = pluginSplit($this->modelClass);
            try {
                /** @var \Cake\ORM\Table $table */
                $table = $this->{$tableClass};
                $behaviors = $table->behaviors();
                if ($behaviors->has('Search') && $behaviors->get('Search') instanceof SearchBehavior) {
                    $this->Crud->addListener('Crud.Search');
                    $this->loadComponent('Search.Prg', [
                        'actions' => $this->searchActions,
                    ]);
                }
            } catch (MissingModelException $e) {
                //Do nothing
            } catch (UnexpectedValueException $e) {
                //Do nothing
            }
        }
    }

    /**
     * @throws \Exception
     */
    protected function loadComponents(): void
    {
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadCrudComponent();
        $this->loadAuthComponent();
        $this->loadComponent('RoleRequirer');
    }

    /**
     * @throws \Exception
     */
    protected function loadCrudComponent(): void
    {
        $this->loadComponent('Crud.Crud', [
            'actions' => [
                'Crud.Index',
                'Crud.Add',
                'Crud.Edit',
                'Crud.View',
                'Crud.Delete',
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog',
                'Crud.Redirect',
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
            ],
        ]);
    }

    /**
     * @throws \Exception
     */
    protected function loadAuthComponent(): void
    {
        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                AuthComponent::ALL => [
                    'finder' => 'auth',
                ],
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password',
                    ],
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'userModel' => 'Users',
                    'fields' => [
                        'username' => 'id',
                    ],
                    'parameter' => 'token',
                    // Boolean indicating whether the "sub" claim of JWT payload
                    // should be used to query the Users model and get user info.
                    // If set to `false` JWT's payload is directly returned.
                    'queryDatasource' => true,
                    'unauthenticatedException' => UnauthorizedException::class,
                ],
            ],
            'authorize' => 'Controller',
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize',

            // If you don't have a login action in your application set
            // 'loginAction' to false to prevent getting a MissingRouteException.
            'loginAction' => false,
        ]);
    }

    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     * @throws \Crud\Error\Exception\ActionNotConfiguredException
     * @throws \Crud\Error\Exception\MissingActionException
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Crud->on('afterPaginate', function (Event $event) {
            foreach ($event->getSubject()->entities as $entity) {
                unset($entity->_matchingData);
            }
        });

        $this->Crud->on('afterFind', function (Event $event) {
            unset($event->getSubject()->entity->_matchingData);
        });

        if ($this->requireActiveAccount) {
            $this->checkForActiveAccount();
        }

        if ($this->filterByAccountId === true) {
            if (($this->Crud instanceof CrudComponent) && $this->Crud->isActionMapped()) {
                $table = $this->getTableLocator()->get($this->modelClass);
                $field = $table->aliasField('account_id');

                $this->Crud->on('beforePaginate', function (Event $event) use ($field) {
                    $event->getSubject()->query->where([$field => $this->Auth->user('active_account_id')]);
                });

                $this->Crud->on('beforeFind', function (Event $event) use ($field) {
                    $event->getSubject()->query->where([$field => $this->Auth->user('active_account_id')]);
                });
            }
        }
    }

    /**
     * @throws \App\Exception\NoAccountSelected
     */
    protected function checkForActiveAccount(): void
    {
        $actionOutsideAuth = in_array($this->getRequest()->getParam('action'), $this->Auth->allowedActions, true);

        if (!$actionOutsideAuth && empty($this->Auth->user('active_account_id'))) {
            throw new NoAccountSelected();
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     * @throws \Crud\Error\Exception\ActionNotConfiguredException
     * @throws \Crud\Error\Exception\MissingActionException
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        if ($this->filterByAccountId === true) {
            if (!($this->Crud instanceof CrudComponent) || !$this->Crud->isActionMapped()) {
                throw new RuntimeException('$filterByAccountId is only supported in controllers using Crud');
            }
        }

        $isRest = in_array($this->getResponse()->getType(), ['application/json', 'application/xml']);

        if (!array_key_exists('_serialize', $this->viewVars) && $isRest) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Check if the provided user is authorized for the request.
     *
     * @param array|\ArrayAccess|null $user The user to check the authorization of.
     *   If empty the user fetched from storage will be used.
     * @return bool True if $user is authorized, otherwise false
     */
    public function isAuthorized($user = null)
    {
        if (in_array($this->getRequest()->getParam('action'), $this->allowedActions, true)) {
            return true;
        }

        return false;
    }
}
