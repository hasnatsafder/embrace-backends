<?php
declare(strict_types=1);

namespace Api\Controller\Traits;

use App\Exception\InvalidIdInSubResource;
use Cake\ORM\Association;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use RuntimeException;

/**
 * Trait IsNestedResource
 *
 * @package Api\Controller\Traits
 * @property string nestedResourceName
 */
trait HasAssociatedEntityTrait
{
    /**
     * @var Query
     */
    protected $_nestedResourceQuery;

    /**
     * @var array
     */
    protected $_nestedResourceFields = ['id'];

    /**
     * @param string $entityId
     * @throws \App\Exception\InvalidIdInSubResource
     */
    protected function setNestedEntity(string $entityId): void
    {
        //Make sure the query has been created
        if (empty($this->_nestedResourceQuery)) {
            $this->setupNestedResource();
        }

        $table = $this->getAssociation();

        foreach ($this->_nestedResourceFields as $field) {
            $this->_nestedResourceQuery = $this->_nestedResourceQuery->select($table->aliasField($field));
        }

        $entity = $this->_nestedResourceQuery
            ->where([$table->aliasField('id') => $entityId])
            ->first();

        if (empty($entity)) {
            throw new InvalidIdInSubResource();
        }

        $variableName = lcfirst(Inflector::classify($this->nestedResourceName));

        $this->{$variableName} = $entity;
    }

    protected function setupNestedResource()
    {
        if (empty($this->nestedResourceName)) {
            throw new RuntimeException('The nested resource name has to be defined when using this trait');
        }

        $this->_nestedResourceQuery = $this->getAssociation()->find();
    }

    protected function getAssociation(): Association
    {
        [, $alias] = pluginSplit($this->modelClass, true);

        return $this->{$alias}->{$this->nestedResourceName};
    }
}
