<?php


namespace Api\Controller\Traits;


trait PaginationTrait
{
    private $pagination_count = 15;

    /**
     * @return int
     */
    public function getPaginationCount(): int
    {
        return $this->pagination_count;
    }

    /**
     * @param int $pagination_count
     */
    public function setPaginationCount(int $pagination_count): void
    {
        $this->pagination_count = $pagination_count;
    }

    public function generatePaginateArray(int $count, int $page) {

        $page_count = (int)($count / $this->getPaginationCount()) + 1;
        // if count is modules of pagination_count then decrease pagecount
        if ($count > $this->getPaginationCount() && $count % $this->getPaginationCount() == 0) {
            $page_count --;
        }

        $pagination_array = [
            'count' => $count,
            'page_count' => $page_count,
            'current_page' => $page == 0 ? 1 : $page
        ];
        $pagination_array['has_next_page'] = (bool)($pagination_array['page_count'] > $pagination_array['current_page']);
        $pagination_array['has_previous_page'] = (bool)($pagination_array['current_page'] != 1);
        return $pagination_array;
    }
}
