<?php
declare(strict_types=1);

namespace Api\Controller;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 */
class RolesController extends AppController
{
    public $modelClass = 'App.Roles';
    protected $allowedActions = ['index', 'view'];
}
