<?php
declare(strict_types=1);

namespace Api\Controller;

use Api\Controller\Traits\HasAccountTrait;
use Api\Controller\Traits\PaginationTrait;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Debtors Controller
 *
 * @property \App\Model\Table\DebtorsTable $Debtors
 * @property \App\Model\Table\CountriesTable $Countries
 */
class DebtorsController extends AppController
{
    use HasAccountTrait;
    use PaginationTrait;

    public $modelClass = 'App.Debtors';
    protected $allowedActions = ['view', 'edit', 'index', 'add', 'delete', 'restore', 'myDebtors'];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $activeAccountId = $this->Auth->user('active_account_id');
        if (empty($activeAccountId) || !is_string($activeAccountId)) {
            throw new RuntimeException('No active account');
        }

        $this->setAccount($activeAccountId);

        $this->Crud->on('beforeFind', function (Event $event) {
            $event->getSubject()->query->matching('Accounts.Users', function (Query $query) {
                return $query->where(['Users.id' => $this->Auth->user('id'), 'Debtors.account_id' => $this->account->id]);
            });
        });

        $this->Crud->on('beforePaginate', function (Event $event) {
            $event->getSubject()->query->matching('Accounts.Users', function (Query $query) {
                return $query->where(['Users.id' => $this->Auth->user('id'), 'Debtors.account_id' => $this->account->id]);
            });
        });

        //set Denmark as default country if none provided
        $this->Crud->on('beforeSave', function (Event $event) {
            //setting the newly created query to be the active one
            if (!isset($event->getSubject()->id)) {
                //inside created method since only need to update on new entity save, not on edit
                if (!isset($event->getSubject()->entity->country_id)) {
                    $this->loadModel('Countries');
                    $denmark = $this->Countries->find(
                        'all',
                        ['conditions' => ['iso_code' => 'DK']]
                    )->select('id')->first();
                    $ev = $event->getSubject()->entity->country_id = $denmark->id;
                }
            }
        });
    }

    public function index(): Response
    {
        if ($this->request->getQuery('showCountries') === '1') {
            $this->Crud->on('beforePaginate', function (Event $event) {
                $event->getSubject()->query->select([
                    'Debtors.id',
                    'company_name',
                    'Debtors.first_name',
                    'Debtors.last_name',
                    'Debtors.vat_number',
                    'Debtors.address',
                    'Debtors.zip_code',
                    'Debtors.city',
                    'Debtors.is_company',
                    'Debtors.email',
                    'Debtors.phone',
                    'Countries.name',
                ])
                    ->contain(['countries']);
            });
        }

        return $this->Crud->execute();
    }

    public function view(): Response
    {
        if ($this->request->getQuery('showCountries') === '1') {
            $this->Crud->on('beforeFind', function (Event $event) {
                $event->getSubject()->query
                    ->contain(['countries']);
            });
        }

        return $this->Crud->execute();
    }
    /**
     * Soft delete debtor
     * @param String $debtor_id
     */
    public function delete($debtor_id)
    {
        $this->loadModel('Claims');
        $claims = $this->Claims->find()->where(['debtor_id' => $debtor_id])->count();
        if ($claims !== 0) {
            $this->set('data', 'Cannot delete, Debtor has ' . $claims . " claim(s) assigned to it");
            $this->set('success', false);
            return;
        }

        $debtor = $this->Debtors->get($debtor_id);
        if ($this->Debtors->trash($debtor)) {
            $this->set('data', "Successfully deleted debtor");
            $this->set('success', true);
        }
    }

    /**
     * restore debtor
     * @param String $debtor_id
     */
    public function restore($debtor_id)
    {
        $debtor = $this->Debtors->find('withTrashed')->where(['id' => $debtor_id])->first();
        if ($this->Debtors->restoreTrash($debtor)) {
            $this->set('data', "Successfully deleted debtor");
            $this->set('success', true);
        }
    }

    /**
     * myDebtors
     */
    public function myDebtors() {
        $page = $this->request->getQuery('page') ?? 1;
        $page = (int)$page;
        $limit = $this->request->getQuery('limit') ?? 15;
        $this->setPaginationCount((int)$limit);
        $debtors = $this->Debtors->find('all')
            ->select([
                'Debtors.id',
                'company_name',
                'Debtors.first_name',
                'Debtors.last_name',
                'Debtors.vat_number',
                'Debtors.address',
                'Debtors.zip_code',
                'Debtors.city',
                'Debtors.is_company',
                'Debtors.erp_integration_foreign_key',
                'Debtors.email',
                'Debtors.phone',
                'Countries.name',
            ])
            ->contain(['countries'])
            ->limit($this->getPaginationCount())->offset(($page-1) * $this->getPaginationCount())
            ->where([
                'Debtors.account_id' => $this->_getCurrentUser()->active_account_id,
            ]);
        if ($this->request->getQuery('search_term'))
        {
            $search_term = str_replace(" " , "", $this->request->getQuery('search_term'));
            $search_term = strtolower($search_term);
            $search_term = str_replace(" ", "", $search_term);

            $debtors->where(['OR' => [
                'LOWER(REPLACE(Debtors.vat_number, " ", "")) LIKE ' => "%".$search_term."%",
                'CONCAT(LOWER(REPLACE(Debtors.first_name, " ", "")),LOWER(REPLACE(last_name, " ", ""))) LIKE ' => "%".$search_term."%",
                'LOWER(REPLACE(Debtors.company_name, " ", "")) LIKE ' => "%".$search_term."%",
                'LOWER(REPLACE(Debtors.erp_integration_foreign_key, " ", "")) LIKE ' => "%".$search_term."%",
            ]]);
        }

        $this->set(['data' => $debtors, 'success' => true, 'pagination' => $this->generatePaginateArray($debtors->count(), $page)]);
        return;
    }
}
