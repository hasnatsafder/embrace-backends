<?php
declare(strict_types=1);

namespace Api\Controller\Claims;

use Cake\Http\Response;

/**
 * ClaimTransactions Controller
 *
 * @property \App\Model\Table\ClaimTransactionsTable $ClaimTransactions
 */
class ClaimTransactionsController extends BaseClaimController
{
    public $modelClass = 'App.ClaimTransactions';
    protected $allowedActions = ['index', 'add', 'view'];

    public function add(): Response
    {
        $this->RoleRequirer->requireAllOf(['root']);

        $this->setRequest(
            $this->getRequest()
                ->withData('claim_id', $this->claim->id)
        );

        $this->Crud->action()->setConfig('saveOptions', [
            'accessibleFields' => [
                'claim_id' => true,
                'claim_transaction_type_id' => true,
                'timestamp' => true,
                'amount' => true,
                'subject' => true,
                'description' => true,
            ],
        ]);

        return $this->Crud->execute();
    }
}
