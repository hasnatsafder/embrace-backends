<?php


namespace Api\Controller;

use Cake\Event\Event;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * AccountVerificationsController Controller
 *
 * @property \App\Model\Table\AccountVerificationsTable $AccountVerifications
 */
class AccountVerificationsController extends AppController
{
    public $modelClass = 'App.AccountVerifications';

    protected $allowedActions = ['index', 'view', 'edit', 'add', 'accountOwners', 'updateData'];

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow( 'updateData');
    }
    public function accountOwners($account_id) {
        $accountOwners = $this->AccountVerifications->find()->where(['account_id' => $account_id])->contain('Documents');

        $this->set('data', $accountOwners);
        $this->set('success', true);
    }

    public function updateData()
    {
        $account_verification = $this->AccountVerifications->find()->where([
            'form_key' => $this->request->getData('form_key')
        ])->firstOrFail();
        $this->AccountVerifications->patchEntity($account_verification, $this->request->getData());
        if ($this->AccountVerifications->save($account_verification)) {
            $this->set('success', true);
            $this->set('record_id', $account_verification->id);
        }
        else {
            throw new \Exception("Data not saved");
        }
    }

}

