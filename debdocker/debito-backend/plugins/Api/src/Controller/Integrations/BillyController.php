<?php
declare(strict_types=1);

namespace Api\Controller\Integrations;

use Api\Controller\AppController;
use Api\Controller\Integrations\Interfaces\IntegrationsControllerInterface;
use Api\Controller\Integrations\Traits\ErpControllerTrait;
use Cake\Core\Exception\Exception;
use Cake\Http\Exception\BadRequestException;
use EconomyPie\Client\Adapter\BillyAdapter;
use GuzzleHttp\Client;
use InvalidArgumentException;

/**
 * D Controller
 *
 * @property \App\Model\Table\ErpIntegrationDataTable $ErpIntegrationData
 * @property \Queue\Model\Table\QueuedJobsTable $QueuedJobs
 */
class BillyController extends AppController implements IntegrationsControllerInterface
{
    use ErpControllerTrait;

    public $modelClass = 'App.ErpIntegrationData';
    protected $requireActiveAccount = true;
    protected $allowedActions = [
        'status',
        'delete',
        'startAuth',
        'finishAuth',
    ];

    /**
     * Used to check if the connection to e-conomic is active and working
     */
    public function status()
    {
        $this->getRequest()->allowMethod('GET');

        $data = [
            'success' => true,
            'data' => [],
        ];

        $erpIntegration = $this->ErpIntegrationData->ErpIntegrations->find()
            ->where([
                'ErpIntegrations.vendor_identifier' => 'billy',
                'ErpIntegrations.account_id' => $this->Auth->user('active_account_id'),
            ])
            ->first();

        $authData = $erpIntegration->getAuthData();

        $billyClient = new BillyAdapter($authData);

        $request = $billyClient->getClient()->get('/organizations');
        $responseCode = $request->getStatusCode();

        if ($responseCode !== 200) {
            $data['success'] = false;
        }

        $this->set($data);
    }

    /**
     * Initializes the connection to e-conomic, returns a URL that can be used to Authenticate via OAuth at e-conomic
     */
    public function startAuth()
    {
        $this->getRequest()->allowMethod(['POST']);
        $accountId = (string)$this->Auth->user('active_account_id');

        if ($this->_hasExistingTokenSaved($accountId, 'billy')) {
            throw new BadRequestException('There is already a token saved for this account');
        }

        $response = [
            'data' => [],
            'success' => true,
        ];

        $billyEmail = $this->request->getData('billy_email');
        $billyPassword = $this->request->getData('billy_password');

        $billyAuth = new \EconomyPie\Auth\Adapter\BillyAdapter([
            'email' => $billyEmail,
            'password' => $billyPassword,
        ]);

        $response['data'] = $billyAuth->getOrganizations();

        $this->set($response);
    }

    public function finishAuth()
    {
        $this->getRequest()->allowMethod('POST');

        $userId = (string)$this->Auth->user('id');
        $accountId = (string)$this->Auth->user('active_account_id');
        $billyEmail = $this->getRequest()->getData('billy_email');
        $billyPassword = $this->getRequest()->getData('billy_password');
        $organizationId = $this->request->getData('organizationId');
        $client = new Client([
            'base_uri' => 'https://api.billysbilling.com/v2/',
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($billyEmail . ':' . $billyPassword),
            ],
        ]);

        $oAuthActorsRequest = $client->post('oAuthActors', [
            'json' => [
                'oAuthActor' => [
                    'description' => 'Debito API',
                    'name' => 'Debito API',
                    'organizationId' => $organizationId,
                ],
            ],
        ]);
        $oAuthActorsResponse = json_decode($oAuthActorsRequest->getBody()->getContents(), true);
        $token = $oAuthActorsResponse['oAuthAccessTokens'][0]['material'];

        if (empty($token)) {
            throw new InvalidArgumentException('There was no token from Billy');
        }

        if (!$this->_saveToken($token, $accountId, $userId, 'billy')) {
            throw new Exception('Token could not be saved');
        }

        // send to queuee token has been saved.
        $erpIntegration = $this->ErpIntegrationData->find()
            ->where([
                'data_value' => $token,
            ])->first();
        if ($erpIntegration) {
            $this->loadModel('Queue.QueuedJobs');
            $this->QueuedJobs->createJob('Erp', ['erpIntegration_id' => $erpIntegration->erp_integration_id],
                ['reference' => $erpIntegration->erp_integration_id]);
        }

        $this->set([
            'data' => ['reference' => $erpIntegration->erp_integration_id],
            'success' => true,
        ]);
    }

    /**
     * Deletes the connection
     */
    public function delete()
    {
        $this->getRequest()->allowMethod(['DELETE', 'POST']);

        $token = $this->ErpIntegrationData->find()
            ->find('IntegrationIs', [
                'integration' => 'billy',
                'account_id' => $this->Auth->user('active_account_id'),
            ])
            ->where([
                'ErpIntegrationData.data_key' => 'auth_token',
            ])
            ->firstOrFail();

        $this->ErpIntegrationData->deleteOrFail($token);

        $this->set([
            'success' => true,
            'data' => [],
        ]);
    }
}
