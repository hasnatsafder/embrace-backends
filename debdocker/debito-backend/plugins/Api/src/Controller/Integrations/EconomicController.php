<?php
declare(strict_types=1);

namespace Api\Controller\Integrations;

use Api\Controller\AppController;
use Api\Controller\Integrations\Interfaces\IntegrationsControllerInterface;
use Api\Controller\Integrations\Traits\ErpControllerTrait;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Http\Exception\BadRequestException;
use Cake\Routing\Router;
use EconomyPie\Client\Adapter\EconomicAdapter;
use App\Utility\ErpIntegrations;
use InvalidArgumentException;

/**
 * Economic Controller
 *
 * @property \App\Model\Table\ErpIntegrationDataTable $ErpIntegrationData
 * @property \Queue\Model\Table\QueuedJobsTable $QueuedJobs
 */
class EconomicController extends AppController implements IntegrationsControllerInterface
{
    use ErpControllerTrait;

    public $modelClass = 'App.ErpIntegrationData';
    protected $requireActiveAccount = true;
    protected $allowedActions = [
        'status',
        'delete',
        'startAuth',
        'finishAuth',
    ];

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow([
            'finishAuth',
        ]);
    }

    /**
     * Used to check if the connection to e-conomic is active and working
     */
    public function status()
    {
        $this->getRequest()->allowMethod('GET');

        $data = [
            'success' => true,
            'data' => [],
        ];

        $erpIntegration = $this->ErpIntegrationData->ErpIntegrations->find()
            ->where([
                'ErpIntegrations.vendor_identifier' => 'economic',
                'ErpIntegrations.account_id' => $this->Auth->user('active_account_id'),
            ])
            ->first();

        $authData = $erpIntegration->getAuthData();

        $economicClient = new EconomicAdapter($authData);

        $request = $economicClient->getClient()->get('self');
        $responseCode = $request->getStatusCode();

        if ($responseCode !== 200) {
            $data['success'] = false;
        }

        $this->set($data);
    }

    /**
     * Deletes the connection
     */
    public function delete()
    {
        $this->getRequest()->allowMethod(['DELETE', 'POST']);

        $token = $this->ErpIntegrationData->find()
            ->find('IntegrationIs', [
                'integration' => 'economic',
                'account_id' => $this->Auth->user('active_account_id'),
            ])
            ->where([
                'ErpIntegrationData.data_key' => 'auth_token',
            ])
            ->firstOrFail();

        $this->ErpIntegrationData->deleteOrFail($token);

        $this->set([
            'success' => true,
            'data' => [],
        ]);
    }

    /**
     * Initializes the connection to e-conomic, returns a URL that can be used to Authenticate via OAuth at e-conomic
     */
    public function startAuth()
    {
        $this->getRequest()->allowMethod('POST');

        if ($this->_hasExistingTokenSaved((string)$this->Auth->user('active_account_id'), 'economic')) {
            throw new BadRequestException('There is already a token saved for this account');
        }

        $redirectBaseUrl = Configure::read('Integrations.erp.economic.authUrl');
        $returnUrl = Router::url([
            'plugin' => 'Api',
            'prefix' => 'integrations',
            'controller' => 'Economic',
            'action' => 'finishAuth',
            '?' => [
                'userId' => $this->Auth->user('id'),
                'accountId' => $this->Auth->user('active_account_id'),
                //TODO WHERE SHOULD WE REDIRECT TO???
                'redirect_url' => Router::url(Configure::read('Frontendurl') . "accountingsystem"),
            ],
        ], true);

        $redirectUrl = sprintf(
            '%s?%s',
            $redirectBaseUrl,
            http_build_query([
                'locale' => 'da-DK',
                'appId' => Configure::read('Integrations.erp.economic.tokens.public'),
                'redirectUrl' => $returnUrl,
            ])
        );

        $this->set([
            'success' => true,
            'data' => [
                'redirect_url' => $redirectUrl,
            ],
        ]);
    }

    /**
     * Completing the integration to e-conomic. Saves the returned Oauth Token from e-conomic APIs.
     * Redirects to a page where the front-end can pick it up again.
     */
    public function finishAuth()
    {
        $this->getRequest()->allowMethod('GET');

        $userId = $this->request->getQuery('userId');
        $accountId = $this->request->getQuery('accountId');

        if ($this->_hasExistingTokenSaved($accountId, 'economic')) {
            throw new BadRequestException('It seems that you already added an accounting system. Go here ' . Router::url(Configure::read('Frontendurl') . "accountingsystem") . ' to check it out ');
        }

        $token = $this->getRequest()->getQuery('token');

        if (empty($token)) {
            throw new InvalidArgumentException('There was no token from e-conomic');
        }

        if (!$this->_saveToken($token, $accountId, $userId, 'economic')) {
            throw new Exception('Token could not be saved');
        }

        // send to queuee token has been saved.
        $erpIntegration = $this->ErpIntegrationData->find()
            ->where([
                'data_value' => $token,
            ])->first();
        if ($erpIntegration) {
            $this->loadModel('Queue.QueuedJobs');
            // we wil create reference of erp_integrtion_id,
            // and will use this id to get if queue is completed.
            $this->QueuedJobs->createJob('Erp', ['erpIntegration_id' => $erpIntegration->erp_integration_id],
                ['reference' => $erpIntegration->erp_integration_id]);
        }

        $redirectUrl = $this->getRequest()->getQuery('redirect_url') . '/syncing/' . $erpIntegration->erp_integration_id;

        $this->redirect(!empty($redirectUrl) ? $redirectUrl : '/');
    }
}
