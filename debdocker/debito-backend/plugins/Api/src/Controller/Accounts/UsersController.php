<?php
declare(strict_types=1);

namespace Api\Controller\Accounts;

use Api\Controller\AppController;
use Api\Controller\Traits\HasAccountTrait;
use Cake\Event\Event;
use Cake\Http\Response;
use InvalidArgumentException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    use HasAccountTrait;

    public $modelClass = 'App.Users';

    protected $allowedActions = [
        'index',
        'view',
        'delete',
        'add',
        'edit',
    ];

    public function beforeFilter(Event $e): void
    {
        parent::beforeFilter($e);

        $this->setAccount($this->getRequest()->getParam('account_id'));

        //We only want to find the users in this single account in this controller.
        $this->Crud->action()->findMethod([
            'InAccount' => [
                'account_id' => $this->account->id,
            ],
        ]);
    }

    public function add(): Response
    {
        //Set the only account to active when creating new users
        $this->Crud->on('beforeSave', function (Event $e) {
            $e->getSubject()->entity->active_account_id = $this->account->id;
        });

        $this->Crud->on('afterSave', function (Event $e) {
            $this->Users->Accounts->link($e->getSubject()->entity, [$this->account]);
        });

        return $this->Crud->execute();
    }

    public function delete($id): Response
    {
        $this->Crud->on('beforeDelete', function (Event $e) {
            // Stop the delete event, the entity will not be deleted
            if ($e->getSubject()->entity->id === $this->Auth->user('id')) {
                throw new InvalidArgumentException("You cannot delete your own user");
            }
        });

        return $this->Crud->execute();
    }

    public function edit($id): Response
    {
        $this->Crud->on('afterFind', function (Event $e) {
            /** @var \App\Model\Entity\User $user */
            $user = $e->getSubject()->entity;

            //Don't allow changing this here
            $user->setAccess('active_account_id', false);
        });

        return $this->Crud->execute();
    }
}
