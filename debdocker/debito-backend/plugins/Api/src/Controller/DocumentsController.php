<?php


namespace Api\Controller;

/**
 * DocumentsController Controller
 *
 * @property \App\Model\Table\DocumentsTable $Documents
 */
class DocumentsController extends AppController
{
    public $modelClass = 'App.Documents';

    protected $allowedActions = ['index', 'view', 'edit', 'add'];

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow( 'add');
    }

}
