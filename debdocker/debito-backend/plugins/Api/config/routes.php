<?php
declare(strict_types=1);

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

$apiResources = [
    'Accounts' => [
        'Users',
    ],
    'AccountVerifications',
    'ClaimActionTypes',
    'ClaimLineTypes',
    'ClaimPhases',
    'Claims' => [
        'ClaimActions',
        'ClaimFinancials',
        'ClaimLines',
        'ClaimTransactions',
    ],
    'CompanyTypes',
    'Countries',
    'Currencies',
    'Debtors',
    'Documents',
    'DunningConfigurations',
    'Invoices',
    'InvoiceStatuses',
    'Roles',
    'Users',
    'DebtorLawyers',
];

Router::plugin('Api', ['path' => '/api/v1'], function (RouteBuilder $routes) use ($apiResources) {
    $routes->setExtensions(['json']);

    foreach ($apiResources as $apiResource => $subResources) {
        if (is_int($apiResource)) {
            $routes->resources($subResources, [
                'inflect' => 'dasherize',
            ]);
        } elseif (is_array($subResources)) {
            $routes->resources($apiResource, [
                'inflect' => 'dasherize',
            ], function (RouteBuilder $routes) use ($subResources, $apiResource) {
                if (!empty($subResources)) {
                    foreach ($subResources as $resource) {
                        $routes->resources($resource, [
                            'inflect' => 'dasherize',
                            'prefix' => $apiResource,
                        ]);
                    }
                }
            });
        }
    }

    $routes->prefix('integrations', function (RouteBuilder $routes) {
        $routes->connect('/:controller/:action');
    });

    $routes->fallbacks(DashedRoute::class);
});
