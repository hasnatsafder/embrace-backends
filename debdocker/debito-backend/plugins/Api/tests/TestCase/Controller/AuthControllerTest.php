<?php
declare(strict_types=1);

namespace Api\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Api\Controller\AuthController Test Case
 *
 * @uses \Api\Controller\AuthController
 */
class AuthControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.auth',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
