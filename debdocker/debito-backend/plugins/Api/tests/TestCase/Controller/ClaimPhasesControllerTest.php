<?php
declare(strict_types=1);

namespace Api\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Api\Controller\ClaimPhasesController Test Case
 *
 * @uses \Api\Controller\ClaimPhasesController
 */
class ClaimPhasesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.claim_phases',
        'plugin.api.claim_action_types',
        'plugin.api.collectors',
        'plugin.api.collector_files',
        'plugin.api.collector_file_types',
        'plugin.api.claim_actions',
        'plugin.api.claims',
        'plugin.api.created_by',
        'plugin.api.active_accounts',
        'plugin.api.company_types',
        'plugin.api.accounts',
        'plugin.api.countries',
        'plugin.api.debtors',
        'plugin.api.accounts_collectors',
        'plugin.api.users',
        'plugin.api.accounts_users',
        'plugin.api.roles',
        'plugin.api.claim_financials',
        'plugin.api.claim_lines',
        'plugin.api.claim_line_types',
        'plugin.api.currencies',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
