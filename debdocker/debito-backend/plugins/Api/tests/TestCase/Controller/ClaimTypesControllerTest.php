<?php
declare(strict_types=1);

namespace Api\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Api\Controller\ClaimTypesController Test Case
 */
class ClaimTypesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.claim_types',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
