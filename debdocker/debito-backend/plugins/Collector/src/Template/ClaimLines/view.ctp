<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimLines
 */
?>
<div class="page-heading">
    <h1 class="page-title"><?= __d('Collector', 'case_line') ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link(__d('Collector', 'case'), ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item"><?= __d('Collector', 'case_line') ?></li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title"><?= __d('Collector', 'case_line') ?> <?= __d('Collector', 'list') ?></div>
        </div>
        <div class="ibox-body">
            <table class="table">
                <thead>
                <tr>
                    <th><?= __d('Collector', 'type') ?></th>
                    <th><?= __d('Collector', 'invoice_date') ?></th>
                    <th><?= __d('Collector', 'amount') ?></th>
                    <th><?= __d('Collector', 'invoice_reference') ?></th>
                    <th class="no-sort"><?= __d('Collector', 'action') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($claimLines as $claimLine): ?>
                    <tr>
                        <td>
                            <?php if ($claimLine->claim_line_type): ?>
                                <?= $claimLine->claim_line_type->name; ?>
                            <?php endif; ?>
                        </td>
                        <td> <?= $claimLine->invoice_date; ?> </td>
                        <td class="amount"> <?= $claimLine->amount; ?> </td>
                        <td> <?= $claimLine->invoice_reference; ?> </td>
                        <td>
                            <a class="btn btn-info" target="_blank"
                               href="<?php echo $this->request->webroot; ?>webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>"><?= __d('Collector',
                                    'file') ?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>

<script>
    numeral.locale("da-dk");
    $(document).ready(function () {
        $('.amount').each(function (i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.toString().splice(-3, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original).format("$0,0.00");
            $(this).html(number)
        });
    });
</script>
