<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimPhases
 * @var mixed $claims
 * @var string $debito_reference
 * @var mixed $status
 */
?>
<div class="page-heading">
    <h1 class="page-title"><?= __d('Collector', 'case') ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><?= __d('Collector', 'cases') ?></li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title"><?= __d('Collector', 'cases') ?></div>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2">Phase:</label>
                    <?php echo $this->Form->control('status',
                        [
                            'options' => $claimPhases,
                            'value' => $status,
                            'class' => "selectpicker show-tick",
                            'empty' => true,
                            'label' => false,
                            'data-style' => 'btn-solid',
                            'data-width' => "150px",
                            'empty' => ["" => 'All'],
                        ]); ?>
                    <label class="mb-0 mr-2 ml-4"><?= __d('Collector', 'Collector Reference') ?>:</label>
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="reference" type="text"
                               placeholder="<?= __d('Collector', 'Search Reference') ?>" value="<?= $debito_reference ?>">
                    </div>
                    <button class="btn btn-blue btn-sm ml-4" id="filter-button"><?= __d('Collector', 'Filter') ?></button>
                    <button class="btn btn-secondary btn-sm ml-4" id="clear"><?= __d('Collector', 'Clear') ?></button>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th><?= __d('Collector', 'created_by') ?></th>
                        <th><?= __d('Collector', 'date') ?></th>
                        <th><?= __d('Collector', 'account') ?></th>
                        <th><?= __d('Collector', 'debtor') ?></th>
                        <th><?= __d('Collector', 'phase') ?></th>
                        <th><?= __d('Collector', 'debito_reference') ?></th>
                        <th><?= __d('Collector', 'Collector Reference') ?></th>
                        <th><?= __d('Collector', 'case_line') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claims as $claim): ?>
                        <tr>
                            <td> <?= $claim->user->email; ?> </td>
                            <td> <?= date_format($claim->created, 'Y-m-d') ?> </td>
                            <td> <?= $claim->account->name; ?> </td>
                            <td>
                                <?php
                                echo $claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name;
                                ?>
                            </td>
                            <td> <?= $claim->claim_phase->order ?> - <?= $claim->claim_phase->name ?> </td>
                            <td>
                                <?= $claim->customer_reference; ?>
                            </td>
                            <td>
                                <?= $claim->collector_reference; ?>
                            </td>
                            <td>
                                <?= $this->Html->link(__d('Collector', 'cases'),
                                    ['controller' => 'ClaimLines', 'action' => 'view', $claim->id],
                                    ['class' => 'btn btn-primary']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                <p><?= $this->Paginator->counter([
                        'format' => __d('Collector', 'Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total'),
                    ]) ?></p>
            </div>
            <div class="dataTables_paginate paging_simple_numbers pull-right">
                <ul class="pagination">
                    <?php if ($this->Paginator->numbers()): ?>
                        <?php echo $this->Paginator->prev(__d('Collector', 'Previous')); ?>
                        <?php echo $this->Paginator->numbers(); ?>
                        <?php echo $this->Paginator->next(__d('Collector', 'Next')); ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<script>

    $('#filter-button').click(function () {
        url = window.location.href.split('?')[0];
        var ref = $('#reference').val();
        var status = $('#status').val();
        if (ref || status) {
            url += "?"
        }
        if (ref) {
            url += "ref=" + ref + "&"
        }
        if (status) {
            url += "status=" + status
        }
        window.location.assign(url)
    })

</script>
