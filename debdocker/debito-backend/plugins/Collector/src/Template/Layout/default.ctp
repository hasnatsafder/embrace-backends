<?php
/**
 * @var \App\View\AppView $this
 */
?>
<!DOCTYPE html>
<html>

<!-- START HEAD-->
<?= $this->element('head') ?>
<!-- END HEAD-->

<body>
<div class="page-wrapper">
    <!-- START HEADER-->
    <?= $this->element('header') ?>
    <!-- END HEADER-->

    <!-- START SIDEBAR-->
    <?= $this->element('sidebar') ?>
    <!-- END SIDEBAR-->

    <!-- START CONTENT-->
    <div class="content-wrapper">
        <?= $this->fetch('content') ?>
    </div>
    <!-- END CONTENT-->

    <!-- START FOOTER-->
    <?= $this->element('footer') ?>
    <!-- End FOOTER-->
</div>
</body>

</html>