<?php
/**
 * @var \App\View\AppView $this
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= h($this->fetch('title')) ?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <?= $this->Html->css('/Admin/css/vendors/bootstrap.css') ?>
    <?= $this->Html->css('/Admin/css/main.css') ?>
    <?= $this->Html->script('/Admin/js/vendors/jquery.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/popper.js') ?>
    <?= $this->Html->script('/Admin/js/vendors/bootstrap.js') ?>
</head>
<body>
<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2">
        <ul class="nav navbar-toolbar">
            <li class="dropdown">
                <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button"
                   aria-haspopup="true" aria-expanded="false" href="#" style="color: #fff;">
                    <?= __d('Collector', 'language_short') ?> <i class="fa fa-menu-en fa-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li></li>
                    <li><?= $this->Html->link("DK",
                            ["controller" => "App", "action" => "changeLanguage", 'dk']); ?></li>
                    <li><?= $this->Html->link("EN",
                            ["controller" => "App", "action" => "changeLanguage", 'en']); ?></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<div style="max-width: 400px;margin: 100px auto 50px;" class="bg-white">
    <?= $this->fetch('content') ?>
</div>
</body>
</html>

<style>
    body {
        background-color: #4a5ab9;
    }

    .login-content {
        max-width: 900px;
        margin: 100px auto 50px;
    }

    }
</style>
