<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="page-sidebar">
    <ul class="side-menu metismenu scroller">
        <li>
            <?= $this->Html->link('References', ['controller' => 'AccountsCollectors', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link(__d('Collector', 'cases'), ['controller' => 'Claims', 'action' => 'index']); ?>
        </li>
    </ul>
</nav>
