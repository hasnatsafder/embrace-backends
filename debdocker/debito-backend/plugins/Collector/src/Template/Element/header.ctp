<?php
/**
 * @var \App\View\AppView $this
 * @var string $COLLECTOR_NAME
 */
?>
<header class="header">
    <!-- START TOP-LEFT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
        <li class="dropdown mega-menu">
            <?php echo $COLLECTOR_NAME ?>
        </li>
    </ul>
    <!-- END TOP-LEFT TOOLBAR-->
    <!--LOGO-->
    <a href="/collector">
        <?= $this->Html->image('/Admin/img/logo_web.png', ['alt' => 'Debito']); ?>
    </a>

    <!-- START TOP-RIGHT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li class="dropdown">
            <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false" href="#">
                <?= __d('Collector', 'language_short') ?> <i class="fa fa-menu-en fa-chevron-down"></i>
            </a>
            <ul class="dropdown-menu">
                <li></li>
                <li><?= $this->Html->link("DK", ["controller" => "App", "action" => "changeLanguage", 'dk']); ?></li>
                <li><?= $this->Html->link("EN", ["controller" => "App", "action" => "changeLanguage", 'en']); ?></li>
            </ul>
        </li>
        <li>
            <?= $this->Html->link(__d('Collector', 'logout'), ['controller' => 'Auth', 'action' => 'logout']); ?>
        </li>
    </ul>
    <!-- END TOP-RIGHT TOOLBAR-->
</header>
