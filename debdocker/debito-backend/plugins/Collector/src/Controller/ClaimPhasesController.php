<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * ClaimPhases Controller
 *
 * @property \App\Model\Table\ClaimPhasesTable $ClaimPhases
 */
class ClaimPhasesController extends AppController
{
    public $modelClass = 'App.ClaimPhases';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
