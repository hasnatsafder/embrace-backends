<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * ClaimLines Controller
 *
 * @property \App\Model\Table\ClaimLinesTable $ClaimLines
 */
class ClaimLinesController extends AppController
{
    public $modelClass = 'App.ClaimLines';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete', 'view']);
    }

    public function view($claim_id)
    {
        $claimLines = $this->ClaimLines->find(
            'all',
            ['conditions' => ['claim_id' => $claim_id]]
        )->contain(['ClaimLineTypes']);
        $this->set(compact('claimLines', 'claim_id'));
    }
}
