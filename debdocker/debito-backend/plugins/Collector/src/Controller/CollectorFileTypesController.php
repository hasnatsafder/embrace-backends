<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * CollectorFileTypes Controller
 *
 * @property \App\Model\Table\CollectorFileTypesTable $CollectorFileTypes
 */
class CollectorFileTypesController extends AppController
{
    public $modelClass = 'App.CollectorFileTypes';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
