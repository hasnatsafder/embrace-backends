<?php
declare(strict_types=1);

namespace Collector\Controller;

use App\Controller\AppController as BaseController;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\ORM\Query;
use CrudView\Menu\MenuDivider;
use CrudView\Menu\MenuItem;

/**
 * @property \App\Model\Table\UsersCollectorsTable $UsersCollectors
 * @property \App\Model\Table\CollectorsTable $Collectors
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 * @property \Crud\Controller\Component\CrudComponent $Crud
 */
class AppController extends BaseController
{
    public $components = ['Cookie'];
    protected $isAdmin = true;

    public function initialize()
    {
        parent::initialize();
        $this->setLangFromSession();
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                ],
            ],
            'authorize' => 'Controller',
            'loginAction' => [
                'prefix' => null,
                'plugin' => 'Collector',
                'controller' => 'Auth',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'prefix' => null,
                'plugin' => 'Collector',
                'controller' => 'AccountsCollectors',
                'action' => 'index',
            ],
            'authError' => __d('Collector', 'logged_in_to_view_page'),
            'storage' => 'Session',
            'checkAuthIn' => 'Controller.initialize',
        ]);
        $this->loadComponent('Paginator');

        // allow change language method for guests too
        $this->Auth->allow(['changeLanguage']);
    }

    private function setLangFromSession()
    {
        if ($this->request->session()->read('Language')) {
            I18n::locale($this->request->session()->read('Language'));
        } else {
            I18n::locale('da_DK');
        }
    }

    // to set the default locale of language, will redirect to page it was called from

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        if ($this->Crud->isActionMapped()) {
            $this->Crud->action()
                ->setConfig('scaffold.utility_navigation', $this->getTopBarMenu())
                ->setConfig('scaffold.sidebar_navigation', $this->getSideBarMenu())
                ->setConfig('scaffold.site_title', __('Debito Admin'));
        }

        $uid = $this->Auth->user('id');

        // Set Collector Name in Cookie
        if ($uid) {
            $this->Cookie->configKey('Collector_Name', 'encryption', false);
            $collectorNameCookie = $this->Cookie->read('Collector_Name');
            if (!$collectorNameCookie) {
                $this->loadModel('UsersCollectors');
                $userCollector = $this->UsersCollectors->find(
                    'all',
                    ['conditions' => ['user_id' => $uid]]
                )->first();

                $this->loadModel('Collectors');
                $collector = $this->Collectors->get($userCollector->collector_id);

                $this->set("COLLECTOR_NAME", $collector->name);
                $this->Cookie->write('Collector_Name', $collector->name);
            } else {
                $this->set("COLLECTOR_NAME", $collectorNameCookie);
            }
        } else {
            $this->set("COLLECTOR_NAME", '');
        }

        // setting language use the changeLanugage Method to set language cookie
        $this->Cookie->configKey('Language', 'encryption', false);
        $languageCookie = $this->Cookie->read('Language');
        if ($languageCookie) {
            $this->set('LOCALE_SITE', $languageCookie['name']); // will use that in view, most commonly for datatables
            I18n::setLocale($languageCookie['code']);
        } else {
            //set default locale to Danish
            I18n::locale('da_DK');
            $this->set('LOCALE_SITE', "Danish");
        }
    }

    protected function getTopBarMenu()
    {
        return [
            new MenuItem(
                __('Sign out'),
                ['controller' => 'Auth', 'action' => 'logouts']
            ),
        ];
    }

    protected function getSideBarMenu()
    {
        return [
            new MenuItem(
                __('Accounts'),
                ['controller' => 'Accounts', 'action' => 'index']
            ),
            new MenuItem(
                __('Users'),
                ['controller' => 'Users', 'action' => 'index']
            ),
            new MenuItem(
                __('Claims'),
                ['controller' => 'Claims', 'action' => 'index']
            ),
            new MenuItem(
                __('Collectors'),
                ['controller' => 'Collectors', 'action' => 'index']
            ),
            new MenuDivider(),
            new MenuItem(
                __('Claim action types'),
                ['controller' => 'ClaimActionTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Claim line types'),
                ['controller' => 'ClaimLineTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Claim phases'),
                ['controller' => 'ClaimPhases', 'action' => 'index']
            ),
            new MenuItem(
                __('Collector file types'),
                ['controller' => 'CollectorFileTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Company types'),
                ['controller' => 'CompanyTypes', 'action' => 'index']
            ),
            new MenuItem(
                __('Countries'),
                ['controller' => 'Countries', 'action' => 'index']
            ),
            new MenuItem(
                __('Currencies'),
                ['controller' => 'Currencies', 'action' => 'index']
            ),
            new MenuItem(
                __('Roles'),
                ['controller' => 'Roles', 'action' => 'index']
            ),
        ];
    }

    public function changeLanguage($lang)
    {
        if (!empty($lang)) {
            if ($lang == 'en') {
                $this->Cookie->write(
                    'Language',
                    ['name' => 'English', 'code' => 'en_US']
                );
                $this->request->session()->write('Language', 'en_US');
            } else {
                if ($lang == 'dk') {
                    $this->Cookie->write(
                        'Language',
                        ['name' => 'Danish', 'code' => 'da_DK']
                    );
                    $this->request->session()->write('Language', 'da_DK');
                }
            }
            //in order to redirect the user to the page from which it was called
            $this->redirect($this->referer());
        }
    }

    public function isAuthorized($user = null)
    {
        $this->loadModel('AccountsUsers');

        $rootRole = $this->AccountsUsers->find()
            ->where(['AccountsUsers.user_id' => $user['id']])
            ->matching('Roles', function (Query $q) {
                return $q->where(['Roles.identifier' => 'consultant']);
            });

        return !$rootRole->isEmpty();
    }
}
