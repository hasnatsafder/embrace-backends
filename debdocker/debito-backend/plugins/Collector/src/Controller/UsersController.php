<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public $modelClass = 'App.Users';
    public $isAdmin = false;

    public function index()
    {
        $this->Crud->action()
            ->setConfig('scaffold.relations', ['ActiveAccounts'])
            ->setConfig('scaffold.fields', ['first_name', 'last_name', 'phone', 'email'])
            ->setConfig('scaffold.actions', ['view', 'add', 'edit']);

        return $this->Crud->execute();
    }

    public function add()
    {
        $this->Crud->action()
            ->setConfig('scaffold.fields', [
                'first_name',
                'last_name',
                'phone',
                'email',
                'password',
                'is_confirmed',
                'is_active',
            ]);

        return $this->Crud->execute();
    }
}
