<?php
declare(strict_types=1);

namespace Collector\Controller;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \App\Model\Table\UsersCollectorsTable $UsersCollectors
 */
class ClaimsController extends AppController
{
    public $modelClass = 'App.Claims';
    public $isAdmin = false;

    public function index()
    {
        $uid = $this->Auth->user('id');

        $this->loadModel('UsersCollectors');
        $userCollector = $this->UsersCollectors->find(
            'all',
            ['conditions' => ['user_id' => $uid]]
        )->first();

        $debito_reference = $this->request->query('ref') ? $this->request->query('ref') : "";
        $status = $this->request->query('status') ? $this->request->query('status') : "";
        $condition['collector_id'] = $userCollector->collector_id;
        if ($debito_reference) {
            $condition['Claims.collector_reference LIKE '] = "%$debito_reference%";
        }
        if ($status) {
            $condition['Claims.claim_phase_id'] = $status;
        } else {
            $condition['ClaimPhases.identifier IN'] = [
                'case_sent',
                'reminder',
                'debt_collection',
                'ended',
            ];
        }

        // we only want to show to the collectors claim phases between 3 and 6
        $claims = $this->Paginator->paginate($this->Claims->find(
            'all',
            [
                'order' => ['Claims.created' => 'ASC'],
                'conditions' => $condition,
            ]
        )->contain(['Users', 'Accounts', 'Debtors', "ClaimPhases"]));

        $claimPhases = $this->Claims->ClaimPhases->find('list')
            ->where([
                'ClaimPhases.identifier' => [
                    'case_sent',
                    'reminder',
                    'debt_collection',
                    'ended',
                ],
            ])
            ->orderAsc('ClaimPhases.order');

        $this->set(compact('claims', 'claimPhases', 'debito_reference', 'status'));
    }
}
