<?php
declare(strict_types=1);

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin('Collector', ['path' => '/collector'], function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'AccountsCollectors', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});
