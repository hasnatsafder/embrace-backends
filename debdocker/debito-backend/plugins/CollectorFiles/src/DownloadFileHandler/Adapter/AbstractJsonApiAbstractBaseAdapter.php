<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

use App\Utility\NotifyAdmin;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Psr\Log\LogLevel;

abstract class AbstractJsonApiAbstractBaseAdapter extends AbstractBaseAdapter
{
    protected const BASE_URL = "";
    protected const COLLECTOR_NAME = "";
    public $http;
    protected $claimTable;
    protected $claimActionTypeTable;
    protected $claimActionTable;
    protected $collectorTable;
    protected $collectorFileTable;
    protected $accountsTable;
    protected $claimLineTable;

    public function __construct()
    {
        $this->claimTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimActionTypeTable = TableRegistry::getTableLocator()->get('ClaimActionTypes');
        $this->claimActionTable = TableRegistry::getTableLocator()->get('ClaimActions');
        $this->collectorTable = TableRegistry::getTableLocator()->get('Collectors');
        $this->collectorFileTable = TableRegistry::getTableLocator()->get('CollectorFiles');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $this->claimLineTable = TableRegistry::getTableLocator()->get('ClaimLines');
    }

    public function downloadClaimReceipts()
    {
        $this->getReceiptFile();
    }

    abstract protected function getReceiptFile();

    public function downloadClaimStatus()
    {
        $this->getStatusFile();
    }

    abstract protected function getStatusFile();

    public function downloadClaimFinancials()
    {
        $this->getFinancialFile();
    }

    abstract protected function getFinancialFile();

    public function downloadClaiminbs()
    {
        $this->getInbFile();
    }

    abstract protected function getInbFile();

    /**
     * Download Json API
     *
     * @param array $data
     * @return Guzzle Response
     */
    public function getApiRequest($end_point, $headers = [], $claim_id = [])
    {
        $this->http = new Client([
            'base_uri' => Configure::read($this::COLLECTOR_NAME . '.api'),
            'headers' => $headers,
        ]);
        try {
            $response = $this->http->get($end_point);

            return ["fetched" => true, "response" => json_decode($response->getBody()->getContents(), true)];
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                // resync claim again and notify admin of resync
                if ($end_point == "SingleReceipt/") {
                    $this->reSyncClaim($claim_id);
                    NotifyAdmin::DownloadClaimsReceiptErrors(Psr7\str($e->getResponse()), $this::COLLECTOR_NAME, $end_point);
                    $this->logAndTerminal("Count not get URL receipt for " . $end_point);
                }
            }

            return ["fetched" => false, "response" => Psr7\str($e->getResponse())];
        }
    }

    /**
     * @param string $baseName of file on collectors FTP - e.g, Sager20180626.csv
     * @return array Array of contents
     * @description
     */

    /**
     * Show in terminal and log
     *
     * @param string text
     * @return bool
     */
    protected function logAndTerminal(string $text)
    {
        $this->log(
            sprintf(
                $text
            ),
            LogLevel::INFO
        );
    }

    /**
     * @return array multidimension array of files
     * @description list files and folders in a given folder
     */

    protected function getFilesList()
    {
        return $this->mountManager->listContents('destination://' . $this::DESTINATION_FILESYSTEM_DIR, false);
    }

    protected function getFileContents(string $baseName)
    {
        return $this->mountManager->read('destination://' . $this::DESTINATION_FILESYSTEM_DIR . $baseName, false);
    }

    /**
     * @param string $customerReference of claim
     * @param string $collectorReference of claim
     * @return bool
     * @description Update Claim to set phase and Customer Reference
     */

    protected function updateClaim(string $customerReference, string $collectorReference)
    {
        if ($customerReference && $customerReference !== "") {
            $claim = $this->claimTable->find(
                "all",
                ['conditions' => ['customer_reference' => $customerReference]]
            )->first();
            if ($claim) {
                if ($claim->claim_phase_id != $this->getClaimPhase('debt_collection')
                    && $claim->claim_phase_id != $this->getClaimPhase('ended')
                    && $claim->claim_phase_id != $this->getClaimPhase('reminder')
                ) {
                    $claim->claim_phase_id = $this->getClaimPhase('reminder');
                    $claim->collector_reference = $collectorReference;
                    $save = $this->claimTable->save($claim);
                } else {
                    echo "Claim Phase for " . $claim->customer_reference . " was already in advance stage" . PHP_EOL;
                }

                return true;
            }
        } else {
            if ($collectorReference) {
                $claim = $this->claimTable->find(
                    "all",
                    ['conditions' => ['collector_reference' => $collectorReference]]
                )->first();
                if ($claim) {
                    if ($claim->claim_phase_id != $this->getClaimPhase('debt_collection')
                        && $claim->claim_phase_id != $this->getClaimPhase('ended')
                        && $claim->claim_phase_id != $this->getClaimPhase('reminder')
                    ) {
                        $claim->claim_phase_id = $this->getClaimPhase('reminder');
                        $save = $this->claimTable->save($claim);
                    } else {
                        echo "Claim Phase for " . $claim->customer_reference . " was already in advance stage" . PHP_EOL;
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $actionType
     * @param string $date
     * @param int $paid
     * @param int $interest_added
     * @param int $debt_collection_fees
     * @param int $is_public
     * @param $claimNewPhase
     * @param $claim
     * @param string $collector_id
     * @return bool
     * @description Update claim phase, add new action type (if required), add new status for the claim
     */

    protected function updateClaimAndClaimActions(
        string $actionType,
        string $date,
        int $paid,
        int $interest_added,
        int $debt_collection_fees,
        int $is_public,
        $claimNewPhase,
        $claim,
        string $collector_id
    ) {
        if ($actionType == "" || $actionType === null || $date == "" || $date === null) {
            echo "Incomplete Values " . PHP_EOL;

            return;
        }

        //search action type of claim in action Types array
        $actionTypeSearch = $this->claimActionTypeTable->find(
            'all',
            ['conditions' => ['identifier' => $actionType]]
        )->first();

        //if value does not exists in action tables then insert it
        $actionTypeId = 0; // initialize ID
        if (!$actionTypeSearch) {
            $newClaimActionType = $this->claimActionTypeTable->newEntity();
            $newClaimActionType->collector_identifier = $actionType;
            $newClaimActionType->identifier = $actionType;
            $newClaimActionType->name = $actionType;
            $newClaimActionType->collector_id = $collector_id;
            $newClaimActionType->is_public = $is_public;
            $newClaimActionType->claim_phase_id = $claimNewPhase;
            $save = $this->claimActionTypeTable->save($newClaimActionType);
            $actionTypeId = $save->id;
            echo "Added a new Claim Action Type for " . $actionType . PHP_EOL;
        } else {
            $actionTypeId = $actionTypeSearch->id;
            //update claim status if phase is present for a particular claim action type
            //and claim is not in rejected phase
            if ($claimNewPhase && $claimNewPhase !== $claim->claim_phase_id && $claim->claim_phase_id !== $this->getClaimPhase('rejected') && $claim->claim_phase_id !== $this->getClaimPhase('ended')) {
                //update claim and set phase to match phase of claim phase order
                $claim->claim_phase_id = $claimNewPhase;
                $saveClaim = $this->claimTable->save($claim);
                echo "Update the Claim status to new phase representing " . $actionType . PHP_EOL;
            }
        }
        //insert status into claim actions
        $newClaimAction = $this->claimActionTable->newEntity();
        $newClaimAction->claim_id = $claim->id;
        $newClaimAction->claim_action_type_id = $actionTypeId;
        $newClaimAction->date = $date;
        $newClaimAction->paid = $paid;
        $newClaimAction->interest_added = $interest_added;
        $newClaimAction->debt_collection_fees = $debt_collection_fees;
        $saveClaimAction = $this->claimActionTable->save($newClaimAction);
        if ($saveClaimAction) {
            echo "Added a new Claim Phase to match phase of " . $actionType . " for claim " . $claim->customer_reference . "with collector_reference" . $claim->collector_reference . PHP_EOL;
        }

        return $saveClaimAction;
    }

    protected function updateClaimActionFinancials(
        $collector_reference,
        $date,
        $paid,
        $interest_added,
        $debt_collection,
        $actual_paid,
        string $fileName
    ) {
        //Get Collector reference
        $claim = $this->claimTable->find(
            'all',
            ['conditions' => ['collector_reference' => $collector_reference]]
        )->first();

        if (!$claim) {
            $this->log("Cound not find claim " . $collector_reference);

            return false;
        }

        $claimAction = $this->claimActionTable->find(
            'all',
            ['conditions' => ['claim_id' => $claim->id, 'date' => $date]]
        )->first();

        //claim not found for that date, add a new action for the date coming in
        if (!$claimAction) {
            $actionTypeSearch = $this->claimActionTypeTable->find(
                'all',
                ['conditions' => ['collector_identifier' => "**"]]
            )->first();
            $newClaimAction = $this->claimActionTable->newEntity();
            $newClaimAction->claim_id = $claim->id;
            $newClaimAction->claim_action_type_id = $actionTypeSearch->id;
            $newClaimAction->date = $date;
            $newClaimAction->collector_file_id = $this->addCollectorfile($fileName)->id;
            $newClaimAction->paid = $paid ? $paid : 0;
            $newClaimAction->interest_added = $interest_added ? $interest_added : 0;
            $newClaimAction->debt_collection_fees = $debt_collection ? $debt_collection : 0;
            $newClaimAction->actual_paid = $actual_paid ? $actual_paid : 0;
            $saveClaimAction = $this->claimActionTable->save($newClaimAction);
            $this->log("Added a new claim action for financial for " . $collector_reference . " on date " . $date);

            return true;
        }
        $claimAction->collector_file_id = $this->addCollectorfile($fileName)->id;
        $claimAction->paid = $paid ? $paid : $claimAction->paid;
        $claimAction->interest_added = $interest_added ? $interest_added : $claimAction->interest_added;
        $claimAction->debt_collection_fees = $debt_collection ? $debt_collection : $claimAction->debt_collection_fees;
        $claimAction->actual_paid = $actual_paid ? $actual_paid : $claimAction->actual_paid;
        if ($this->claimActionTable->save($claimAction)) {
            $this->log("Claim action update for " . $claim->customer_reference . " on date " . $date);
        } else {
            $this->log("Failed to update action for " . $collector_reference);
        }
    }

    private function addCollectorfile($fileName)
    {
        $collector_file = $this->collectorFileTable->find(
            'all',
            ['conditions' => ['name' => $fileName]]
        )->first();

        if ($collector_file) {
            return $collector_file;
        }

        $newCollectorFile = $this->collectorFileTable->newEntity();
        $newCollectorFile->name = $fileName;
        $newCollectorFile->extension = ".csv";
        $newCollectorFile->size = 1240;
        $newCollectorFile->dir = "savebox";
        $newCollectorFile->mime_type = "text/csv";
        $newCollectorFile->collector_file_type_id = "5bae97d0-352e-4379-9242-42771b7fea48";

        return $this->collectorFileTable->save($newCollectorFile);
    }

    protected function compareFileStatusDates($file_object, $date_column)
    {
        $file_name = $file_object['basename'];
        $str = explode(".", $file_name);
        $str = explode("_", $str[0]);
        $str = array_reverse($str);
        $date_file = date_create_from_format("U", (string)$file_object['timestamp']);
        $date_column = date_create_from_format("Y/m/d H:i:s", $date_column);
        if ($date_file > $date_column) {
            return true;
        }

        return false;
    }

    /**
     * Sync/Unsync all claimlines of a claim
     * @param String $claim_id
     * @param bool $sync
     * @return bool
     */
    protected function syncClaimLines($claim_id, $sync = 1)
    {
      return $this->claimLineTable->updateAll(
        [
          // fields
          'synced' => $sync
        ],
        [
          // condition
          'claim_id' => $claim_id
        ]
      );
    }

    /**
     * Resync claim in case of failed receipt
     */
    public function reSyncClaim($claim_id)
    {
        $claimLines = TableRegistry::getTableLocator()->get('ClaimLines');
        $query = $claimLines->query();
        $res = $query->update()
            ->set(['synced' => 0])
            ->where(['claim_id' => $claim_id])
            ->execute();

        // move claim to phase 2 and mark synced as null
        $claimPhasesTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $createdClaimPhase = $claimPhasesTable->find('all')
                                ->where(['identifier' => 'case_created'])->first();
        $claim = $this->claimTable->get($claim_id);
        $claim->synced = null;
        $claim->claim_phase_id = $createdClaimPhase->id;
        $this->claimTable->save($claim);
    }
}
