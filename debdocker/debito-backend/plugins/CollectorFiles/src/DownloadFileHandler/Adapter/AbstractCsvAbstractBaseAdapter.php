<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

use Cake\ORM\TableRegistry;
use League\Flysystem\MountManager;
use WyriHaximus\FlyPie\FilesystemsTrait;

abstract class AbstractCsvAbstractBaseAdapter extends AbstractBaseAdapter
{
    use FilesystemsTrait;

    protected const DESTINATION_FILESYSTEM_NAME = '';
    protected const DESTINATION_FILESYSTEM_DIR = '/';
    public $mountManager;
    /**
     * @var \League\Flysystem\AdapterInterface
     */
    protected $casesFileSystem;
    protected $claimTable;
    protected $claimActionTypeTable;
    protected $claimActionTable;
    protected $collectorTable;
    protected $collectorFileTable;

    public function __construct()
    {
        // from https://csv.thephpleague.com/9.0/connections - important only on macs
        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        $this->casesFileSystem = $this->filesystem('internal_case_files');

        $this->mountManager = new MountManager([
            'destination' => $this->filesystem($this::DESTINATION_FILESYSTEM_NAME),
        ]);

        $this->claimTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimActionTypeTable = TableRegistry::getTableLocator()->get('ClaimActionTypes');
        $this->claimActionTable = TableRegistry::getTableLocator()->get('ClaimActions');
        $this->collectorTable = TableRegistry::getTableLocator()->get('Collectors');
        $this->collectorFileTable = TableRegistry::getTableLocator()->get('CollectorFiles');
    }

    public function downloadClaimReceipts()
    {
        $this->getReceiptFile();
    }

    abstract protected function getReceiptFile();

    public function downloadClaimStatus()
    {
        $this->getStatusFile();
    }

    abstract protected function getStatusFile();

    public function downloadClaimFinancials()
    {
        $this->getFinancialFile();
    }

    abstract protected function getFinancialFile();

    public function downloadClaiminbs()
    {
        $this->getInbFile();
    }

    abstract protected function getInbFile();

    /**
     * @return array multidimension array of files
     * @description list files and folders in a given folder
     */

    protected function getFilesList()
    {
        return $this->mountManager->listContents('destination://' . $this::DESTINATION_FILESYSTEM_DIR, false);
    }

    /**
     * @param string $baseName of file on collectors FTP - e.g, Sager20180626.csv
     * @return array Array of contents
     * @description
     */

    protected function getFileContents(string $baseName)
    {
        return $this->mountManager->read('destination://' . $this::DESTINATION_FILESYSTEM_DIR . $baseName, false);
    }

    /**
     * @param string $customerReference of claim
     * @param string $collectorReference of claim
     * @return bool
     * @description Update Claim to set phase and Customer Reference
     */

    protected function updateClaim(string $customerReference, string $collectorReference)
    {
        if ($customerReference && $customerReference !== "") {
            $claim = $this->claimTable->find(
                "all",
                ['conditions' => ['customer_reference' => $customerReference]]
            )->first();
            if ($claim) {
                if ($claim->claim_phase_id != $this->getClaimPhase('debt_collection')
                    && $claim->claim_phase_id != $this->getClaimPhase('ended')
                    && $claim->claim_phase_id != $this->getClaimPhase('reminder')
                ) {
                    $claim->claim_phase_id = $this->getClaimPhase('reminder');
                    $claim->collector_reference = $collectorReference;
                    $save = $this->claimTable->save($claim);
                } else {
                    echo "Claim Phase for " . $claim->customer_reference . " was already in advance stage" . PHP_EOL;
                }

                return true;
            }
        } else {
            if ($collectorReference) {
                $claim = $this->claimTable->find(
                    "all",
                    ['conditions' => ['collector_reference' => $collectorReference]]
                )->first();
                if ($claim) {
                    if ($claim->claim_phase_id != $this->getClaimPhase('debt_collection')
                        && $claim->claim_phase_id != $this->getClaimPhase('ended')
                        && $claim->claim_phase_id != $this->getClaimPhase('reminder')
                    ) {
                        $claim->claim_phase_id = $this->getClaimPhase('reminder');
                        $save = $this->claimTable->save($claim);
                    } else {
                        echo "Claim Phase for " . $claim->customer_reference . " was already in advance stage" . PHP_EOL;
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $baseName
     * @return bool
     * @description Move file to savebox
     */

    protected function moveFileToSaveBox(string $baseName)
    {
        // if file already exists remove it, so that we can overwrite
        $source = 'destination://' . $this::DESTINATION_FILESYSTEM_DIR . $baseName;
        $destination = 'destination://' . $this::DESTINATION_FILESYSTEM_DIR . "savebox//" . $baseName;
        if ($this->mountManager->has($destination)) {
            $this->mountManager->delete($destination);
        }
        $moveFile = $this->mountManager->move($source, $destination);

        return $moveFile;
    }

    /**
     * @param string $actionType
     * @param string $customerReference
     * @param string $collectorReference
     * @param string $date
     * @param int $paid
     * @param int $interest_added
     * @param int $debt_collection_fees
     * @return bool
     * @description Update claim phase, add new action type (if required), add new status for the claim
     */

    protected function updateClaimAndClaimActions(
        string $actionType,
        $customerReference,
        $collectorReference,
        string $date,
        int $paid,
        int $interest_added,
        int $debt_collection_fees
    ) {
        $claim = null;
        if ($actionType == "" || $actionType === null || (($customerReference == "" || $customerReference === null) && ($collectorReference == "" || $collectorReference === null)) || $date == "" || $date === null) {
            echo "Incomplete Values " . PHP_EOL;

            return;
        }

        //search action type of claim in action Types array
        $actionTypeSearch = $this->claimActionTypeTable->find(
            'all',
            ['conditions' => ['collector_identifier' => $actionType]]
        )->first();

        // if customer reference is not null then search claim from that else search it using collector reference

        if ($customerReference !== null && $customerReference !== "") {
            // get the claim against a customer reference
            $claim = $this->claimTable->find(
                "all",
                ['conditions' => ['customer_reference' => $customerReference]]
            )->first();
        } else {
            if ($collectorReference !== null && $collectorReference !== "") {
                // get the claim against a collector reference
                $claim = $this->claimTable->find(
                    "all",
                    ['conditions' => ['collector_reference' => $collectorReference]]
                )->first();
            }
        }

        //if no claim found against the claim customer reference then continue
        if (!$claim) {
            $this->log("No Claim found against " . $customerReference . "with collector_reference;" . $collectorReference);

            return false;
        }

        //if action for date and claim already exits, dont overwrite and exit
        $claimActionDate = $this->claimActionTable->find(
            'all',
            ['conditions' => ['claim_id' => $claim->id, 'date' => $date]]
        )->first();
        if ($claimActionDate) {
            echo "Action already exists for " . $customerReference . "with collector_reference" . $collectorReference . " on date " . $date . PHP_EOL;

            return false;
        }

        //if last claim action was of same status then dont insert a new one, but just update it.
        if ($actionTypeSearch) {
            $claimActionStatus = $this->claimActionTable->find(
                'all',
                ['order' => ['date' => 'ASC'], 'conditions' => ['claim_id' => $claim->id]]
            )->last();
            if ($claimActionStatus && $claimActionStatus->claim_action_type_id == $actionTypeSearch->id) {
                $claimActionStatus->paid = $paid;
                $claimActionStatus->interest_added = $interest_added;
                $claimActionStatus->debt_collection_fees = $debt_collection_fees;

                return $saveClaimAction = $this->claimActionTable->save($claimActionStatus);
            }
        }

        //if value does not exists in action tables then insert it
        $actionTypeId = 0; // initialize ID
        if (!$actionTypeSearch) {
            $newClaimActionType = $this->claimActionTypeTable->newEntity();
            $newClaimActionType->collector_identifier = $actionType;
            $newClaimActionType->identifier = $actionType;
            $newClaimActionType->name = $actionType;
            $newClaimActionType->collector_id = $this->getCollectorInfo()->id;
            $newClaimActionType->is_public = 0;
            $save = $this->claimActionTypeTable->save($newClaimActionType);
            $actionTypeId = $save->id;
            echo "Added a new Claim Action Type for " . $actionType . PHP_EOL;
        } else {
            $actionTypeId = $actionTypeSearch->id;
            //update claim status if phase is present for a particular claim action type
            if ($actionTypeSearch->claim_phase_id) {
                //update claim and set phase to match phase of claim phase order
                $claim->claim_phase_id = $actionTypeSearch->claim_phase_id;
                $saveClaim = $this->claimTable->save($claim);
                echo "Update the Claim status to new phase representing " . $actionType . PHP_EOL;
            }
        }
        //insert status into claim actions
        $newClaimAction = $this->claimActionTable->newEntity();
        $newClaimAction->claim_id = $claim->id;
        $newClaimAction->claim_action_type_id = $actionTypeId;
        $newClaimAction->date = $date;
        $newClaimAction->paid = $paid;
        $newClaimAction->interest_added = $interest_added;
        $newClaimAction->debt_collection_fees = $debt_collection_fees;
        $saveClaimAction = $this->claimActionTable->save($newClaimAction);
        if ($saveClaimAction) {
            echo "Added a new Claim Phase to match phase of " . $actionType . " for claim " . $customerReference . "with collector_reference" . $collectorReference . PHP_EOL;
        }

        return $saveClaimAction;
    }

    /**
     * @return object of collector
     * @description Get information of collector
     */

    protected function getCollectorInfo()
    {
        $collector = $this->collectorTable->find(
            "all",
            ['conditions' => ['identifier' => $this::DESTINATION_FILESYSTEM_NAME]]
        )->first();

        return $collector;
    }

    /**
     * @param float $value
     * @return int value in danish
     * @description Convert value to danish e.g. 1000.5 to 100050
     */

    protected function convertToDanishCurrency($value)
    {
        if (strpos($value, '.') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, "."), 1)) == 1) {
                $float = number_format((float)$value, 2, '.', '');
                $integer = str_replace('.', '', $float);

                return abs($integer);
            }
            $integer = str_replace('.', '', $value);

            return abs($integer);
        }
        if (strpos($value, ',') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, ","), 1)) == 1) {
                $float = number_format((float)$value, 2, ',', '');
                $integer = str_replace(',', '', $float);

                return abs($integer);
            }
            $integer = str_replace(',', '', $value);

            return abs($integer);
        }
        $float = number_format((float)$value, 2, '.', '');
        $integer = str_replace('.', '', $float);

        return abs($integer);
    }

    protected function updateClaimActionFinancials(
        $collector_reference,
        $date,
        $paid,
        $interest_added,
        $debt_collection,
        $actual_paid,
        string $fileName
    ) {
        //Get Collector reference
        $claim = $this->claimTable->find(
            'all',
            ['conditions' => ['collector_reference' => $collector_reference]]
        )->first();

        if (!$claim) {
            $this->log("Cound not find claim " . $collector_reference);

            return false;
        }

        $claimAction = $this->claimActionTable->find(
            'all',
            ['conditions' => ['claim_id' => $claim->id, 'date' => $date]]
        )->first();

        //claim not found for that date, add a new action for the date coming in
        if (!$claimAction) {
            $actionTypeSearch = $this->claimActionTypeTable->find(
                'all',
                ['conditions' => ['collector_identifier' => "**"]]
            )->first();
            $newClaimAction = $this->claimActionTable->newEntity();
            $newClaimAction->claim_id = $claim->id;
            $newClaimAction->claim_action_type_id = $actionTypeSearch->id;
            $newClaimAction->date = $date;
            $newClaimAction->collector_file_id = $this->addCollectorfile($fileName)->id;
            $newClaimAction->paid = $paid ? $paid : 0;
            $newClaimAction->interest_added = $interest_added ? $interest_added : 0;
            $newClaimAction->debt_collection_fees = $debt_collection ? $debt_collection : 0;
            $newClaimAction->actual_paid = $actual_paid ? $actual_paid : 0;
            $saveClaimAction = $this->claimActionTable->save($newClaimAction);
            $this->log("Added a new claim action for financial for " . $collector_reference . " on date " . $date);

            return true;
        }
        $claimAction->collector_file_id = $this->addCollectorfile($fileName)->id;
        $claimAction->paid = $paid ? $paid : $claimAction->paid;
        $claimAction->interest_added = $interest_added ? $interest_added : $claimAction->interest_added;
        $claimAction->debt_collection_fees = $debt_collection ? $debt_collection : $claimAction->debt_collection_fees;
        $claimAction->actual_paid = $actual_paid ? $actual_paid : $claimAction->actual_paid;
        if ($this->claimActionTable->save($claimAction)) {
            $this->log("Claim action update for " . $claim->customer_reference . " on date " . $date);
        } else {
            $this->log("Failed to update action for " . $collector_reference);
        }
    }

    private function addCollectorfile($fileName)
    {
        $collector_file = $this->collectorFileTable->find(
            'all',
            ['conditions' => ['name' => $fileName]]
        )->first();

        if ($collector_file) {
            return $collector_file;
        }

        $newCollectorFile = $this->collectorFileTable->newEntity();
        $newCollectorFile->name = $fileName;
        $newCollectorFile->extension = ".csv";
        $newCollectorFile->size = 1240;
        $newCollectorFile->dir = "savebox";
        $newCollectorFile->mime_type = "text/csv";
        $newCollectorFile->collector_file_type_id = "5bae97d0-352e-4379-9242-42771b7fea48";

        return $this->collectorFileTable->save($newCollectorFile);
    }

    protected function compareFileStatusDates($file_object, $date_column)
    {
        $file_name = $file_object['basename'];
        $str = explode(".", $file_name);
        $str = explode("_", $str[0]);
        $str = array_reverse($str);
        $date_file = date_create_from_format("U", (string)$file_object['timestamp']);
        $date_column = date_create_from_format("Y/m/d H:i:s", $date_column);
        if ($date_file > $date_column) {
            return true;
        }

        return false;
    }
}
