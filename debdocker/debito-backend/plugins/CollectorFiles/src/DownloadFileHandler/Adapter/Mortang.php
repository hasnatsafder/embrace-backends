<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

class Mortang extends AbstractCsvAbstractBaseAdapter
{
    protected const DESTINATION_FILESYSTEM_DIR = '/';
    protected const DESTINATION_FILESYSTEM_NAME = 'mortang';

    /**
     * @return bool
     */
    protected function getReceiptFile()
    {
        $contentsList = $this->getFilesList();
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                // if reciept file - starts with Sager
                if (substr($object['basename'], 0, 5) === "Sager") {
                    echo "-------------------------------" . PHP_EOL;
                    echo "Wokring on file " . $object['basename'] . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $contentsArray = array_map("str_getcsv", explode("\n", $contents));
                    foreach ($contentsArray as $data) {
                        // if data in third and ignore row dentoed by "sags_reference" heading
                        if (array_key_exists(0, $data) && $data[0] != "jnr" && $data[0] != "") {
                            $customer_reference = array_key_exists(1, $data) ? $data[1] : "";
                            $customer_reference = utf8_encode($customer_reference);
                            $customer_reference = str_replace(".", "-", $customer_reference);
                            $collector_reference = $data[0]; //get first 8 digits of third string
                            //update Claim status
                            if ($this->updateClaim($customer_reference, $collector_reference)) {
                                echo 'Reciept Recieved for Claim ' . $customer_reference . " with collector refenece" . $collector_reference . PHP_EOL;
                            } else {
                                $this->log('Claim not found for ' . $customer_reference . " with collector refenece " . $collector_reference . PHP_EOL);
                                echo 'Claim not found for ' . $customer_reference . " with collector refenece " . $collector_reference . PHP_EOL;
                            }
                        } // end if key exists
                    } // end for each inner
                    //move file to saveBox
                    if ($this->moveFileToSaveBox($object['basename'])) {
                        echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    } else {
                        echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    }
                } // end if file is of recepts
            } // end if type is file
        } // end for each outer
    }

    protected function getStatusFile()
    {
        $contentsList = $this->getFilesList();
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                if (substr($object['basename'], 0, 6) === "Status" && substr($object['basename'], 0, 7) !== "StatusL") {
                    echo "-------------------------------" . PHP_EOL;
                    echo "Wokring on file " . $object['basename'] . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $contentsArray = array_map("str_getcsv", explode("\n", $contents));
                    foreach ($contentsArray as $contentKey => $contentData) {
                        // if first row or empty row
                        if (array_key_exists('0', $contentData) && $contentData[0] != "jnr" && $contentData[0] != "") {
                            $contentsArrayInner = array_map("str_getcsv", explode(";", $contentData[0]));
                            //getting values for claim customer reference an the status of claim
                            $actionType = utf8_encode(rtrim($contentData[2])); // action type is in 3rd column // enocode to UTF-8
                            $collectorReference = trim($contentData[0]); // collector reference is in 1st column
                            $customerReference = null; //customer reference is not in mortang status file
                            $comingDate = strtotime(substr($object['basename'], 6, 8));
                            $claimDate = date('Y-m-d', $comingDate);
                            $paid = array_key_exists(6, $contentData) ? $this->convertToDanishCurrency(trim($contentData[6])) : 0; // paid is in 7st column-G
                            $interest_added = array_key_exists(5,
                                $contentData) ? $this->convertToDanishCurrency(trim($contentData[5])) : 0; // Interest added is in 6th column-F
                            $debt_collection_fees = array_key_exists(4,
                                $contentData) ? $this->convertToDanishCurrency(trim($contentData[4])) : 0; // Debt Collection Fees is in 5th column-E

                            //Update Claim Phase, Add a claim action type if not found, add claim action for the claim
                            $saveClaim = $this->updateClaimAndClaimActions(
                                $actionType,
                                $customerReference,
                                $collectorReference,
                                $claimDate,
                                (int)$paid,
                                (int)$interest_added,
                                (int)$debt_collection_fees
                            );
                        }
                    }
                    //move file to saveBox
                    if ($this->moveFileToSaveBox($object['basename'])) {
                        echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    } else {
                        echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    }
                }
            }
        }
    }

    /**
     * @return bool
     * @description Download and process financial files from Collectors Server
     */

    protected function getFinancialFile()
    {
        echo "No Financial file for mortang yet";
    }

    protected function getInbFile()
    {
        echo "No Financial file for mortang yet";
    }
}
