<?php
declare(strict_types=1);

namespace CollectorFiles\DownloadFileHandler\Adapter;

class Lindorff extends AbstractCsvAbstractBaseAdapter
{
    protected const DESTINATION_FILESYSTEM_DIR = '/outbox/';
    protected const DESTINATION_FILESYSTEM_NAME = 'lindorff';

    /**
     * @return bool
     */
    protected function getReceiptFile()
    {
        $contentsList = $this->getFilesList();
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                // if reciept file - starts with Sager
                if (substr($object['basename'], 0, 6) === "RPT0DB") {
                    echo "-------------------------------" . PHP_EOL;
                    echo "Wokring on file " . $object['basename'] . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $lines = preg_split('/\r\n|\n|\r/', $contents);
                    foreach ($lines as $line) {
                        if (!$line) {
                            continue;
                        }
                        $parts = preg_split('/\s+/', $line);
                        $customer_reference = $parts[0];
                        $collector_reference = substr($parts[2], 0, 8); //get first 8 digits of third string
                        // if data in customer reference
                        if ($customer_reference) {
                            //update Claim status
                            if ($this->updateClaim($customer_reference, $collector_reference)) {
                                echo 'Reciept Recieved for Claim ' . $customer_reference . PHP_EOL;
                            } else {
                                echo 'Claim not found for ' . $customer_reference . PHP_EOL;
                            }
                        } // end if key exists
                    } // end for each inner
                    //move file to saveBox
                    // if ($this->moveFileToSaveBox($object['basename'])) {
                    //     echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    // } else {
                    //     echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    // }
                } // end if file is of recepts
            } // end if type is file
        } // end for each outer
    }

    protected function getStatusFile()
    {
        $contentsList = $this->getFilesList();
        $limit = 0;
        $start = 0;
        $end = 5;
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                if (strpos($object['basename'], 'Seneste_handlinger_paa_sager_per_') !== false) {
                    // if($start > $limit){
                    //   $limit++;
                    //   continue;
                    // }
                    // $start++;
                    //   if($start >= $end){
                    //     echo "Start is " . $start . " and end is " .  $end . " exiting at file " . $object['basename'] . PHP_EOL;
                    //     exit;
                    //   }
                    $this->log("Wokring on file " . $object['basename']);
                    echo "-------------------------------" . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $contentsArray = array_map("str_getcsv", explode("\n", $contents));
                    foreach ($contentsArray as $contentKey => $contentData) {
                        if ($contentKey == 0 || !$contentData[0]) { // first row of lindroff is column header, we should ignroe that
                            continue;
                        }
                        $contentsArrayInner = array_map("str_getcsv", explode(";", $contentData[0]));
                        //getting values for claim customer reference an the status of claim
                        $actionType = utf8_encode(rtrim($contentsArrayInner[3][0])); // action type is in 3rd column // enocode to UTF-8
                        $customerReference = trim($contentsArrayInner[1][0]); // Custoemr Refernece is in 1st column
                        $collectorReference = trim($contentsArrayInner[0][0]); // Custoemr Refernece is in 0th column
                        $claimDate = rtrim($contentsArrayInner[2][0]); // Date is in 2nd column
                        $paid = 0; // No info present at lindorff at the moment
                        $interest_added = 0; // No info present at lindorff at the moment
                        $debt_collection_fees = 0; // No info present at lindorff at the moment

                        //compare dates, if file date is smaller than claim date then do not add a new status
                        if (!$this->compareFileStatusDates($object, $claimDate)) {
                            echo "Date Greater than File date" . PHP_EOL;
                            continue;
                        }
                        //Update Claim Phase, Add a claim action type if not found, add claim action for the claim
                        $saveClaim = $this->updateClaimAndClaimActions(
                            $actionType,
                            $customerReference,
                            $collectorReference,
                            $claimDate,
                            $paid,
                            $interest_added,
                            $debt_collection_fees
                        );
                    }
                    //move file to saveBox
                    // if ($this->moveFileToSaveBox($object['basename'])) {
                    //     echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    // } else {
                    //     echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    // }
                }
            }
        }
    }

    /**
     * @return bool
     * @description Download and process financial files from Collectors Server
     */

    protected function getFinancialFile()
    {
        $headerArray = "";
        $bodyArray = [];
        $financeArray = [];
        $contentsList = $this->getFilesList();
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                if (substr($object['basename'], 0, 7) === "Debito_") {
                    echo "-------------------------------" . PHP_EOL;
                    echo "Wokring on file " . $object['basename'] . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $contentsArray = array_map("str_getcsv", explode("\n", $contents));
                    foreach ($contentsArray as $contentKey => $contentData) {
                        // if empty row of csv file
                        if (!$contentData[0]) {
                            continue;
                        } // if header of file
                        else {
                            if ($contentKey === 0) { // first row of lindroff is column header
                                $header = $contentData[0];
                                $headerArray = array_map("str_getcsv", explode(";", $contentData[0]));
                                continue;
                            } //if body
                            else {
                                if ($contentKey != 0) {
                                    $bodyArray = array_map("str_getcsv", explode(";", $contentData[0]));
                                    //for each value in body
                                    for ($i = 0; $i < sizeof($headerArray); $i++) {
                                        $financeArray[$headerArray[$i][0]] = $bodyArray[$i][0];
                                    }
                                    $this->updateClaimActionFinancials(
                                        $financeArray['LD_case_no'],
                                        $financeArray['Registration_date'],
                                        $this->convertToDanishCurrency($financeArray['Payment_increment']),
                                        $this->convertToDanishCurrency($financeArray['Remaining_interest']),
                                        null,
                                        null,
                                        $object['basename']
                                    );
                                }
                            }
                        }
                    }
                    //move file to saveBox
                    // if ($this->moveFileToSaveBox($object['basename'])) {
                    //   echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    // } else {
                    //   echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    // }
                }
            }
        }
    }

    /**
     * @return bool
     * @description Download and process financial files from Collectors Server
     */

    protected function getInbFile()
    {
        $headerArray = "";
        $bodyArray = [];
        $financeArray = [];
        $contentsList = $this->getFilesList();
        foreach ($contentsList as $object) {
            // if file type is file
            if ($object['type'] == 'file') {
                if (substr($object['basename'], 0, 3) === "INB") {
                    echo "-------------------------------" . PHP_EOL;
                    echo "Wokring on file " . $object['basename'] . PHP_EOL;
                    $contents = $this->getFileContents($object['basename']);
                    $contentsArray = array_map("str_getcsv", explode("\n", $contents));
                    foreach ($contentsArray as $contentKey => $contentData) {
                        // if empty row of csv file
                        if (!$contentData[0]) {
                            continue;
                        } // if header of file
                        else {
                            if ($contentKey === 0) { // first row of lindroff is column header
                                $header = $contentData[0];
                                $headerArray = array_map("str_getcsv", explode(";", $contentData[0]));
                                continue;
                            } //if body
                            else {
                                if ($contentKey != 0) {
                                    $whole_array = implode(',', $contentData);
                                    $bodyArray = array_map("str_getcsv", explode(";", $whole_array));
                                    //for each value in body
                                    for ($i = 0; $i < sizeof($headerArray); $i++) {
                                        $financeArray[$headerArray[$i][0]] = $bodyArray[$i][0];
                                    }
                                    $paid = $this->convertToDanishCurrency($financeArray['Brutto indbet.']);
                                    $interest_added = null;
                                    $date = date_create_from_format("d.m.Y", $financeArray['Indb.dato']);
                                    $debt_collection_fees = $this->convertToDanishCurrency($financeArray['Indb. Ikke M.pligt. Omk/geb']) + $this->convertToDanishCurrency($financeArray['Indb. M.pligt. Omk/geb'])
                                        + $this->convertToDanishCurrency($financeArray['Moms omk/geb']) + $this->convertToDanishCurrency($financeArray['Provision']) + $this->convertToDanishCurrency($financeArray['Moms prov.']);
                                    $actual_paid = $this->convertToDanishCurrency($financeArray['Udbet. Kred']);
                                    $this->updateClaimActionFinancials(
                                        $financeArray['Sagsnr'],
                                        date_format($date, "Y-m-d"),
                                        $paid,
                                        $interest_added,
                                        $debt_collection_fees,
                                        $actual_paid,
                                        $object['basename']
                                    );
                                }
                            }
                        }
                    }
                    //move file to saveBox
                    // if ($this->moveFileToSaveBox($object['basename'])) {
                    //   echo 'File ' . $object['basename'] . " moved to savebox" . PHP_EOL;
                    // } else {
                    //   echo "Unable to Move file " . $object['basename'] . " to savebox" . PHP_EOL;
                    // }
                }
            }
        }
    }
}
