<?php

declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use App\Model\Entity\Claim;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Psr\Log\LogLevel;
use RuntimeException;
use Cake\Collection\CollectionInterface;
use Cake\Core\Configure;

class Collectia extends AbstractJsonApiAbstractBaseAdapter
{
    protected const COLLECTOR_NAME = "Collectia";

    /**
     * Overwrite upload claims since we need to send one call per each claim line
     *
     * @param CollectionInterface $claims
     * @return boolean
     */
    public function uploadClaims(CollectionInterface $claims)
    {
        if ($claims->isEmpty()) {
            throw new RuntimeException('No claims to uploader...');
        }
        $claimPhaseOrderSent = $this->claimPhaseTable->find(
            'all',
            ['conditions' => ["identifier" => 'case_sent']]
        )->first();
        foreach ($claims as $claim) {
            $sendData = $this->buildJsonData($claim);
            // if successfully uploaded all claims
            if ($sendData) {
                //Moved Claim to phase 3
                $claim->claim_phase_id = $claimPhaseOrderSent->id;
                $claim->synced = date("Y-m-d H:i:s");
                $save = $this->claimsTable->save($claim);
                $this->log(
                    sprintf(
                        'Claim Successfully sent to Collectia ' . $claim->customer_reference
                    ),
                    LogLevel::INFO
                );
            }
        }
    }

    protected function buildJsonData(Claim $claim)
    {
        $data = [];
        $account = $this->accountsTable->get($claim->account_id);
        $debtor = $this->debtorsTable->get($claim->debtor_id);
        if($debtor->country_id) {
            $debtorCountry = $this->countriesTable->get($debtor->country_id);
        }
        else {
            $rejectedPhase = $this->getClaimPhase('rejected');
            $claim->claim_phase_id = $rejectedPhase;
            $claim->rejected_reason = "Debtor Country not found - for case " . $claim->customer_refernce;
            $claim->completed = null;
            $this->claimsTable->save($claim);

            return false;
        }
        if($account->country_id) {
            $accountCountry = $this->countriesTable->get($account->country_id);
        }
        else {
            $rejectedPhase = $this->getClaimPhase('rejected');
            $claim->claim_phase_id = $rejectedPhase;
            $claim->rejected_reason = "Account Country not found - for case " . $claim->customer_refernce;
            $claim->completed = null;
            $this->claimsTable->save($claim);

            return false;
        }

        // Enter Caseinfo
        $data['caseInfo']['reference'] = $claim->customer_reference;
        $data['caseInfo']['debtorName'] = $debtor->company_name ? $debtor->company_name : ($debtor->first_name . " " . $debtor->last_name);
        $data['caseInfo']['debtorName'] = substr($data['caseInfo']['debtorName'], 0, 50);
        $data['caseInfo']['debtorAddress'] = substr($debtor->address, 0, 100);
        $data['caseInfo']['debtorAddress2'] = "";
        $data['caseInfo']['debtorZipCode'] = $debtor->zip_code;
        $data['caseInfo']['debtorCity'] = $debtor->city;
        $data['caseInfo']['debtorCountry'] = $debtorCountry->iso_code;
        $data['caseInfo']['debtorCpr'] = "";
        $data['caseInfo']['debtorCvr'] = $debtor->vat_number;
        $data['caseInfo']['debtorPhoneNumber'] = $debtor->phone ? trim($debtor->phone) : "";
        $data['caseInfo']['debtorPhoneNumber2'] = "";
        $data['caseInfo']['debtorEmail'] = $debtor->email ? trim($debtor->email) : $this->getUserEmail($account->created_by_id);
        $data['caseInfo']['debtorObjection'] = $claim->dispute ? true : false;

        // Enter Client info
        $data['clientInfo']['name'] = utf8_encode(substr($account->name, 0, 100));
        $data['clientInfo']['address'] = utf8_encode(substr($account->address, 0, 100));
        $data['clientInfo']['poBox'] = "";
        $data['clientInfo']['postCode'] = $account->zip_code;
        $data['clientInfo']['city'] = $account->city;
        $data['clientInfo']['contact'] = utf8_encode(substr($account->name, 0, 30));
        $data['clientInfo']['name'] = utf8_encode($account->name);
        $data['clientInfo']['validationMail'] = $account->email ? trim($account->email) : $this->getUserEmail($account->created_by_id);
        $data['clientInfo']['validationCCMail'] = "";
        $data['clientInfo']['clientPhone'] = $account->phone ? trim($account->phone) : "";
        $data['clientInfo']['clientCVR'] = $account->vat_number ? $account->vat_number : explode("-",
        $claim->customer_reference)[0]; // send first letters fo customer reference
        $data['clientInfo']['taxRegistered'] = $account->vat_number ? true : false;
        // in case of international (non: DK accounts, set to IBAN and Swtift code)
        if($accountCountry->iso_code !== "DK"){
            $data['clientInfo']['clientRegNr'] = "SWIFT: " . $account->swift_code;
            $data['clientInfo']['clientKontoNr'] = "IBAN:" . $account->iban;
        }
        else{ // use registeration and account number
            $data['clientInfo']['clientRegNr'] = $account->bank_reg_number;
            $data['clientInfo']['clientKontoNr'] = $account->bank_account_number;
        }

        // get claim lines, who have not been synced
        $claimLines = $this->claimLineTable->find(
            "all",
            ['conditions' => ['claim_id' => $claim->id, 'synced' => 0]]
        )
            ->contain(['ClaimLineTypes']);
        // if status of this does not change, we will move claim to next phase.
        $claimSuccessful = true;
        foreach ($claimLines as $claimLine) {
            $data['caseInfo']['invoiceNumber'] = $claimLine->invoice_reference ? utf8_encode(substr($claimLine->invoice_reference, 0, 20)) : ""; // 20 is collectia limit
            $data['caseInfo']['invoiceDate'] = $claimLine->invoice_date ? date_format($claimLine->invoice_date, 'Y-m-d') : date('Y-m-d');
            $data['caseInfo']['invoiceDueDate'] = $claimLine->maturity_date ? date_format($claimLine->maturity_date,
                'Y-m-d') : $data['caseInfo']['invoiceDate']; // if not found, same as invoice date
            $data['caseInfo']['invoiceAmount'] = 0.0; // intitalize
            $data['caseInfo']['invoiceInterest'] = 0.0; // intitalize
            $data['caseInfo']['invoiceFee'] = 0.0; // intitalize
            $data['caseInfo']['invoiceChargeAmount'] = 0.0; // intitalize
            $data['caseInfo']['invoiceText'] = 0.0; // intitalize
            $data['caseInfo']['invoicePaidAmount'] = 0.0; // intitalize
            $data['caseInfo']['invoiceDocUrl'] = Configure::read('App.fullBaseUrl') . "/" . $claimLine->file_dir . $claimLine->file_name;

            if ($claimLine->amount) {
                if ($claimLine->claim_line_type->identifier == "invoice") {
                    $data['caseInfo']['invoiceAmount'] = substr_replace($claimLine->amount, ".", -2, 0);
                } else {
                    if ($claimLine->claim_line_type->identifier == "payment" || $claimLine->claim_line_type->identifier == "credit_note") {
                        $data['caseInfo']['invoicePaidAmount'] = substr_replace($claimLine->amount, ".", -2, 0);
                    } else {
                        if ($claimLine->claim_line_type->identifier == "late_payment_fee") {
                            $data['caseInfo']['invoiceFee'] = substr_replace($claimLine->amount, ".", -2, 0);
                            $data['caseInfo']['invoiceChargeAmount'] = substr_replace($claimLine->amount, ".", -2, 0);
                        }
                    }
                }
            }
            $headers = [
                "Authorization" => "Basic " . Configure::read($this::COLLECTOR_NAME . '.key'),
                "Content-Type" => "application/json",
            ];
            print_r($data);
            $createCase = $this->sendApiRequest($data, "createcase", $headers);

            if (!$createCase["created"]) {
                //return false and move claim to rejected with error
                $rejectedPhase = $this->getClaimPhase('rejected');
                $claim->claim_phase_id = $rejectedPhase;
                $claim->rejected_reason = "Error during upload - " . $createCase["response"];
                $claim->completed = null;
                $this->claimsTable->save($claim);

                return false;
            } else {
                $claimLine->synced = 1;
                $this->claimLineTable->save($claimLine);
            }
        }

        return true;
    }

    protected function getValidator(): Validator
    {
        $validator = new Validator();

        $validator->requirePresence('accounts-collector')
            ->requirePresence('vat_number');

        return $validator;
    }
}
