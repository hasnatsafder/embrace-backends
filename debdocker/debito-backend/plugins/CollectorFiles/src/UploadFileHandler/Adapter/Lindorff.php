<?php
declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler\Adapter;

use App\Model\Entity\Claim;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use League\Csv\RFC4180Field;
use League\Csv\Writer;

class Lindorff extends AbstractCsvAbstractBaseAdapter
{

    protected const CSV_HEADERS = [
        'customer_number',
        'name',
        'first_name',
        'last_name',
        'address',
        'address2',
        'address3',
        'zip',
        'city',
        'country_code',
        'phone_number1',
        'phone_number2',
        'cvr',
        'cpr',
        'extra_description',
        'invoice_number',
        'invoice_amount',
        'remaining_principle',
        'invoice_date',
        'due_date',
        'compensation_fee',
        'minimum_settlement_value',
        'interest_rate',
        'company_name',
        'case_type',
    ];
    protected const DESTINATION_FILESYSTEM_DIR = '/inbox/';
    protected const DESTINATION_FILESYSTEM_NAME = 'lindorff';

    protected function setupCsvFileFromStream($fileStream)
    {
        $csvWriter = Writer::createFromStream($fileStream);

        //Handle RFC4180 compliance (https://csv.thephpleague.com/9.0/interoperability/rfc4180-field/)
        $csvWriter->setNewline("\r\n");
        RFC4180Field::addTo($csvWriter);

        $csvWriter
            //Prepare for MS Excel
            ->setOutputBOM(Writer::BOM_UTF8)
            ->setDelimiter(';');
        //->setEnclosure(' ')


        //$this->insertCsvHeaders($csvWriter); - BY Hasnat (7-13-2018), we dont need to add Headers to mrotang file
        return $csvWriter;
    }

    /**
     * @param \League\Csv\Writer $csvWriter
     * @param \App\Model\Entity\Claim $claim
     * @return bool
     * @throws \Exception
     */
    protected function insertCsvDataFromClaim(Writer $csvWriter, Claim $claim)
    {

        $claimTable = TableRegistry::getTableLocator()->get('Claims');
        $claimPhaseTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $debtorTable = TableRegistry::getTableLocator()->get('Debtors');
        $accountTable = TableRegistry::getTableLocator()->get('Accounts');
        $claimLineTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claimLineTypeTable = TableRegistry::getTableLocator()->get('ClaimLineTypes');
        $currencyTable = TableRegistry::getTableLocator()->get('Currencies');
        $accountCollectorTable = TableRegistry::getTableLocator()->get('AccountsCollectors');

        //evaluate collectors reference
        $accountsCollector = $accountCollectorTable->find(
            "all",
            ['conditions' => ['account_id' => $claim->account_id, 'collector_id' => $claim->collector_id]]
        )->first();

        $debtor = $debtorTable->get($claim->debtor_id);
        $account = $accountTable->get($claim->account_id);
        $claimLines = $claimLineTable->find(
            "all",
            ['conditions' => ['claim_id' => $claim->id]]
        );

        //valdiating data
        $data = ["account_collector" => $accountsCollector->collector_identifier, "vat_number" => $account->vat_number];
        $ca = $this->validate($data);
        if ($this->validate($data) !== true) {
            return false;
        }

        //evaluate debtors name
        $debtorName = $debtor->first_name . " " . $debtor->last_name;
        if (!empty($debtor->company_name)) {
            $debtorName = $debtor->company_name;
        }
        //Debtor Company or Private
        $debtorType = $debtor->is_company == 1 ? "F" : "P";

        foreach ($claimLines as $claimLine) {
            $curreny = $currencyTable->get($claimLine->currency_id);

            //evaluate claim line type
            $amount = $claimLine->amount;
            $costDeduction = "";
            $claimLineType = $claimLineTypeTable->find(
                "all",
                ['conditions' => ['id' => $claimLine->claim_line_type_id]]
            )->first();
            if ($claimLineType->name == "Rykkergebyr") {
                $amount = "";
                $costDeduction = $claimLine->amount;
            }

            $claimLineArray = [
                $accountsCollector->collector_identifier,
                // K-Number
                "P",
                // Type of Claim
                $claim->customer_reference,
                // Creditor's reference
                $debtorName,
                // Surname or full name
                "",
                // first name
                $debtor->address,
                // Addressline 1
                "",
                // Addressline 2
                $debtor->country_id,
                // Country Code
                $debtor->zip_code,
                //Debtor Zip Code
                $debtor->city,
                //Debtor City
                $debtor->vat_number,
                //Debtor VAT Number,
                "",
                // birthday
                $debtorType,
                // P or C for debtor
                $debtor->phone,
                // Debtor Phone
                "",
                // Debtor home home
                "",
                //Misc
                "",
                // Misc2
                date_format($claimLine->invoice_date, 'd.m.Y'),
                // Claim Invoice Date
                date_format($claimLine->maturity_date, 'd.m.Y'),
                // Claim Maturity Date
                "STD",
                // interest rate
                $amount,
                // Claim Line amount
                $claimLine->invoice_reference,
                // Invoice Reference on Invoice
                $costDeduction,
                // Cost Deduction
                "",
                //Customer interest rate
                $curreny->iso_code,
                // three digit code of currency
                $account->name . " " . $account->ean_number,
                // Account name and ean number of account
                $account->bank_reg_number . " " . $account->bank_account_number,
                // Bank Reg number and bank account number
                $account->email,
                // will be changed once email is added to accounts,
                "END",
            ];
            $csvWriter->insertOne($claimLineArray);
        }
        //Moved Claim to phase 3
        $claimPhaseOrderSent = $this->claimPhaseTable->find(
            'all',
            ['conditions' => ["identifier" => 'case_sent']]
        )->first();
        //set claim phase to order 3 and marked claim as synced to FTP
        $claim->claim_phase_id = $claimPhaseOrderSent->id;
        $claim->synced = date("Y-m-d H:i:s");
        $save = $claimTable->save($claim);

        if ($save) {
            return true;
        }

        return false;
    }

    protected function getValidator(): Validator
    {
        /**
         * 'customer_number',
         * 'name',
         * 'first_name',
         * 'last_name',
         * 'address',
         * 'address2',
         * 'address3',
         * 'zip',
         * 'city',
         * 'country_code',
         * 'phone_number1',
         * 'phone_number2',
         * 'cvr',
         * 'cpr',
         * 'extra_description',
         * 'invoice_number',
         * 'invoice_amount',
         * 'remaining_principle',
         * 'invoice_date',
         * 'due_date',
         * 'compensation_fee',
         * 'minimum_settlement_value',
         * 'interest_rate',
         * 'company_name',
         * 'case_type',
         */

        $validator = new Validator();

        $validator->requirePresence('account_collector')
            ->requirePresence('vat_number');

        return $validator;
    }
}
