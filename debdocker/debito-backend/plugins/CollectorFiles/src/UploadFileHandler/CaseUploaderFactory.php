<?php
declare(strict_types=1);

namespace CollectorFiles\UploadFileHandler;

use CollectorFiles\Exceptions\CollectorNotImplementedException;

class CaseUploaderFactory
{
    public static function build($collectorName)
    {
        $capitalizedCollectorIdentifier = ucfirst($collectorName);
        $className = __NAMESPACE__ . '\Adapter\\' . $capitalizedCollectorIdentifier;

        if (!class_exists($className)) {
            throw new CollectorNotImplementedException([$collectorName]);
        }

        return new $className();
    }
}
