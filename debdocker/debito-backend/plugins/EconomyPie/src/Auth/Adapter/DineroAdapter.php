<?php
declare(strict_types=1);

namespace EconomyPie\Auth\Adapter;

use EconomyPie\Auth\AbstractAdapter;
use Exception;
use GuzzleHttp\Client;
use Cake\Core\Configure;

class DineroAdapter extends AbstractAdapter
{
    private $http;
    private $authData;

    public function __construct(array $data)
    {
        parent::__construct($data);

        $this->authData = $data;
        $dinero_clientId = Configure::read('Dinero.client.id');
        $dinoro_clientSecret = Configure::read('Dinero.client.secret');
        $this->http = new Client([
            'base_uri' => 'https://authz.dinero.dk/',
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode("{$dinero_clientId}:{$dinoro_clientSecret}"),
            ],
        ]);
    }

    /**
     * @param string $accessToken
     * @return array
     * @throws \Exception
     */
    public function getOrganizations(string $accessToken = ''): array
    {
        if ($accessToken === '') {
            $accessToken = $this->getToken();
        }

        try {
            $clientBearerAuthorization = new Client([
                'base_uri' => 'https://api.dinero.dk/v1/',
                'headers' => ['Authorization' => 'Bearer ' . $accessToken],
            ]);

            $request = $clientBearerAuthorization->get('organizations?fields=id,name');
            $response = json_decode($request->getBody()->getContents(), true);
        } catch (Exception $e) {
            throw $e;
        }

        return !empty($response) ? $response : [];
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        if (empty($this->authData['api_key'])) {
            return null;
        }

        $oAuthActorsData = $this->http->post('dineroapi/oauth/token', [
            'form_params' => [
                'username' => $this->authData['api_key'],
                'password' => $this->authData['api_key'],
                'grant_type' => 'password',
                'scope' => 'read write',
            ],
        ]);

        $oAuthActorsData = json_decode($oAuthActorsData->getBody()->getContents(), true);
        if (empty($oAuthActorsData['access_token'])) {
            return null;
        }

        return $oAuthActorsData['access_token'];
    }
}
