<?php
declare(strict_types=1);

namespace EconomyPie\Auth\Adapter;

use EconomyPie\Auth\AbstractAdapter;
use Exception;
use GuzzleHttp\Client;

class BillyAdapter extends AbstractAdapter
{
    private $http;
    private $authData;

    public function __construct(array $data)
    {
        parent::__construct($data);

        $this->authData = $data;
        $this->http = new Client([
            'base_uri' => 'https://api.billysbilling.com/v2/',
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($data['email'] . ':' . $data['password']),
            ],
        ]);
    }

    /**
     * @param string $accessToken
     * @return array
     * @throws \Exception
     */
    public function getOrganizations(string $accessToken = ''): array
    {
        if ($accessToken === '') {
            $accessToken = $this->getToken();
        }

        try {
            $clientBearerAuthorization = new Client([
                'base_uri' => 'https://api.billysbilling.com',
                'headers' => ['Authorization' => 'Bearer ' . $accessToken],
            ]);

            $request = $clientBearerAuthorization->get('user/organizations');
            $response = json_decode($request->getBody()->getContents(), true);
        } catch (Exception $e) {
            throw $e;
        }

        return !empty($response['data']) ? $response['data'] : [];
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        if (empty($this->authData['email'] || $this->authData['password'])) {
            return null;
        }

        $oAuthActorsData = $this->http->post('user/login', [
            'json' => [
                'credentials' => [
                    'email' => $this->authData['email'],
                    'password' => $this->authData['password'],
                    'remember' => false,
                ],
            ],
        ]);

        $oAuthActorsData = json_decode($oAuthActorsData->getBody()->getContents(), true);

        if (empty($oAuthActorsData['meta']['accessToken']) || !$oAuthActorsData['meta']['success']) {
            return null;
        }

        return $oAuthActorsData['meta']['accessToken'];
    }
}
