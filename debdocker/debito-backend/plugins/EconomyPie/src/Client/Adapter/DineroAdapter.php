<?php
declare(strict_types=1);

namespace EconomyPie\Client\Adapter;

use EconomyPie\Client\ClientAdapterInterface;
use GuzzleHttp\Client;

class DineroAdapter implements ClientAdapterInterface
{
    public $http;

    public function __construct(array $data)
    {
        $headers = [];
        if (!empty($data['auth_token'])) {
            $headers = ['Authorization' => "Bearer " . $data['auth_token'],];
        }
        $baseUri = 'https://api.dinero.dk/v1/';
        // if org id sent then attach
        if (isset($data['org_id'])) {
            $baseUri .= $data['org_id'] . "/";
        }
        $this->http = new Client([
            'base_uri' => $baseUri,
            'headers' => $headers,
        ]);
    }

    public function getClient()
    {
        return $this->http;
    }
}
