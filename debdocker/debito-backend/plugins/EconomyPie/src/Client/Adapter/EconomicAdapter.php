<?php
declare(strict_types=1);

namespace EconomyPie\Client\Adapter;

use Cake\Core\Configure;
use EconomyPie\Client\ClientAdapterInterface;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;

class EconomicAdapter implements ClientAdapterInterface
{
    private $http;

    /**
     * Creates a connection to the desired ERP's API
     *
     * @param array $authData
     * @internal param $payment_type_number
     */
    public function __construct(array $authData)
    {
        $handlerStack = HandlerStack::create( new CurlHandler() );
        $handlerStack->push( Middleware::retry( $this->retryDecider(), $this->retryDelay() ) );
        $this->http = new Client([
            'handler' => $handlerStack,
            'base_uri' => 'https://restapi.e-conomic.com/',
            'headers' => [
                'AppID' => Configure::read('Integrations.erp.economic.tokens.secret'),
                'AccessID' => $authData['auth_token'],
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    public function getClient()
    {
        return $this->http;
    }
    private function retryDecider() {
        return function (
           $retries,
           Request $request,
           Response $response = null,
           RequestException $exception = null
        ) {
           // Limit the number of retries to 5
           if ( $retries >= 5 ) {
              return false;
           }
     
           // Retry connection exceptions
           if( $exception instanceof ConnectException || $exception instanceof ServerException) {
              return true;
           }
     
           if( $response ) {
              // Retry on server errors
              if( $response->getStatusCode() >= 500 ) {
                 return true;
              }
           }
     
           return false;
        };
     }

     private function retryDelay() {
        return function( $numberOfRetries ) {
            return 1000 * $numberOfRetries;
        };
    }
}
