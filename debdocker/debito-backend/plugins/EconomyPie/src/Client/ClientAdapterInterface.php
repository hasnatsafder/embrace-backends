<?php
declare(strict_types=1);

namespace EconomyPie\Client;

interface ClientAdapterInterface
{
    /**
     * Creates a connection to the desired ERP's API
     *
     * @param array $data
     * @internal param $payment_type_number
     */
    public function __construct(array $data);

    /**
     * Returns the right ErpClient
     *
     * @return mixed
     */
    public function getClient();
}
