<?php
declare(strict_types=1);

namespace EconomyPie\Client;

use GuzzleHttp\Client;

/**
 * Class AbstractAdapter
 *
 * @var Client
 */
abstract class AbstractAdapter
{
}
