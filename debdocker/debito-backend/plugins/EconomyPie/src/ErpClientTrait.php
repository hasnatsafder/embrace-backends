<?php
declare(strict_types=1);

namespace EconomyPie;

use EconomyPie\Client\ClientAdapterFactory;
use ReflectionClass;

/**
 * Class ErpClientTrait
 *
 */
trait ErpClientTrait
{
    /**
     * @var \EconomyPie\Client\ClientAdapterInterface
     */
    protected $erpClient;

    public function createErpClient($data): void
    {

        $className = (new ReflectionClass($this))->getShortName();
        $endOfAapterName = mb_strrpos($className, 'Adapter');
        $erpIdentifier = mb_strtolower(mb_substr($className, 0, $endOfAapterName));

        $clientFactory = new ClientAdapterFactory();

        $this->erpClient = $clientFactory->build($erpIdentifier, $data);
    }
}
