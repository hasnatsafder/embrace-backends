<?php
declare(strict_types=1);

namespace EconomyPie\Debtors;

use Cake\Log\LogTrait;
use Cake\Utility\Hash;
use EconomyPie\ErpClientTrait;

/**
 * Class AbstractAdapter
 *
 * @package EconomyPie\Invoices
 */
abstract class AbstractAdapter implements DebtorAdapterInterface
{
    public const VENDOR = null;

    use ErpClientTrait;
    use LogTrait;

    protected $propertyMap;

    public function __construct(array $authData)
    {
        if (!property_exists($this, 'erpClient') || $this->erpClient === null) {
            $this->createErpClient($authData);
        }
    }

    protected function _transformToDebtorDataObject(array $invoice): DebtorData
    {
        $data = [
            '_erp_integration' => $this::VENDOR,
        ];

        foreach ($this->propertyMap as $ownKey => $foreignKey) {
            $data[$ownKey] = Hash::get($invoice, $foreignKey);
        }

        return new DebtorData($data);
    }
}
