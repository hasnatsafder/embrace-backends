<?php
declare(strict_types=1);

namespace EconomyPie\Debtors;

interface DebtorAdapterInterface
{
    /**
     * @param string $contactNumber
     * @return \EconomyPie\Debtors\DebtorData
     */
    public function get(string $contactNumber): DebtorData;
}
