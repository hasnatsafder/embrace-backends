<?php
declare(strict_types=1);

namespace EconomyPie\Debtors\Adapter;

use Cake\Http\Exception\UnauthorizedException;
use EconomyPie;
use EconomyPie\Debtors\AbstractAdapter;
use EconomyPie\Debtors\DebtorData;
use GuzzleHttp\Exception\ClientException;

class BillyAdapter extends AbstractAdapter
{
    public const VENDOR = 'billy';
    private const BASE_ENDPOINT = 'contacts';

    protected $propertyMap = [
        'id' => 'id',
        'name' => 'name',
        'vatNumber' => 'registrationNo',
        'streetName' => 'street',
        'zipCode' => 'zipcodeText',
        'city' => 'cityText',
        'country' => 'countryId',
        'phone' => 'phone',
        'email' => 'contactPersons.0.email',
    ];

    /**
     * @param string $contactId
     * @return \EconomyPie\Debtors\DebtorData
     */
    public function get(string $contactId): DebtorData
    {
        try {
            $response = $this->erpClient->getClient()->get(
                static::BASE_ENDPOINT . '/' . $contactId . '?' .
                http_build_query([
                    'include' => 'contact.contactPersons:embed',
                ])
            );

            $contact = json_decode($response->getBody()->getContents(), true);

            return $this->_transformToDebtorDataObject($contact['contact']);
        } catch (ClientException $e) {
            throw new UnauthorizedException($e->getMessage());
        }
    }
}
