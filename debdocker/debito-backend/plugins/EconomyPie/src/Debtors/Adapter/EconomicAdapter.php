<?php
declare(strict_types=1);

namespace EconomyPie\Debtors\Adapter;

use Cake\Http\Exception\UnauthorizedException;
use EconomyPie;
use EconomyPie\Debtors\AbstractAdapter;
use EconomyPie\Debtors\DebtorData;
use GuzzleHttp\Exception\ClientException;

/**
 * Class EconomicAdapter
 *
 * @property \EconomyPie\Client\Adapter\EconomicAdapter $erpClient
 */
class EconomicAdapter extends AbstractAdapter
{
    public const VENDOR = 'economic';
    private const BASE_ENDPOINT = '/customers';

    protected $propertyMap = [
        'id' => 'customerNumber',
        'name' => 'name',
        'vatNumber' => 'corporateIdentificationNumber',
        'streetName' => 'address',
        'zipCode' => 'zip',
        'city' => 'city',
        'country' => 'country',
        'phone' => 'telephoneAndFaxNumber',
        'email' => 'email',
    ];

    /**
     * @param string $contactId
     * @return \EconomyPie\Debtors\DebtorData
     */
    public function get(string $contactId): DebtorData
    {
        try {
            $response = $this->erpClient->getClient()->get(static::BASE_ENDPOINT . '/' . $contactId);

            $debtor = json_decode($response->getBody()->getContents(), true);

            return $this->_transformToDebtorDataObject($debtor);
        } catch (ClientException $e) {
            throw new UnauthorizedException($e->getMessage());
        }
    }
}
