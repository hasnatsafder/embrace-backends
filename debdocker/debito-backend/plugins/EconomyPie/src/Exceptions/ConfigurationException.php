<?php
declare(strict_types=1);

namespace EconomyPie\Exceptions;

/**
 * {@inheritdoc}
 */
class ConfigurationException extends Exception
{
}
