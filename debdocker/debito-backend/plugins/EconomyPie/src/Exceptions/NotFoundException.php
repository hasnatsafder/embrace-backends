<?php
declare(strict_types=1);

namespace EconomyPie\Exceptions;

class NotFoundException extends Exception
{
}
