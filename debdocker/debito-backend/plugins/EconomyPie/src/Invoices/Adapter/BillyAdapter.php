<?php
declare(strict_types=1);

namespace EconomyPie\Invoices\Adapter;

use EconomyPie\Invoices\AbstractAdapter;
use EconomyPie\Invoices\InvoiceData;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\StreamWrapper;
use Psr\Log\LogLevel;

/**
 * Class DineroAdapter
 *
 *
 * @var \EconomyPie\Client\Adapter\BillyAdapter
 *
 */
class BillyAdapter extends AbstractAdapter
{
    public const VENDOR = 'billy';
    private const BASE_ENDPOINT = 'invoices';

    protected $propertyMap = [
        'id' => 'id',
        'invoiceNumber' => 'invoiceNo',
        'dueDate' => 'dueDate',
        'issueDate' => 'entryDate',
        'vatAmount' => 'tax',
        'pdfLink' => 'downloadUrl',
        'grossAmount' => 'grossAmount',
        'currency' => 'currencyId',
        'debtorNumber' => 'contactId',
        'netAmount' => 'amount',
    ];

    /**
     * @param bool $bookedOnly
     * @return array
     */
    public function getAll(array $filter = [], $date): array
    {
        //TODO Implement $bookedOnly
        $finished = false;
        $invoices = [];
        $skipping = 1;

        while (!$finished) {
            $filterArray = ['pageSize' => 1000, 'page' => $skipping];
            if (isset($filter['unpaid']) && $filter['unpaid'] === true) {
                $filterArray['isPaid'] = 0;
            }
            if (isset($filter['paid']) && $filter['paid'] === true) {
                $filterArray['isPaid'] = 1;
            }
            try {
                $response = $this->erpClient->getClient()->get(static::BASE_ENDPOINT, [
                    'query' => $filterArray,
                ]);
                $body = json_decode($response->getBody()->getContents(), true);
            } catch (ClientException $e) {
                $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);

                $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);

                throw new Exception($e->getMessage(), $e->getCode());
            }
            if (count($body['invoices'] ) == 0) {
                $finished = true;
                continue;
            }
            array_push($invoices, ...$body['invoices']);
            $skipping ++;
        }

        return $this->_transformListOfInvoices($invoices);
    }

    /**
     * @param string $invoiceUrl
     * @return resource
     */
    public function getPDF(string $invoiceUrl)
    {
        $response = $this->erpClient->getClient()->get($invoiceUrl)->getBody();

        return StreamWrapper::getResource($response);
    }

    /**
     * @param string $contactId
     * @return \EconomyPie\Invoices\InvoiceData
     */
    public function get(string $contactId): InvoiceData
    {
        try {
            $response = $this->erpClient->getClient()->get(
                static::BASE_ENDPOINT . '/' . $contactId
            );
            $body = json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            $responseBody = json_decode($e->getResponse()->getBody()->getContents(), true);

            $this->log('Error occured fetching list of invoices', LogLevel::ERROR, $responseBody);

            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $this->_transformToInvoiceDataObject($body['invoices']);
        //TODO Implement - works?
        die("NOT IMPLEMENTED YET");
    }

    /**
     * @param integer $erp_id
     * return integer count of total booked invoices
     */

    public function getCount(): array
    {
        // to be worked on later
        return [];
    }
}
