<?php
declare(strict_types=1);

namespace Admin\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

/**
 * Admin\Controller\DashboardController Test Case
 */
class DashboardControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.dashboard',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
