<?php
declare(strict_types=1);

namespace Admin\Controller;

use Cake\Mailer\Email;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 * @property \App\Model\Table\AccountsCollectorsTable $AccountsCollectors
 */
class AccountsCollectorsController extends AppController
{

    public $modelClass = 'App.AccountsCollectors';
    public $isAdmin = false;

    //Load all Accounts Data in tables jenkins3

    public function index()
    {
        $accounts_collectors = $this->AccountsCollectors->find('all')
            ->contain(['Accounts', 'Collectors'])->order(['AccountsCollectors.created' => 'desc']);

        $this->set(compact('accounts_collectors'));
    }

    //Edit Accounts Data

    public function edit($id)
    {
        $accounts_collector = $this->AccountsCollectors->get($id);
        if ($this->request->is('put')) {
            $accounts_collector = $this->AccountsCollectors->patchEntity($accounts_collector, $this->request->getData());
            $save = $this->AccountsCollectors->save($accounts_collector);
            if ($save) {
                $this->Flash->success(
                    'Account Collector has been saved',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to save your Account Collector',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->set(compact(['accounts_collector']));
    }

    //Send Email reminder to collectors
    public function sendEmailReminder($account_id, $collector_id)
    {

        $this->loadModel("Collectors");
        $collector = $this->Collectors->get($collector_id);

        $emailSubject = "Ny kunde til Debito";
        $kNumber = "kundenummer";
        if ($collector->identifier == "lindorff") {
            $emailSubject = "K-nummer til 0DB";
            $kNumber = "K-nummer";
        }

        //get account info and create email message
        $this->loadModel("Accounts");
        $account = $this->Accounts->get($account_id);
        $emailMessage = "Firmanavn: " . $account->name . "<br><br>";
        $emailMessage .= "Adresse: " . $account->address . "<br><br>";
        $emailMessage .= "Postnr.: " . $account->zip_code . "<br><br>";
        $emailMessage .= "By: " . $account->city . "<br><br>";
        $emailMessage .= "Email: " . $account->email . "<br><br>";
        $emailMessage .= "Tlf: " . $account->phone . "<br><br>";
        $emailMessage .= "CVR: " . $account->vat_number . "<br><br>";
        $emailMessage .= "Reg. nr.: " . $account->bank_reg_number . "<br>";
        $emailMessage .= "Kontonr: " . $account->bank_account_number . "<br><br>";
        $emailMessage .= "<a href='https://admin.debito.dk/collector'>Klik her for at tilføje " . $kNumber . " til debitoren.</a> <br><br>";
        $emailMessage .= "Debito ApS <br>";
        $emailMessage .= "Effektiv online inkasso <br>";
        $emailMessage .= "<a href='https://debito.dk/'>debito.dk</a> <br>";

        $email = new Email('sparkpost');
        $myemail = $email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to($collector->email)
            ->subject($emailSubject)
            ->emailFormat('html')
            ->bcc("christoffer@debito.dk")
            ->send($emailMessage);
        $this->Flash->success(
            "An email has been sent to " . $collector->name . " for registering account",
            [
                'params' => [
                    'class' => 'alert alert-danger',
                ],
            ]
        );

        return $this->redirect(['action' => 'index']);
    }
}
