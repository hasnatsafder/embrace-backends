<?php
declare(strict_types=1);

namespace Admin\Controller;

use Cake\Event\Event;

/**
 * Auth Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 * @property \App\Model\Table\AccountsUsersTable $AccountsUsers
 */
class AuthController extends AppController
{
    public $modelClass = null;
    protected $isAdmin = false;

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['login']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function login()
    {
        $this->viewBuilder()->setLayout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user) {
                $this->Auth->setUser($user);

                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error(
                'Your email or password is incorrect.',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }
    }

    public function logout()
    {
        $this->Flash->success(
            'You are now logged out.',
            [
                'params' => [
                    'class' => 'alert alert-success',
                ],
            ]
        );

        return $this->redirect($this->Auth->logout());
    }
}
