<?php
declare(strict_types=1);

namespace Admin\Controller;

use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
use Smalot\PdfParser\Parser;

/**
 * ClaimLines Controller
 *
 * @property \App\Model\Table\ClaimLinesTable $ClaimLines
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \App\Model\Table\ClaimPhasesTable $ClaimPhases
 * @property \App\Model\Table\CurrenciesTable $Currencies
 * @property \App\Model\Table\ClaimLineTypesTable $ClaimLineTypes
 * @property \App\Model\Table\CollectorsTable $Collectors
 */
class ClaimLinesController extends AppController
{
    public $modelClass = 'App.ClaimLines';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();
    }

    public function view($claim_id)
    {
        $claimLines = $this->ClaimLines->find()
            ->where(['ClaimLines.claim_id' => $claim_id])
            ->contain(['ClaimLineTypes', 'Currencies']);

        $this->loadModel('Claims');
        $claim = $this->Claims->get($claim_id);

        $this->loadModel('ClaimPhases');

        $claimPhases = $this->ClaimPhases->find('list')
            ->order(['ClaimPhases.order' => 'ASC']);

        $claimPhaseOrder = $this->ClaimPhases->get($claim->claim_phase_id);

        $this->loadModel('Collectors');
        $collectors = $this->Collectors->find('list');

        $this->set(compact('claimLines', 'claim_id', 'claimPhaseOrder', 'claim', 'collectors'));
    }

    public function edit($claim_line_id)
    {
        $claimLine = $this->ClaimLines->get($claim_line_id);

        //Get Total Pages in Document if PDF
        $number_of_pages = null;
        if ($claimLine->file_mime_type == "application/pdf") {
            $file = new File('webroot/files/ClaimLines/file_name/' . $claimLine->file_name, true, 0644);

            $parser = new Parser();
            try {
                $pdf = $parser->parseFile($file->path);

                // Retrieve all details from the pdf file.
                $details = $pdf->getDetails();
                $number_of_pages = $details['Pages'];
            } catch (\Exception $e) {
                $number_of_pages = "Not defined";
            }
        }
        //Get total number of claim Lines for Claim
        $claim_count = $this->ClaimLines->find()->where(['claim_id' => $claimLine->claim_id])->count();

        //For saving Claim Line
        if ($this->request->is(['post', 'put'])) {
            //if currency changed
            if ($claimLine->currency_id !== $this->request->getData('currency_id')) {
                //set for claim the new currency
                $this->loadModel("Claims");
                $claim = $this->Claims->get($claimLine->claim_id);
                $claim->currency_id = $this->request->getData('currency_id');
                $this->Claims->save($claim);
            }

            $file_name = $this->request->getData('file_name');
            if (!$this->request->getData('file_name')) {
                $file_name = $claimLine->file_name;
            }
            $this->ClaimLines->patchEntity($claimLine, $this->request->getData());
            $claimLine->file_name = $file_name;
            if ($this->ClaimLines->save($claimLine)) {
                $this->Flash->success(
                    'Claim Line has been updated',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );
                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['action' => 'view', $claimLine->claim_id]);
            }
            $this->Flash->error(
                'Unable to Update your Claim Line',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->loadModel('Currencies');

        $currencies = $this->Currencies->find('list', [
            'fields' => ['name' => 'CONCAT(Currencies.name, " - ", Currencies.iso_code)', 'Currencies.id'],
        ]);

        $this->loadModel('ClaimLineTypes');
        $claimLineTypes = $this->ClaimLineTypes->find('list');

        $www_root = WWW_ROOT;

        $this->set(compact(['claimLine', 'currencies', 'claimLineTypes', 'number_of_pages', 'claim_count']));
    }

    //Duplidate a claimLine

    public function duplicate($claim_line_id)
    {

        $claimLine = $this->ClaimLines->newEntity();

        //For saving Claim Line
        if ($this->request->is(['get', 'put'])) {
            $odlClaimLine = $this->ClaimLines->get($claim_line_id);
            $odlClaimLine->isNew(true);
            $odlClaimLine->unsetProperty('id');
            $this->ClaimLines->removeBehavior('Upload');
            $save = $this->ClaimLines->save($odlClaimLine);
            if ($save) {
                $this->Flash->success(
                    'Claim Line has been duplicated',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'edit', $save->id]);
            }
            $this->Flash->error(
                'Unabel to Duplicate your Claim Line',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );

            return $this->redirect(['action' => 'edit', $odlClaimLine->claim_id]);
        }
    }

    function add($claim_id)
    {

        $claimLine = $this->ClaimLines->newEntity();

        if ($this->request->is('post')) {
            $claimLine = $this->ClaimLines->patchEntity($claimLine, $this->request->getData());
            $claimLine->placement = 0;

            // Added: Set the claim_id from the request.
            $claimLine->claim_id = $claim_id;
            $save = $this->ClaimLines->save($claimLine);
            if ($save) {
                $this->Flash->success(
                    'Claim Line has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );
                if ($this->request->getQuery('redirect')) {
                    return $this->redirect($this->request->getQuery('redirect'));
                }
                return $this->redirect(['action' => 'view', $claim_id]);
            }
            $this->Flash->error(
                'Unabel to Add your Claim Line',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $this->loadModel('Currencies');

        $currencies = $this->Currencies->find('list', [
            'fields' => ['name' => 'CONCAT(Currencies.name, " - ", Currencies.iso_code)', 'Currencies.id'],
        ]);
        //get danish currency

        $danish = $this->Currencies->find(
            "all",
            ['conditions' => ['iso_code' => "DKK"]]
        )->first();

        $this->loadModel('ClaimLineTypes');
        $claimLineTypes = $this->ClaimLineTypes->find('list');
        $invoice = $this->ClaimLineTypes->find('all')->where(['identifier' => 'invoice'])->first();

        $this->set(compact(['claimLine', 'currencies', 'claimLineTypes', 'claim_id', 'danish', 'invoice']));
    }

    function delete($claimLineId)
    {
        $claimLine = $this->ClaimLines->get($claimLineId);
        $claim_id = $claimLine->claim_id;
        if ($this->ClaimLines->delete($claimLine)) {
            $this->Flash->success(
                'Claim Line has been deleted',
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );

            return $this->redirect(['action' => 'view', $claim_id]);
        }
    }

    function changeSyncStatus($claimLineId)
    {
        $claimLine = $this->ClaimLines->get($claimLineId);
        $claimLine->synced = !$claimLine->synced;
        $claim_id = $claimLine->claim_id;
        if ($this->ClaimLines->save($claimLine)) {
            $this->Flash->success(
                'Claim Line ' . ($claimLine->synced ? 'synced' : 'desynced'),
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
            return $this->redirect(['controller' => 'Claims', 'action' => 'edit', $claim_id]);
        }
    }

    function changeSyncStatusAll($claim_id, $synced)
    {
        $claimLines = TableRegistry::getTableLocator()->get('ClaimLines');
        $query = $claimLines->query();
        $res = $query->update()
            ->set(['synced' => $synced])
            ->where(['claim_id' => $claim_id])
            ->execute();

        if ($res) {
            $this->Flash->success(
                'All Claim Lines ' . ($synced ? 'synced' : 'desynced'),
                [
                    'params' => [
                        'class' => 'alert alert-success',
                    ],
                ]
            );
            return $this->redirect(['controller' => 'Claims', 'action' => 'edit', $claim_id]);
        }
    }
}
