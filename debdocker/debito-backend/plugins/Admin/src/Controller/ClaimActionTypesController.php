<?php
declare(strict_types=1);

namespace Admin\Controller;

use App\Model\Entity\ClaimPhase;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\ORM\Query;
use Cake\Utility\Hash;

/**
 * ClaimActionTypes Controller
 *
 * @property \App\Model\Table\ClaimActionTypesTable $ClaimActionTypes
 * @property \App\Model\Table\ClaimPhasesTable $ClaimPhases
 * @property \App\Model\Table\CollectorsTable $Collectors
 */
class ClaimActionTypesController extends AppController
{
    public $modelClass = 'App.ClaimActionTypes';
    public $isAdmin = false;
    public $paginate = [
        'limit' => 1000,
    ];

    public function index()
    {
        $claimActionTypes = $this->ClaimActionTypes->find('all')->contain(['ClaimPhases', 'Collectors'])
            ->where(['Collectors.name !=' => 'Lindorff', 'Collectors.name !=' => 'Mortang']);
        $this->set(compact(['claimActionTypes']));
    }

    public function add()
    {
        $claimActionType = $this->ClaimActionTypes->newEntity();
        $claimActionType->is_public = 1;

        //For saving Claim
        if ($this->request->is(['post', 'put'])) {
            $this->ClaimActionTypes->patchEntity($claimActionType, $this->request->getData());
            $claimActionType->is_public = 1;
            if ($this->ClaimActionTypes->save($claimActionType)) {
                $this->Flash->success(
                    'Claim Action Type has been added',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to Update your Claim Action Type',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $collectors = $this->ClaimActionTypes->Collectors->find('list')->orderDesc('created');
        $claimPhases = $this->_getClaimPhases();

        $this->set(compact(['claimActionType', 'collectors', 'claimPhases']));
    }

    public function edit($claimActionTypeId)
    {
        $this->viewBuilder()->setTemplate('form');

        $claimActionType = $this->ClaimActionTypes->get($claimActionTypeId);

        //For saving Claim
        if ($this->request->is(['post', 'put'])) {
            $this->ClaimActionTypes->patchEntity($claimActionType, $this->request->getData());
            if ($this->ClaimActionTypes->save($claimActionType)) {
                $this->Flash->success(
                    'Claim Action Type has been updated',
                    [
                        'params' => [
                            'class' => 'alert alert-success',
                        ],
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(
                'Unable to Update your Claim Action Type',
                [
                    'params' => [
                        'class' => 'alert alert-danger',
                    ],
                ]
            );
        }

        $collectors = $this->ClaimActionTypes->Collectors->find('list');
        $claimPhases = $this->_getClaimPhases();

        $this->set(compact(['claimActionType', 'collectors', 'claimPhases']));
    }

    private function _getClaimPhases()
    {
        $this->loadModel('ClaimPhases');
        return $this->ClaimPhases
            ->find('list')
            ->orderAsc('ClaimPhases.order')
;
    }
}
