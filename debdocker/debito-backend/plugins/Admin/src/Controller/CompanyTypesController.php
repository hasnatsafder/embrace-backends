<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * CompanyTypes Controller
 *
 * @property \App\Model\Table\CompanyTypesTable $CompanyTypes
 */
class CompanyTypesController extends AppController
{
    public $modelClass = 'App.CompanyTypes';

    public function initialize()
    {
        parent::initialize();

        $this->Crud->disable(['add', 'edit', 'delete']);
    }
}
