<?php
declare(strict_types=1);

namespace Admin\Controller;

use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountVerificationsTable $AccountVerifications
 */
class AccountVerificationsController extends AppController
{
    public $modelClass = 'App.AccountVerifications';
    public $isAdmin = false;
    protected $searchActions = ['index'];

    public function view(string $account_id) {
        $account = TableRegistry::getTableLocator()->get('Accounts')->get($account_id);
        $account_verifications = $this->AccountVerifications->find('all')
                                    ->where(['account_id' => $account_id])->contain(['Documents', 'Accounts']);
        $this->set(compact(['account_verifications', 'account']));
    }

    public function approve(string $id, $approve = true) {
        $account_verification = $this->AccountVerifications->get($id);
        $account_verification->approved = $approve ? FrozenTime::now() : null;
        if ($this->AccountVerifications->save($account_verification)) {
            $flashText = 'Account verification ' . ($approve ? "Approved" : "Unapproved") . " for " . $account_verification->name;
            $this->generateFlash($flashText, "success", "success");
        }
        else {
            $flashText = 'Account verification failed to ' . ($approve ? "Approved" : "Unapproved") . " for " . $account_verification->name;
            $this->generateFlash($flashText, "error", "danger");
        }
        return $this->redirect(['action' => 'view', $account_verification->account_id]);
    }

    public function approveAll(string $account_id, $approve = true) {

        $account_verifications = $this->AccountVerifications->updateAll(
            ['approved' =>  $approve ? FrozenTime::now() : null],
            ['account_id' => $account_id]
        );
        if ($account_verifications) {
            $flashText = 'Account verifications ' . ($approve ? "Approved" : "Unapproved");
            $this->generateFlash($flashText, "success", "success");
        }
        else {
            $flashText = 'Account verifications failed to ' . ($approve ? "Approved" : "Unapproved");
            $this->generateFlash($flashText, "error", "danger");
        }
        return $this->redirect(['action' => 'view', $account_id]);
    }
}
