<?php
declare(strict_types=1);

namespace Admin\Controller;

/**
 * Invoices Controller
 * @property \App\Model\Table\InvoicesTable $invoices
 */
class InvoicesController extends AppController
{
    public $modelClass = 'App.Invoices';
    public $isAdmin = false;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index($account_id)
    {
      $this->loadModel('ErpIntegrations');
      $integration = $this->ErpIntegrations->find('all')->where(['account_id' => $account_id])->last();
      if(!$integration) {
        die;
      }
      $invoices = $this->Invoices->find('all')
                  ->where(['erp_integration_id' => $integration->id])
                  ->contain(['InvoiceStatuses', 'Currencies']);
      $this->set(compact(['integration', 'invoices']));

    }
}
