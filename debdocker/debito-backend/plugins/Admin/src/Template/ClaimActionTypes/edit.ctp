<?php
/**
 * @var \App\View\AppView $this
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Action</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claim Action Types', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit Action Type</div>
                </div>
                <?= $this->Form->create($claimAction, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('collector_id',
                            [
                                'options' => $collectors,
                                'value' => $claimAction->collector_id,
                            ]);
                        echo $this->Form->control('claim_phase_id',
                            [
                                'options' => $claimPhases,
                                'value' => $claimAction->claim_phase_id,
                                'empty' => true,
                            ]);
                        echo $this->Form->control('identifier', ['type' => 'text']);
                        echo $this->Form->control('collector_identifier', ['type' => 'text']);
                        echo $this->Form->control('name', ['type' => 'text']);
                        echo $this->Form->control('description', ['type' => 'text']);
                        echo $this->Form->control('is_public',
                            [
                                'options' => [0 => "No", 1 => "Yes"],
                                'value' => $claimAction->is_public,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary mr-2']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
