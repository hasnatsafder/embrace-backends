<?php
declare(strict_types=1);
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClaimActionType $claimActionType
 * @var \App\Model\Entity\ClaimPhase[] $claimPhases
 * @var array $collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title"><?= __('Claims Action') ?></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claim Action Types', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= __('Edit') ?>
        </li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title"><?= __('Edit Action Type') ?></div>
                </div>
                <?= $this->Form->create($claimActionType, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('collector_id', [
                            'options' => $collectors,
                            'value' => $claimActionType->collector_id,
                        ]);

                        echo $this->Form->control('claim_phase_id', [
                            'options' => $claimPhases,
                            'value' => $claimActionType->claim_phase_id,
                            'empty' => sprintf('-- %s --', __('None')),
                        ]);
                        ?>
                        <p class="help-block text-warning">
                            <?= __('Claim will change to this phase after the claim action has been added') ?>
                        </p>
                        <br>

                        <?php
                        echo $this->Form->control('identifier', ['type' => 'text']);
                        echo $this->Form->control('collector_identifier', ['type' => 'text']);
                        echo $this->Form->control('name', ['type' => 'text']);
                        echo $this->Form->control('description', ['type' => 'text']);
                        echo $this->Form->control('is_public', [
                            'options' => ['No', 'Yes'],
                            'value' => $claimActionType->is_public,
                        ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Save'), ['class' => 'btn btn-primary mr-2']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
