<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $accounts_collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title">Account Collectors</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">Accounts Collectors</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="input-group-icon input-group-icon-left mr-3">
                    <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                    <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text" placeholder="Search ...">
                </div>
            </div>
            <table class="table table-bordered table-hover" id="datatable">
                <thead class="thead-default thead-lg">
                <tr>
                    <th><?= __("Created") ?></th>
                    <th>Collector</th>
                    <th>Account</th>
                    <th>Identifier</th>
                    <th>Invalid</th>
                    <th>Send Reminder</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($accounts_collectors as $accounts_collector) : ?>
                    <tr class="<?= $accounts_collector->invalid ? 'warning' : '' ?>">
                        <td></td>
                        <td> <?= $accounts_collector->collector->name ?> </td>
                        <td>
                            <?= $this->Html->link(
                                $accounts_collector->account->name,
                                ['controller' => 'Claims', 'action' => 'accounts', $accounts_collector->account_id],
                                ['class' => 'btn btn-link']
                            );
                            ?>
                        </td>
                        <td> <?= $accounts_collector->collector_identifier ?> </td>
                        <td> <?php echo $accounts_collector->invalid ? "Yes" : "No" ?> </td>
                        <td>
                            <?php if (!$accounts_collector->invalid) { ?>
                                <?= $this->Html->link(
                                    'Send',
                                    ['action' => 'sendEmailReminder', $accounts_collector->account_id, $accounts_collector->collector_id],
                                    ['class' => 'btn btn-danger']
                                );
                            } ?>
                        </td>
                        <td>
                            <?= $this->Html->link(
                                'Edit',
                                ['action' => 'edit', $accounts_collector->id],
                                ['class' => 'btn btn-warning']
                            ); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?> 