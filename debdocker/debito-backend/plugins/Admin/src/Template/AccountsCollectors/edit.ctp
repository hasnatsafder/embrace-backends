<?php
/**
 * @var \App\View\AppView $this
 * @var object $accounts_collector
 */
?>
<div class="page-heading">
    <h1 class="page-title">Accounts Collectors</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('AccountsCollectors', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit Accounts Collector</div>
                </div>
                <?= $this->Form->create($accounts_collector) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('collector_reference', ['type' => 'text']);
                        echo $this->Form->control('invalid',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => $accounts_collector->invalid,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>