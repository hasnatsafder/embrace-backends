<?php
/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $countries
 * @var mixed $debtor
 * @var object $denmarkId
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Debtors</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['controller' => 'users', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Accounts", ['controller' => 'Accounts', 'action' => 'user', $user->id]); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Debtors", ['action' => 'accounts', $account->id]); ?>
        </li>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Debtors List for user <span class="text-pink"><?= $user->email ?> </span> and Account <span
                                class="text-pink"><?= $account->name ?> </span></div>
                </div>
                <?= $this->Form->create($debtor, ['id' => 'debtor-form']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('first_name');
                        echo $this->Form->control('last_name');
                        echo $this->Form->control('company_name');
                        echo $this->Form->control('address');
                        echo $this->Form->control('zip_code');
                        echo $this->Form->control('city');
                        echo $this->Form->control('phone');
                        echo $this->Form->control('vat_number');
                        echo $this->Form->control('email');
                        echo $this->Form->control('country_id',
                            [
                                'options' => $countries,
                                'value' => $denmarkId->id,
                            ]);
                        echo $this->Form->control('is_company',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 1,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>

<script>
    $(document).ready(function () {
        $("select").select2();
    });
</script>