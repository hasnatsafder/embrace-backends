<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="page-sidebar">
    <ul class="side-menu metismenu scroller">
        <li>
            <?= $this->Html->link('Users', ['controller' => 'Users', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link('Accounts', ['controller' => 'Accounts', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link('Claim', ['controller' => 'Claims', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link('Claim Actions', ['controller' => 'ClaimActionTypes', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link('Claims Stuck', ['controller' => 'Claims', 'action' => 'stuck']); ?>
        </li>
        <li>
            <?= $this->Html->link('Claims Idle', ['controller' => 'Claims', 'action' => 'idle']); ?>
        </li>
        <li>
            <?= $this->Html->link('Accounts Collectors', ['controller' => 'AccountsCollectors', 'action' => 'index']); ?>
        </li>
        <li>
            <?= $this->Html->link('Rejected Claims', ['controller' => 'Claims', 'action' => 'rejected']); ?>
        </li>
        <li>
            <?= $this->Html->link('Synced Today Claims', ['controller' => 'Claims', 'action' => 'synced']); ?>
        </li>
        <li>
            <?= $this->Html->link('Subscriptions Plans', ['controller' => 'Plans', 'action' => 'index']); ?>
        </li>
    </ul>
</nav>
