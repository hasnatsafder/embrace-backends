<?php
/**
 * @var \App\View\AppView $this
 */
?>
<header class="header">
    <!-- START TOP-LEFT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li>
            <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Accounts', ['controller' => 'Accounts', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Users', ['controller' => 'Users', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Claims', ['controller' => 'Claims', 'action' => 'index'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('New Claims', ['controller' => 'Claims', 'action' => 'newClaims'], ['class' => 'nav-link']); ?>
        </li>
        <li class="nav-link">
            <?= $this->Html->link('Rejected Claims', ['controller' => 'Claims', 'action' => 'rejected'], ['class' => 'nav-link']); ?>
        </li>
    </ul>
    <!-- END TOP-LEFT TOOLBAR-->

    <!--LOGO-->
    <a href="index.html">
        <?= $this->Html->image('/Admin/img/logo_web.png', ['alt' => 'Debito']); ?>
    </a>
    <!-- START TOP-RIGHT TOOLBAR-->
    <ul class="nav navbar-toolbar">
        <li class="dropdown dropdown-notification">
            <a class="nav-link dropdown-toggle toolbar-icon" data-toggle="dropdown" href="javascript:;" aria-expanded="false"><i class="ti-bell rel"><span class="notify-signal"></span></i></a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" x-placement="bottom-end" style="position: absolute; transform: translate3d(45px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                <div class="dropdown-arrow"></div>
                <div class="dropdown-header text-center">
                    <div>
                        <span class="font-18"><strong><?= $account_verifications->count(); ?> New</strong> Notifications</span>
                    </div>
                </div>
                <div class="p-3" style="max-height: 200px; overflow-y: auto;">
                    <ul>
                        <?php foreach ($account_verifications as $account_verification): ?>
                            <li style="list-style: none">
                                <?= $this->Html->link(
                                    $account_verification['Accounts']['name'],
                                    ['controller' => 'Claims', 'action' => 'accounts', $account_verification->account_id, 'source' => 'admin/accounts/edit-user-account/'.$account_verification->account_id],
                                    ['class' => '']
                                ); ?>
                                <small class="float-right text-muted ml-2 nowrap"><?= new \Cake\I18n\FrozenDate($account_verification->created) ?></small>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </li>
        <li class="dropdown mega-menu">
            Debito Admin
        </li>
        <li>
            <?= $this->Html->link('Logout', ['controller' => 'Auth', 'action' => 'logout']); ?>
        </li>
    </ul>
    <!-- END TOP-RIGHT TOOLBAR-->
</header>
