<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $accounts
 * @var object $user
 */
?>
    <div class="page-heading">
        <h1 class="page-title">User Accounts</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html"><i class="la la-home font-20"></i></a>
            </li>
            <li class="breadcrumb-item">
                <?= $this->Html->link('Users', ['controller' => 'Users', 'action' => 'index']); ?>
            </li>
            <li class="breadcrumb-item">Accounts</li>
        </ol>
    </div>

    <div class="page-content">
        <div class="ibox">
            <?= $this->Flash->render() ?>
            <div class="ibox-head">
                <div class="ibox-title">Accounts List for <span class="text-pink"><?= $user->email ?> </span></div>
                <?= $this->Html->link('Add', ['action' => 'addUserAccount', $user->id], ['class' => 'btn btn-success']); ?>
            </div>
            <div class="ibox-body">
                <div class="flexbox mb-4">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                               placeholder="Search ...">
                    </div>
                </div>
                <div class="table-responsive row">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead class="thead-default thead-lg">
                            <tr>
                                <th width="10%">CVR number</th>
                                <th width="10%">Name</th>
                                <th width="10%">User(owner)</th>
                                <th width="10%">Phone</th>
                                <th width="10%">Email</th>
                                <th width="10%">Integration</th>
                                <th width="10%">Subscription</th>
                                <th width="10%">Debtors</th>
                                <th width="10%">Claims</th>
                                <th width="10%">Edit</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($accounts as $account) : ?>

                                <tr  <?= $user->active_account_id == $account->id ? "class='success'" : ""; ?> >
                                    <td> <?= $account->vat_number ?> </td>
                                    <td> <?= $account->name ?> </td>
                                    <td>
                                        <?php foreach ($account->users as $user) : ?>
                                            <?php if ($user->id == $account->created_by_id):?>
                                            <?= $this->Html->link(
                                                $user->email,
                                                ['controller' => 'Accounts', 'action' => 'user', $user->id],
                                                ['class' => 'btn btn-link']
                                            ); 
                                            endif; ?>
                                        <?php endforeach ?>
                                    </td>
                                    <td> <?= $account->phone ?> </td>
                                    <td> <?= $account->email ?> </td>
                                    <td>
                                        <?php 
                                            if (count($account->erp_integrations) > 0):
                                                echo $account->erp_integrations[count($account->erp_integrations) - 1]->vendor_identifier;
                                            else:
                                                echo "None";
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            if (count($account->account_plans) > 0):
                                                echo $plans[$account->account_plans[count($account->account_plans) - 1]->plan_id]['name'];
                                            else:
                                                echo "None";
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(
                                            'Debtors',
                                            ['controller' => 'Debtors', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/user/'.$user->id],
                                            ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
                                        ); ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(
                                            'Claims',
                                            ['controller' => 'Claims', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/user/'.$user->id],
                                            ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
                                        ); ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(
                                            'Edit',
                                            ['action' => 'editUserAccount', $account->id, 'redirect' => '/admin/accounts'],
                                            ['class' => 'btn btn-primary btn-sm']
                                        ); ?>
                                    </td>
                                    <td>
                                        <?= $user->active_account_id != $account->id ? $this->Html->link(
                                            'Activate',
                                            ['action' => 'activeAccount', $account->id. "/". $user->id, 'redirect' => '/admin/accounts/user/'.$user->id],
                                            ['class' => 'btn btn-success btn-sm']
                                        ) : ""; ?>
                                    </td>
                                    <!-- <td> <?= $account->zip_code ?> </td>
                                    <td> <?= $account->city ?> </td>
                                    <td> <?= $account->country->name ? $account->country->name : "-" ?> </td>
                                    <td>
                                        <?php if ($account->country->name == "Denmark") : ?>
                                            <?= $account->bank_reg_number ?> - <?= $account->bank_account_number ?>
                                        <?php else : ?>
                                            <?= $account->iban ?> - <?= $account->swift_code ?>
                                        <?php endif; ?>
                                    </td>
                                    <td> <?php echo $account->is_company ? "Yes" : "No"; ?> </td>
                                    <td> <?= $account->address ?> </td> -->
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>