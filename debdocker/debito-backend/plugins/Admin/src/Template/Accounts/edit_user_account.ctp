<?php

/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $countries
 * @var object $denmarkId
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Accounts</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['controller' => 'users', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Accounts", $this->request->getQuery('redirect')); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
    <span style="position: absolute;right: 20px; top:110px;">
        <?= $this->Html->link(
            'Claims',
            ['controller' => 'Claims', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/edit-user-account/'.$account->id],
            ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
        ); ?>
        <?= $this->Html->link(
            'Debtors',
            ['controller' => 'Debtors', 'action' => 'accounts', $account->id, 'source' => 'admin/accounts/edit-user-account/'.$account->id],
            ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
        ); ?>
        <?= $this->Html->link(
            'Verifications',
            ['controller' => 'AccountVerifications', 'action' => 'view', $account->id, 'source' => 'admin/accounts/edit-user-account/'.$account->id],
            ['class' => 'btn btn-blue btn-sm', 'target' => "_blank"]
        ); ?>
        <?= $this->Html->link(
            'Delete',
            ['controller' => 'Accounts', 'action' => 'delete', $account->id, 'source' => 'admin/accounts/edit-user-account/'.$account->id],
            ['class' => 'btn btn-danger btn-sm']
        ); ?>
    </span>
</div>

<div class="page-content">
    <?= $this->Flash->render() ?>
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit Account for user <span class="text-pink"><?= $user->email ?></div>
                </div>
                <?= $this->Form->create($account, ['id' => 'account-form']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <label class="font-strong">Account info</label>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('vat_number', ['label' => 'VAT / Company ID']); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('ean_number'); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('name'); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control(
                                    'is_company',
                                    [
                                        'options' => [1 => "Yes", 0 => "No"],
                                        'value' => $account->is_company,
                                    ]
                                ); ?>
                            </div>
                        </div>
                        <label class="font-strong">Contact details</label>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('phone'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('website'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('email'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->Form->control('address'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?= $this->Form->control('city'); ?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->control('zip_code'); ?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->control(
                                    'country_id',
                                    [
                                        'options' => $countries,
                                        'value' => $account->country_id,
                                    ]
                                ); ?>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="font-strong">Integrations</label>
                            <br>
                            <?php if (isset($integration->vendor_identifier)) : ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <?= $this->Html->image('/Admin/img/' . $integration->vendor_identifier . ".png", ['alt' => $integration->vendor_identifier]); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <b>Setup date :</b> <?= date_format($integration->created, "d-m-yy") ?>
                                        </p>
                                        <p>
                                            <b>Total invoices:</b> <?= $invoices ?>
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                    <?= $this->Html->link(
                                        'Invoices',
                                        ['controller' => 'Invoices', 'action' => 'index', $account->id, 'source' => 'admin/accounts/edit-user-account/'.$account->id],
                                        ['class' => 'btn btn-blue pull-right', 'target' => "_blank"]
                                    ); ?>
                                    </div>
                                </div>
                            <?php else : ?>
                                <p class="text-muted">No account linked</p>
                            <?php endif; ?>
                        </div>
                        <hr>
                        <label class="font-strong">Bank details</label>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('bank_reg_number'); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('bank_account_number'); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('iban'); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('swift_code'); ?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('notes'); ?>
                            </div>
                            <input type="hidden" name="saveAndStay" value="0" id="save-stay">
                        </div>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="button" class="btn btn-info" onclick="submitForm(true)">Save</button>
                    <button type="button" class="btn btn-primary" onclick="submitForm(false)">Save and exit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="col-md-5">
            <div class="ibox">
                <?= $this->Form->create(
                    $account,
                    [
                        'url' =>
                        [
                            'action' => 'savePlan',
                            'redirect' => $this->request->getQuery('redirect') ? $this->request->getQuery('redirect') : ""
                        ],
                        'id' => 'action-form'
                    ]
                ) ?>
                <div class="ibox-head">
                    <span>
                        Select Subscription
                    </span>
                    <span>
                        <h5><b><?= $current_plan->plan ? $current_plan->plan->name : "" ?></b> </h5>
                    </span>
                </div>
                <div class="ibox-body">
                    <div class="form-group">
                        <label class="control-label" for="address">Select Plan: </label>
                        <select name="plan_id" id="plan-val" class="form-control">
                            <?php foreach ($plans as $plan) :
                                if ($plan->name == "Trial" || $plan->name == "Free subscription") {
                                    $option_text = $plan->name;
                                    if ($plan->name == "Trial") {
                                        $option_text .= " - Default";
                                    }
                                } else {
                                    $option_text = $plan->name . " - " . ucfirst(strtolower($plan->billing_interval));
                                }
                                $selected = "";
                                if ($plan->id == $current_plan->plan_id) {
                                    $selected = "selected='selected'";
                                }
                                // check for selected
                                echo "<option $selected data='$plan->price' value='$plan->id'>" . $option_text . "</option>";
                            endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="address">Discount: </label>
                        <input name="discount" id="discount-val" type="text" class="form-control" value="<?= $current_plan->discount ? $current_plan->discount : 0 ?>">
                        <small>The total is : <span class="amount" id="calculated-sum"></span></small>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="address">Internal notes: </label>
                        <input name="internal_note" type="text" class="form-control" value="<?= $current_plan->internal_note ?>">
                    </div>
                </div>
                <div class="ibox-footer">
                    <button data-toggle="modal" data-target="#LogModel" id="log-modal-button" class="btn btn">View pevious log</button>
                    <button type="button" onclick="submitSubscription(true)" class="btn btn-info ml-2">Save</button>
                    <button type="button" onclick="submitSubscription(false)" class="btn btn-primary ml-2">Save and exit</button>
                </div>
                <input type="hidden" name="saveAndStay" value="0" id="save-stay-subscription">
                <?= $this->Form->end() ?>
            </div>
            <div class="ibox">
                <div class="ibox-head">
                    <p>Users</p>
                </div>
                <div class="ibox-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?= $this->Html->link(
                                        $user->email,
                                        ['controller' => 'Accounts', 'action' => 'user', $user->id],
                                        ['class' => 'btn btn-link']
                                    ); ?>
                                </td>
                                <td>Owner</td>
                                <td></td>
                            </tr>
                            <?php foreach ($user_accounts as $user_account):?>
                                <?php if ($user_account->user->id != $user->id): ?>
                                <tr>
                                    <td>
                                        <?= $this->Html->link(
                                            $user_account->user->email,
                                            ['controller' => 'Accounts', 'action' => 'user', $user->id],
                                            ['class' => 'btn btn-link']
                                        ); ?>
                                    </td>
                                    <td>
                                        <?= $user_account->role->name ?>
                                    </td>
                                    <td>
                                    <i data-href="/admin/accounts/delete-user/<?= $user_account->id ?>" class="fa fa-trash cursor-pointer" data-toggle="modal" data-target="#deleteUserModel" label="Delete user"></i></td>
                                </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <hr>
                    <?= $this->Form->create(
                    $account,
                    [
                        'url' =>
                        [
                            'action' => 'userAccountsSave',
                            'redirect' => $this->request->getQuery('redirect') ? $this->request->getQuery('redirect') : ""
                        ],
                        'id' => 'user-form',
                        'class' => 'form-inline form-purple'
                    ],
                ) ?>
                <div class="row">
                    <div class="col">
                        <?= $this->Form->select(
                        'user_id',
                        $users,
                        ['empty' => '(choose one)'],
                        [
                            'class' => 'pl-4',
                            'required' => true,
                            ['empty' => '(choose one)']
                        ]
                        ); ?>
                    </div>
                    <div class="col">
                        <?= $this->Form->control(
                        'role_id',
                        [
                            'options' => $roles,
                            'class' => 'ml-4',
                            'required' => true,
                        ]
                        ); ?>
                    </div>
                </div>
                <input type="hidden" name="saveAndStay" value="0" id="save-stay-user">
                <?= $this->Form->end() ?>
                </div>
                <div class="ibox-footer">
                    <button onclick="submitUser(true)" type="button" class="btn btn-info ml-2">Save</button>
                    <button onclick="submitUser(false)" type="button" class="btn btn-primary ml-2">Save and exit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Modal for log -->
<div class="modal fade" id="LogModel">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Log for previous plan</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <ul class="media-list media-list-divider">
                    <?php foreach ($all_plans as $plan) :
                        echo '<li class="media align-items-center">';
                        echo '<div class="flex-1">';
                        echo "<div class='media-heading'>" . $plan->plan->name . " - " . $plan->plan->billing_interval  . "</div>";
                        echo '<small class="text-muted">Discount : ' . $plan->discount . '</small><br>';
                        echo '<small class="text-muted">Assigned : ' . date_format($plan->created, "d-m-yy") . '</small><br>';
                        echo '<small class="text-muted">Notes : ' . $plan->internal_note . '</small>';
                        echo '</div>';
                        echo '<li>';
                    endforeach; ?>
                </ul>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal for delete user -->
<div class="modal fade" id="deleteUserModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this user from account?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>

<script>
    function submitForm(saveStay) {

        if(saveStay) {
            $('#save-stay').val(1);
        }
        if ($('#country-id').val() == "<?= $denmarkId->id ?>") {
            var len = 10 - $('#bank-account-number').val().length;
            var str = new Array(len + 1).join('0');
            $('#bank-account-number').val(str + $('#bank-account-number').val())
        }
        $('#account-form').submit();
    }

    function submitSubscription(saveStay) {
        if(saveStay) {
            $('#save-stay-subscription').val(1);
        }
        $('#action-form').submit()
    }

    function submitUser(saveStay) {
        if(saveStay) {
            $('#save-stay-user').val(1);
        }
        $('#user-form').submit()
    }

    // delete user accounts
    $('#deleteUserModel').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });

    $(document).ready(function() {
        $("select").select2();
        $("#log-modal-button").click(function(e) {
            e.preventDefault();
        });
        calculatedSum()
    });
    $('#plan-val').change(function() {
        calculatedSum()
    })
    $('#discount-val').change(function() {
        calculatedSum()
    })

    function calculatedSum() {
        numeral.locale("da-dk");
        let text = $("#plan-val option:selected").attr('data');
        let discount = parseInt($("#discount-val").val()) * 100;
        total = (parseInt(text) - discount)
        $('#calculated-sum').html(total)
        $('.amount').each(function(i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.toString().splice(-2, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original).format("$0,0.00");
            $(this).html(number)
        });
    }
</script>
<style>
    .modal-body {
        max-height: 500px;
        overflow: auto;
    }
</style>
