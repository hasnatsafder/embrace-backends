<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $account
 * @var mixed $countries
 * @var object $denmarkId
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Accounts</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Accounts', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Add Account</div>
                </div>
                <?= $this->Form->create($account, ['id' => 'account-form']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('user_id',
                        [
                            'options' => $users,
                        ]);
                        echo $this->Form->control('name');
                        echo $this->Form->control('address');
                        echo $this->Form->control('zip_code');
                        echo $this->Form->control('city');
                        echo $this->Form->control('phone');
                        echo $this->Form->control('vat_number', ['label' => 'VAT / Company ID']);
                        echo $this->Form->control('ean_number');
                        echo $this->Form->control('website');
                        echo $this->Form->control('bank_reg_number');
                        echo $this->Form->control('bank_account_number');
                        echo $this->Form->control('iban');
                        echo $this->Form->control('swift_code');
                        echo $this->Form->control('email');
                        echo $this->Form->control('is_company',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 1,
                            ]);
                        echo $this->Form->control('country_id',
                            [
                                'options' => $countries,
                                'value' => $denmarkId->id,
                            ]);
                        echo $this->Form->control('is_active',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 0,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="button" class="btn btn-primary mr-2" onclick="submitForm()">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<script>

    function submitForm() {
        if ($('#country-id').val() == "<?= $denmarkId->id ?>") {
            var len = 10 - $('#bank-account-number').val().length;
            var str = new Array(len + 1).join('0');
            $('#bank-account-number').val(str + $('#bank-account-number').val())
        }
        $('#account-form').submit();
    }

    $(document).ready(function () {
        $("select").select2();
    });

</script>