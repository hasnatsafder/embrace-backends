<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimLine
 * @var mixed $claimLineTypes
 * @var mixed $claim_id
 * @var mixed $currencies
 * @var object $danish
 * @var object $invoice
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <?php if ($this->request->getQuery('redirect')):?>
            <li class="breadcrumb-item">
                <?= $this->Html->link('Back', $this->request->getQuery('redirect')); ?>
            </li>            
        <?php else: ?>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['controller' => 'claims', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claim Lines', ['action' => 'view', $claim_id]); ?>
        </li>
        <?php endif; ?>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Add Claim Line</div>
                </div>
                <?= $this->Form->create(
                    $claimLine,
                    [
                        'url' =>
                        [
                            'action' => 'add/', $claim_id,
                            'redirect' => $this->request->getQuery('redirect') ? $this->request->getQuery('redirect') : ""
                        ],
                        'id' => 'my_form', 'type' => 'file'
                    ]
                ) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('file_name', ['type' => 'file']);
                        echo $this->Form->control('currency_id',
                            [
                                'options' => $currencies,
                                'value' => $danish->id,
                            ]);
                        echo $this->Form->control('amount', ['type' => 'text']);
                        echo $this->Form->control('invoice_date', ['type' => 'text']);
                        echo $this->Form->control('maturity_date', ['type' => 'text']);
                        echo $this->Form->control('claim_line_type_id',
                            [
                                'options' => $claimLineTypes,
                                'value' => $invoice->id,
                            ]);
                        echo $this->Form->control('invoice_reference', ['type' => 'text']);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="button" class="btn btn-primary mr-2" onclick="submitform()">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="col-lg-6" id="preview-section">

        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-datepicker.js') ?>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/numeral.min.js') ?>
<?= $this->Html->script('/Admin/js/vendors/numeral/da-dk.min.js') ?>

<script>
    $(function () {
        $("#maturity-date, #invoice-date").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });
    });

    $(document).ready(function () {
        $('#currency-id').select2();

        $('#file-name').change(function () {
            var input = this;
            var url = $(this).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                if (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg") {
                    reader.onload = function (e) {
                        $('#preview-section').html('<img src="' + e.target.result + '" />')
                    };
                    reader.readAsDataURL(input.files[0]);
                } else if (ext == "pdf") {
                    reader.onload = function (e) {
                        $('#preview-section').html('<object data="' + e.target.result + '" id="fileViewInner" width="700" height="800" />');
                    };
                    reader.readAsDataURL(input.files[0]);
                } else {
                    ('#preview-section').html("");
                }
            }
        });

        //when changing invoice field auto change maturity field
        $('#invoice-date').change(function () {
            let invoice_date = $(this).val();
            $('#maturity-date').val(invoice_date);
        });
    });

    function submitform() {
        var removedText = $("#amount").val().replace(/\D+/g, '');
        $("#amount").val(removedText);
        $("#my_form").submit();
    }
</script>
