<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimLine
 * @var mixed $claimLineTypes
 * @var mixed $claim_line_id
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims Lines</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claims', ['action' => 'index'], ['class' => 'font-weight-bold']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Claim Line', ['action' => 'edit', $claim_line_id],
                ['class' => 'font-weight-bold']); ?>
        </li>
        <li class="breadcrumb-item">Duplicate</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Duplicate Claim Line</div>
                </div>
                <?= $this->Form->create($claimLine) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('amount', ['type' => 'text']);
                        echo $this->Form->control('claim_line_type_id',
                            [
                                'options' => $claimLineTypes,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
