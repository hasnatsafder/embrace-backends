<?php
/**
 * @var \App\View\AppView $this
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Users</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Edit User</div>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModel">
                            <?= __("Delete") ?>
                    </button>
                </div>
                <?= $this->Form->create($user) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('first_name', ['type' => 'text']);
                        echo $this->Form->control('last_name', ['type' => 'text']);
                        echo $this->Form->control('email', ['data-lpignore' => 'true']);
                        echo $this->Form->control('password', ['data-lpignore' => 'true']);
                        echo $this->Form->control('is_active',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => $user->is_active,
                            ]);
                        echo $this->Form->control('is_confirmed',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => $user->is_confirmed,
                            ]);
                        echo $this->Form->control('verified',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => $user->verified,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this User?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link('Delete', ['action' => 'delete', $user->id],
                    ['class' => 'btn btn-danger']); ?>
            </div>
        </div>
    </div>
</div>