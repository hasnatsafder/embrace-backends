<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $role
 * @var mixed $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Users</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <?= $this->Flash->render() ?>
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Add User</div>
                </div>
                <?= $this->Form->create($user) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('first_name', ['type' => 'text']);
                        echo $this->Form->control('last_name', ['type' => 'text']);
                        echo $this->Form->control('email', ['data-lpignore' => 'true']);
                        echo $this->Form->control('password', ['data-lpignore' => 'true']);
                        echo $this->Form->control('is_active',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 1,
                            ]);
                        echo $this->Form->control('is_confirmed',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 1,
                            ]);
                        echo $this->Form->control('verified',
                            [
                                'options' => [1 => "Yes", 0 => "No"],
                                'value' => 1,
                            ]);
                        echo $this->Form->control('role',
                            [
                                'options' => $role,
                                'empty' => true,
                            ]);
                        ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>