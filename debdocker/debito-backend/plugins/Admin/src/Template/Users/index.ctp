<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $users
 */
?>
    <div class="page-heading">
        <h1 class="page-title">Users</h1>
    </div>

    <div class="page-content">
        <div class="ibox">
            <?= $this->Flash->render() ?>
            <div class="ibox-head">
                <div class="ibox-title">Users List</div>
                <?= $this->Html->link('Add', ['action' => 'add'], ['class' => 'btn btn-success']); ?>
            </div>
            <div class="ibox-body">
                <div class="flexbox mb-4">
                    <div class="input-group-icon input-group-icon-left mr-3">
                        <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                        <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                               placeholder="Search ...">
                    </div>
                </div>
                <div class="table-responsive row">
                    <table class="table table-bordered table-hover" id="datatable">
                        <thead class="thead-default thead-lg">
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Active</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td> <?= $user->first_name . " " . $user->last_name; ?> </td>
                                <td> <?= $user->email; ?> </td>
                                <td> <?= $user->phone; ?> </td>
                                <td> <?php echo $user->is_active == 1 ? "Yes" : "No"; ?> </td>
                                <td>
                                    <?= $this->Html->link('Edit',
                                        ['action' => 'edit', $user->id],
                                        ['class' => 'btn btn-warning']); ?>
                                </td>
                                <td>
                                    <?= $this->Html->link('Accounts',
                                        ['controller' => 'Accounts', 'action' => 'user', $user->id],
                                        ['class' => 'btn btn-success']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>