<div class="page-heading">
    <h1 class="page-title">Subscription plans</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">Plans</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <div class="ibox-body">
          <?= $this->Flash->render() ?>
            <div class="row font-strong mb-4">
              <div class="col-md-4">Name</div>
              <div class="col-md-4">Price</div>
              <div class="col-md-3">Interval</div>
              <div class="col-md-1"></div>
            </div>
              <?php foreach($plans as $plan): ?>
                <?= $this->Form->create($plan, ['action' => 'edit', 'id' => 'account-form']) ?>
                  <div class="row sub-row my-4">
                    <div class="col-md-4"><?= $this->Form->control('name', ['label'=> false]) ?></div>
                    <div class="col-md-4"><?= $this->Form->control('price', ['label'=> false]) ?></div>
                    <div class="col-md-3"><?= $this->Form->select('billing_interval', $interval_options, ['label'=> false]) ?></div>
                    <div class="col-md-1"><button class="btn btn-primary col-md-offset-1">Edit</button></div>
                  </div>
                <?= $this->Form->end(); ?>
              <?php endforeach; ?>
        </div>
    </div>
</div>

<style>
  .sub-row{
    border-bottom: 1px solid #eee;
  }
  .sub-row:last-child:after{
    border-bottom: 0px solid #eee;
  }
</style>