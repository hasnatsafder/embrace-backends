<?php
/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $claims
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Account Claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <?php if ($this->request->getQuery('source')):?>
            <li class="breadcrumb-item">
                <?= $this->Html->link("Account", $this->request->getQuery('source')); ?>
            </li>
        <?php else: ?>
            <li class="breadcrumb-item">
                <?= $this->Html->link("Accounts", ['controller' => 'Accounts', 'action' => 'user', $user->id]); ?>
            </li>
        <?php endif; ?>
        <li class="breadcrumb-item">Claims</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Claims List for user <span class="text-pink"><?= $user->email ?> </span> and Account <span
                        class="text-pink"><?= $account->name ?> </span></div>
            <?= $this->Html->link('Add', ['action' => 'accountsAdd', $account->id], ['class' => 'btn btn-success']); ?>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="input-group-icon input-group-icon-left mr-3">
                    <span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
                    <input class="form-control form-control-rounded form-control-solid" id="key-search" type="text"
                           placeholder="Search ...">
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="datatable">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th>Date</th>
                        <th>Account</th>
                        <th>Debtor</th>
                        <th>Phase</th>
                        <th>Collector</th>
                        <th>Ref</th>
                        <th>Collector-Ref</th>
                        <th>Hold</th>
                        <th class="no-sort">Action</th>
                        <th class="no-sort">Claim Lines</th>
                        <th class="no-sort">Statuses</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claims as $claim): ?>
                        <tr>
                            <td> <?= date_format($claim->created, "d-M-Y") ?> </td>
                            <td> <?= $claim->account->name; ?> </td>
                            <td>
                                <?php
                                echo $claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name;
                                ?>
                            </td>
                            <td> <?= $claim->claim_phase->order ?> - <?= $claim->claim_phase->name ?> </td>
                            <td>
                                <?= $claim->collector->name; ?>
                            </td>
                            <td>
                                <?= $claim->customer_reference; ?>
                            </td>
                            <td>
                                <?= $claim->collector_reference; ?>
                            </td>
                            <td>
                                <?= $claim->on_hold ? "Yes" : "No" ?>
                            </td>
                            <td>
                                <?= $this->Html->link('Edit',
                                    ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                                    ['class' => 'btn btn-warning']); ?>
                            </td>
                            <td>
                                <?= $this->Html->link('View',
                                    ['controller' => 'ClaimLines', 'action' => 'view', $claim->id],
                                    ['class' => 'btn btn-primary']); ?>
                            </td>
                            <td>
                                <?= $this->Html->link('View',
                                    ['controller' => 'ClaimActions', 'action' => 'view', $claim->id],
                                    ['class' => 'btn btn-success']); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>
