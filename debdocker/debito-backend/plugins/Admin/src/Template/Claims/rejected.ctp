<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claims
 * @var mixed $collector
 * @var mixed $collectors
 */
?>
<div class="page-heading">
    <h1 class="page-title">Rejected claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Rejected claims</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">List of rejected claims</div>
            <div>
                <?= $this->Html->link('All claims', ['action' => 'index'], ['class' => 'btn btn-info']); ?>
            </div>
        </div>
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div class="flexbox">
                    <label class="mb-0 mr-2 ml-4">Collectors:</label>
                    <?= $this->Form->control('collectors',
                        [
                            'options' => $collectors,
                            'value' => $collector,
                            'class' => "selectpicker show-tick",
                            'empty' => true,
                            'label' => false,
                            'data-style' => 'btn-solid',
                            'data-width' => "150px",
                            'empty' => ["" => 'All'],
                        ]) ?>
                    <button class="btn btn-blue btn-sm ml-4" id="filter-button">Filter</button>
                </div>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover" id="datatable">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th width="40%">Rejection Reason</th>
                        <th width="20%">Account</th>
                        <th>Public</th>
                        <th>Collector</th>
                        <th>Ref</th>
                        <th class="no-sort">Actions</th>
                        <th class="no-sort"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claims as $claim): ?>
                        <tr>
                            <td> <?= $claim->rejected_reason; ?> </td>
                            <td>
                                <?= $this->Html->link($claim->account->name,
                                    ['controller' => 'Claims', 'action' => 'accounts', $claim->account_id],
                                    ['class' => 'btn btn-link']);
                                ?>
                            </td>
                            <td> <?= ($claim->rejected_is_public) ? 'Yes' : 'No'; ?> </td>
                            <td>
                                <?= $claim->collector->name ?>
                            </td>
                            <td>
                                <?= $claim->customer_reference; ?>
                            </td>


                            <td>
                                <?= $this->Html->link('Edit Claim',
                                    ['controller' => 'Claims', 'action' => 'edit', $claim->id],
                                    ['class' => 'btn btn-warning']); ?>
                            </td>
                            <td>
                                <?= $this->Html->link('Confirm Rejection',
                                    ['controller' => 'Claims', 'action' => 'confirmRejection', $claim->id],
                                    ['class' => 'btn btn-danger']); ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
            <div class="dataTables_paginate paging_simple_numbers pull-right">
                <ul class="pagination">
                    <?php if ($this->Paginator->numbers()): ?>
                        <?php echo $this->Paginator->prev(__d('Collector', 'Previous')); ?>
                        <?php echo $this->Paginator->numbers(); ?>
                        <?php echo $this->Paginator->next(__d('Collector', 'Next')); ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<script>
    $('#filter-button').click(function () {
        url = window.location.href.split('?')[0];
        var collector = $('#collectors').val();
        if (collector) {
            url += "?"
        }
        if (collector) {
            url += "collector=" + collector
        }
        window.location.assign(url)
    })

</script>