<?php
/**
 * @var \App\View\AppView $this
 * @var string $accountName
 * @var object $claim
 * @var string $debtorName
 */
?>
<?= $this->Html->css('/Admin/vendors/summernote/dist/summernote.css') ?>
<div class="page-heading">
    <h1 class="page-title">Claims Rejection</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Rejected List', ['action' => 'rejected']); ?>
        </li>
        <li class="breadcrumb-item">Confirm Rejection</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Confirm Rejection for <b><?= $claim->customer_reference ?></b></div>
                </div>
                <?= $this->Form->create($claim, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <fieldset>
                        <?php
                        echo $this->Form->control('rejected_reason', ['type' => 'textarea']);
                        ?>
                        <div class="form-group">
                            <label class="control-label">Send Email to customer</label>
                            <select onchange="toggleContentArea(this)" id="send_email" name="send_email" class="form-control form-control-air">
                                <option selected value="false">No</option>
                                <option value="true">Yes</option>
                            </select>
                        </div>
                        <div class="form-group email-area">
                            <hr>
                            <label class="control-label">Email Template</label>
                            <select onchange="toggleContent(this)" id="email_template" name="email_template" class="form-control form-control-air">
                                <option selected value="general_html">General</option>
                                <option value="debtors_bankrupt_html">Bankruptcy</option>
                            </select>
                        </div>
                        <div class="form-group email-area">
                            <label class="control-label">Enter email subject</label>
                            <input type="text" id="email_subject" value="Vi kan desværre ikke køre dit krav" name="email_subject"
                                   class="form-control form-control-air">
                            <span style="display:none;" class="text-danger" id="subject-error">You must provide a value</span>
                        </div>
                        <div class="form-group email-area">
                            <label class="control-label">Enter email body</label>
                            <div id="summernote" data-plugin="summernote" data-air-mode="true">

                            </div>
                        </div>
                        <?php echo $this->Form->textarea('mail_text', ['escape' => false, 'id' => 'mail_text']); ?>
                    </fieldset>
                </div>
                <div class="ibox-footer">
                    <?= $this->Form->button(__('Reject Claim'), ['class' => 'btn btn-primary mr-2', 'type' => 'button', 'id' => 'submit-button']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('/Admin/vendors/summernote/dist/summernote.min.js') ?>
<script>

    var general_html = "Message goes here";
    var debtors_bankrupt_html = 'Din debitor er desværre gået konkurs og det er derfor ikke muligt, at køre en inkassosag.<br><br>';
    debtors_bankrupt_html += '<strong>Vi kan hjælpe med at anmelde dit krav i konkursboet.</strong><br>';
    debtors_bankrupt_html += 'Det betyder at du har mulighed for at få dit krav helt eller delvist dækket, hvis der er penge i konkursboet.<br><br>';
    debtors_bankrupt_html += 'Prisen er <strong>DKK 299,00 + moms</strong><br><br>';
    debtors_bankrupt_html += 'Send os en mail på <a href="mailto:kundeservice@debito.dk?subject=Anmelde krav i konkursbo&body=Hej Debito, %0D%0A %0D%0AJeg vil gerne lade jer anmelde mit krav i konkursboet for <?= $debtorName; ?>.%0D%0A %0D%0AJeg er indforstået med prisen på DKK 299 kr. ex. moms som jeg modtager en faktura på. %0D%0A %0D%0ASagsreference: 36965762-00001 %0D%0A %0D%0AMed venlig hilsen %0D%0A %0D%0A<?= $accountName; ?>" target=_blank"" style="color: #18c5a9">kundeservice@debito.dk</a><br>';
    debtors_bankrupt_html += 'Husk sagsreference: <?= $claim->customer_reference; ?><br><br>';
    debtors_bankrupt_html += 'For at vi kan garantere, at vi kan nå det inden tidsfristen, beder vi dig svare <strong>indenfor 1 uge</strong>.';
    // set templates
    var templates = {
        general: "Message goes here",
        debtors_bankrupt_html: debtors_bankrupt_html

    };
    // end set templates

    $(function () {
        $('.email-area').hide();
        $('#subject-error').hide();
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
        // initializing summer note values
        $('#summernote').summernote('code', templates['general']);
        $("#mail_text").html(general_html);
    });

    $('#submit-button').click(function (e) {
        $('#subject-error').hide();
        if ($('#send_email').val() == "true" && $('#email_subject').val() == "") {
            $('#subject-error').show();
            return;
        }
        $("form").submit();
    });

    function toggleContentArea(select) {
        if ($(select).val() == "true") {
            $('.email-area').show();
        } else {
            $('.email-area').hide();
        }
    }

    function toggleContent(select) {
        $('#summernote').summernote('code', templates[$(select).val()]);
    }

    // summernote.change
    $('#summernote').on('summernote.change', function (we, contents, $editable) {
        $('#mail_text').html(contents);
    });
</script>
<style>
    #mail_text {
        display: none;
    }
</style>
