<?php
/**
 * @var \App\View\AppView $this
 * @var object $account
 * @var mixed $claim
 * @var mixed $collectors
 * @var mixed $deb
 * @var object $user
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="index.html"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link('Users', ['controller' => 'users', 'action' => 'index']); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Accounts", ['controller' => 'Accounts', 'action' => 'user', $user->id]); ?>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Claims", ['action' => 'accounts', $account->id]); ?>
        </li>
        <li class="breadcrumb-item">Add</li>
    </ol>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="ibox-title">Add Claim</div>
                </div>
                <?= $this->Form->create($claim, ['class' => 'form-info']) ?>
                <div class="ibox-body">
                    <?php
                    echo $this->Form->control('debtor_id',
                        [
                            'class' => "show-tick",
                            'options' => $deb,
                        ]);
                    echo $this->Form->control('collector_id',
                        [
                            'options' => $collectors,
                            'class' => "show-tick",
                            'empty' => true,
                        ]); ?>
                    <?= $this->Form->control('dispute'); ?>
                    <?= $this->Form->control('customer_reference', ['type' => 'text', 'placeholder' => 'Auto Generted if empty']); ?>
                    <?= $this->Form->control('collector_reference', ['type' => 'text', 'placeholder' => "Not Required"]); ?>

                </div>
                <div class="ibox-footer">
                    <button type="submit" class="btn btn-primary mr-2" onclick="submitform()">Submit</button>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/select2.full.js') ?>

<script>

    $(document).ready(function () {
        $("select").select2();

        if ($('#collector-id').val() == "") {
            $("#collector-id option:contains('Mortang')").attr('selected', 'selected');
        }
        $('#collector-id').select2();

    });
</script>