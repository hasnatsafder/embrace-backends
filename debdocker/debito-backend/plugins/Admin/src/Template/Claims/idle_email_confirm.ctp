<?php
/**
 * @var \App\View\AppView $this
 * @var string $claim_action_id
 * @var object $collector
 * @var string $emailMessage
 */
?>
<?= $this->Html->css('/Admin/vendors/summernote/dist/summernote.css') ?>
<div class="page-heading">
    <h1 class="page-title">Idle claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <?= $this->Html->link('Idle Claims', ['action' => 'idle']); ?>
        </li>
        <li class="breadcrumb-item">Idle claims Confirm</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Email will be sent to <?= $collector->name ?> on
                <span class="text-success"><?= $collector->email ?></span>
            </div>
        </div>
        <div class="ibox-body">
            <div id="summernote" data-plugin="summernote" data-air-mode="true">
                <?= $emailMessage ?>
            </div>
            <form class="mt-5" method="post" action="/admin/claims/idleEmail/<?php echo $claim_action_id ?>">
                <?php echo $this->Form->textarea('mail_text', ['escape' => false, 'id' => 'mail_text']); ?>
                <button type="submit" class="btn btn-success">Send Email</button>
            </form>
        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/vendors/summernote/dist/summernote.min.js') ?>
<script>
    $(function () {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });

    // summernote.change
    $('#summernote').on('summernote.change', function (we, contents, $editable) {
        $('#mail_text').html(contents);
    });
</script>
<style>
    #mail_text {
        display: none;
    }
</style>