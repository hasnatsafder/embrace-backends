<div class="page-heading">
    <h1 class="page-title">Claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Claims", $back_to_page); ?>
        </li>
        <li class="breadcrumb-item">Edit</li>
    </ol>
</div>

<div class="page-content">
    <?= $this->Flash->render() ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-fullheight">
                <div class="ibox-head">
                    <div class="pull-left">
                        Assinged to :
                        <?php if (isset($claim->admin)) : ?>
                            <button data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= isset($claim->admin) ? $claim->admin->email . " - " . $claim->admin->first_name . " " . $claim->admin->last_name : "" ?>" class="btn btn-primary btn-sm btn-icon-only btn-circle btn-air avatar" id="<?= isset($claim->admin) ? $claim->admin->first_name . " " . $claim->admin->last_name : "" ?>" data-toggle="button">
                            </button>
                        <?php else : ?>
                            N/A
                        <?php endif; ?>
                        <?php if ($claim->claim_phase->order == -1) : ?>
                            <button type="button" class="ml-2 btn btn-danger" data-toggle="modal" data-target="#deleteModel">
                                <?= __("Delete") ?>
                            </button>
                        <?php endif; ?>
                        <?php if ($claim->claim_phase->order == 1) : ?>
                            <?= $this->Html->link(
                                'Validate',
                                ['action' => 'submit', $claim->id, $claim->collector_id, ''],
                                ['id' => 'submit_to_collector_btn', 'class' => 'ml-2 btn btn-success']
                            ); ?>
                        <?php endif; ?>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-outline-info btn-fix btn-rounded btn-thick" data-toggle="modal" data-target="#checkListModel">
                            <span class="btn-icon"><i class="la la-list"></i></i>Checklist</span>
                        </button>
                    </div>
                </div>
                <div class="ibox-body">
                    <h6 class="font-strong mb-4">Claims information</h6>
                    <div class="row">
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Creditor</div>
                            <p class="mt-1"><?= $claim->account->name ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">CVR number</div>
                            <p class="mt-1"><?= $claim->account->vat_number ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Claim reference</div>
                            <p class="mt-1"><?= $claim->customer_reference ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Created</div>
                            <p class="mt-1"><?= date_format($claim->created, "d-m-y") ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Principle amount</div>
                            <p class="mt-1 amount"><?= $principle; ?></p>
                        </div>
                        <div class="col-md text-center">
                            <div class="text-muted">Currency</div>
                            <p class="mt-1"><?= $claim->currency->name ?>(<?= $claim->currency->iso_code ?>)</p>
                        </div>
                        <div class="col-md text-center">
                            <button class="btn btn-outline-info btn-fix btn-rounded" data-toggle="modal" data-target="#editModel">
                                <span class="btn-icon"><i class="la la-pencil"></i></i>Edit</span>
                            </button>
                        </div>
                    </div>
                    <h6 class="font-strong mb-4 mt-4">Claims phase</h6>
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <div class="text-muted">Phase</div>
                            <p class="mt-1"><?= $claim->claim_phase->name ?></p>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="text-muted">Collector</div>
                            <p class="mt-1"><?= $claim->collector->name ?></p>
                        </div>
                        <div class="col-md-6 text-center">
                            <div class="text-muted">Rejected reason</div>
                            <p class="mt-1"><?= $claim->rejected_reason ? $claim->rejected_reason : "-" ?></p>
                        </div>
                    </div>
                    <hr>
                    <!-- Claims Actions area -->
                    <h6 class="font-strong mb-4 mt-4">Claims actions</h6>
                    <table class="table inner-tables">
                        <thead>
                            <tr>
                                <th></th>
                                <th width="15%">Name</th>
                                <th width="15%">Date</th>
                                <th width="10%">Paid</th>
                                <th width="10%">Interest Added</th>
                                <th width="10%">Debt Coll. Fees</th>
                                <th width="10%">Actual Paid</th>
                                <th width="20%">Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($claimActions as $claimAction) : ?>
                                <tr>
                                    <td>
                                        <i data-href="/admin/claim-actions/delete/<?= $claimAction->id ?>" class="fa fa-trash cursor-pointer" data-toggle="modal" data-target="#delete-action" label="Delete action"></i>
                                    </td>
                                    <td> <?= $claimAction->claim_action_type->name ?> </td>
                                    <td> <?= date_format($claimAction->date, "d-M-Y") ?> </td>
                                    <td class="amount"> <?= $claimAction->paid ?> </td>
                                    <td class="amount"> <?= $claimAction->interest_added ?> </td>
                                    <td class="amount"> <?= $claimAction->debt_collection_fees ?> </td>
                                    <td class="amount"> <?= $claimAction->actual_paid ?> </td>
                                    <td> <?= $claimAction->message ?> </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <form method="post" id="action-form" action="/admin/claim-actions/save/<?= $claim->id ?>" class="mt-4 form-success">
                        <div class="row">
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->control(
                                    'claim_action_type_id',
                                    [
                                        'options' => $allClaimActions,
                                    ]
                                );
                                ?>
                            </div>
                            <div class="col-md-4">
                                <label for="ex-pass">Action Date</label>
                                <input value="<?= date('Y-m-d') ?>" class="form-control mb-2 mr-sm-2 mb-sm-0" name="date" id="action-date" type="text">
                            </div>
                            <div class="col-md-12" id="message-area">
                                <label for="ex-pass">Internal note</label>
                                <?= $this->Form->textarea('message', ['type' => 'text', 'maxlength' => 1000]); ?>
                            </div>
                            <div class="col-md">
                                <button class="btn btn-info pull-right mt-4" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <h6 class="font-strong mb-4 mt-4">Debtors detail</h6>
                    <div class="row">
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Debtor</div>
                            <p class="mt-1"><?= $claim->debtor->company_name ? $claim->debtor->company_name : ($claim->debtor->first_name . " " . $claim->debtor->last_name) ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">CVR number</div>
                            <p class="mt-1"><?= $claim->debtor->vat_number ? $claim->debtor->vat_number : "-" ?></p>
                        </div>
                        <div class="col-md text-center">
                            <div class="text-muted">Address</div>
                            <p class="mt-1"><?= $claim->debtor->address ?></p>
                        </div>
                        <div class="col-md text-center">
                            <div class="text-muted">City</div>
                            <p class="mt-1"><?= $claim->debtor->city ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Post code</div>
                            <p class="mt-1"><?= $claim->debtor->zip_code ?></p>
                        </div>
                        <div class="col-md text-center" style="border-right: 1px solid rgba(0,0,0,.1);">
                            <div class="text-muted">Phone</div>
                            <p class="mt-1"><?= $claim->debtor->phone ?></p>
                        </div>
                        <div class="col-md text-center">
                            <div class="text-muted">Email</div>
                            <p class="mt-1"><?= $claim->debtor->email ?></p>
                        </div>
                        <div class="col-md text-center">
                            <?= $this->Html->link(
                                '<span class="btn-icon"><i class="la la-pencil"></i></i>Edit</span>',
                                ['controller' => 'Debtors', 'action' => 'edit', $claim->debtor->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                ['escape' => false, 'class' => 'btn btn-outline-info btn-fix btn-rounded']
                            );
                            ?>
                        </div>
                    </div>
                    <hr>
                    <h6 class="font-strong mb-4 mt-4">Financial information</h6>
                    <b>Will be done after we start debt collection</b>
                    <hr>
                    <h6 class="font-strong mb-4 mt-4">Claim lines</h6>
                    <div class="row">
                        <div class="col-md">
                            <?= $this->Html->link('Desync All',
                                ['controller' => 'ClaimLines', 'action' => 'changeSyncStatusAll', $claim->id, 0],
                                ['class' => 'pull-right btn btn-outline-info btn-fix btn-rounded']); ?>
                            <?= $this->Html->link('Sync all',
                                ['controller' => 'ClaimLines', 'action' => 'changeSyncStatusAll', $claim->id, 1],
                                ['class' => 'pull-right btn btn-outline-info btn-fix btn-rounded mr-2']); ?>
                            <?= $this->Html->link(
                                '<i class="la la-upload"></i> Add claim line',
                                ['controller' => 'ClaimLines', 'action' => 'add', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                ['escape' => false, 'class' => 'pull-right btn btn-outline-info btn-fix btn-rounded mr-2']
                            );
                            ?>
                            <button data-toggle="modal" data-target="#all-file-modal" class="pull-right btn btn-outline-info btn-rounded mr-2">View all</button>
                        </div>
                    </div>
                    <table class="table inner-tables">
                        <thead>
                            <tr>
                                <th width="15%">Invoice date</th>
                                <th width="15%">Type</th>
                                <th width="15%">Reference</th>
                                <th width="10%">Amount</th>
                                <th width="10%">Due date</th>
                                <th class="no-sort" width="10%">File</th>
                                <th width="10%">Sync</th>
                                <th class="no-sort" width="10%">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($claimLines as $claimLine) : ?>
                                <tr>
                                    <td> <?= date_format($claimLine->invoice_date, "d-M-Y") ?> </td>
                                    <td> <?= $claimLine->claim_line_type->name ?> </td>
                                    <td> <?= $claimLine->invoice_reference ?> </td>
                                    <td class="amount"> <?= $claimLine->amount ?> </td>
                                    <td> <?= $claimLine->maturity_date ? date_format($claimLine->maturity_date, "d-M-Y"): "" ?> </td>
                                    <td>
                                        <?php if ($claimLine->file_mime_type == "image/jpeg" || $claimLine->file_mime_type == "image/jpg" || $claimLine->file_mime_type == "image/png" || $claimLine->file_mime_type == "image/tif" || $claimLine->file_mime_type == 'application/pdf') : ?>
                                            <a href="javascript:;" onClick='window.open("/webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>","", "width=800,height=600")' class="btn btn-outline-info btn-rounded mr-2">
                                                View
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(($claimLine->synced ? 'Desync' : 'Sync'),
                                            ['controller' => 'ClaimLines', 'action' => 'changeSyncStatus', $claimLine->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                            ['class' => 'btn btn-rounded mr-2 btn-outline-' . ($claimLine->synced ? 'warning' : 'success')]
                                        ); ?>
                                    </td>
                                    <td>
                                        <?= $this->Html->link(
                                            '<span class="btn-icon"><i class="la la-pencil"></i></i>Edit</span>',
                                            ['controller' => 'ClaimLines', 'action' => 'edit', $claimLine->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                            ['escape' => false, 'class' => 'btn btn-outline-info btn-rounded']
                                        );
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this Claim?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Html->link(
                    'Delete',
                    ['action' => 'delete', $claim->id],
                    ['class' => 'btn btn-danger']
                ); ?>
            </div>
        </div>
    </div>
</div>

<!-- Checklist Modal -->
<div class="modal fade" id="checkListModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Checklist of claims") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group list-group-full list-group-divider">
                    <li class="list-group-item active">Active item</li>
                    <li class="list-group-item flexbox">List Group Item
                        <span class="badge badge-primary badge-circle">4</span>
                    </li>
                    <li class="list-group-item">List Group Item</li>
                    <li class="list-group-item disabled flexbox">Disabled item
                        <span class="badge badge-danger badge-pill">HOT</span>
                    </li>
                    <li class="list-group-item flexbox">List Group Item
                        <span class="badge badge-info">3</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Modal for delete claim actions -->
<div class="modal fade" id="delete-action" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Delete Confirmation") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= __("Are you sure you want to delete this action?") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="single-file-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <img id="file-image" src="">
            <embed type="application/pdf" id="file-pdf" width="800" height="800" src="">
        </div>
    </div>
</div>

<div class="modal fade" id="all-file-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php
                    $counter = 0;
                    foreach ($claimLines as $claimLine) :
                        $counter++;
                        if ($counter == 1) :
                            echo '<div class="carousel-item active">';
                        else :
                            echo '<div class="carousel-item">';
                        endif;
                    ?>
                        <?php if ($claimLine->file_mime_type == 'application/pdf') : ?>
                            <embed width="800" class="file-all" height="800" src="/webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>" />
                        <?php else : ?>
                            <img class="file-all" src="/webroot/files/ClaimLines/file_name/<?php echo $claimLine->file_name; ?>" />
                        <?php endif; ?>
                </div>
            <?php endforeach; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
</div>
<!-- Include other modals -->
<!-- Modal -->
<div class="modal fade" id="editModel" role="dialog" aria-hidden="true">
    <?= $this->Form->create($claim, ['url' => "/admin/claims/edit/$claim->id", 'class' => 'form-info']) ?>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?= __("Edit information") ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->control(
                            'collector_id',
                            [
                                'options' => $collectors,
                                'value' => $claim->collector_id,
                            ]
                        );
                        echo $this->Form->control(
                            'claim_phase_id',
                            [
                                'options' => $claimPhases,
                                'value' => $claim->claim_phase_id,
                            ]
                        );
                        echo $this->Form->control(
                            'on_hold',
                            [
                                'options' => [0 => "No", 1 => "Yes"],
                                'value' => $claim->on_hold,
                            ]
                        );
                        echo $this->Form->control(
                            'currency_id',
                            [
                                'options' => $currencies,
                                'value' => $claim->currency_id,
                                'empty' => true,
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        echo $this->Form->control('customer_reference');
                        echo $this->Form->control('collector_reference');
                        echo $this->Form->control('rejected_reason');
                        echo $this->Form->control(
                            'rejected_is_public',
                            [
                                'options' => [0 => "No", 1 => "Yes"],
                                'value' => $claim->rejected_is_public,
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-md-12">
                        <?php echo $this->Form->control('dispute'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= __("Close") ?></button>
                <?= $this->Form->button(__('Save Claim'), ['class' => 'btn btn-primary mr-2']) ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

<style>
    #editModel .select2-container {
        display: block;
        width: 100% !important;
    }
</style>

<script>
    $(document).ready(function() {

        numeral.locale("da-dk");

        $('#currency-id').select2({
            dropdownParent: $('#editModel  .modal-content'),
            display: '100%',
        });
        $('.amount').each(function(i, obj) {
            var number_original = $(this).html().toString();
            number_original = number_original.trim()
            number_original = number_original.toString().splice(-2, 0, ".");
            number_original = parseFloat(number_original);
            var number = numeral(number_original).format("$0,0.00");
            $(this).html(number)
        });

        $("#action-date").datepicker({
            format: "yyyy-mm-dd",
            defaultDate: new Date(),
            autoclose: true,
        });

        $("select").select2();

        if ($('.inner-tables').DataTable) {
            $('.inner-tables').dataTable({
                "pageLength": 4,
                "searching": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
            });
        }

        // delete claim action
        $('#delete-action').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });

    });

    function openFileModal(path, type) {
        let src_image = $('#file-image');
        let src_pdf = $('#file-pdf');
        src_image.hide()
        src_pdf.hide()
        if (type == "image") {
            src_image.show();
            src_image.attr('src', "/webroot/files/ClaimLines/file_name/" + path)
        } else {
            src_pdf.show()
            src_pdf.attr('src', "/webroot/files/ClaimLines/file_name/" + path)
        }
        $('#single-file-modal').modal('toggle');
    }
</script>

<style>
    .dataTables_info {
        display: none;
    }
</style>
