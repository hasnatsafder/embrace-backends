<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $claimPhases
 * @var mixed $claims
 * @var mixed $collector
 * @var mixed $collectors
 * @var string $debito_reference
 * @var mixed $status
 */
?>
<div class="page-heading">
    <h1 class="page-title">Claims</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Claims</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <div class="ibox-body">
            <?= $this->Flash->render() ?>
            <div class="flexbox mb-4">
            <?= $this->Form->create(null, ['valueSources' => 'query', 'id' => 'search-form', 'class' => 'form-inline form-success']) ?>
                <div class="btn-group">
                    <?php foreach($claimPhases as $claimPhase):
                        $class = $status == $claimPhase->id ? "btn-info" : "btn-outline-info";
                        echo "<div class='btn $class' onClick='setClaimPhase(`$claimPhase->id`)'>$claimPhase->name</div> ";
                    endforeach;
                    ?>
                    <div id="clear-button" class="btn btn-success ml-4">Clear Filter</div>
                </div>
                <div class="form-group mt-2">
                    <?= $this->Form->control('claimsSearch', [
                            'type' => 'text',
                            'placeholder' => __('Press enter to search (case sensitive)'),
                            'label' => false,
                            'id' => 'search-input',
                            'class' => 'form-control mr-sm-2 mb-sm-0',
                            'style' => 'min-width: 700px;',
                    ]) ?>
                    <label class="mb-0 mr-2 ml-2">Sort:</label>
                    <?= $this->Form->select(
                        'sortBy',
                        [
                            'recent' => 'Recent', 'oldest' => 'Oldest'],
                        [
                        'class' => 'form-control mb-2 mr-sm-2 mb-sm-0',
                        'id' => 'sort-by'
                        ]
                    );?>
                </div>
                <input value="<?= $status ?>" type="hidden" id="claim-status-hidden" name="status">
            <?= $this->Form->end(); ?>
            </div>
            <div class="table-responsive row">
                <table class="table table-bordered table-hover table-responsive" id="datatable">
                    <thead class="thead-default thead-lg">
                    <tr>
                        <th class="no-sort">Account</th>
                        <th class="no-sort">Ref</th>
                        <th class="no-sort">Date</th>
                        <th class="no-sort">Phase</th>
                        <th class="no-sort">Debtor</th>
                        <th class="no-sort">Collector</th>
                        <th class="no-sort">Currency</th>
                        <th class="no-sort">Admin</th>
                        <th class="no-sort">Preview</th>
                        <th class="no-sort">Handle</th>
                        <th class="no-sort">Merge</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($claims as $claim): ?>
                        <tr class="<?= $claim->on_hold ? 'warning' : '' ?>">
                            <td>
                                <?= $this->Html->link(substr($claim->account->name, 0, 15),
                                    ['controller' => 'Claims', 'action' => 'accounts', $claim->account_id],
                                    ['class' => 'btn btn-link']);
                                ?>
                            </td>
                            <td>
                                <?= $claim->customer_reference; ?> <?= $claim->collector_reference ? "<br>cref- " .  $claim->collector_reference : "" ?>
                            </td>
                            <td> <?= date_format($claim->created, "d M Y") ?> </td>
                            <td>
                                <?php if ($claim->claim_phase->order === -1): ?>
                                    <button class="btn btn-link" data-container="body" data-toggle="popover" data-trigger="hover" data-placement="left"
                                            data-content="<?= $claim->rejected_reason ?>" data-original-title="" title="">
                                        <?= $claim->claim_phase->name ?>
                                    </button>
                                <?php else: ?>
                                    <?= $claim->claim_phase->name ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php
                                echo substr($claim->debtor->company_name ? $claim->debtor->company_name : $claim->debtor->first_name . ' ' . $claim->debtor->last_name, 0, 15);
                                ?>
                            </td>
                            <td>
                                <?= $claim->collector->name; ?>
                            </td>
                            <td>
                                <?= $claim->currency->iso_code; ?>
                            </td>
                            <td>
                                <?php if (isset($claim->admin)):?>
                                <button data-toggle="tooltip" data-placement="top" title="" data-original-title="<?= isset($claim->admin) ? $claim->admin->email . " - " . $claim->admin->first_name . " " . $claim->admin->last_name : "" ?>" class="btn btn-primary btn-icon-only btn-circle btn-air avatar" id="<?= isset($claim->admin) ? $claim->admin->first_name . " " . $claim->admin->last_name : "" ?>" data-toggle="button">
                                </button>
                                <?php else: ?>
                                N/A
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (isset($claim->admin) && $claim->admin->id != $auth_user_id):
                                echo $this->Html->link('View',
                                    ['controller' => 'Claims', 'action' => 'edit', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                    ['class' => 'btn btn-info btn-sm']); 
                                else:
                                echo $this->Html->link('Edit',
                                    ['controller' => 'Claims', 'action' => 'edit', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                    ['class' => 'btn btn-info btn-sm']);
                                endif; 
                                ?>
                            </td>
                            <td>
                                <?= $this->Html->link('Handle',
                                    ['controller' => 'Claims', 'action' => 'handle', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                    ['class' => 'btn btn-info btn-sm']); ?>
                            </td>
                            <td>
                                <?php if ($claim->claim_phase->order <= 2):
                                    echo $this->Html->link('Merge',
                                        ['controller' => 'Debtors', 'action' => 'claims', $claim->id, 'redirect' => $this->Url->build($this->request->here(), true)],
                                        ['class' => 'btn btn-link']); 
                                endif; ?>   
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>


            </div>
            <div class="dataTables_info">
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
            <div class="dataTables_paginate paging_simple_numbers pull-right">
                <ul class="pagination">
                    <?php if ($this->Paginator->numbers()): ?>
                        <?php echo $this->Paginator->prev(__d('Collector', 'Previous')); ?>
                        <?php echo $this->Paginator->numbers(); ?>
                        <?php echo $this->Paginator->next(__d('Collector', 'Next')); ?>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    </div>
</div>

<?= $this->Html->script('/Admin/js/vendors/bootstrap-select.js') ?>
<?= $this->Html->script('/Admin/js/vendors/datatables.min.js') ?>
<script>
    
    function setClaimPhase(id) {
        $('#search-form').find('#claim-status-hidden').val(id)
        $('#search-form').submit()
    }

    // submit form on enter
    $('#search-input').keypress(function(e) {
        if (e.which == 13) {
            $('#search-form').submit();
        }
    });
    $('#search-input').change(function() {
        $('#search-form').submit();
    })

    $('#sort-by').change(function() {
        $('#search-form').submit();
    })

    $('#clear-button').click(function () {
        url = window.location.href.split('?')[0];
        window.location.assign(url)
    })

</script>
<style>
    #datatable_paginate {
        display: none;
    }
</style>