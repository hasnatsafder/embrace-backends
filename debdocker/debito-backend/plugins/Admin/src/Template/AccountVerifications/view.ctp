<?php
/**
 * @var \App\View\AppView $this
 * @var object $account_verifications
 */
?>
<div class="page-heading">
    <h1 class="page-title">Account verification</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="/admin"><i class="la la-home font-20"></i></a>
        </li>
        <li class="breadcrumb-item">
            <?= $this->Html->link("Account", ['controller' => 'Accounts', 'action' => "editUserAccount", $account->id]); ?>
        </li>
        <li class="breadcrumb-item">Account verification</li>
    </ol>
</div>

<div class="page-content">
    <div class="ibox">
        <?= $this->Flash->render() ?>
        <div class="ibox-head">
            <div class="ibox-title">Account verification for <span class="text-success"><?= $account->name ?></span></div>
            <div>
                <?= $this->Html->link(
                    'Approve all',
                    ['controller' => 'AccountVerifications', 'action' => 'approveAll', $account->id . "/" . 1],
                    ['class' => 'btn btn-success btn-sm']
                ); ?>
                <?= $this->Html->link(
                    'Unapprove all',
                    ['controller' => 'AccountVerifications', 'action' => 'approveAll', $account->id . "/" . 0],
                    ['class' => 'btn btn-warning btn-sm']
                ); ?>
            </div>
        </div>
        <div class="ibox-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>City</th>
                    <th>Zip</th>
                    <th>Share</th>
                    <th>Documents</th>
                    <th>Filled</th>
                    <th>Approved</th>
                    <th>Action</th>
                    <th>Address</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($account_verifications as $account_verification): ?>
                        <tr>
                            <td> <?= $account_verification->name; ?> </td>
                            <td> <?= $account_verification->email; ?> </td>
                            <td> <?= $account_verification->user_type; ?> </td>
                            <td> <?= $account_verification->city; ?> </td>
                            <td> <?= $account_verification->zip_code; ?> </td>
                            <td> <?= $account_verification->share_percent; ?> %</td>
                            <td>
                                <?php foreach ($account_verification->documents as $document): ?>
                                    <?php if ($document->file_mime_type == "image/jpeg" || $document->file_mime_type == "image/jpg" || $document->file_mime_type == "image/png" || $document->file_mime_type == "image/tif" || $document->file_mime_type == 'application/pdf') : ?>
                                        <a href="javascript:;" onClick='window.open("/webroot/files/Documents/file_name/<?php echo $document->file_name; ?>","", "width=800,height=600")' class="btn btn-link file-buttons">
                                            <?= ucwords(str_replace("_", " ", $document->type)); ?>
                                        </a><br>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <?= $account_verification->filled ? \Cake\I18n\FrozenTime::parseDate($account_verification->filled)->toFormattedDateString() : "-"; ?>
                            </td>
                            <td>
                                <?= $account_verification->approved ? ("Approved on " . \Cake\I18n\FrozenTime::parseDate($account_verification->approved)->toFormattedDateString()) : ("Not approved") . "<br>"; ?>
                            </td>
                            <td>
                                <?= $this->Html->link(
                                    $account_verification->approved ? "Unapprove" : "Approve",
                                    "/admin/account-verifications/approve/" . $account_verification->id . "/" . ($account_verification->approved ? "0" : "1"),
                                    ['class' => 'btn btn-blue btn-sm']
                                ); ?>
                            </td>
                            <td> <?= $account_verification->address; ?> </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
</script>

<style>
    /*.file-buttons {*/
    /*    margin-bottom: 5px;*/
    /*}*/
    /*.file-buttons:last-child {*/
    /*    margin-bottom: unset;*/
    /*}*/
</style>
