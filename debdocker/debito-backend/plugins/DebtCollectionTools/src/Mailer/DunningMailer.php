<?php
declare(strict_types=1);

namespace DebtCollectionTools\Mailer;

use App\Model\Entity\Claim;
use App\Model\Entity\ClaimAction;
use App\Model\Entity\Debtor;
use App\Model\Entity\Account;
use App\Utility\HelperFunctions;
use Cake\I18n\FrozenDate;
use Cake\Mailer\Mailer;
use WyriHaximus\FlyPie\FilesystemsTrait;
use Cake\ORM\TableRegistry;

/**
 * Class DunningMailer
 *
 * @package DebtCollectionTools\Mailer
 */
class DunningMailer extends Mailer
{
    use FilesystemsTrait;

    /**
     * Send soft warning email about claim before it goes into real dunning process with fees
     *
     * @param \App\Model\Entity\Debtor $debtor
     * @param \App\Model\Entity\Claim $claim
     * @param \App\Model\Entity\ClaimAction $claimAction
     * @param string $softWarningPdfFilePath
     */
    public function softWarning(Debtor $debtor, Claim $claim, ClaimAction $claimAction, Account $account, string $softWarningPdfFilePath): void
    {
        $currency =  TableRegistry::getTableLocator()->get('currencies')->get($claim->currency_id);
        $total = HelperFunctions::getTotalClaimSum($claim);
        $total = HelperFunctions::converToCurrency($total, $currency->iso_code);
        $date = FrozenDate::today()->format('d-m-y');
        $email = $this
        ->setTransport('sparkpost')
        ->setFrom(['kommunikation@debito.dk' => 'Debito'])
        ->setTo($debtor->email, $debtor->full_name)
        //todo internal debt collection
        ->setSubject('Venlig påmindelse fra ' . $account->name)
        ->setViewVars(
            compact('debtor', 'claim', 'claimAction', 'account', 'total', 'date', 'currency')
        )
        ->emailFormat('html')
        ->attachments([
            'Venlig_påmindlese_' . $account->name . '.pdf' => [
                'file' => $softWarningPdfFilePath,
                'mimetype' => 'application/pdf',
            ],
        ]);
        $email->viewBuilder()->setTemplate('DebtCollectionTools.softwarning');
    }

    /**
     * Send Warning 1,2 or 3 PDFs to Debtor
     *
     * @param \App\Model\Entity\Debtor $debtor
     * @param \App\Model\Entity\Claim $claim
     * @param \App\Model\Entity\ClaimAction $claimAction
     * @param \App\Model\Entity\Account $account
     * @param string $warningPdfFilePath
     * @param int $warning_number
     */
    public function warning(Debtor $debtor, Claim $claim, ClaimAction $claimAction, Account $account, string $warningPdfFilePath, int $warning_number = 1): void
    {
        $currency =  TableRegistry::getTableLocator()->get('currencies')->get($claim->currency_id);
        $total = HelperFunctions::getTotalClaimSum($claim);
        $total = HelperFunctions::converToCurrency($total, $currency->iso_code);
        $date = FrozenDate::today()->format('d-m-y');
        $email = $this
            ->setTransport('sparkpost')
            ->setFrom(['kommunikation@debito.dk' => 'Debito'])
            ->setTo($debtor->email, $debtor->full_name)
            ->setSubject($this->get_warning_subject_text($warning_number, $account->name))
            ->emailFormat('html')
            ->setViewVars(
                compact('debtor', 'claim', 'claimAction', 'account', 'total', 'date', 'currency')
            )
            ->attachments([
                'betalingspåmindelse_' . $account->name . '.pdf' => [
                    'file' => $warningPdfFilePath,
                    'mimetype' => 'application/pdf',
                ],
            ]);
        $email->viewBuilder()->setTemplate('DebtCollectionTools.warning_' . $warning_number);
    }

    /**
     * Get warning subject text
     * @param int $warning_number
     * @param string $account_name
     * @return string
     */
    private function get_warning_subject_text(int $warning_number, string $account_name): string {
        $subject_array = [
            1 => 'Første betalingspåmindelse fra ' . $account_name,
            2 => 'Anden betalingspåmindelse fra ' . $account_name,
            3 => 'Tredje betalingspåmindelse fra ' . $account_name
        ];
        return $subject_array[$warning_number] ?? $subject_array[1];
    }
}
