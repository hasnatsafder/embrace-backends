<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

use Cake\I18n\FrozenTime;
use Psr\Log\LogLevel;

/**
 * Class StartReminderPhaseAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class StartReminderPhaseAdapter extends BaseAdapter
{
    public function _handle(): void
    {
        $this->createNextClaimActionEntity();
    }

    /**
     * Creates the next ClaimAction entity depending on the DunningConfiguration for the claim
     */
    private function createNextClaimActionEntity(): void
    {
        $dunningConfiguration = $this->getDunningConfigurationForClaim($this->getClaim());
        $nextActionTypeIdentifier = $dunningConfiguration->warning_two_freeze === null ? 'add_warning_of_debt_collection' : 'send_warning_one';

        $this->log(
            sprintf('Planning next claim action: "%s', $nextActionTypeIdentifier),
            LogLevel::DEBUG
        );

        $date = new FrozenTime();
        if ($nextActionTypeIdentifier == "send_warning_one" && $this->getClaimLine()->maturity_date->wasWithinLast('2 days')) {
            $date = $this->getClaimLine()->maturity_date->addDays(2);
        }

        $this->createClaimActionEntity([
            'user_id' => $this->getClaimAction()->user_id,
            'claim_id' => $this->getClaimAction()->claim_id,
            'claim_line_id' => $this->getClaimAction()->claim_line_id,
            'claim_action_type_id' => $this->getClaimActionTypeIdFromIdentifier($nextActionTypeIdentifier),
            'date' => $date,
            'done' => null,
        ]);
    }
}
