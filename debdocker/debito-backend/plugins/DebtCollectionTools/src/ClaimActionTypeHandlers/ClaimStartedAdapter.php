<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

use App\Model\Entity\Claim;
use Cake\I18n\FrozenTime;
use Psr\Log\LogLevel;

/**
 * Class ClaimStartedAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class ClaimStartedAdapter extends BaseAdapter
{
    public function _handle(): void
    {
        if ( $this->ensureClaimHasDunningConfiguration($this->getClaim())) {
            $this->createNextClaimActionEntity();
        }
    }

    /**
     * Make sure we create a DunningConfiguration if none exists already, and that one is attached to the Claim
     *
     * @param \App\Model\Entity\Claim $claim
     */
    private function ensureClaimHasDunningConfiguration(Claim $claim): bool
    {
        if ($claim->dunning_configuration_id !== null) {
            return true;
        }
        else {
            return false;
        }
        $claimsTable = $this->getTableLocator()->get('Claims');

        $dunningConfiguration = $claimsTable->DunningConfigurations->findOrCreate([
            'account_id' => $this->getAccount()->id,
        ]);

        $claim->dunning_configuration_id = $dunningConfiguration->id;

        $claimsTable->saveOrFail($claim);

        $this->setClaim($claim);
        return true;
    }

    /**
     * Creates the next ClaimAction entity depending on the DunningConfiguration for the claim
     *
     * @return void
     */
    private function createNextClaimActionEntity(): void
    {
        $dunningConfiguration = $this->getDunningConfigurationForClaim($this->getClaim());
        $nextClaimActionIdentifier = $dunningConfiguration->soft_reminder_freeze !== null ? 'send_soft_warning' : 'start_reminder_phase';

        $this->log(
            sprintf('Planning next claim action: "%s', $nextClaimActionIdentifier),
            LogLevel::DEBUG
        );

        $this->createClaimActionEntity([
            'user_id' => $this->getClaimAction()->user_id,
            'claim_id' => $this->getClaimAction()->claim_id,
            'claim_line_id' => $this->getClaimAction()->claim_line_id,
            'claim_action_type_id' => $this->getClaimActionTypeIdFromIdentifier($nextClaimActionIdentifier),
            'date' => new FrozenTime(),
            'done' => null,
        ]);
    }
}

