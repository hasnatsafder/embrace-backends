<?php
declare(strict_types=1);

namespace DebtCollectionTools\ClaimActionTypeHandlers;

use App\Model\Entity\Notification;

/**
 * Class ExternalNoteAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class ExternalNoteAdapter extends BaseAdapter
{
    public function _handle(): void
    {
        $this->createNotificationForCreditor();
    }

    /**
     * Creates notification for all users in account
     */
    private function createNotificationForCreditor(): void
    {
        /** @var \App\Model\Table\NotificationsTable $notificationsTable */
        $notificationsTable = $this->getTableLocator()->get('Notifications');

        $notificationsTable->sendNotificationToAccount(
            $this->getAccount(),
            [
                'template' => Notification::TEMPLATE_CLAIM_ACTION_EXTERNAL_NOTE,
                'data_values' => [
                    'claimActionId' => $this->getClaimAction()->id,
                ],
            ]
        );
    }
}

