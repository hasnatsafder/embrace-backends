<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

/**
 * Class SendWarningOneAdapter
 *
 * @package DebtCollectionTools\ClaimActionTypeHandlers
 */
class SendWarningOneAdapter extends SendWarningAbstractAdapter
{
    protected $warningNumber = 1;
}

