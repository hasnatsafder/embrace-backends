<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;

use Psr\Log\LogLevel;

class SendSoftWarningTextMessageAdapter extends BaseAdapter
{
    /**
     * @todo internal debt collection Send notification text message to debtor about the soft-warning that has been sent with email
     */
    public function _handle(): void
    {
        $this->log(
            'NOT IMPLEMENTED YET: Send text message notification to debtor about warning email',
            LogLevel::DEBUG
        );
    }
}

