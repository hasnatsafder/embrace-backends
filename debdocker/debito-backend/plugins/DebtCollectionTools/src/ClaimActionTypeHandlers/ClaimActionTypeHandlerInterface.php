<?php
declare(strict_types=1);


namespace DebtCollectionTools\ClaimActionTypeHandlers;


interface ClaimActionTypeHandlerInterface
{
    public function _handle(): void;
}