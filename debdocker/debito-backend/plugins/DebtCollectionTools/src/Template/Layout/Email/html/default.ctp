<?php
declare(strict_types=1);

?>

<div class="emailContainer">
    <?= $this->fetch('content') ?>
</div>
