<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Poppins';
                font-style: normal;
                font-weight: 400;
                src: local('Poppins Regular'), local('Poppins-Regular'), url('https://fonts.gstatic.com/s/poppins/v9/pxiEyp8kv8JHgFVrJJnedA.woff') format('woff'), url('https://fonts.gstatic.com/s/poppins/v9/pxiEyp8kv8JHgFVrJJnecg.woff2') format('woff2');
            }
            @font-face {
                font-family: 'Poppins';
                font-style: normal;
                font-weight: 500;
                src: local('Poppins Medium'), local('Poppins-Medium'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLGT9Z1JlEw.woff') format('woff'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLGT9Z1JlFQ.woff2') format('woff2');
            }
            @font-face {
                font-family: 'Poppins';
                font-style: normal;
                font-weight: 700;
                src: local('Poppins Bold'), local('Poppins-Bold'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLCz7Z1JlEw.woff') format('woff'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLCz7Z1JlFQ.woff2') format('woff2');
            }
            @font-face {
                font-family: 'Poppins';
                font-style: normal;
                font-weight: 800;
                src: local('Poppins ExtraBold'), local('Poppins-ExtraBold'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLDD4Z1JlEw.woff') format('woff'), url('https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLDD4Z1JlFQ.woff2') format('woff2');
            }
        }

        #outlook a {
            padding: 0;
        }

        .ReadMsgBody,
        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass td,
        .ExternalClass div,
        .ExternalClass span,
        .ExternalClass font {
            line-height: 100%;
        }

        div[style*="margin: 14px 0"],
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        table,
        td {
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

        table,
        tr,
        td {
            border-collapse: collapse;
        }

        body,
        td,
        th,
        p,
        div,
        li,
        a,
        span {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            mso-line-height-rule: exactly;
        }

        img {
            border: 0;
            outline: none;
            line-height: 100%;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100% !important;
            -webkit-font-smoothing: antialiased;
        }

        .pc-gmail-fix {
            display: none;
            display: none !important;
        }

        @media screen and (min-width: 621px) {
            .pc-email-container {
                width: 620px !important;
            }
        }

        @media screen and (max-width:620px) {
            .pc-sm-p-20 {
                padding: 20px !important
            }
            .pc-sm-p-30-20 {
                padding: 30px 20px !important
            }
            .pc-sm-fs-30 {
                font-size: 30px !important
            }
            .pc-sm-p-35-10-30 {
                padding: 35px 10px 30px !important
            }
            .pc-sm-mw-50pc {
                max-width: 50% !important
            }
            .pc-sm-p-38-30-40 {
                padding: 38px 30px 40px !important
            }
        }

        @media screen and (max-width:525px) {
            .pc-xs-p-10 {
                padding: 10px !important
            }
            .pc-xs-p-25-10 {
                padding: 25px 10px !important
            }
            .pc-xs-br-disabled br {
                display: none !important
            }
            .pc-xs-p-25-0-20 {
                padding: 25px 0 20px !important
            }
            .pc-xs-mw-100pc {
                max-width: 100% !important
            }
            .pc-xs-p-25-20 {
                padding: 25px 20px !important
            }
            .pc-xs-fs-14 {
                font-size: 14px !important
            }
        }
    </style>
    <!--[if mso]>
    <style type="text/css">
        .pc-fb-font {
            font-family: Helvetica, Arial, sans-serif !important;
        }
    </style>
    <![endif]-->
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
</head>
<body style="width: 100% !important; margin: 0; padding: 0; mso-line-height-rule: exactly; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background-color: #f4f4f4" class="">

<table class="pc-email-body" width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation" style="table-layout: fixed;">
    <tbody>
    <tr>
        <td class="pc-email-body-inner" align="center" valign="top">
            <!--[if gte mso 9]>
            <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
                <v:fill type="tile" src="" color="#f4f4f4"/>
            </v:background>
            <![endif]-->
            <!--[if (gte mso 9)|(IE)]><table width="620" align="center" border="0" cellspacing="0" cellpadding="0" role="presentation"><tr><td width="620" align="center" valign="top"><![endif]-->
            <table class="pc-email-container" width="100%" align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="margin: 0 auto; max-width: 620px;">
                <tbody>
                <tr>
                    <td align="left" valign="top" style="padding: 0 10px;">
<!--                        <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">-->
<!--                            <tbody>-->
<!--                            <tr>-->
<!--                                <td height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>-->
<!--                            </tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
                        <!-- BEGIN MODULE: Menu 6 -->
<!--                        <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">-->
<!--                            <tbody>-->
<!--                            <tr>-->
<!--                                <td class="pc-sm-p-20 pc-xs-p-10" bgcolor="#ffffff" valign="top" style="padding: 25px 30px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);">-->
<!--                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">-->
<!--                                        <tbody>-->
<!--                                        <tr>-->
<!--                                            <td align="center" valign="top" style="padding: 10px;">-->
<!--                                                <a href="https://debito.dk">-->
<!--                                                    <img class="w320" src="https://res.cloudinary.com/debito/image/upload/v1536241685/logo_web.png" alt="Debito">-->
<!--                                                </a>-->
<!--                                            </td>-->
<!--                                        </tr>-->
<!--                                        </tbody>-->
<!--                                    </table>-->
<!--                                </td>-->
<!--                            </tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
                        <!-- END MODULE: Menu 6 -->
                        <!-- BEGIN MODULE: Transactional 26 -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                            <tbody>
                            <tr>
                                <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                            <tbody>
                            <tr>
                                <td class="pc-sm-p-30-20 pc-xs-p-25-10" bgcolor="#ffffff" style="padding: 40px 30px; background: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                        <tbody>
                                        <tr>
                                            <td class="pc-fb-font" style="line-height: 20px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 500; padding: 0 10px" valign="top">
                                                Sagsreference: <span style="color: #18c5a9 "><?= $claim->customer_reference ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="line-height: 1px; font-size: 1px;" height="15">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="line-height: 1px; font-size: 1px;" height="25">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="padding: 0 0px;" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th class="pc-fb-font" width="300" style="letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; padding: 10px 10px 10px 0; border-bottom: 1px solid #E5E5E5; color: #151515" align="left">

                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                    <tbody>
                                                    <tr>
                                                        <td class="pc-fb-font" style="padding: 20px 10px 20px 0; font-weight: 500; letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; border-bottom: 0px solid #E5E5E5; color: #151515" valign="top">
                                                            Kære <?= $debtor->company_name ? $debtor->company_name : ($debtor->first_name . " ") . $debtor->last_name ?>,
                                                            <br><br>
                                                            Da vi endnu ikke har hørt fra dig eller modtaget betaling på baggrund af vores tidligere påmindelse, modtager du derfor din første påmindelse med et rykkergebyr på 100 kr. Manglende betaling af denne påmindelse vil medføre yderligere omkostninger i form af gebyrer og renter.
                                                            <br><br>
                                                            Hvis du har problemer med din økonomi, eller du mener, at du ikke skal betale denne faktura, skal du svare på denne e-mail <span style="color: #18c5a9">kommunikation@debito.dk</span> for at give os besked.
                                                            <br><br>
                                                            Vi beder om, at den udestående saldo betales så hurtigt som muligt og senest 10 dage fra <?= $date ?>. Hvis betalingen allerede er foretaget og har krydset denne, bedes du kontakte os.                                      <br>
                                                            <br>
                                                            Se vedhæftede faktura for detaljer.
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>

                                        <tbody>

                                        <tr>
                                            <td style="line-height: 1px; font-size: 1px;" height="25">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0 10px;" valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th class="pc-fb-font" width="300" style="letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; padding: 10px 10px 10px 0; border-bottom: 1px solid #E5E5E5; color: #151515" align="left">
                                                            <!-- Item -->
                                                        </th>

                                                        <th class="pc-fb-font" width="156" style="letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; border-bottom: 1px solid #E5E5E5; padding: 10px 0; color: #151515" align="right">
                                                            Beløb
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                    <tbody>
                                                    <tr>
                                                        <td class="pc-fb-font" style="padding: 20px 10px 20px 0; font-weight: 500; letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; border-bottom: 1px solid #E5E5E5; color: #151515" valign="top">
                                                            <strong>Samlet beløb, inkl. rykkere, mm.</strong>
                                                        </td>
                                                        <td class="pc-fb-font" style="padding: 20px 0; letter-spacing: -0.2px; line-height: 26px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 16px; border-bottom: 1px solid #E5E5E5; color: #151515" valign="top" align="right">
                                                            <?= $currency? $currency->iso_code : "DKK" ?> <?= $total ?>
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="padding: 25px 0;" valign="top" align="left">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                    <tbody>
                                                    <tr>
                                                        <td style="padding: 0 10px;" valign="top">
                                                            <a href="https://debito.dk">
                                                                <img class="w320" src="https://res.cloudinary.com/debito/image/upload/v1536241685/logo_web.png" alt="Debito">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END MODULE: Transactional 26 -->

                        <!-- BEGIN MODULE: Footer 7 -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" role="presentation">
                            <tbody>
                            <tr>
                                <td height="8" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">
                            <tbody>
                            <tr>
                                <td class="pc-sm-p-38-30-40 pc-xs-p-25-20" style="padding: 38px 40px 40px 40px; background-color: #ffffff; border-radius: 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);" valign="top" bgcolor="#ffffff">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" role="presentation">

                                        <tbody>
                                        <tr>
                                            <td class="pc-fb-font" style="text-align: center; line-height: 20px; letter-spacing: -0.2px; font-family: 'Poppins', Helvetica, Arial, sans-serif; font-size: 14px; color: #9B9B9B;" valign="top">
                                                <strong>Debito ApS</strong><br>
                                                Skelagervej 15<br>
                                                9000 Aalborg</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="13" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- END MODULE: Footer 7 -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
                            <tbody>
                            <tr>
                                <td height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<!-- Fix for Gmail on iOS -->
<div class="pc-gmail-fix" style="white-space: nowrap; font: 15px courier; line-height: 0;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>
