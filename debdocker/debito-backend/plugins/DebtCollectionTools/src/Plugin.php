<?php
declare(strict_types=1);

namespace DebtCollectionTools;

use Cake\Core\BasePlugin;

/**
 * Plugin for DebtCollectionTools
 */
class Plugin extends BasePlugin
{
}
