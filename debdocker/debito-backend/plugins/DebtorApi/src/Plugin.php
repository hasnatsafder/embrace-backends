<?php
declare(strict_types=1);

namespace DebtorApi;

use Cake\Core\BasePlugin;

/**
 * Plugin for DebtorApi
 */
class Plugin extends BasePlugin
{
}
