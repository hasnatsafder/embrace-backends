<?php
declare(strict_types=1);

namespace DebtorApi\Controller\Claims;

use App\Exception\InvalidIdInSubResource;
use Cake\Event\Event;
use Cake\ORM\Association;
use DebtorApi\Controller\AppController;

/**
 * Class BaseClaimController
 *
 * @package Api\Controller\Claims
 */
abstract class BaseClaimController extends AppController
{
    /**
     * @var \App\Model\Entity\Claim|null
     */
    protected $claim;

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
                'Crud.beforeFind' => 'crudFinders',
                'Crud.beforePaginate' => 'crudFinders',
                'Crud.beforeSave' => 'saveChecker',
            ];
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->setClaim($this->getRequest()->getParam('claim_id'));
    }

    /**
     * @return Association
     */
    protected function getClaimAssociation(): Association
    {
        [, $alias] = pluginSplit($this->modelClass, true);

        return $this->{$alias}->Claims;
    }

    /**
     * @param string $claimId
     */
    protected function setClaim(string $claimId): void
    {
        $claim = $this->getClaimAssociation()->getTarget()->find()
            ->select(['id'])
            ->where([
                'Claims.id' => $claimId,
                'Claims.debtor_id' => $this->Auth->user('id'),
            ])
            ->first();

        if (empty($claim)) {
            throw new InvalidIdInSubResource();
        }

        $this->claim = $claim;
    }

    /**
     * @param \Cake\Event\Event $e
     */
    public function crudFinders(Event $e)
    {
        $this->setClaim($this->getRequest()->getParam('claim_id'));

        if (!$this->getClaimAssociation() instanceof Association\BelongsTo) {
            throw new \RuntimeException(
                sprintf('Association type %s not supported', get_class($this->getClaimAssociation()))
            );
        }

        $fk = $this->getClaimAssociation()->getForeignKey();

        $e->getSubject()->query->where([
            $this->getClaimAssociation()->getSource()->aliasField($fk) => $this->claim->id,
        ]);
    }

    public function saveChecker(Event $e)
    {
        $this->setClaim($this->getRequest()->getParam('claim_id'));
    }
}
