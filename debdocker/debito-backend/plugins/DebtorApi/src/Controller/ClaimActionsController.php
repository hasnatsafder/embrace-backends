<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

use Cake\Event\Event;
use Cake\Http\Response;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;

/**
 * Claims Controller
 *
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 */
class ClaimActionsController extends AppController
{
    public $modelClass = 'App.ClaimActions';
    protected $allowedActions = ['message'];

    public function initialize()
    {
        parent::initialize();

        $this->Crud->mapAction('message', 'Crud.Add');
    }

    /**
     * @param string $claimId
     * @return \Cake\Http\Response
     * @throws \Crud\Error\Exception\ActionNotConfiguredException
     * @throws \Crud\Error\Exception\MissingActionException
     */
    public function message(string $claimId): Response
    {
        $this->getRequest()->allowMethod(['POST']);

        $this->Crud->action()->setConfig('saveOptions', [
            'validate' => 'DebtorApiMessage',
            'accessibleFields' => [
                '*' => false,
                'message' => true,
            ],
        ]);

        $this->Crud->on('beforeSave', function (\Cake\Event\Event $event) use ($claimId) {
            $claim = $this->ClaimActions->Claims->find()
                ->where([
                    'Claims.id' => $claimId,
                    'Claims.debtor_id' => $this->Auth->user('id'),
                ])
                ->firstOrFail();
            $claimActionType = $this->ClaimActions->ClaimActionTypes->getClaimActionTypeForCollector('debtor_message', 'debito');
            $event->getSubject()->entity->set(
                [
                    'date' => FrozenDate::now()->toDateString(),
                    'done' => FrozenTime::now(),
                    'claim_id' => $claim->id,
                    'claim_action_type_id' => $claimActionType->id,
                ],
                ['guard' => false]
            );
        });

        return $this->Crud->execute();
    }
}
