<?php
declare(strict_types=1);

namespace DebtorApi\Controller;

use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\FrozenTime;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

/**
 * Auth Controller
 *
 * @property \App\Model\Table\UsersTable Users
 */
class AuthController extends AppController
{
    public $modelClass = 'App.Users';

    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['token']);
    }

    public function token()
    {
        $this->getRequest()->allowMethod('POST');

        $debtor = $this->Auth->identify();

        if (!$debtor) {
            throw new UnauthorizedException('Invalid username or password');
        }

        $this->set([
            'success' => true,
            'data' => [
                'token' => JWT::encode(
                    [
                        'sub' => $debtor['id'],
                        'exp' => (int)(new FrozenTime())->addMinutes(60)->toUnixString(),
                        'iat' => time(),
                    ],
                    Security::getSalt()
                ),
            ],
            '_serialize' => ['success', 'data'],
        ]);
    }
}
