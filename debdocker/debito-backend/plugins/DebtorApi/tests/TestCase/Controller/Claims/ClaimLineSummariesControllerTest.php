<?php
namespace DebtorApi\Test\TestCase\Controller\Claims;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use DebtorApi\Controller\Claims\ClaimLineSummariesController;

/**
 * DebtorApi\Controller\Claims\ClaimLineSummariesController Test Case
 *
 * @uses \DebtorApi\Controller\Claims\ClaimLineSummariesController
 */
class ClaimLineSummariesControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.DebtorApi.ClaimLineSummaries',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
