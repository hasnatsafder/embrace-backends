<?php
namespace DebtorApi\Test\TestCase\Controller\Claims;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use DebtorApi\Controller\Claims\ClaimActionsController;

/**
 * DebtorApi\Controller\Claims\ClaimActionsController Test Case
 *
 * @uses \DebtorApi\Controller\Claims\ClaimActionsController
 */
class ClaimActionsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.DebtorApi.ClaimActions',
        'plugin.DebtorApi.ModifiedBy',
        'plugin.DebtorApi.CreatedBy',
        'plugin.DebtorApi.Users',
        'plugin.DebtorApi.Claims',
        'plugin.DebtorApi.ClaimLines',
        'plugin.DebtorApi.CollectorFiles',
        'plugin.DebtorApi.ClaimActionTypes',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
