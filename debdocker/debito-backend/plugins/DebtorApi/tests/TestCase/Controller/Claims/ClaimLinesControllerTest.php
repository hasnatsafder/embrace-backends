<?php
namespace DebtorApi\Test\TestCase\Controller\Claims;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use DebtorApi\Controller\Claims\ClaimLinesController;

/**
 * DebtorApi\Controller\Claims\ClaimLinesController Test Case
 *
 * @uses \DebtorApi\Controller\Claims\ClaimLinesController
 */
class ClaimLinesControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.DebtorApi.ClaimLines',
        'plugin.DebtorApi.Claims',
        'plugin.DebtorApi.ClaimLineTypes',
        'plugin.DebtorApi.Currencies',
        'plugin.DebtorApi.Invoices',
        'plugin.DebtorApi.ClaimTransactions',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
