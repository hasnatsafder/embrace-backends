<?php
namespace DebtorApi\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use DebtorApi\Controller\ClaimsController;

/**
 * DebtorApi\Controller\ClaimsController Test Case
 *
 * @uses \DebtorApi\Controller\ClaimsController
 */
class ClaimsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.DebtorApi.Claims',
        'plugin.DebtorApi.ModifiedBy',
        'plugin.DebtorApi.Accounts',
        'plugin.DebtorApi.CreatedBy',
        'plugin.DebtorApi.Debtors',
        'plugin.DebtorApi.ClaimPhases',
        'plugin.DebtorApi.Collectors',
        'plugin.DebtorApi.DunningConfigurations',
        'plugin.DebtorApi.ClaimActions',
        'plugin.DebtorApi.ClaimFinancials',
        'plugin.DebtorApi.ClaimFreezes',
        'plugin.DebtorApi.ClaimLines',
        'plugin.DebtorApi.ClaimTransactions',
        'plugin.DebtorApi.Users',
        'plugin.DebtorApi.Currencies',
        'plugin.DebtorApi.DebtorLawyers',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
