#!/usr/bin/env bash

set -ex

cd app/

# Prepare configuration file
sed -i "s|<db_user>|$DB_USER|g" app_config
sed -i "s|<db_password>|$DB_PASSWORD|g" app_config
sed -i "s|<db_host>|$DB_HOST|g" app_config
sed -i "s|<db_name>|$DB_NAME|g" app_config
sed -i "s|<web_url>|$WEB_URL|g" app_config
mv app_config config/.env

# Run migrations and async tasks
#bin/cake migrations migrate
bin/cake queue runworker &

# Run nginx
nginx

# Pass execution to Main container process (set by RUN directive)
exec "$@"
