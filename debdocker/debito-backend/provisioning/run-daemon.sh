#!/usr/bin/env bash

docker run -d -t -i -p 8000:80 --restart always \
  -e DB_USER=$DB_USER \
  -e DB_PASSWORD=$DB_PASSWORD \
  -e DB_HOST=$DB_HOST \
  -e DB_NAME=$DB_NAME \
  -e WEB_URL=$WEB_URL \
  --name debito-server-main 232423455232.dkr.ecr.eu-central-1.amazonaws.com/debito-server-main
