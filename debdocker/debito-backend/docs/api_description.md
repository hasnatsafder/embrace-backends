# Public Debito API

Base URL: /api/v1/


## Authentication

- /auth/register
- /auth/token

## Authenticated

- /accounts - GET, POST
- /accounts/:account_id - GET, PUT, DELETE

- /accounts/:account_id/users - GET, POST
- /accounts/:account_id/users/:user_id - PUT, DELETE

- /integrations/economic/status - GET
- /integrations/economic/delete - POST/DELETE
- /integrations/economic/start-auth - POST
- /integrations/economic/finish-auth - GET
    - Is used as a redirect URL and should NOT be called from our own app
    
- /claim-phases/ - GET
- /claim-phases/:claim_phase_id - GET

- /claim-action-types/ - GET
- /claim-action-types/:claim_action_type_id - GET    

- /claims - GET, POST
- /claims/:claim_id - GET, PUT, DELETE
- /claims/add-from-invoices - POST
- /claims/freeze/:claim_id - PUT (root only)

- /claims/:claim_id/claim-lines - GET, POST
- /claims/:claim_id/claim-lines/:claim_line_id - GET, PUT, DELETE

- /claims/:claim_id/claim-actions - GET
- /claims/:claim_id/claim-actions - POST (root only)
- /claims/:claim_id/claim-actions/:claim_action_id - GET
- /claims/:claim_id/claim-actions/:claim_action_id - PUT (root only)

- /claims/:claim_id/claim-transactions - GET
- /claims/:claim_id/claim-transactions - POST (root only)
- /claims/:claim_id/claim-transactions/:claim_transaction_id - GET

- /claims/:claim_id/claim-financials - GET
- /claims/:claim_id/claim-financials/:claim_financial_id - GET

- /claim-line-types/ - GET
- /claim-line-types/:claim_line_type_id - GET

- /claim-transaction-types/ - GET
- /claim-transaction-types/:claim_transaction_type_id - GET

- /collectors/ - GET (root only)
- /collectors/:collector_id - GET (root only)

- /company-types - GET
- /company-types/:company_type_id - GET

- /countries - GETc
- /countries/:country_id - GET

- /currencies - GET
- /currencies/:currency_id - GET

- /debtors - GET, POST
- /debtors/:debtor_id - GET, PUT, DELETE

- /dunning-configurations/ - GET, POST
- /dunning-configurations/:dunning-configuration_id - GET, PUT, DELETE

- invoices/ - GET, POST
    - Available filters
        - `?invoice_status_id=foo`
        - `?only_hidden=1`
        - `?hide_hidden=1`
        - `?hidden_until[][gt]=2019-02-01` `hidden_until` greater than `2019-02-01`
        - `?hidden_until[][lt]=2019-02-01` `hidden_until` less than `2019-02-01`
        - `?hidden_until[][eq]=2019-02-01` `hidden_until` equal to `2019-02-01`
        - `?hidden_until[][null]=1` `hidden_until` is `null`
        - Can be chained:
            - `?only_hidden=1&hidden_until[][gt]=2019-06-01&invoice_status=foo`
- invoices/:invoice_id/ - GET, PUT, DELETE
- invoices/integrationcheck - GET
- invoices/refreshAccountSync - GET

- invoice-statuses/ - GET
- invoice-statuses/:invoice_status_id/ - GET

- notifications/ - GET
- notifications/:notification_id - GET

- /roles - GET
- /roles/:roles_id - GET

- /users/ - GET
- /users/:user_id - GET, PUT

# Non-authenticated (TODO!)

- /claims/create-from-website - POST w/ form data
	- email

# ERP integrations how-to

Create claims from a set of invoices 
```bash
curl -X PUT \
  http://app.debito.local/api/v1/claims/add-from-invoices \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer TOKEN' \
  -H 'Content-Type: application/json' \
  -d '{
    "invoice_ids": [
        "59abd6e5-8f73-4142-9901-35ef04d46645",
        "1bc4dcdf-f99f-4e79-b40f-a1145b5cc0d1",
        "d148eeec-e2e5-4c3b-b494-3a1b1ab63941",
        "d1fd3eeb-0760-45fb-b196-88adb52fe52d"
    ]
}'
```



# ROOT INTERFACE (TODO!)

Base url: /admin/

- /users
	- base details
	- change email/password
	- accounts

- /accounts
	- base details
	- collectors details
	- users

- /claims/not-synced - GET

- /claims/:claim_id - GET/PUT
	- Select or add debtor (in this account!)
	- Set customer reference
	- Add claim action
		- Missing debtor details
		- Missing invoice detals 
		- Misisng ....
	- Add/edit claim lines
		- Enter base details (claim_line_type, currency, invoice_date, maturity_date, amount, invoice_reference)

## 100% CRUD things
- /company-types
- /roles
- /collectors
- /currencies 
- /countries

- /claim-action-types
- /claim-phases

- /claim-line-types

# Public Debtor API
## Details
- Base URL: `/debtor-api/v1/`

## Endpoints

- accounts/ - GET
- accounts/:claim_id - GET

- auth/token/ - POST (JSON body: `{"username": "$claim_reference","login_code": "$debtor_login_code"}`)

- claims-actions/message/:claim_id - POST (JSON body: `{"message": "I do not want to pay this 😅"}`)

- claims/ - GET
- claims/:claim_id - GET

- claims/:claim_id/claim-actions - GET
- claims/:claim_id/claim-actions/:claim_action_id - GET

- claims/:claim_id/claim-lines - GET
- claims/:claim_id/claim-lines/:claim_line_id - GET

- claims/summary/:claim_id - GET

# CLI commands
- `$ cake create_user`
- `$ cake download_claims_from_collector`
- `$ cake migrate_old_lindorff_files`
- `$ cake migrate_old_mortang_data`
- `$ cake update_erp_invoices`
    - Used to update all erp integrations and fetch new invoices
- `$ cake upload_claims_to_collector`
