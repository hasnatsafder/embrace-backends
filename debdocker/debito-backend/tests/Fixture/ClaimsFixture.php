<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClaimsFixture
 */
class ClaimsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created_by_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified_by_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'account_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'debtor_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'claim_phase_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'collector_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dunning_configuration_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'customer_reference' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null, 'fixed' => null],
        'collector_reference' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null, 'fixed' => null],
        'dispute' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null],
        'completed' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'approved' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'synced' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'ended_at' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'frozen_until' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'rejected_is_public' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'currency_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'on_hold' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'rejected_reason' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'deleted' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'old_id' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'BY_CREATED_BY_ID' => ['type' => 'index', 'columns' => ['created_by_id'], 'length' => []],
            'BY_ACCOUNT_ID' => ['type' => 'index', 'columns' => ['account_id'], 'length' => []],
            'BY_DEBTOR_ID' => ['type' => 'index', 'columns' => ['debtor_id'], 'length' => []],
            'BY_CLAIM_PHASE_ID' => ['type' => 'index', 'columns' => ['claim_phase_id'], 'length' => []],
            'BY_CUSTOMER_REFERENCE' => ['type' => 'index', 'columns' => ['customer_reference'], 'length' => []],
            'BY_COLLECTOR_REFERENCE' => ['type' => 'index', 'columns' => ['collector_reference'], 'length' => []],
            'BY_APPROVED' => ['type' => 'index', 'columns' => ['approved'], 'length' => []],
            'BY_COLLECTOR_ID' => ['type' => 'index', 'columns' => ['collector_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 'dad7dd62-e2ab-40a0-b382-f790d0e9e4c9',
                'created_by_id' => '7c01fdea-d876-4272-b029-256fcf017504',
                'modified_by_id' => '03ae0ab1-6ce4-4bf0-9ad8-5a9543756776',
                'account_id' => '8ab2e85b-36ac-4818-b5d4-d0a9b19c5f25',
                'debtor_id' => '72451869-3831-435c-80ae-b86d3709b17f',
                'claim_phase_id' => 'fc16648a-0ff4-484d-a95a-84d1bbf3416e',
                'collector_id' => '38898aa5-3eb8-4779-9f05-e6e174e4f6e4',
                'dunning_configuration_id' => '7650bb2e-5993-43e2-815e-3b60117b7bb9',
                'customer_reference' => 'Lorem ipsum dolor sit amet',
                'collector_reference' => 'Lorem ipsum dolor sit amet',
                'dispute' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'completed' => '2020-02-21 09:54:15',
                'approved' => '2020-02-21 09:54:15',
                'synced' => '2020-02-21 09:54:15',
                'ended_at' => '2020-02-21 09:54:15',
                'frozen_until' => '2020-02-21 09:54:15',
                'rejected_is_public' => 1,
                'currency_id' => '7bad30ad-36cf-405b-8dc5-acfcf06d72dc',
                'on_hold' => 1,
                'rejected_reason' => 'Lorem ipsum dolor sit amet',
                'created' => '2020-02-21 09:54:15',
                'modified' => '2020-02-21 09:54:15',
                'deleted' => '2020-02-21 09:54:15',
                'old_id' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
