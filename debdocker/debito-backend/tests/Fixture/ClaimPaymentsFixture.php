<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClaimPaymentsFixture
 *
 */
class ClaimPaymentsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => [
            'type' => 'uuid',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'claim_id' => [
            'type' => 'uuid',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'collector_file_id' => [
            'type' => 'uuid',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'currency_id' => [
            'type' => 'uuid',
            'length' => null,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'payment_date' => [
            'type' => 'date',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'paid_fee_without_vat' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'paid_fee_with_vat' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'vat_of_fees' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'vat_of_commision' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'commision' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'gross_paid' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'paid_to_creditor' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'paid_principal' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'paid_fees' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'paid_interest' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'remaining_principal' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'remaining_total' => [
            'type' => 'integer',
            'length' => 11,
            'unsigned' => false,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        'raw_content' => [
            'type' => 'text',
            'length' => null,
            'null' => true,
            'default' => null,
            'collate' => 'utf8_unicode_ci',
            'comment' => '',
            'precision' => null,
        ],
        'created' => [
            'type' => 'datetime',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'modified' => [
            'type' => 'datetime',
            'length' => null,
            'null' => false,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'deleted' => [
            'type' => 'datetime',
            'length' => null,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        '_indexes' => [
            'BY_CLAIM_ID' => ['type' => 'index', 'columns' => ['claim_id'], 'length' => []],
            'BY_COLLECTOR_FILE_ID' => ['type' => 'index', 'columns' => ['collector_file_id'], 'length' => []],
            'BY_PAYMENT_DATE' => ['type' => 'index', 'columns' => ['payment_date'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '8de2e0a6-dbd7-4cb5-b5da-ec2b7a58d9d6',
                'claim_id' => '1058580b-d795-444b-94c2-92eca21c9453',
                'collector_file_id' => '26c538a9-d1f2-4047-9472-1b3f6832d671',
                'currency_id' => '9822e260-df5a-4a62-a2b9-915b880fb94e',
                'payment_date' => '2018-08-28',
                'paid_fee_without_vat' => 1,
                'paid_fee_with_vat' => 1,
                'vat_of_fees' => 1,
                'vat_of_commision' => 1,
                'commision' => 1,
                'gross_paid' => 1,
                'paid_to_creditor' => 1,
                'paid_principal' => 1,
                'paid_fees' => 1,
                'paid_interest' => 1,
                'remaining_principal' => 1,
                'remaining_total' => 1,
                'raw_content' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'created' => '2018-08-28 12:25:50',
                'modified' => '2018-08-28 12:25:50',
                'deleted' => '2018-08-28 12:25:50',
            ],
        ];
        parent::init();
    }
}
