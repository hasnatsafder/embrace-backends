<?php

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SessionsFixture
 *
 */
class SessionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => [
            'type' => 'string',
            'fixed' => true,
            'length' => 40,
            'null' => false,
            'default' => null,
            'collate' => 'ascii_bin',
            'comment' => '',
            'precision' => null,
        ],
        'created' => [
            'type' => 'datetime',
            'length' => null,
            'null' => true,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => '',
            'precision' => null,
        ],
        'modified' => [
            'type' => 'datetime',
            'length' => null,
            'null' => true,
            'default' => 'CURRENT_TIMESTAMP',
            'comment' => '',
            'precision' => null,
        ],
        'data' => [
            'type' => 'binary',
            'length' => null,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
        ],
        'expires' => [
            'type' => 'integer',
            'length' => 10,
            'unsigned' => true,
            'null' => true,
            'default' => null,
            'comment' => '',
            'precision' => null,
            'autoIncrement' => null,
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci',
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '3539e1f1-84c1-4794-b08d-8518c89e3e1a',
                'created' => '2018-08-28 11:11:03',
                'modified' => '2018-08-28 11:11:03',
                'data' => 'Lorem ipsum dolor sit amet',
                'expires' => 1,
            ],
        ];
        parent::init();
    }
}
