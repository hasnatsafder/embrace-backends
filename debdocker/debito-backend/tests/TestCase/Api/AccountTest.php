<?php

namespace App\Test\TestCase\Api;

use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\Api\ApiTestTrait;
use Exception;
use function GuzzleHttp\json_encode;

class AccountTest extends IntegrationTestCase
{
    use ApiTestTrait;

    public function testCanAddAccount()
    {
        $this->setUserToken();
        $data = [
            'name' => "My new test Account",
            'address' => 'Lorem ipsum dolor sit amet',
            'zip_code' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum dolor sit amet',
            'vat_number' => 'Lorem ipsum dolor sit amet',
            'ean_number' => 'Lorem ipsum dolor sit amet',
            'website' => 'Lorem ipsum dolor sit amet',
            'bank_reg_number' => 'Lorem ipsum dolor sit amet',
            'bank_account_number' => 'Lorem ipsum dolor sit amet',
            'is_company' => 1,
            'latitude' => 'Lorem ipsum dolor sit amet',
            'longitude' => 'Lorem ipsum dolor sit amet',
            'created' => '2018-08-28 12:25:48',
            'modified' => '2018-08-28 12:25:48',
            'deleted' => '2018-08-28 12:25:48',
            'email' => 'test@debito.com',
        ];
        $this->post('/api/v1/accounts', json_encode($data));
        $this->assertResponseOk();
        $this->assertResponseContains('id');

        $account = $this->accountsTable->find()->where(['name' => "My new test Account"])->first();

        //check account exists
        if (!$account) {
            throw new Exception("Account not regisetered");
        }
    }

    public function testCanGetAccounts()
    {
        $user = $this->setUserToken();
        $this->get('/api/v1/accounts/');
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
        $this->assertResponseContains('vat_number');
    }

    public function testCanEditAccount()
    {
        $this->setUserToken();
        $this->put('/api/v1/accounts/' . $this->savedData['account']->id, json_encode(['name' => 'Edited Account']));
        $this->assertResponseOk();

        $account = $this->accountsTable->find()->where(['name' => "Edited Account"])->first();

        //check account exists
        if (!$account) {
            throw new Exception("Account not edited");
        }
    }

    public function testCanGetOneAccountDetail()
    {
        $this->setUserToken();
        $this->get('/api/v1/accounts/' . $this->savedData['account']->id);
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
        $this->assertResponseContains('vat_number');
    }
}
