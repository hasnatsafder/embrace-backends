<?php

namespace App\Test\TestCase\Api;

use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\Api\ApiTestTrait;
use Exception;
use function GuzzleHttp\json_encode;

class UserAuthTest extends IntegrationTestCase
{
    use ApiTestTrait;

    // ============ Registertaion Tests ============

    public function testCanRegister()
    {
        $this->post('/api/v1/auth/register', json_encode(['email' => "testuser@debito.com", 'password' => "131077"]));
        $this->assertResponseOk();
        $this->assertResponseContains('token');

        $user = $this->usersTable->find()->where(['email' => "testuser@debito.com"])->first();

        //check user exists
        if (!$user) {
            throw new Exception("User not regisetered");
        }

        //check password is hashed
        if (strlen($user->password) < 10) {
            throw new Exception("Password not hashed");
        }
    }

    public function testRequiresEmailAndPassword()
    {
        $this->post('/api/v1/auth/register', []);
        $this->assertResponseCode(422);
        $this->assertResponseContains("validation errors occurred");
    }

    public function testPasswordMinLength()
    {
        $this->post('/api/v1/auth/register', json_encode(['email' => "test@debito.com", 'password' => ""]));
        $this->assertResponseCode(422);
        $this->assertResponseContains("Password length must be greater");
    }

    // ============ Login Test ============

    public function testCanLogin()
    {
        $this->post('/api/v1/auth/token', json_encode(['email' => "test@debito.com", 'password' => "131077"]));
        $this->assertResponseOk();
        $this->assertResponseContains('token');
    }

    public function testIncorrectLogin()
    {
        $this->post('/api/v1/auth/token', json_encode(['email' => "falseuser@debito.com", 'password' => "i am a false user"]));
        $this->assertResponseCode(401);
        $this->assertResponseContains("Invalid username or password");
    }

    // =========== User Info =============

    public function testUserInfo()
    {
        $user = $this->setUserToken();
        $this->get('/api/v1/users/' . $user->id);
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
    }

    public function testCanEditUser()
    {
        $user = $this->setUserToken();
        $this->put('/api/v1/users/' . $user->id, json_encode(['first_name' => 'debito-admin']));
        $this->assertResponseOk();
    }

    public function testCanChangeActiveAccount()
    {
        $user = $this->setUserToken();
        $account = $this->accountsTable->find()->where(['name' => $this->accountName])->first();
        $this->put('/api/v1/users/' . $user->id, json_encode(['active_account_id' => $account->id]));
        $this->assertResponseOk();
    }

    // ========== Forgot Password ===========

    /**
     * @group email
     */
    public function testCanRequestPasswordReset()
    {
        $this->post('/api/v1/auth/forgotpassword/', json_encode(['email' => $this->userEmail]));
        $this->assertResponseOk();
    }

    public function testUserTokenIsValid()
    {
        $this->get('/api/v1/auth/verify/' . $this->userRemeberMeToken);
        $this->assertResponseOk();
    }

    public function testUserTokenIsNotValid()
    {
        $this->get('/api/v1/auth/verify/SOME_INCORRECT_TOKEN');
        $this->assertResponseError();
    }

    public function testCanResetPassword()
    {
        $this->post('/api/v1/auth/resetpassword/' . $this->userRemeberMeToken, json_encode(['password' => "testpass"]));
        $this->assertResponseOk();
    }
}
