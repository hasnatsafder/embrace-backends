<?php

namespace App\Test\TestCase\Api;

use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\Api\ApiTestTrait;
use Exception;
use function GuzzleHttp\json_encode;

class ClaimTest extends IntegrationTestCase
{
    use ApiTestTrait;

    public function testCanAddClaim()
    {
        $this->setUserToken();
        $data = [
            'debtor_id' => $this->savedData['debtor']->id,
            'old_id' => "292232",
        ];
        $this->post('/api/v1/claims', json_encode($data));
        $this->assertResponseOk();
        $this->assertResponseContains('id');

        $account = $this->claimsTable->find()->where(['old_id' => "292232"])->first();

        //check account exists
        if (!$account) {
            throw new Exception("Claim not regisetered");
        }
    }

    public function testCanGetClaims()
    {
        $this->setUserToken();
        $this->get('/api/v1/claims/');
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('created_by_id');
        $this->assertResponseContains('account_id');
        $this->assertResponseContains('collector_id');
        $this->assertResponseContains('customer_reference');
        $this->assertResponseContains('collector_reference');
        $this->assertResponseContains('currency_id');
        $this->assertResponseContains('dispute');
    }

    public function testCanGetClaimsSummary()
    {
        $this->setUserToken();
        $this->get('/api/v1/claims?includeClaimLineSum=1');
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('created');
        $this->assertResponseContains('customer_reference');
        $this->assertResponseContains('claimLinesSum');
        $this->assertResponseContains('claimLinesDeduct');
        $this->assertResponseContains('claimLinesDeduct');
        $this->assertResponseContains('claim_phase');
        $this->assertResponseContains('Debtors');
    }

    public function testCanGetOneClaimDetail()
    {
        $this->setUserToken();
        $this->get('/api/v1/claims/' . $this->savedData['claim']->id);
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('created_by_id');
        $this->assertResponseContains('account_id');
        $this->assertResponseContains('collector_id');
        $this->assertResponseContains('customer_reference');
        $this->assertResponseContains('collector_reference');
        $this->assertResponseContains('currency_id');
        $this->assertResponseContains('dispute');
    }
}
