<?php

namespace App\Test\TestCase\Api;

use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\Api\ApiTestTrait;
use Exception;
use function GuzzleHttp\json_encode;

class DebtorTest extends IntegrationTestCase
{
    use ApiTestTrait;

    public function testCanAddDebtor()
    {
        $this->setUserToken();
        $data = [
            'account_id' => $this->savedData['account']->id,
            'vat_number' => 'Lorem ipsum dolor sit amet',
            'company_name' => 'Lorem ipsum dolor sit amet',
            'first_name' => 'Lorem ipsum dolor sit amet',
            'last_name' => 'Lorem ipsum dolor sit amet',
            'address' => 'Lorem ipsum dolor sit amet',
            'zip_code' => 'Lorem ipsum dolor sit amet',
            'city' => 'Lorem ipsum dolor sit amet',
            'phone' => 'Lorem ipsum dolor sit amet',
            'email' => 'new_debtor_test@debtio.com',
            'latitude' => 'Lorem ipsum dolor sit amet',
            'langitude' => 'Lorem ipsum dolor sit amet',
            'created' => '2018-08-28 11:11:03',
            'modified' => '2018-08-28 11:11:03',
            'deleted' => '2018-08-28 11:11:03',
            'is_company' => 1,
        ];
        $this->post('/api/v1/debtors', json_encode($data));
        $this->assertResponseOk();
        $this->assertResponseContains('id');

        $debtor = $this->debtorsTable->find()->where(['email' => "new_debtor_test@debtio.com"])->first();

        //check debtor exists
        if (!$debtor) {
            throw new Exception("Debtor not regisetered");
        }
    }

    public function testCanGetDebtors()
    {
        $user = $this->setUserToken();
        $this->get('/api/v1/debtors/');
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
        $this->assertResponseContains('vat_number');
    }

    public function testCanEditDebtor()
    {
        $this->setUserToken();
        $this->put('/api/v1/debtors/' . $this->savedData['debtor']->id, json_encode(['email' => 'editemail@debito.com']));
        $this->assertResponseOk();

        $debtor = $this->debtorsTable->find()->where(['email' => "editemail@debito.com"])->first();

        //check debtor exists
        if (!$debtor) {
            throw new Exception("Debtor not edited");
        }
    }

    public function testCanGetOneDebtorDetail()
    {
        $this->setUserToken();
        $this->get('/api/v1/debtors/' . $this->savedData['debtor']->id);
        $this->assertResponseOk();
        $this->assertResponseContains('id');
        $this->assertResponseContains('email');
        $this->assertResponseContains('vat_number');
    }
}
