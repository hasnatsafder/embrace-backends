<?php

namespace App\Test\TestCase\Api;

use Firebase\JWT\JWT;
use Cake\Utility\Security;
use Cake\I18n\FrozenTime;
use App\Test\TestCase\Api\DataTrait;

/**
 * @group api
 */
trait ApiTestTrait
{
    use DataTrait;

    public $userToken;

    public function setUp()
    {
        parent::setUp();
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
        error_reporting(E_ALL ^ ~E_DEPRECATED);
        $this->initializeData();
    }

    public function tearDown()
    {
        $this->clearData();
        parent::tearDown();
    }

    private function setUserToken()
    {
        $user = $this->usersTable->find()->where(['email' => $this->userEmail])->first();
        $this->userToken = JWT::encode(
            [
                'sub' => $user->id,
                'exp' => (int)(new FrozenTime())->addDays(7)->toUnixString(),
                'iat' => time(),
            ],
            Security::getSalt()
        );
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->userToken,
            ],
        ]);

        return $user;
    }
}
