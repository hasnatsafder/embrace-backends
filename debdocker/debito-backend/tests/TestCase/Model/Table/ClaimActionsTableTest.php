<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimActionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimActionsTable Test Case
 */
class ClaimActionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimActionsTable
     */
    public $ClaimActionsTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimActions',
        'app.Claims',
        'app.CollectorFiles',
        'app.ClaimActionTypes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimActions') ? [] : ['className' => ClaimActionsTable::class];
        $this->ClaimActionsTable = TableRegistry::getTableLocator()->get('ClaimActions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimActionsTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
