<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersCollectorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersCollectorsTable Test Case
 */
class UsersCollectorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersCollectorsTable
     */
    public $UsersCollectorsTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersCollectors',
        'app.Users',
        'app.Collectors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersCollectors') ? [] : ['className' => UsersCollectorsTable::class];
        $this->UsersCollectorsTable = TableRegistry::getTableLocator()->get('UsersCollectors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersCollectorsTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
