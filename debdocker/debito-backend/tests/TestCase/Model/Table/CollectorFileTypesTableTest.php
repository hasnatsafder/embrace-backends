<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CollectorFileTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CollectorFileTypesTable Test Case
 */
class CollectorFileTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CollectorFileTypesTable
     */
    public $CollectorFileTypesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CollectorFileTypes',
        'app.CollectorFiles',
        'app.Collectors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CollectorFileTypes') ? [] : ['className' => CollectorFileTypesTable::class];
        $this->CollectorFileTypesTable = TableRegistry::getTableLocator()->get('CollectorFileTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CollectorFileTypesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
