<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimPhasesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimPhasesTable Test Case
 */
class ClaimPhasesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimPhasesTable
     */
    public $ClaimPhasesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimPhases',
        'app.ClaimActionTypes',
        'app.Claims',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimPhases') ? [] : ['className' => ClaimPhasesTable::class];
        $this->ClaimPhasesTable = TableRegistry::getTableLocator()->get('ClaimPhases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimPhasesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getFromIdentifier method
     *
     * @return void
     */
    public function testGetFromIdentifier()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
