<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClaimFinancialsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClaimFinancialsTable Test Case
 */
class ClaimFinancialsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClaimFinancialsTable
     */
    public $ClaimFinancialsTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ClaimFinancials',
        'app.Claims',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClaimFinancials') ? [] : ['className' => ClaimFinancialsTable::class];
        $this->ClaimFinancialsTable = TableRegistry::getTableLocator()->get('ClaimFinancials', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClaimFinancialsTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
