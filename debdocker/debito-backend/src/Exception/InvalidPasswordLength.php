<?php
declare(strict_types=1);

namespace App\Exception;

class InvalidPasswordLength extends Exception
{
    protected $code = 422;
    protected $message = 'Password length must be greater than 6';
}
