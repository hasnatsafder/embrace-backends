<?php
declare(strict_types=1);

namespace App\Exception;

/**
 * Class InvalidIdInSubResource
 *
 * @package App\Exception
 */
class InvalidIdInSubResource extends Exception
{
    protected $code = 400;
    protected $message = 'Invalid ID in the sub-resource';
}
