<?php
declare(strict_types=1);

namespace App\Exception;

class NoAccountSelected extends Exception
{
    protected $code = 400;
    protected $message = 'No account has been selected for currently authenticated user';
}
