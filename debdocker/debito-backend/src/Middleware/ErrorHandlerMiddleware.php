<?php
declare(strict_types=1);

namespace App\Middleware;

class ErrorHandlerMiddleware extends \Cake\Error\Middleware\ErrorHandlerMiddleware
{
    /**
     * {@inheritDoc}
     */
    public function handleException($exception, $request, $response)
    {
        $response = parent::handleException($exception, $request, $response);

        return CorsMiddleware::setCorsHeaders($request, $response);
    }
}
