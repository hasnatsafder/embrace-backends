<?php
declare(strict_types=1);

namespace App\Middleware;

use Cake\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Cors middleware
 */
class CorsMiddleware
{
    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        $response = static::setCorsHeaders($request, $response);

        if ($request->getMethod() === 'OPTIONS') {
            return $response;
        }

        return $next($request, $response);
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return Response
     */
    public static function setCorsHeaders(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $response->cors($request)
            ->allowOrigin(['*'])
            ->allowMethods(['GET', 'POST', 'PUT', 'DELETE'])
            ->allowHeaders([
                'Authorization',
                'Content-Type',
                'Accept',
                'Access-Control-Allow-Origin',
                'X-Auth-Token',
            ])
            ->maxAge(300)
            ->build();
    }
}
