<?php
declare(strict_types=1);

/**
 * @var \App\Model\Entity\User $user
 * @var \App\Model\Entity\Notification[] $notifications
 * @var \Cake\View\View $this
 */

?>

<< EMAIL HEADER >>

<?php foreach ($notifications as $notification) : ?>

    << BEFORE NOTIFICATION >>

    <?php
    //We could use elements for the notification involved
    //Like:

    /**
     * @todo internal debt collection implement elements per template enum
     */
    $emailTemplate = 'fallback';
    switch ($notification->template) {
        case $notification::TEMPLATE_CLAIM_ACTION:
            $emailTemplate = 'claim_action_fallback';
            break;
        case $notification::TEMPLATE_CLAIM_ACTION_EXTERNAL_NOTE:
            $emailTemplate = 'claim_action_external_note';
            break;

    }

    echo $this->element(
        'emails/notification_summaries/notification_summary/' . $emailTemplate,
        [
            'notification' => $notification,
            'user' => $user,
        ]
    ) ?>

    <?= pj($notification) ?>

    << AFTER NOTIFICATION >>

<?php endforeach; ?>

<< EMAIL FOOTER >>
