<?php
/**
 * @var \App\View\AppView $this
 * @var object $error
 * @var mixed $message
 */
declare(strict_types=1);

use Cake\Core\Configure;
use Cake\Error\Debugger;

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
    ?>
    <?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
    <?php if (!empty($error->params)) : ?>
    <strong>SQL Query Params: </strong>
    <?php Debugger::dump($error->params) ?>
<?php endif; ?>
    <?= $this->element('auto_table_warning') ?>
    <?php
    if (extension_loaded('xdebug')) :
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>
<?= $this->Html->css('/Admin/css/vendors/bootstrap.css') ?>
<div class="row pt-5">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <img id="error-img" src="/webroot/img/404.png" alt="">
        <h3 class="mt-2" id="error-heading">Some error occurred</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <p class="error-message"><?= h($message) ?></p>
        <p id="error-admin" class="error-message">Please contact administrator for further support</p>
    </div>

</div>

<style>
    #error-img {
        width: 333px;
        height: 200px;
    }

    #error-heading {
        letter-spacing: 0;
        color: #D33F42;
        padding-left: 11%;
    }

    .error-message {
        text-align: center;
        color: #4E5A64;
        font-size: 16px;
    }

    #error-admin {
        margin-left: -11%;
    }
</style>