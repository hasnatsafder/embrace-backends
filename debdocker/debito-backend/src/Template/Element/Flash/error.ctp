<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 */
declare(strict_types=1);

if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= $message ?>
</div>
