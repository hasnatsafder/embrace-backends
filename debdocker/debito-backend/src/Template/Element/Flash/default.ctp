<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 */
declare(strict_types=1);

$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-<?= !empty($class) ? '' : 'info' ?>">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?= $message ?>
</div>
