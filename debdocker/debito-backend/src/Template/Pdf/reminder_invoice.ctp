<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $data
 * @var string $date
 * @var string $total
 * @var string $warning_number
 * @var \App\Model\Entity\Account $account
 * @var \App\Model\Entity\Debtor $debtor
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h2>Rykker <?= $warning_number ?></h2>
                <h3 class="pull-right"><?= $account->name ?></h3>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong><?= $debtor->is_company ? $debtor->company_name : ($debtor->first_name . " " . $debtor->last_name) ?></strong><br>
                        Phone : <?= $debtor->phone ?><br>
                        Address : <?= $debtor->zip_code ?> <?= $debtor->city ?><br>
                        <?= $debtor->address ?><br>
                        Email : <?= $debtor->email ?><br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Dato:</strong><br>
                        <?= $date ?><br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-borderless">
                    <thead>
                    <tr>
                        <td><strong>Nr</strong></td>
                        <td class="text-center"><strong>Tekst</strong></td>
                        <td class="text-center"><strong>Fakturadato</strong></td>
                        <td class="text-center"><strong>Forfaldsdato</strong></td>
                        <td class="text-right"><strong>Beløb</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $invoiceData) : ?>
                        <tr>
                            <td><?= $invoiceData['nr'] ?></td>
                            <td class="text-center"><?= $invoiceData['text'] ?></td>
                            <td class="text-center"><?= $invoiceData['issue_date'] ?></td>
                            <td class="text-center"><?= $invoiceData['due_date'] ?></td>
                            <td class="text-right"><?= $invoiceData['amount'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="no-line"></td>
                        <td class="no-line"></td>
                        <td class="no-line"></td>
                        <td class="no-line text-center"><strong>Total DKK</strong></td>
                        <td class="no-line text-right"><?= $total ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>

</html>

<style>
    .invoice-title h2,
    .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > {
        border-top: 1px solid;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table-responsive {
        border-width: 0px !important;
    }

    .container {
        padding-top: 50px;
    }
</style>