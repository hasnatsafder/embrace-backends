<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $data
 * @var string $date
 * @var string $total
 * @var \App\Model\Entity\Debtor $debtor
 * @var \App\Model\Entity\Account $account
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h5>Demokunde A/S</h5>
                <span><?= $debtor->address ?></span><br>
                <span><?= $debtor->zip_code ?> <?= $debtor->city ?></span><br>
                <span>DENMARK</span><br>
                <span>CVR-nr: <?= $debtor->vat_number ? $debtor->vat_number : "" ?></span><br>
            </div>
            <div class="row">
                <div class="col-xs-9">
                    <hr class="dkk-border">
                </div>
                <div class="col-xs-3 pull-right text-left">
                    <hr class="dkk-border">
                    <div class="table-margin">
                        <span>Nr. ..........: 9990000000</span><br>
                        <span>Dato. ..........: <?= $date ?></span><br>
                        <span>Kundenr. ..........: 2345</span><br>
                        <span>Side ..........: 1 af 1</span><br><br>
                        <span>Deres ref.......: <?= $account->name ?></span><br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-condensed table-borderless">
                    <thead>
                    <tr>
                        <td class="border-table"><strong>Nr</strong></td>
                        <td class="text-left border-table"><strong>Tekst</strong></td>
                        <td class="text-left border-table"><strong>Fakturadato</strong></td>
                        <td class="text-left border-table"><strong>Forfaldsdato</strong></td>
                        <td class="text-right border-table"><strong>Beløb</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data as $invoiceData) : ?>
                        <tr>
                            <td class="text-left no-line"><?= $invoiceData['nr'] ?></td>
                            <td class="text-left no-line"><?= $invoiceData['text'] ?></td>
                            <td class="text-left no-line"><?= $invoiceData['issue_date'] ?></td>
                            <td class="text-left no-line"><?= $invoiceData['due_date'] ?></td>
                            <td class="text-right no-line"><?= $invoiceData['amount'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr class="table-footer">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-center dkk-border"><strong>Total DKK:</strong></td>
                        <td class="text-right dkk-border"><?= $total ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 invoice-footer">
            <div class="invoice-info">
                <span>Betalingsbetingelser: Netto 8 rage - four fold 08.09.2019</span><br>
                <span>Belobet indbetales til for bank. Pengibanken - Regnr: 4563/ Kontonr: 34000</span><br>
                <span>Nr. 900000 bodes anfort bankoverforsel</span><br>
                <h5>Bed for sen beetling paregnes rents / henhold til gaeldende lovgivning</h5>
            </div>
            <hr class="dkk-border">
            <div class="text-center">
                <h5>Design go layout Denmark Aps - Demovej 1 - 4000 Roskilde - DANMARK - CVR-nr.:100050</h5>
                <span>Tlf: 8888000 - Mobil: 40404040 - Mail: info@e-conomic.dk - Web: e-conomic.dk</span><br>
                <span>Bank: pengebanken - Kontonr.: 4563/ 34632 - IBAN-nr.: DK206882999288 - SWIFT-kode: NDEADKK</span>
            </div>
        </div>
    </div>
</div>
</body>

</html>

<style>
    .invoice-footer {
        position: absolute;
        top: 800px;
    }

    .invoice-info {
        margin-top: 100px;
    }

    .table-margin {
        margin-bottom: 15px;
    }

    .dkk-border {
        border-bottom: 2px solid grey;
    }

    .table-footer {
        border-top: 2px solid grey;
    }

    .border-table {
        border-top: 2px solid grey !important;
        border-bottom: 2px solid grey;
    }

    .invoice-title h2,
    .invoice-title h3 {
        display: inline-block;
    }

    .table > tbody > tr > {
        border-top: 1px solid;
    }

    .table > tbody > tr > .no-line {
        border-top: none;
    }

    .table > thead > tr > .no-line {
        border-bottom: none;
    }

    .table-responsive {
        border-width: 0px !important;
    }

    .container {
        padding-top: 50px;
    }
</style>