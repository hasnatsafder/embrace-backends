<?php
/**
 * @var \App\View\AppView $this
 */
declare(strict_types=1);

echo $this->fetch('content');
