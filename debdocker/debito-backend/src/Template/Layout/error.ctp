<?php
/**
 * @var \App\View\AppView $this
 */
declare(strict_types=1);

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<div id="container">
    <div id="header">
        <h1><?= __('Error') ?></h1>
    </div>
    <div id="content">
        <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
    <div id="footer">
        <?= $this->Html->link(__('Back'), 'javascript:history.back()') ?>
    </div>
</div>
</body>
</html>
