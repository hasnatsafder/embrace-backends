<?php
declare(strict_types=1);

namespace App\Event;

use App\Model\Entity\ClaimAction;
use Cake\Chronos\Date;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\I18n\FrozenTime;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Class ClaimActionsListener
 *
 * @package App\Event
 */
class ClaimActionListener implements EventListenerInterface
{
    use LocatorAwareTrait;

    /**
     * @inheritDoc
     */
    public function implementedEvents()
    {
        return [
            'Model.ClaimAction.created' => 'created',
        ];
    }

    public function created(Event $event, ClaimAction $claimAction)
    {
        $date = new Date($claimAction->date);
        $this->getTableLocator()->get('Queue.QueuedJobs')->createJob(
            'ClaimActionCreated',
            ['claimActionId' => $claimAction->id],
            ['notBefore' => $date->toDateTimeString()]
        );
    }
}
