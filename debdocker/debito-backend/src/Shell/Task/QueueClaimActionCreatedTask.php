<?php
declare(strict_types=1);

namespace App\Shell\Task;

use App\Model\Entity\Claim;
use App\Model\Entity\ClaimAction;
use App\Model\Entity\ClaimPhase;
use Cake\Utility\Inflector;
use Psr\Log\LogLevel;
use Queue\Model\QueueException;
use Queue\Shell\Task\QueueTask;
use RuntimeException;

/**
 * Class QueueClaimActionCreatedTask
 *
 * @package App\Shell\Task
 * @property \App\Model\Table\ClaimActionsTable $ClaimActions
 */
class QueueClaimActionCreatedTask extends QueueTask
{
    public $modelClass = 'App.ClaimActions';

    /**
     * @var ClaimAction
     */
    protected $claimAction;

    /**
     * Main execution of the task.
     *
     * @param array $data The array passed to QueuedJobsTable::createJob()
     * @param int $jobId The id of the QueuedJob entity
     */
    public function run(array $data, $jobId)
    {
        if (empty($data['claimActionId'])) {
            throw new QueueException('"claimActionId" is required');
        }

        $this->claimAction = $this->ClaimActions->find()
            ->where(['ClaimActions.id' => $data['claimActionId']])
            ->contain([
                'Claims' => [
                    'ClaimPhases',
                    'Collectors',
                ],
                'ClaimActionTypes' => [
                    'Collectors',
                    'ClaimPhases',
                ],
            ])
            ->first();
            if (!$this->claimAction) {
                $this->QueuedJobs->delete($this->QueuedJobs->get($jobId));
                $this->log('There is no claim action found, deleting the queue', LogLevel::DEBUG);
                return;
            }

        $this->log(
            sprintf('Processing Claim action of type: "%s"', $this->claimAction->claim_action_type->identifier),
            LogLevel::DEBUG
        );

        //TODO internal debt collection: What if Claim is paid out?
        //TODO internal debt collection: What if Claim is in freeze period?
        //TODO internal debt collection: Consider ClaimAction.user_id
        //TODO internal debt collection: What if today's payment file is not loaded into the system?

        if (!isset($this->claimAction->claim_action_type->identifier)) {
            $this->log('There is no claim action type - nothing to do here...', LogLevel::DEBUG);

            return;
        }

        //There might be a claim phase change happening here!
        $this->updateClaimPhase();

        if ($this->claimAction->claim_action_type->is_automated || $this->claimAction->done !== null) {
            $this->processClaimActionType();
        }
    }

    /**
     * @return \App\Model\Entity\Claim
     */
    private function updateClaimPhase(): Claim
    {
        /** @var Claim $claim */
        $claim = $this->claimAction->claim;

        // claim is ended or in legal phase so we dont update its phase
        if ($claim->claim_phase_id == "e003063d-518c-4903-b45b-7df2eddfbf5b" || $claim->claim_phase_id == "f571fcbf-8c59-4726-aeac-f05354b0811a") {
            return $claim;
        }

        if (!isset($this->claimAction->claim_action_type->claim_phase_id)) {
            return $claim;
        }

        /** @var ClaimPhase|null $oldClaimPhase */
        $oldClaimPhase = $this->claimAction->claim->claim_phase;
        $newClaimPhase = $this->claimAction->claim_action_type->claim_phase;

        if ($newClaimPhase === null) {
            return $claim;
        }

        if (isset($oldClaimPhase, $newClaimPhase) && $oldClaimPhase->id === $newClaimPhase->id) {
            return $claim;
        }

        $this->log(
            sprintf(
                'Changing claim: "%s" from phase "%s" to phase "%s"',
                $claim->id,
                $oldClaimPhase->id ?? 'null',
                $newClaimPhase->id
            ),
            LogLevel::DEBUG
        );

        $claim = $this->ClaimActions->Claims->patchEntity($claim, ['claim_phase_id' => $newClaimPhase->id]);

        if ($claim->isDirty()) {
            $this->ClaimActions->Claims->saveOrFail($claim);

            return $claim;
        }

        $this->log('There was nothing to change...', LogLevel::DEBUG);

        return $claim;
    }

    /**
     *
     */
    private function processClaimActionType(): void
    {
        if (!isset($this->claimAction->claim_action_type->collector)) {
            $this->log(
                sprintf('No collector set for the ClaimActionType %s', $this->claimAction->claim_action_type_id),
                LogLevel::WARNING
            );

            return;
        }

        if ($this->claimAction->claim_action_type->collector->identifier !== 'debito') {
            return;
        }

        $classname = sprintf(
            '\DebtCollectionTools\ClaimActionTypeHandlers\\%sAdapter',
            Inflector::classify($this->claimAction->claim_action_type->identifier)
        );

        if (class_exists($classname)) {
            /** @var \DebtCollectionTools\ClaimActionTypeHandlers\ClaimActionTypeHandlerInterface $handler */
            $handler = (new $classname($this->claimAction));
            $handler->process();

            return;
        }

        throw new RuntimeException(
            sprintf('No handler found for ClaimActionType: %s', $this->claimAction->claim_action_type->identifier)
        );
    }
}