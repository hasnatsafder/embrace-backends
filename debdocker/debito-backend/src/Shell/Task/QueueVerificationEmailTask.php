<?php
namespace App\Shell\Task;

use App\Utility\NotifyUser;
use Cake\ORM\TableRegistry;
use Queue\Shell\Task\QueueTask;
use Queue\Shell\Task\QueueTaskInterface;

class QueueVerificationEmailTask extends QueueTask implements QueueTaskInterface {

    /**
     * @var int
     */
    public $timeout = 20;

    /**
     * @var int
     */
    public $retries = 1;

    /**
     * @param array $data The array passed to QueuedJobsTable::createJob()
     * @param int $jobId The id of the QueuedJob entity
     * @return void
     */
    public function run(array $data, $jobId) {
        $verification_record = TableRegistry::getTableLocator()->get('AccountVerifications')->get($data['record_id']);
        $account = TableRegistry::getTableLocator()->get('Accounts')->get($verification_record->account_id);
        NotifyUser::sendAccountVerificationEmail($account, $verification_record);
    }

}
