<?php
declare(strict_types=1);

namespace App\Shell\Task;

use App\Model\Entity\Notification;
use Cake\Mailer\MailerAwareTrait;
use Queue\Model\QueueException;
use Queue\Shell\Task\QueueTask;

/**
 * Class QueueSendNotificationTask
 *
 * @package App\Shell\Task
 * @property \App\Model\Table\NotificationSummariesTable $NotificationSummaries
 */
class QueueSendNotificationSummaryTask extends QueueTask
{
    use MailerAwareTrait;

    public $modelClass = 'App.NotificationSummaries';

    /**
     * Main execution of the task.
     *
     * @param array $data The array passed to QueuedJobsTable::createJob()
     * @param int $jobId The id of the QueuedJob entity
     */
    public function run(array $data, $jobId): void
    {
        if (empty($data['notificationSummaryId'])) {
            throw new QueueException('"notificationSummaryId" is required');
        }

        $notificationSummary = $this->NotificationSummaries->get($data['notificationSummaryId']);

        $this->getMailer('NotificationSummaries')->send('notification_summary', [$notificationSummary]);
    }

    /**
     * @param \App\Model\Entity\Notification $notification
     * @return string
     */
    public function getMailElement(Notification $notification): string
    {
        switch ($notification->template) {
            case Notification::TEMPLATE_CLAIM_ACTION:
                return 'claim_action';
            case Notification::TEMPLATE_CLAIM_ACTION_EXTERNAL_NOTE:
                return 'claim_action_external_note';
            default:
                return 'fallback';
        }
    }
}