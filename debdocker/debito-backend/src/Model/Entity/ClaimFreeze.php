<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimFreeze Entity
 *
 * @property string $id
 * @property string $created_by_id
 * @property string $claim_id
 * @property \Cake\I18n\FrozenTime $frozen_until
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User $created_by
 * @property \App\Model\Entity\Claim $claim
 * @property string|null $message
 */
class ClaimFreeze extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'frozen_until' => true,
    ];
}
