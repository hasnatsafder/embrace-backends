<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * ClaimLine Entity
 *
 * @property string $id
 * @property string $claim_id
 * @property string|null $claim_line_type_id
 * @property string|null $currency_id
 * @property string|null $invoice_id
 * @property int $placement
 * @property \Cake\I18n\FrozenDate|null $invoice_date
 * @property \Cake\I18n\FrozenDate|null $maturity_date
 * @property int|null $amount
 * @property string|null $invoice_reference
 * @property string $file_name
 * @property string|null $file_mime_type
 * @property int|null $file_size
 * @property string|null $file_dir
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\ClaimLineType|null $claim_line_type
 * @property \App\Model\Entity\Currency|null $currency
 * @property string|null $old_id
 * @property bool $synced
 * @property \App\Model\Entity\Invoice|null $invoice
 * @property \App\Model\Entity\ClaimTransaction[] $claim_transactions
 */
class ClaimLine extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'claim_line_type_id' => true,
        'currency_id' => true,
        'invoice_id' => true,
        'placement' => true,
        'invoice_date' => true,
        'maturity_date' => true,
        'amount' => true,
        'invoice_reference' => true,
        'file_name' => true,
        'file_mime_type' => true,
        'file_size' => true,
        'file_dir' => true,
    ];

    /**
     * The amount of interest that should be added per year
     *
     * @param \Cake\I18n\FrozenTime $time
     * @return float
     */
    public function yearlyInterestAmount(FrozenTime $time): float
    {
        $mainPrincipal = $this->getRemainingMainPrinciple($time);

        return round($mainPrincipal * Configure::read('DebtCollection.InterestRateForClaims') / 100, 4);

    }

    /**
     * The amount of interest that should be added per day
     *
     * @param \Cake\I18n\FrozenTime $time
     * @return float
     */
    public function dailyInterestAmount(FrozenTime $time): float
    {
        return round($this->yearlyInterestAmount($time) / Configure::read('DebtCollection.DaysInDebtCollectionYear'), 4);
    }

    /**
     * Returns what is the remaining main principle at some specific point in time
     *
     * @param \Cake\I18n\FrozenTime $time
     * @return float
     */
    protected function getRemainingMainPrinciple(FrozenTime $time = null): float
    {
        $initialMainPrinciple = round($this->amount / 100, 4);
        $accountPrinciplePaymentSplit = $this->getPrincipleSplitFactor($this->claim_id);

        $payments = $this->getSumOfClaimTransactions('PaymentTypes', $time);
        $feesAndInterestAdded = round($this->getSumOfClaimTransactions('FeesAndInterest', $time), 4);

        $paymentsTowardsPrinciple = round($payments * ($accountPrinciplePaymentSplit / 100), 4);
        $paymentsTowardsFees = round($payments - $paymentsTowardsPrinciple, 4);

        if (abs($feesAndInterestAdded - $paymentsTowardsFees) > 0.0001 && $feesAndInterestAdded > $paymentsTowardsPrinciple) {
            //So fees has not yet been paid fully!
            return round($initialMainPrinciple - $paymentsTowardsPrinciple, 4);
        }

        //All fess has been paid - include some payments
        $paymentsTowardsPrinciple += round($paymentsTowardsFees - $feesAndInterestAdded, 4);

        return round($initialMainPrinciple - $paymentsTowardsPrinciple, 4);
    }

    /**
     * Get a sum of specific ClaimTransactions' sum
     *
     * @param string $claimTransactionTypeFinder
     * @param \Cake\I18n\FrozenTime $endTime
     * @return float
     */
    protected function getSumOfClaimTransactions(string $claimTransactionTypeFinder, FrozenTime $endTime): float
    {
        $claimTransactionTypes = $this->getTableLocator()->get('ClaimTransactionTypes')
            ->find($claimTransactionTypeFinder)
            ->select(['ClaimTransactionTypes.id'])
            ->enableHydration(false)
            ->extract('id')
            ->toList();

        $claimTransactions = $this->getTableLocator()->get('ClaimTransactions')->find();
        $claimTransactions = $claimTransactions
            ->select([
                'sum' => $claimTransactions->func()->sum('ClaimTransactions.amount'),
            ])
            ->where([
                'ClaimTransactions.timestamp <=' => $endTime,
                'ClaimTransactions.claim_line_id' => $this->id,
                'ClaimTransactions.claim_transaction_type_id IN' => $claimTransactionTypes,
            ])
            ->enableHydration(false)
            ->first();

        return round(abs($claimTransactions['sum']) ?? 0.00, 4);
    }

    /**
     * Returns the split that is configured by the account that owns this claim
     *
     * @param string $claimId
     * @return float
     */
    protected function getPrincipleSplitFactor(string $claimId): float
    {
        $claim = $this->getTableLocator()->get('Claims')->get($claimId, ['finder' => 'withTrashed']);
        $account = $this->getTableLocator()->get('Accounts')->get($claim->account_id, ['finder' => 'withTrashed']);


        return $account->principal_split;
    }
}
