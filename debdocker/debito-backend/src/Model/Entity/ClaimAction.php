<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimAction Entity
 *
 * @property string $id
 * @property string|null $created_by_id
 * @property string|null $modified_by_id
 * @property string|null $user_id
 * @property string $claim_id
 * @property string|null $claim_line_id
 * @property string|null $collector_file_id
 * @property string $claim_action_type_id
 * @property int|null $paid
 * @property int|null $interest_added
 * @property int|null $debt_collection_fees
 * @property int|null $actual_paid
 * @property \Cake\I18n\FrozenDate $date
 * @property \Cake\I18n\FrozenTime|null $done
 * @property string|null $message
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User|null $created_by
 * @property \App\Model\Entity\User|null $modified_by
 * @property \App\Model\Entity\User|null $user
 * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\ClaimLine|null
 * @property \App\Model\Entity\CollectorFile|null $collector_file
 * @property \App\Model\Entity\ClaimActionType $claim_action_type
 */
class ClaimAction extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'claim_id' => true,
        'claim_line_id' => true,
        'collector_file_id' => true,
        'claim_action_type_id' => true,
        'date' => true,
        'done' => true,
        'message' => true,
    ];
}
