<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use LogicException;
use Tools\Model\Entity\EnumTrait;

/**
 * Account Entity
 *
 * @property string $id
 * @property string $created_by_id
 * @property string|null $company_type_id
 * @property string|null $country_id
 * @property string|null $name
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $vat_number
 * @property string|null $ean_number
 * @property string|null $website
 * @property string|null $bank_reg_number
 * @property string|null $bank_account_number
 * @property string|null $iban
 * @property bool $is_company
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $notification_summary_interval
 * @property float $principal_split
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property string|null $email
 *
 * @property \App\Model\Entity\User $created_by
 * @property \App\Model\Entity\CompanyType|null $company_type
 * @property \App\Model\Entity\Country|null $country
 * @property \App\Model\Entity\Claim[] $claims
 * @property \App\Model\Entity\Debtor[] $debtors
 * @property \App\Model\Entity\Collector[] $collectors
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\ErpIntegrationData[] $erp_integration_data
 * @property \App\Model\Entity\AccountsUser[] $accounts_users
 */
class Account extends Entity
{
    use EnumTrait;

    public const NOTIFICATION_SUMMARY_INTERVAL_INSTANT = 'INSTANTLY';
    public const NOTIFICATION_SUMMARY_INTERVAL_DAILY = 'DAILY';
    public const NOTIFICATION_SUMMARY_INTERVAL_WEEKLY = 'WEEKLY';
    public const NOTIFICATION_SUMMARY_INTERVAL_MONTHLY = 'MONTHLY';

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_type_id' => true,
        'country_id' => true,
        'name' => true,
        'address' => true,
        'zip_code' => true,
        'city' => true,
        'phone' => true,
        'vat_number' => true,
        'ean_number' => true,
        'website' => true,
        'email' => true,
        'bank_reg_number' => true,
        'bank_account_number' => true,
        'is_company' => true,
        'iban' => true,
        'swift_code' => true,
        'notification_summary_interval' => true,
        'notes' => true
    ];

    protected $_hidden = [
        'principal_split',
        'swift_code' => true,
    ];

    /**
     * @param null $value
     * @return array|string
     */
    public static function notificationSummaryIntervals($value = null)
    {
        $options = [
            static::NOTIFICATION_SUMMARY_INTERVAL_INSTANT => __('Instantly'),
            static::NOTIFICATION_SUMMARY_INTERVAL_DAILY => __('Daily'),
            static::NOTIFICATION_SUMMARY_INTERVAL_WEEKLY => __('Weekly'),
            static::NOTIFICATION_SUMMARY_INTERVAL_MONTHLY => __('Monthly'),
        ];

        return static::enum($value, $options);
    }

    public function addUser(string $userId, string $roleIdentifier): bool
    {
        $accountsUsersTable = TableRegistry::getTableLocator()->get('AccountsUsers');
        $rolesTable = TableRegistry::getTableLocator()->get('Roles');

        $role = $rolesTable->find()
            ->where(['Roles.identifier' => $roleIdentifier])
            ->firstOrFail();

        $accountsUserEntity = $accountsUsersTable->newEntity([
            'user_id' => $userId,
            'account_id' => $this->id,
            'role_id' => $role->id,
        ]);

        if (!$accountsUsersTable->save($accountsUserEntity)) {
            throw new LogicException(sprintf('Could not add user as role "%s"', $roleIdentifier));
        }

        return true;
    }
}
