<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Country Entity
 *
 * @property string $id
 * @property string $name
 * @property string|null $vat_number
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $city
 * @property string|null $zip_code
 * @property string|null $address
 * @property string $phone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 *
 * @property string|null $claim_id
 * @property \App\Model\Entity\Claim|null $claim
 */
class DebtorLawyer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'vat_number' => true,
        'email' => true,
        'phone' => true,
        'city' => true,
        'claim_id' => true,
        'zip_code' => true,
        'address' => true,
    ];
}
