<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * ClaimTransaction Entity
 *
 * @property string $id
 * @property string|null $created_by_id
 * @property string $claim_id
 * @property string|null $claim_line_id
 * @property string $claim_transaction_type_id
 * @property \Cake\I18n\FrozenTime $timestamp
 * @property \Cake\I18n\FrozenDate $date Read-only
 * @property float $amount
 * @property float $total
 * @property string|null $subject
 * @property string|null $description
 * @property int $position
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User|null $created_by
 * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\ClaimTransactionType $claim_transaction_type
 * @property \App\Model\Entity\ClaimLine|null $claim_line
 */
class ClaimTransaction extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'claim_line_id' => true,
        'claim_transaction_type_id' => true,
        'timestamp' => true,
        'amount' => true,
        'subject' => true,
        'description' => true,
    ];

    /**
     * Returns the previous ClaimTransaction in case there is any.
     *
     * @return \App\Model\Entity\ClaimTransaction|null
     */
    public function getPreviousClaimTransaction(): ?ClaimTransaction
    {
        if ($this->position <= 1) {
            return null;
        }

        $where = [
            'ClaimTransactions.claim_id' => $this->claim_id,
            'ClaimTransactions.position' => $this->position - 1,
        ];

        if ($this->claim_line_id !== null) {
            $where['ClaimTransactions.claim_line_id'] = $this->claim_line_id;
        } else {
            $where['ClaimTransactions.claim_line_id IS'] = null;
        }

        return $this->getTableLocator()->get('ClaimTransactions')
            ->find()
            ->where($where)
            ->first();
    }

    public function isNewestTransactionOnClaim()
    {

    }
}
