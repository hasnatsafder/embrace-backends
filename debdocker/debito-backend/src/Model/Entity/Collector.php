<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Collector Entity
 *
 * @property string $id
 * @property string $name
 * @property string $identifier
 * @property bool $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\ClaimActionType[] $claim_action_types
 * @property \App\Model\Entity\CollectorFile[] $collector_files
 * @property \App\Model\Entity\Account[] $accounts
 * @property string|null $email
 * @property \App\Model\Entity\Claim[] $claims
 */
class Collector extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'identifier' => true,
        'is_active' => true,
    ];
}
