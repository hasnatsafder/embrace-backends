<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimActionType Entity
 *
 * @property string $id
 * @property string|null $claim_phase_id
 * @property string $collector_id
 * @property string $identifier
 * @property string|null $collector_identifier
 * @property string $name
 * @property string|null $description
 * @property bool $is_public
 * @property bool $is_automated
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\ClaimPhase|null $claim_phase
 * @property \App\Model\Entity\Collector $collector
 * @property \App\Model\Entity\ClaimAction[] $claim_actions
 */
class ClaimActionType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_phase_id' => true,
        'collector_id' => true,
        'identifier' => true,
        'collector_identifier' => true,
        'name' => true,
        'description' => true,
        'is_public' => true,
        'is_automated' => true,
    ];
}
