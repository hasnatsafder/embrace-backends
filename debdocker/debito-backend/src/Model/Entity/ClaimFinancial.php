<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClaimFinancial Entity
 *
 * @property string $id
 * @property string $claim_id
 * @property string $collector_file_id
 * @property \Cake\I18n\FrozenDate $date
 * @property string $content
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Claim $claim
 * @property \App\Model\Entity\CollectorFile $collector_file
 * @property int|null $hovedstol
 * @property int|null $rykkergebyrer
 * @property int|null $ovrige_gebyrer
 * @property int|null $renter
 * @property int|null $retsgebyrer
 * @property int|null $inkassoomkostninger
 * @property int|null $tilkendt_modesalar
 * @property int|null $indbetalt
 * @property int|null $restgald
 */
class ClaimFinancial extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'claim_id' => true,
        'hovedstol' => true,
        'rykkergebyrer' => true,
        'ovrige_gebyrer' => true,
        'retsgebyrer' => true,
        'tilkendt_modesalar' => true,
        'indbetalt' => true,
        'restgald' => true,
    ];
}
