<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use RuntimeException;

/**
 * DunningConfiguration Entity
 *
 * @property string $id
 * @property string|null $created_by_id
 * @property string|null $modified_by_id
 * @property string $account_id
 * @property string|null $name
 * @property int|null $soft_reminder_freeze
 * @property int $warning_one_freeze
 * @property int|null $warning_two_freeze
 * @property int|null $warning_three_freeze
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User|null $created_by
 * @property \App\Model\Entity\User|null $modified_by
 * @property \App\Model\Entity\Account $account
 */
class DunningConfiguration extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'name' => true,
        'soft_reminder_freeze' => true,
        'warning_one_freeze' => true,
        'warning_two_freeze' => true,
        'warning_three_freeze' => true,
    ];

    public function warningToAddWarningOfDebtCollection(): int
    {
        if ($this->warning_three_freeze !== null) {
            return 3;
        }

        if ($this->warning_two_freeze !== null) {
            return 2;
        }

        if ($this->warning_one_freeze !== null) {
            return 1;
        }

        throw new RuntimeException('Did not find a warning where the debt collection warning should be added');
    }
}
