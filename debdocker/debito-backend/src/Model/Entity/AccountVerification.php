<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * AccountsCollector Entity
 *
 * @property string $id
 * @property string $account_id
 * @property string $user_id
 * @property \Cake\I18n\FrozenTime|null $form_key_expire
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\User $user
 */
class AccountVerification extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'account_id' => true,
        'user_id' => true,
        'name' => true,
        'email' => true,
        'user_type' => true,
        'address' => true,
        'zip_code' => true,
        'city' => true,
        'phone' => true,
        'share_percent' => true,
        'approved' => true,
        'filled' => true,
        'remarks' => true,
        'form_key' => true,
        'form_key_expire' => true,
    ];

    /**
     * Add a new task for sending email in Queue
     * @return bool
     */
    public function addEmailVerificationQueue() : bool {
        // Somewhere in the model or lib
        $add = TableRegistry::getTableLocator()->get('Queue.QueuedJobs')->createJob('VerificationEmail',
            ['record_id' => $this->id]);
        if ($add) {
            return true;
        }
        return false;
    }
}
