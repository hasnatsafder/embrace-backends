<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;

/**
 * User Entity
 *
 * @property string $id
 * @property string|null $active_account_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $phone
 * @property string $email
 * @property string $password
 * @property bool $is_confirmed
 * @property bool $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Account|null $active_account
 * @property \App\Model\Entity\Account[] $accounts
 * @property string $token
 * @property bool $verified
 * @property \App\Model\Entity\Claim[] $claims
 * @property \App\Model\Entity\AccountsUser[] $accounts_users
 * @property string $full_name
 */
class User extends Entity
{
    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'active_account_id' => true,
        'first_name' => true,
        'last_name' => true,
        'phone' => true,
        'email' => true,
        'password' => true,
        'token' => true,
        'verified' => true,
        'old_password' => true,
    ];

    protected $_virtual = [
        'full_name',
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    /**
     * @param string $value New password in raw string
     * @return bool|null|string Null if no password, false if hashing failed and string if password hashed successfully
     */
    protected function _setPassword($value)
    {
        if (!empty($value)) {
            return (new DefaultPasswordHasher())->hash($value);
        }

        return null;
    }

    /**
     * @return string
     */
    protected function _getFullName()
    {
        if (empty($this->last_name)) {
            return $this->first_name;
        }

        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    /**
     * Checks for a role from it's identifier, optionally in a specific account
     *
     * @param string $string
     * @param string|null $accountId
     * @return bool
     */
    public function hasRole(string $string, ?string $accountId = null): bool
    {
        $where = [
            'AccountsUsers.user_id' => $this->id,
        ];

        if ($accountId !== null) {
            $where['AccountsUsers.account_id'] = $accountId;
        }

        return !$this->getTableLocator()->get('AccountsUsers')->find()
            ->where($where)
            ->matching('Roles', function (Query $q) use ($string) {
                return $q->where(['Roles.identifier' => $string]);
            })
            ->isEmpty();
    }
}
