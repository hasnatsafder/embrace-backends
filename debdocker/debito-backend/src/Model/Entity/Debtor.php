<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Debtor Entity
 *
 * @property string $id
 * @property string|null $created_by_id
 * @property string $account_id
 * @property string|null $country_id
 * @property string|null $erp_integration_id
 * @property string|null $vat_number
 * @property string|null $company_name
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $address
 * @property string|null $zip_code
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $latitude
 * @property string|null $langitude
 * @property string|null $erp_integration_foreign_key
 * @property string $login_code
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\User|null $created_by
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\Country|null $country
 * @property \App\Model\Entity\ErpIntegration|null $erp_integration
 * @property \App\Model\Entity\Claim[] $claims
 * @property string|null $old_id
 * @property bool $is_company
 *
 * @property string $full_name
 */
class Debtor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'country_id' => true,
        'erp_integration_id' => true,
        'vat_number' => true,
        'company_name' => true,
        'first_name' => true,
        'last_name' => true,
        'address' => true,
        'zip_code' => true,
        'city' => true,
        'phone' => true,
        'email' => true,
        'is_company' => true,
        'erp_integration_foreign_key' => true,
    ];

    protected $_virtual = [
        'full_name',
    ];

    protected $_hidden = [
        'login_code',
    ];

    public function _getFullName(): ?string
    {
        $personName = $this->first_name;
        if (!empty($this->last_name)) {
            $personName .= ' ' . $this->last_name;
        }

        if (!$this->is_company || $this->company_name === null) {
            return $personName;
        }

        if ($this->is_company && empty($personName)) {
            return $this->company_name;
        }
        return sprintf('%s (%s)', $this->company_name, $personName);
    }
}
