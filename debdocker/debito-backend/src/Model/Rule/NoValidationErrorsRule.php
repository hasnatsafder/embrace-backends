<?php
declare(strict_types=1);

namespace App\Model\Rule;

use Cake\ORM\Entity;
use Cake\Validation\Validator;

/**
 * Class NotChangeableRule
 *
 * @package App\Model\Rule
 */
class NoValidationErrorsRule
{
    /**
     * The list of fields to check
     *
     * @var array
     */
    protected $_fields;

    /**
     * @var \Cake\Validation\Validator
     */
    protected $validator;

    public function __construct(array $columns, Validator $validator)
    {
        $this->_fields = $columns;
        $this->validator = $validator;
    }

    public function __invoke(Entity $entity, array $options)
    {
        $data = $entity->extract($this->_fields, true);
        $errors = $this->validator->errors($data, $entity->isNew());
        $entity->setErrors($errors);

        return empty($errors);
    }
}
