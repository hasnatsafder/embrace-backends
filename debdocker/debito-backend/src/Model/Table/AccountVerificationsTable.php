<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\AccountVerification;
use Cake\Datasource\EntityInterface;
use ArrayObject;
use Cake\Event\Event;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * AccountVerification Model
 *
 */
class AccountVerificationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('account_verifications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Documents', [
            'foreignKey' => 'external_key',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('email')
            ->maxLength('email', 255);

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->allowEmptyString('phone');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmptyString('name');

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->allowEmptyString('address');

        $validator
            ->scalar('zip_code')
            ->maxLength('zip_code', 255)
            ->allowEmptyString('zip_code');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->allowEmptyString('city');

        $validator
            ->scalar('form_key')
            ->maxLength('form_key', 255)
            ->allowEmptyString('form_key');

        $validator
            ->scalar('city')
            ->maxLength('city', 255)
            ->allowEmptyString('city');

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 255)
            ->allowEmptyString('remarks');

        $validator
            ->dateTime('form_key_expiry')
            ->allowEmptyString('form_key_expiry');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }

    /**
     * @param Event $event
     * @param AccountVerification $account_verification
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, AccountVerification $account_verification, ArrayObject $options)
    {
        if ($account_verification->isNew()) {
            $user = TableRegistry::getTableLocator()->get("users")->find('all')->where(['email' => $account_verification->email])->first();
            if ($user) {
                $account_verification->user_id = $user->id;
            }
            $account_verification->form_key = Text::uuid();
        }
        if ($account_verification->isDirty('form_key')) {
            $account_verification->form_key_expire = FrozenTime::now()->addMonth();
        }
    }
    /**
     * @param Event $event
     * @param AccountVerification $account_verification
     * @param ArrayObject $options
     */
    public function afterSave(Event $event, AccountVerification $account_verification, ArrayObject $options)
    {
        if ($account_verification->isDirty('form_key')) {
            $account_verification->addEmailVerificationQueue();
        }
    }
}
