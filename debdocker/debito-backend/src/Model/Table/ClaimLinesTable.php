<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimLine;
use ArrayObject;
use Cake\Event\Event;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use InvalidArgumentException;

/**
 * ClaimLines Model
 *
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\BelongsTo $Claims
 * @property \App\Model\Table\ClaimLineTypesTable&\Cake\ORM\Association\BelongsTo $ClaimLineTypes
 * @property \App\Model\Table\CurrenciesTable&\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\InvoicesTable&\Cake\ORM\Association\BelongsTo $Invoices
 *
 * @method \App\Model\Entity\ClaimLine get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimLine newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimLine[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLine|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimLine patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLine[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimLine findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimLine saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @mixin \Josegonzalez\Upload\Model\Behavior\UploadBehavior
 * @method \App\Model\Entity\ClaimLine[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @property \App\Model\Table\ClaimTransactionsTable&\Cake\ORM\Association\HasMany $ClaimTransactions
 */
class ClaimLinesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_lines');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file_name' => [
                'fields' => [
                    'dir' => 'file_dir',
                    'size' => 'file_size',
                    'type' => 'file_mime_type',
                ],
                'nameCallback' => function (array $data, array $settings) {
                    $match = [];
                    preg_match('/\.[0-9a-z]+$/i', $data['name'], $match);

                    return Text::uuid() . $match[0];
                },
            ],
        ]);

        $this->belongsTo('Claims', [
            'foreignKey' => 'claim_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ClaimLineTypes', [
            'foreignKey' => 'claim_line_type_id',
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
        ]);
        $this->hasMany('ClaimTransactions');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('placement')
            ->allowEmptyString('placement', null, 'create');

        $validator
            ->uploadedFile('file', []);

        $validator
            ->date('invoice_date')
            ->allowEmptyString('invoice_date');

        $validator
            ->date('maturity_date')
            ->greaterThanOrEqualToField('maturity_date', 'invoice_date')
            ->allowEmptyString('maturity_date');

        $validator
            ->integer('amount')
            ->allowEmptyString('amount');

        $validator
            ->scalar('invoice_reference')
            ->maxLength('invoice_reference', 255)
            ->allowEmptyString('invoice_reference');

        return $validator;
    }

    public function validationWithCustomFile(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('file_name', 'create')
            ->notEmptyFile('file_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['claim_id'], 'Claims'));
        $rules->add($rules->existsIn(['claim_line_type_id'], 'ClaimLineTypes'));
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));

        return $rules;
    }

    public function beforeSave(Event $event, ClaimLine $entity, ArrayObject $options)
    {
        // Automagically handles the "placement" and "total" property
        if (empty($entity->placement)) {
            $entity->placement = 1;

            //Set it to the the highest existing claim line on the claim +1
            $highestPlacementInClaim = $this
                ->find('HighestPlacementInClaim', ['claimId' => $entity->claim_id])
                ->first();

            if ($highestPlacementInClaim !== null) {
                $entity->placement = $highestPlacementInClaim->placement + 1;
            }
        }
    }

    public function afterSave(Event $event, ClaimLine $entity, ArrayObject $options)
    {
        //Automatically creates new ClaimTransactions so it is up to date
        if ($entity->isNew() && $this->isInternalCollection($entity)) {
            $this->createClaimTransaction($entity);
        }
    }

    /**
     * Find the current max placement on an existing claim
     *
     * @param \Cake\ORM\Query $q
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findHighestPlacementInClaim(Query $q, array $options): Query
    {
        if (empty($options['claimId'])) {
            throw new InvalidArgumentException('No `claimId` was provided');
        }

        return $q
            ->where(['ClaimLines.claim_id' => $options['claimId']])
            ->order(['ClaimLines.placement' => 'DESC'])
            ->limit(1);
    }

    /**
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findAddingInterest(Query $query, array $options): Query
    {
        $claimLineTypesWithInterest = $this->ClaimLineTypes->find('WillAddInterest')
            ->select(['id'])
            ->enableHydration(false)
            ->extract('id')
            ->toArray();

        return $query->where([$this->aliasField('claim_line_type_id') . ' IN' => $claimLineTypesWithInterest]);
    }

    /**
     * @param \App\Model\Entity\ClaimLine $entity
     * @return bool
     */
    private function isInternalCollection(ClaimLine $entity): bool
    {
        $claim = $this->Claims->get($entity->claim_id, ['contain' => ['Collectors']]);

        return $claim->collector->identifier === 'debito';
    }

    /**
     * @param \App\Model\Entity\ClaimLine $claimLine
     */
    private function createClaimTransaction(ClaimLine $claimLine)
    {
        $claimLineType = $this->ClaimLineTypes->get($claimLine->claim_line_type_id);
        $claimLineTransactionType = $this->ClaimTransactions->ClaimTransactionTypes->getTypeFromClaimLineTypeIdentifier($claimLineType->identifier);

        if ($claimLineTransactionType === null) {
            return;
        }

        $amount = round($claimLine->amount / 100, 4);

        switch ($claimLineType->identifier) {
            case 'invoice':
            case 'document':
            case 'created_by_error':
            case 'late_payment_fee':
            case 'compensation_fee':
                break;
            case 'payment':
            case 'credit_note':
                $amount *= -1;
                break;
        }

        $claimTransaction = $this->ClaimTransactions->newEntity([
            'claim_id' => $claimLine->claim_id,
            'claim_line_id' => $claimLine->id,
            'claim_transaction_type_id' => $claimLineTransactionType,
            'timestamp' => FrozenTime::now(),
            'amount' => $amount,
            'subject' => null,
            'description' => null,
        ]);

        $this->ClaimTransactions->saveOrFail($claimTransaction);
    }
}
