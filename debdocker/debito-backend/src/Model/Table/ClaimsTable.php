<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Claim;
use App\Model\Entity\User;
use App\Model\Rule\NoValidationErrorsRule;
use App\Model\Table\Traits\InAccountFinderTrait;
use ArrayObject;
use Cake\Event\Event;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use InvalidArgumentException;
use Search\Model\Filter\Callback;

/**
 * Claims Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\DebtorsTable&\Cake\ORM\Association\BelongsTo $Debtors
 * @property \App\Model\Table\ClaimPhasesTable&\Cake\ORM\Association\BelongsTo $ClaimPhases
 * @property \App\Model\Table\CollectorsTable&\Cake\ORM\Association\BelongsTo $Collectors
 * @property \App\Model\Table\DunningConfigurationsTable&\Cake\ORM\Association\BelongsTo $DunningConfigurations
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CurrenciesTable&\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\ClaimActionsTable&\Cake\ORM\Association\HasMany $ClaimActions
 * @property \App\Model\Table\ClaimFinancialsTable&\Cake\ORM\Association\HasMany $ClaimFinancials
 * @property \App\Model\Table\ClaimFreezesTable&\Cake\ORM\Association\HasMany $ClaimFreezes
 * @property \App\Model\Table\ClaimLinesTable&\Cake\ORM\Association\HasMany $ClaimLines
 * @property \App\Model\Table\DebtorLawyersTable&\Cake\ORM\Association\HasOne $DebtorLawyers
 * @method \App\Model\Entity\Claim newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Claim[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Claim get($primaryKey, $options = [])
 * @method \App\Model\Entity\Claim findOrCreate($search, callable $callback = null, $options = [])
 * @method \App\Model\Entity\Claim patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Claim[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Claim|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Claim saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Claim[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @property \App\Model\Table\ClaimTransactionsTable&\Cake\ORM\Association\HasMany $ClaimTransactions
 */
class ClaimsTable extends Table
{
    use InAccountFinderTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claims');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('CreatedBy', ['setAccountIdOnNew' => true]);
        $this->addBehavior('Search.Search');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
        ]);
        $this->belongsTo('Debtors', [
            'foreignKey' => 'debtor_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('ClaimPhases', [
            'foreignKey' => 'claim_phase_id',
        ]);
        $this->belongsTo('Collectors', [
            'foreignKey' => 'collector_id',
            'joinType' => 'LEFT',
        ]);
        $this->belongsTo('DunningConfigurations', [
            'foreignKey' => 'dunning_configuration_id',
            'joinType' => 'LEFT',
            'finder' => 'withTrashed',
        ]);
        $this->hasMany('ClaimActions', [
            'foreignKey' => 'claim_id',
        ]);
        $this->hasMany('ClaimFinancials', [
            'foreignKey' => 'claim_id',
        ]);
        $this->hasMany('ClaimFreezes', [
            'foreignKey' => 'claim_id',
        ]);
        $this->hasMany('ClaimLines', [
            'foreignKey' => 'claim_id',
        ]);
        $this->hasMany('ClaimTransactions', [
            'foreignKey' => 'claim_id',
        ]);
        $this->belongsTo('Admin', [
            'foreignKey' => 'user_handle',
            'joinType' => 'LEFT',
            'className' => 'Users'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'created_by_id',
        ]);
        $this->belongsTo('ClaimPhases', [
            'foreignKey' => 'claim_phase_id',
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
        ]);
        $this->hasOne('DebtorLawyers', [
            'foreignkey' => 'claim_id',
        ]);
        // $this->hasMany('ClaimDunnings', [
        //     'foreignKey' => 'claim_id',
        // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('customer_reference')
            ->maxLength('customer_reference', 255)
            ->allowEmptyString('customer_reference');

        $validator
            ->scalar('collector_reference')
            ->maxLength('collector_reference', 255)
            ->allowEmptyString('collector_reference');

        $validator
            ->integer('fi_payment_reference')
            ->lessThanOrEqual('fi_payment_reference', 99999999999999)
            ->allowEmptyString('fi_payment_reference');

        $validator
            ->scalar('fi_check_digit')
            ->inList('fi_check_digit', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
            ->allowEmptyString('fi_check_digit');

        $validator
            ->scalar('dispute')
            ->allowEmptyString('dispute');

        $validator
            ->dateTime('approved')
            ->allowEmptyString('approved');

        $validator
            ->dateTime('completed')
            ->allowEmptyString('completed');

        $validator
            ->dateTime('synced')
            ->allowEmptyString('synced');

        $validator
            ->dateTime('ended_at')
            ->allowEmptyString('ended_at');

        $validator
            ->dateTime('frozen_until')
            ->allowEmptyDateTime('frozen_until');

        $validator
            ->scalar('rejected_reason')
            ->maxLength('rejected_reason', 255)
            ->allowEmptyString('rejected_reason');

        return $validator;
    }

    public function validationApproval(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('debtor_id');

        $validator
            ->requirePresence('collector_id');

        $validator
            ->requirePresence('customer_reference')
            ->notEmptyString('customer_reference');

        return $validator;
    }

    public function validationFreezeClaim(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->allowEmptyDateTime('frozen_until', false)
            ->requirePresence('frozen_until');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['debtor_id'], 'Debtors'));
        $rules->add($rules->existsIn(['claim_phase_id'], 'ClaimPhases'));
        $rules->add($rules->existsIn(['collector_id'], 'Collectors'));
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));

        $rules->add(
            function (Claim $claim, array $options) {
                if (!$claim->isDirty('frozen_until') || $claim->frozen_until === null) {
                    return true;
                }

                if ($claim->collector_id === null) {
                    return true;
                }

                $collector = $this->Collectors->get($claim->collector_id, ['finder' => 'withTrashed']);

                return $collector->identifier === 'debito';
            },
            'onlyFreezeInternalClaims',
            [
                'errorField' => 'frozen_until',
                'message' => __('You can only freeze Claims that has either Debito or no collector'),
            ]
        );

        $rules->add(
            new NoValidationErrorsRule(
                $this->getSchema()->columns(),
                $this->getValidator('default')
            )
        );

        $rules->add(
            function (Claim $claim, array $options) {
                if ($claim->fi_payment_reference === null) {
                    return true;
                }
                return $claim->fi_check_digit == $this->generateFiCheckCipher(sprintf('%014d', $claim->fi_payment_reference));
            },
            'validFiPaymentReference',
            [
                'errorField' => 'fi_payment_reference',
                'message' => __('"fi_payment_reference" does not match check cipher'),
            ]
        );

        //TODO: Maybe require fi code to match with check digit?

        return $rules;
    }

    public function findApproved(Query $query, array $options): Query
    {
        return $query->where(['Claims.approved IS NOT' => null]);
    }

    public function findNotApproved(Query $query, array $options): Query
    {
        return $query->where(['Claims.approved IS' => null]);
    }

    /**
     * @param \Cake\ORM\Query $query
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findActivelyAddingInterest(Query $query, array $options): Query
    {
        $claimPhasesWithInterest = $this->ClaimPhases->find('WillAddInterest')->select(['id'])->enableHydration(false)->extract('id')->toArray();
        $debitoCollector = $this->Collectors->getFromIdentifier('debito');

        return $query
            ->where([
                $this->aliasField('completed') . ' IS NOT' => null,
                $this->aliasField('claim_phase_id') . ' IN' => $claimPhasesWithInterest,
                $this->aliasField('collector_id') => $debitoCollector->id,
            ]);
    }

    public function beforeSave(Event $event, Claim $entity, $options)
    {
        if ($entity->fi_payment_reference === null) {
            $entity->getOrGeneratePaymentFiCode();
        }

        if ($entity->isNew()) {
            //setting customer_reference contactenation of VAT and number of times it got added
            if (!$entity->customer_reference) {
                $entity->customer_reference = $this->assignCustomerReference($entity->account_id);
            }

            //setting claim phase to order 1
            if (!$entity->claim_phase_id) {
                $claimPhaseOrder1 = $this->ClaimPhases->find()
                    ->where(['ClaimPhases.identifier' => 'case_received'])
                    ->first();
                $entity->claim_phase_id = $claimPhaseOrder1->id;
            }

            //setting currency to danish
            if (!$entity->currency_id) {
                $currencyDkk = $this->Currencies->find('all', ['conditions' => ['iso_code' => 'DKK']])->first();
                $entity->currency_id = $currencyDkk->id;
            }

            //if no collector is selected then set collectia
            if (!$entity->collector_id) {
                $collectia = $this->Collectors->find('all')->where(['identifier' => 'collectia'])->select(['id'])->first();
                $entity->collector_id = $collectia->id;
            }
        }
    }

    /**
     * @param \Cake\Event\Event $event
     * @param \App\Model\Entity\Claim $claim
     * @param \ArrayObject $options
     */
    public function afterSave(Event $event, Claim $claim, ArrayObject $options)
    {
        if ($claim->isDirty('frozen_until')) {
            $user = $this->Users->get($options['_footprint']['id'], ['finder' => 'withTrashed']);

            $this->createClaimFreezeRecords($claim, $user, $options['freezeMessage'] ?? null);
        }

        //TODO Internal debt collection maybe??? (Create claim action: "claim_started")
        //if ($claim->completed !== null && $claim->isDirty('completed') && $claim->getOriginal('completed') === null) {
        //}
        if ($claim->isDirty('claim_phase_id')) {
            $claim_phase_table = TableRegistry::getTableLocator()->get('claim_phases');
            $claim_phase = $claim_phase_table->get($claim->claim_phase_id);

            $claim_action_types_table = TableRegistry::getTableLocator()->get('claim_action_types');
            $claim_action_type = $claim_action_types_table->find()
                                ->where(['identifier' => "transition_to_" . strtolower($claim_phase->identifier)])
                                ->first();
            if ($claim_action_type) {
                $claim_action_table = TableRegistry::getTableLocator()->get('claim_actions');
                // if last claim transition action was same then dont insert a new one
                $claim_action_last = $claim_action_table->find('all')
                                    ->where(['claim_id' => $claim->id])
                                    ->order(['date' => 'asc'])->last();
                if ($claim_action_last && $claim_action_last->claim_action_type_id == $claim_action_type->id) {
                    // do nothing
                }
                else {
                    $claim_action = $claim_action_table->newEntity();
                    $claim_action->created_by_id = $claim->created_by_id;
                    $claim_action->user_id = $claim->created_by_id;
                    $claim_action->claim_id = $claim->id;
                    $claim_action->claim_action_type_id = $claim_action_type->id;
                    $claim_action->date = FrozenTime::now();
                    $claim_action_table->save($claim_action);
                }
            }
        }
    }

    /**
     *  To assign new customer reference to a claim
     *
     * @params : account_id
     */
    private function assignCustomerReference($account_id)
    {
        $claims = TableRegistry::getTableLocator()->get('claims');
        $claimAccountsTotal = $claims->find('all', ['conditions' => ['account_id' => $account_id]])->count();

        $accounts = TableRegistry::getTableLocator()->get('accounts');
        $accountClaim = $accounts->get($account_id);

        //if client has vat_number, else use random numbers for vat_number
        if ($accountClaim->vat_number) {
            $vatNumber = $accountClaim->vat_number;
        } else {
            // if account has claim then get last claim
            $claimLast = $claims->find()->where(['account_id' => $account_id])->order(['created' => 'ASC'])->last();
            if ($claimLast && $claimLast->customer_reference) {
                // check if claim has '-' in the string
                if (strpos($claimLast->customer_reference, '-') !== false) {
                    // get everthing before '-'
                    $randomNumberArray = explode('-', $claimLast->customer_reference);
                    $vatNumber = $randomNumberArray[0];
                } else { // else assign a new random number
                    $vatNumber = "R" . rand(1, 100000000);
                }
            } else {// else assign a new random number
                $vatNumber = "R" . rand(1, 100000000);
            }
        }

        // keep increasing total claim number if we find customer_referene already exists.
        do {
            $claimAccountsTotal++;
            $newCustomerReference = $vatNumber . '-' . sprintf('%05d', $claimAccountsTotal);
            $found = $claims->find()->where(['customer_reference' => $newCustomerReference])->count();
        } while ($found != 0);

        return $newCustomerReference;
    }

    public function findWithCalculatedSumOfClaimLineAmounts(Query $query, array $options): Query
    {
        //Claim Phase for credit_note
        $claimLineTypeTable = TableRegistry::getTableLocator()->get('claim_line_types');
        $krediNotePhase = $claimLineTypeTable->find()->where(['identifier' => 'credit_note'])->select(['id'])->first();
        $paymentPhase = $claimLineTypeTable->find()->where(['identifier' => 'payment'])->select(['id'])->first();

        return $query
            ->select([
                'Claims.id',
                'Claims.created',
                'Claims.customer_reference',
                'Claims.rejected_reason',
                'Claims.rejected_is_public',
                'Debtors.first_name',
                'Debtors.last_name',
                'Debtors.company_name',
                'ClaimPhases.name',
                'ClaimPhases.order',
                'Currencies.iso_code',
                'claimLinesSum' => "SUM(CASE WHEN ClaimLines.claim_line_type_id NOT IN ('$krediNotePhase->id', '$paymentPhase->id')THEN ClaimLines.amount ELSE 0 END)",
                'claimLinesDeduct' => "SUM(CASE WHEN ClaimLines.claim_line_type_id IN ('$krediNotePhase->id', '$paymentPhase->id')THEN ClaimLines.amount ELSE 0 END)",
            ])
            ->leftJoinWith('ClaimLines')
            ->distinct('Claims.id');
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager()
    {
        /** @var \Search\Manager $searchManager */
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->add('search_term', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    $search_term = $args['search_term'];
                    $search_term = trim($search_term);
                    $search_term = strtolower($search_term);
                    $search_term = str_replace(" ", "", $search_term);
                    $query->where(['OR' => [
                        'LOWER(TRIM(Claims.customer_reference)) LIKE ' => "%".$search_term."%",
                        'CONCAT(LOWER(TRIM(Debtors.first_name)),LOWER(TRIM(Debtors.last_name))) LIKE ' => "%".$search_term."%",
                        'LOWER(TRIM(Debtors.company_name)) LIKE ' => "%".$search_term."%",
                    ]]);
                },
            ]);

        return $searchManager;
    }

    /**
     * @param \App\Model\Entity\Claim $claim
     * @param \App\Model\Entity\User $user
     * @param string|null $freezeMessage
     */
    private function createClaimFreezeRecords(Claim $claim, User $user, ?string $freezeMessage)
    {
        if ($freezeMessage === '') {
            $freezeMessage = null;
        }

        $freezeClaimActionType = $this->ClaimActions->ClaimActionTypes
            ->getClaimActionTypeForCollector('claim_freeze', 'debito');

        $claimAction = $this->ClaimActions->newEntity([
            'claim_id' => $claim->id,
            'date' => FrozenTime::now(),
            'claim_action_type_id' => $freezeClaimActionType->id,
            'message' => $freezeMessage,
        ]);

        $this->ClaimActions->saveOrFail($claimAction);

        $claimFreezeData = [
            'claim_id' => $claim->id,
            'claim_action_id' => $claimAction->id,
            'created_by_id' => $user->id,
            'frozen_until' => $claim->frozen_until,
        ];

        $claimFreeze = $this->ClaimFreezes->newEntity($claimFreezeData, [
            'accessibleFields' => [
                'claim_id' => true,
                'claim_action_id' => true,
                'created_by_id' => true,
                'message' => true,
            ],
        ]);

        $this->ClaimFreezes->saveOrFail($claimFreeze);
    }

    /**
     * @param string $paymentCode
     * @return int
     * @link https://danskebank.dk/PDF/Blanketter/ERHVERV/Indbetalingskort/Fremstilling_af_71-kort.pdf
     */
    public function generateFiCheckCipher(string $paymentCode): int
    {
        if (mb_strlen($paymentCode) !== 14) {
            throw new InvalidArgumentException('Payment code has to be 14 digits');
        }
        $counter = 0;

        foreach (str_split($paymentCode) as $digitNumber => $digit) {
            $digit = (int)$digit;
            $weight = $digitNumber % 2 === 0 ? 1 : 2;
            $sum = $digit * $weight;

            if ($sum >= 10) {
                $sum = array_sum(str_split((string)$sum));
            }
            $counter += $sum;
        }

        $controlDigit = 10 - ($counter % 10);

        if ($controlDigit === 10) {
            return 0;
        }
        return $controlDigit;
    }

    /**
     * @return int
     */
    public function getNextFiPaymentReference(): int
    {
        $previousFiPaymentReference = 1;

        $newestClaim = $this->find('withTrashed')
            ->select(['Claims.fi_payment_reference'])
            ->disableHydration()
            ->where(['Claims.fi_payment_reference IS NOT' => null])
            ->orderDesc('Claims.fi_payment_reference')
            ->limit(1)
            ->first();

        if ($newestClaim !== null) {
            $previousFiPaymentReference = $newestClaim['fi_payment_reference'];
        }

        return (int)$previousFiPaymentReference + 1;
    }
}
