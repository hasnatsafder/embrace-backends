<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Collector;
use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Collectors Model
 *
 * @property \App\Model\Table\ClaimActionTypesTable&\Cake\ORM\Association\HasMany $ClaimActionTypes
 * @property \App\Model\Table\CollectorFilesTable&\Cake\ORM\Association\HasMany $CollectorFiles
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 * @property \App\Model\Table\AccountsCollectorsTable&\Cake\ORM\Association\HasMany $AccountsCollectors
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsToMany $Accounts
 *
 * @method \App\Model\Entity\Collector newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Collector[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Collector get($primaryKey, $options = [])
 * @method \App\Model\Entity\Collector findOrCreate($search, callable $callback = null, $options = [])
 * @method \App\Model\Entity\Collector patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Collector[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Collector|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Collector saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Collector[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class CollectorsTable extends Table
{
    use IdentifierFindersTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('collectors');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->hasMany('ClaimActionTypes', [
            'foreignKey' => 'collector_id',
        ]);
        $this->hasMany('CollectorFiles', [
            'foreignKey' => 'collector_id',
        ]);
        $this->hasMany('Claims', [
            'foreignKey' => 'collector_id',
        ]);
        $this->belongsToMany('Accounts', [
            'foreignKey' => 'collector_id',
            'targetForeignKey' => 'account_id',
            'joinTable' => 'accounts_collectors',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('identifier');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add('identifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identifier']));

        return $rules;
    }
}
