<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Role;
use App\Model\Entity\User;
use App\Model\Table\Traits\InAccountFinderTrait;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $ActiveAccounts
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsToMany $Accounts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @property \App\Model\Table\AccountsUsersTable&\Cake\ORM\Association\HasMany $AccountsUsers
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class UsersTable extends Table
{
    use InAccountFinderTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setDisplayField('email');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');

        $this->belongsTo('ActiveAccounts', [
            'foreignKey' => 'active_account_id',
            'joinType' => 'LEFT',
            'className' => 'Accounts',
        ]);

        $this->belongsToMany('Accounts', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'account_id',
            'joinTable' => 'accounts_users',
        ]);

        $this->hasMany('AccountsUsers');

        $this->hasMany('Claims', [
            'foreignKey' => 'created_by_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 255)
            ->allowEmptyString('phone');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)

            //Can't be used while migrating old data since this was not a requirement by then
            //->minLength('password', 6)

            ->requirePresence('password', 'create')
            ->notEmptyString('password');

        $validator
            ->boolean('is_confirmed');

        $validator
            ->boolean('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['active_account_id'], 'ActiveAccounts'));

        return $rules;
    }

    public function beforeSave(Event $event, User $user, ArrayObject $options)
    {
        if ($user->isNew()) {
            $user->is_confirmed = true;
            $user->is_active = true;
        }
    }

    public function findAuth(Query $query, array $options): Query
    {
        return $query
            ->select(['id', 'active_account_id', 'email', 'password'])
            ->where([
                'Users.is_active' => true,
            ]);
    }

    public function findRootUsers(Query $q, array $options): Query
    {
        $rootRole = $this->AccountsUsers->Roles->getFromIdentifier('root');
        assert($rootRole instanceof Role);

        return $q
            ->find('Auth')
            ->matching('AccountsUsers', function (Query $q) use ($rootRole) {
                return $q->where(['AccountsUsers.role_id' => $rootRole->id]);
            });
    }
}
