<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Document;
use ArrayObject;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Document Model
 */
class DocumentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('documents');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'file_name' => [
                'fields' => [
                    'dir' => 'file_dir',
                    'size' => 'file_size',
                    'type' => 'file_mime_type',
                ],
                'nameCallback' => function (array $data, array $settings) {
                    $match = [];
                    preg_match('/\.[0-9a-z]+$/i', $data['name'], $match);

                    return Text::uuid() . $match[0];
                },
            ],
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->uploadedFile('file', []);

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 255)
            ->allowEmptyString('remarks');

        return $validator;
    }

    public function validationWithCustomFile(Validator $validator): Validator
    {
        $validator = $this->validationDefault($validator);

        $validator
            ->requirePresence('file_name', 'create')
            ->notEmptyFile('file_name');

        return $validator;
    }

    /**
     * After delete actions
     * @param Event $e
     * @param Document $document
     * @param \ArrayObject $options
     */
    public function afterDelete(Event $e, Document $document, ArrayObject $options)
    {
        $file = New File($document->file_dir . DS . $document->file_name);
        if ($file) {
            $file->delete();
            $file->close();
        }
    }
}
