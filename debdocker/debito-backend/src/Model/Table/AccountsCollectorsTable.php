<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\AccountsCollector;
use Cake\Datasource\EntityInterface;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccountsCollectors Model
 *
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\CollectorsTable&\Cake\ORM\Association\BelongsTo $Collectors
 *
 * @method AccountsCollector get($primaryKey, $options = [])
 * @method AccountsCollector newEntity($data = null, array $options = [])
 * @method AccountsCollector[] newEntities(array $data, array $options = [])
 * @method AccountsCollector|bool save(EntityInterface $entity, $options = [])
 * @method AccountsCollector patchEntity(EntityInterface $entity, array $data, array $options = [])
 * @method AccountsCollector[] patchEntities($entities, array $data, array $options = [])
 * @method AccountsCollector findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method AccountsCollector|bool saveOrFail(EntityInterface $entity, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class AccountsCollectorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts_collectors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Collectors', [
            'foreignKey' => 'collector_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('collector_identifier')
            ->maxLength('collector_identifier', 255)
            ->allowEmptyString('collector_identifier');

        $validator
            ->dateTime('requested_at')
            ->allowEmptyString('requested_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['collector_id'], 'Collectors'));

        return $rules;
    }
}
