<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\Claim;
use App\Model\Entity\ClaimLineType;
use App\Model\Entity\ClaimPhase;
use App\Model\Entity\PaidInvoice;
use App\Model\Entity\User;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;
use League\Flysystem\MountManager;
use RuntimeException;
use Search\Model\Filter\Callback;
use WyriHaximus\FlyPie\FilesystemsTrait;

/**
 * Invoices Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $CreatedBy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $ModifiedBy
 * @property \App\Model\Table\AccountsTable&\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\InvoiceStatusesTable&\Cake\ORM\Association\BelongsTo $InvoiceStatuses
 * @property \App\Model\Table\CurrenciesTable&\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\ErpIntegrationsTable&\Cake\ORM\Association\BelongsTo $ErpIntegrations
 * @property \App\Model\Table\DebtorsTable&\Cake\ORM\Association\BelongsTo $Debtors
 * @property \App\Model\Table\ClaimLinesTable&\Cake\ORM\Association\HasMany $ClaimLines
 * @property \App\Model\Table\ClaimDunningsTable|\Cake\ORM\Association\HasMany $ClaimDunningsTable
 *
 * @method \App\Model\Entity\Invoice get($primaryKey, $options = [])
 * @method \App\Model\Entity\Invoice newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Invoice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Invoice|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoice saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Invoice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Invoice findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Muffin\Footprint\Model\Behavior\FootprintBehavior
 * @mixin \App\Model\Behavior\CreatedByBehavior
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 * @mixin \Search\Model\Behavior\SearchBehavior
 * @property \App\Model\Table\ClaimDunningsTable&\Cake\ORM\Association\HasMany $ClaimDunnings
 * @method \App\Model\Entity\Invoice[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 */
class PaidInvoicesTable extends Table
{
    use FilesystemsTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('paid_invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CreatedBy', ['setAccountIdOnNew' => true]);
        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('InvoiceStatuses', [
            'foreignKey' => 'invoice_status_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Currencies');
        $this->belongsTo('ErpIntegrations', [
            'foreignKey' => 'erp_integration_id',
        ]);
        $this->belongsTo('Debtors', [
            'foreignKey' => 'debtor_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ClaimDunnings', [
            'foreignKey' => 'invoice_id',
        ]);
        $this->hasMany('ClaimLines');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('erp_integration_foreign_key')
            ->maxLength('erp_integration_foreign_key', 255)
            ->allowEmptyString('erp_integration_foreign_key');

        $validator
            ->integer('invoice_number')
            ->requirePresence('invoice_number', 'create')
            ->notEmptyString('invoice_number');

        $validator
            ->date('issue_date')
            ->requirePresence('issue_date', 'create')
            ->notEmptyDate('issue_date');

        $validator
            ->date('due_date')
            ->requirePresence('due_date', 'create')
            ->notEmptyDate('due_date');

        $validator
            ->dateTime('hidden_until');

        $validator
            ->decimal('net_amount')
            ->requirePresence('net_amount', 'create')
            ->notEmptyString('net_amount');

        $validator
            ->decimal('gross_amount')
            ->requirePresence('gross_amount', 'create')
            ->notEmptyString('gross_amount');

        // $validator
        //     ->maxLength('pdf_file_path', 255);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['invoice_status_id'], 'InvoiceStatuses'));
        $rules->add($rules->existsIn(['erp_integration_id'], 'ErpIntegrations'));
        $rules->add($rules->existsIn(['debtor_id'], 'Debtors'));

        return $rules;
    }

    /**
     * Search options for this model
     *
     * @return \Search\Manager
     */
    public function searchManager()
    {
        /** @var \Search\Manager $searchManager */
        $searchManager = $this->behaviors()->Search->searchManager();

        $searchManager
            ->value('invoice_status_id')
            ->value('converted_to_claim')
            ->add('only_passed_due_date', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    if ($args['only_passed_due_date'] === '1') {
                        $query->where(['Invoices.due_date <' => FrozenTime::now()]);
                    }
                },
            ])
            ->add('only_hidden', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    if ($args['only_hidden'] === '1') {
                        $query->where(['Invoices.hidden_until >=' => FrozenTime::now()]);
                    }
                },
            ])
            ->add('hide_hidden', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    if ($args['hide_hidden'] !== '1') {
                        return;
                    }

                    $query->where([
                        'OR' => [
                            'Invoices.hidden_until <=' => FrozenTime::now(),
                            'Invoices.hidden_until IS' => null,
                        ],
                    ]);
                },
            ])
            ->add('hidden_until', 'Search.Callback', [
                'callback' => function (Query $query, $args, $filter) {
                    //TODO Encapsulate this logic in a custom (reuseable filter)
                    $ormWhere = [];

                    foreach ($args['hidden_until'] as $arg) {
                        $operator = array_keys($arg)[0];
                        $value = array_values($arg)[0];

                        switch ($operator) {
                            case 'gt':
                                $ormWhere['Invoices.hidden_until >='] = $value;
                                break;
                            case 'lt':
                                $ormWhere['Invoices.hidden_until <='] = $value;
                                break;
                            case 'eq':
                                if ($value === 'null') {
                                    $ormWhere['Invoices.hidden_until IS'] = null;
                                    break;
                                }

                                $ormWhere['Invoices.hidden_until'] = $value;
                                break;
                            case 'null':
                                $ormWhere['Invoices.hidden_until IS'] = null;
                                break;
                        }
                    }

                    if (!empty($ormWhere)) {
                        $query->where($ormWhere);
                    }
                },
            ])
            ->add('status', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    $query->innerJoinWith('InvoiceStatuses', function (Query $q) use ($args) {
                        return $q->where(['InvoiceStatuses.identifier' => $args['status']]);
                    });
                },
            ])
            ->add('search_term', 'Search.Callback', [
                'callback' => function (Query $query, array $args, Callback $filter) {
                    $search_term = $args['search_term'];
                    $search_term = trim($search_term);
                    $search_term = strtolower($search_term);
                    $search_term = str_replace(" ", "", $search_term);
                    $query->where(['OR' => [
                        'LOWER(TRIM(Invoices.invoice_number)) LIKE ' => "%".$search_term."%",
                        'CONCAT(LOWER(TRIM(first_name)),LOWER(TRIM(last_name))) LIKE ' => "%".$search_term."%",
                        'LOWER(TRIM(Debtors.company_name)) LIKE ' => "%".$search_term."%",
                        'LOWER(TRIM(Debtors.erp_integration_foreign_key)) LIKE ' => "%".$search_term."%",
                    ]]);
                },
            ]);

        return $searchManager;
    }

    /**
     * @param \App\Model\Entity\Invoice[] $invoices
     * @return \App\Model\Entity\Claim
     * @throws \Exception
     */
    public function createClaim(array $invoices = [], User $createdBy, String $dunning_configuration_id = null): Claim
    {
        if ($invoices === []) {
            throw new RuntimeException('No invoices were provided - cannot create claim');
        }

        $tmpFilesystem = $this->filesystem('local_tmp');
        $fileMountManager = new MountManager([
            'erpInvoices' => $this->filesystem('erp_invoices'),
            'localTmp' => $tmpFilesystem,
        ]);

        $invoiceClaimLineType = $this->ClaimLines->ClaimLineTypes->getFromIdentifier('invoice');
        assert($invoiceClaimLineType instanceof ClaimLineType);

        $invoiceClaimLineTypeCreditNote = $this->ClaimLines->ClaimLineTypes->getFromIdentifier('credit_note');
        assert($invoiceClaimLineTypeCreditNote instanceof ClaimLineType);

        $claimPhase = $this->ClaimLines->Claims->ClaimPhases->getFromIdentifier('case_received');
        assert($claimPhase instanceof ClaimPhase);

        $claim = $this->ClaimLines->Claims->newEntity([
            'created_by_id' => $createdBy->id,
            'debtor_id' => $invoices[0]->debtor_id,
            'account_id' => $invoices[0]->account_id,
            'claim_phase_id' => $claimPhase->id,
            'currency_id' => $invoices[0]->currency_id,
            "dunning_configuration_id" => $dunning_configuration_id ? $dunning_configuration_id : null
        ], [
            'accessibleFields' => [
                'created_by_id' => true,
                'claim_lines' => true,
                'id' => true
            ],
        ]);
        $claimLines = [];
        $claimActions = [];
        foreach ($invoices as $key => $invoice) {
            $tmpFileName = Text::uuid() . '.pdf';
            //Move file to local_tmp
            //If file found in table, for paid invoices we dont save them,
            //Ideally we will never have paid invoices converted to claims
            //But incase we should handle for errors
            // TODO hasnat: handle it.
            if ($invoice->pdf_file_path != "" || $invoice->pdf_file_path != null) {
                $fileMountManager->copy(
                    'erpInvoices://' . $invoice->pdf_file_path,
                    'localTmp://' . $tmpFileName
                );
                $localPath = Configure::read('WyriHaximus.FlyPie.local_tmp.vars.path');
            }

            $claimLines[] = $this->ClaimLines->newEntity([
                'created_by_id' => $createdBy->id,
                'claim_line_type_id' => (int)($invoice->gross_amount) < 0 ? $invoiceClaimLineTypeCreditNote->id : $invoiceClaimLineType->id,
                'currency_id' => $invoice->currency_id,
                'invoice_id' => $invoice->id,
                'placement' => $key,
                'invoice_date' => $invoice->issue_date->i18nFormat('yyyy-MM-dd'),
                'maturity_date' => $invoice->due_date->i18nFormat('yyyy-MM-dd'),
                'amount' => (int)str_replace(".", "", number_format((float)$invoice->gross_amount, 2, '.', '')),
                'invoice_reference' => $invoice->invoice_number,
                'file_name' => [
                    'name' => $tmpFileName,
                    'type' => 'application/pdf',
                    'size' => $tmpFilesystem->getSize($tmpFileName),
                    'tmp_name' => $localPath . $tmpFileName,
                    'error' => 0,
                ],
            ], [
                'accessibleFields' => [
                    'created_by_id' => true,
                ],
            ]);
            // create claim created action
            $invoiceClaimLineType = $this->ClaimLines->Claims->ClaimActions->ClaimActionTypes->getFromIdentifier('claim_started');
            $claimActions[] = $this->ClaimLines->Claims->ClaimActions->newEntity([
                'user_id' => $createdBy->id,
                'created_by_id' => $createdBy->id,
                'claim_action_type_id' => $invoiceClaimLineType->id,
                'date' => FrozenDate::now(),
                'done' => null,
            ]);
        }

        $c = $claim->set('claim_lines', $claimLines);

        $this->ClaimLines->Claims->saveOrFail($claim, ['associated' => 'ClaimLines']);

        // assign the saved claim id to each
        $counter_action = 0;
        foreach ($claimActions as $claimAction) {
            $claimAction['claim_id']= $c->id;
            $claimAction['claim_line_id']= $c->claim_lines[$counter_action]->id;
            $claimAction['created']= FrozenTime::now()->addSecond();
            $counter_action = $counter_action++;
        }
        $this->ClaimLines->Claims->ClaimActions->saveMany($claimActions);
        return $claim;
    }
}
