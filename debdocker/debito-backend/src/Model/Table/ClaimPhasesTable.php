<?php
declare(strict_types=1);

namespace App\Model\Table;

use App\Model\Entity\ClaimPhase;
use App\Model\Table\Traits\IdentifierFindersTrait;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClaimPhases Model
 *
 * @property \App\Model\Table\ClaimActionTypesTable&\Cake\ORM\Association\HasMany $ClaimActionTypes
 * @property \App\Model\Table\ClaimsTable&\Cake\ORM\Association\HasMany $Claims
 *
 * @method \App\Model\Entity\ClaimPhase get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClaimPhase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ClaimPhase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClaimPhase|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimPhase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimPhase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClaimPhase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @method \App\Model\Entity\ClaimPhase saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClaimPhase[]|\Cake\Datasource\ResultSetInterface|false saveMany($entities, $options = [])
 * @mixin \Muffin\Trash\Model\Behavior\TrashBehavior
 */
class ClaimPhasesTable extends Table
{
    use IdentifierFindersTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('claim_phases');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Muffin/Trash.Trash');

        $this->hasMany('ClaimActionTypes', [
            'foreignKey' => 'claim_phase_id',
        ]);
        $this->hasMany('Claims', [
            'foreignKey' => 'claim_phase_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->ascii('identifier')
            ->maxLength('identifier', 255)
            ->requirePresence('identifier', 'create')
            ->notEmptyString('identifier')
            ->add('identifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->integer('order')
            ->requirePresence('order', 'create')
            ->notEmptyString('order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identifier']));

        return $rules;
    }

    /**
     * @param \Cake\ORM\Query $q
     * @param array $options
     * @return \Cake\ORM\Query
     */
    public function findWillAddInterest(Query $q, array $options)
    {
        return $q->where([
            $this->aliasField('identifier') . ' IN' => ['debt_collection', 'surveillance'],
        ]);
    }
}
