<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;

/**
 * ChangePassword command.
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class ChangePasswordCommand extends AppCommand
{
    public $modelClass = 'App.Users';

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $userId = $io->ask('User ID?');

        $user = $this->Users->get($userId);

        $io->out("User found: " . json_encode($user));

        $newPassword = $io->ask('New password?');
        $user = $this->Users->patchEntity($user, ['password' => $newPassword]);
        $this->Users->saveOrFail($user);

        $io->success('Password has been changed');

        return static::CODE_SUCCESS;
    }
}
