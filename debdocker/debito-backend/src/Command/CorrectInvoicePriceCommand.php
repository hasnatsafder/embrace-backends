<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\FrozenDate;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use EconomyPie\Invoices\InvoicesAdapterFactory;

/**
 * CreateUser command.
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 */
class CorrectInvoicePriceCommand extends Command
{
    protected $invoicesTable;
    protected $accountsTable;
    protected $debtorsTable;
    /**
     * @var \Cake\ORM\Table
     */
    private $erpIntegrationsTable;

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    public function __construct()
    {
        $this->erpIntegrationsTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
        $this->invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        Log::notice(" ======== Starting updating invoices data ======== ");
        $erpIntegrations = $this->erpIntegrationsTable->find('all')->contain('Accounts');
        foreach ($erpIntegrations as $erpIntegration) {
            Log::notice("Working on " . $erpIntegration->account->name);
            $authData = $erpIntegration->getAuthData();
            $invoicesAdapter = (new InvoicesAdapterFactory())->build($erpIntegration->vendor_identifier, $authData);
            // get invoices for just last 3 years
            $date = FrozenDate::today();
            $date = $date->subYears(3);
            $invoices = $invoicesAdapter->getAll(['unpaid' => true], $date);
            foreach ($invoices as $invoice) {
                if ($invoice === true) {
                    continue;
                }
                $dbInvoices = $this->invoicesTable->find('all')->where([
                    'erp_integration_id' => $erpIntegration->id,
                    'erp_integration_foreign_key' => $invoice->id
                ]);
                foreach ($dbInvoices as $dbInvoice) {
                    $dbInvoice->net_amount = $invoice->netAmount;
                    $dbInvoice->gross_amount = $invoice->grossAmount;
                    $this->invoicesTable->save($dbInvoice);
                }

            }
        }
        return static::CODE_SUCCESS;
    }
}
