<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\I18n\FrozenTime;

class ClearInvoiceFilesCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io)
    {
      $invoicesTable = TableRegistry::getTableLocator()->get('Invoices');
      $invoices = $invoicesTable->find('all');
      // Find all .png in your webroot/img/ folder and sort the results
      $dir = new Folder(WWW_ROOT . 'files/EconomyPie/invoices');
      $files = $dir->find('.*\.pdf', true);
      foreach ($files as $file) {
        $invoices = $invoicesTable->find()
                                  ->where(['pdf_file_path' => $file])->first();
        if (!$invoices) {
          $file = new File($dir->pwd() . DS . $file);
          $file->delete();
        }
      }

      // delete files for deleted erp integrations
      $erpTable = TableRegistry::getTableLocator()->get('ErpIntegrations');
      $erp_data = $erpTable->find('withTrashed')->where('deleted is not null');
      foreach ($erp_data as $erp_row) {
        $invoices = $invoicesTable->find('withTrashed')->where(['erp_integration_id' => $erp_row->id]);
        foreach ($invoices as $invoice) {
          $invoice->deleted = new FrozenTime();
          $invoicesTable->save($invoice);
          $file = new File($dir->pwd() . DS . $invoice->pdf_file_path);
          $file->delete();
        }
      }
    }

    // delete invoices that are converted to claims

}