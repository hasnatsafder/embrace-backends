<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * CreateUser command.
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class AssingFreeSubscriptionCommand extends Command
{
    public $modelClass = 'App.AccountPlans';
    private $accountsTable;
    private $plansTable;

    public function initialize()
    {
        parent::initialize();
        $this->plansTable = TableRegistry::getTableLocator()->get('Plans');
        $this->accountsTable = TableRegistry::getTableLocator()->get('Accounts');
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
      $free_plan = $this->plansTable->find('all')->where(['name' => 'Trial'])->first();
      $accounts = $this->accountsTable->find('all');
      foreach ($accounts as $account) {
        $accountPlanOld = $this->AccountPlans->find('all')->where('account_id', $account->id);
        if(!$accountPlanOld) {
          $account_plan = $this->AccountPlans->newEntity();
          $account_plan->plan_id = $free_plan->id;
          $account_plan->account_id = $account->id;
          $account_plan->discount = 0;
          $account_plan->internal_note = "";
          $this->AccountPlans->save($account_plan);
        }
      }
    }
}
