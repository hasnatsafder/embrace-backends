<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\FrozenTime;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use RuntimeException;
use function openssl_decrypt;

class MigrateOldLindorffFilesCommand extends Command
{
    /**
     * @var \Cake\Datasource\ConnectionInterface
     */
    protected $dbConnection;
    private $cipher = 'AES-256-CBC';
    private $key = 'W0MSxGMNkuYASgRr9t8VjI8POndeX1RJ';

    public function initialize()
    {
        parent::initialize();

        $this->dbConnection = ConnectionManager::get('legacy');
    }

    /**
     * @param \Cake\Console\ConsoleOptionParser $parser
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    /**
     * - 1
     * - users
     * - companies
     *
     * - 2
     * - debtors
     * - cases (Include hardcoded CaseTypes)
     *
     * - 3
     * - case_lines
     * - case_payments
     * - case_actions
     * - case_financials
     *
     * @param \Cake\Console\Arguments $args
     * @param \Cake\Console\ConsoleIo $io
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        //Working
        $this->updateCollectorReferences();
        //$this->collectorsAdd();
        // $this->migrateUsers($io);
        // $this->migrateCompanies($io);
        // $this->migrateDebtors($io);
        // $this->migrateClaims($io);

        // //WIP
        // $this->migrateClaimLines($io);
    }

    protected function updateCollectorReferences()
    {
        $legacyCasesTable = $this->getLegacyTableClass('Cases');
        $legacyCases = $legacyCasesTable->find('all')->where(['customer_reference is not ' => 'null']);
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');
        foreach ($legacyCases as $case) {
            $claim = $claimsTable->find()->where(['customer_reference' => $case->customer_reference])->first();
            if ($claim && $case->external_reference) {
                if ($claim->collector_reference != $case->external_reference) {
                    $this->log($case->customer_reference . ";" . $case->external_reference . ";" . $claim->collector_reference);
                }
            }
        }
    }

    protected function getLegacyTableClass(string $name, array $options = []): Table
    {
        return TableRegistry::getTableLocator()->get(
            $name,
            $options +
            [
                'alias' => 'Legacy' . $name,
                'className' => 'Cake\ORM\Table',
                'connection' => $this->dbConnection,
            ]
        );
    }

    protected function migrateUsers(ConsoleIo $io): void
    {
        /** @var \App\Model\Table\UsersTable $usersTable */
        $usersTable = TableRegistry::getTableLocator()->get('Users');

        $legacyUsersTable = $this->getLegacyTableClass('LegacyUsers', [
            'table' => 'users',
        ]);

        $legacyUsers = $legacyUsersTable->find()
            ->order(['id' => 'DESC'])
            ->all();

        foreach ($legacyUsers as $legacyUser) {
            $existingUser = $usersTable->find()
                ->where(['Users.email' => $legacyUser->email]);

            if ($existingUser->count() > 0) {
                $io->error('User already exists: ' . $legacyUser->email);
                $io->err('Moving on...');

                return;
            }

            $user = $usersTable->newEntity();
            $user->setAccess('*', true);

            $user = $usersTable->patchEntity($user, [
                'active_account_id' => null,
                'first_name' => $legacyUser->first_name,
                'last_name' => $legacyUser->last_name,
                'email' => $legacyUser->email,
                'password' => $legacyUser->password ?: Text::uuid(),
                'is_confirmed' => false,
                'is_active' => true,
                'created' => $legacyUser->created_at,
                'modified' => $legacyUser->updated_at,
                'token' => "",
                "verified" => 1,
            ]);

            if ($legacyUser->deleted_at !== null) {
                $user->set('deleted', $legacyUser->deleted_at);
            }

            if (!$usersTable->save($user)) {
                $this->log('User could not be saved: ' . $legacyUser->email);
                pj($user->getErrors());
                continue;
            }

            $io->success('User created: ' . $legacyUser->email);
        }

        $io->success('All users saved!');
    }

    protected function migrateCompanies(ConsoleIo $io): void
    {
        /** @var \App\Model\Table\AccountsTable $accountsTable */
        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');

        /** @var \App\Model\Table\RolesTable $rolesTable */
        $rolesTable = TableRegistry::getTableLocator()->get('Roles');

        $legacyAccountsTable = $this->getLegacyTableClass('Companies');
        $legacyCompanyTypesTable = $this->getLegacyTableClass('MigratedCompanyTypes');
        $legacyUsersTable = $this->getLegacyTableClass('LegacyUsers', [
            'table' => 'users',
        ]);

        $legacyCompanyTypes = $legacyCompanyTypesTable
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'company_type_name',
            ])
            ->toArray();

        $companyTypes = $accountsTable->CompanyTypes
            ->find('list', [
                'keyField' => 'name',
                'valueField' => 'id',
            ])
            ->toArray();

        $legacyAccounts = $legacyAccountsTable->find()
            ->order(['id' => 'DESC'])
            ->map(function ($row) {
                $row = json_decode(json_encode($row), true);

                return array_map(function ($row) {
                    if ("eyJpdiI6" === substr((string)$row, 0, 8)) { //make sure value is encrypted
                        return $this->decryptValue($row);
                    }

                    return $row;
                }, $row);
            })
            ->toArray();

        $denmarkCountryId = $accountsTable->Countries->findByName('Denmark')->firstOrFail()->id;
        $lindorffCollectorId = $accountsTable->Collectors->findByIdentifier('lindorff')->firstOrFail()->id;
        $accountOwnerRoleId = $rolesTable->find()->where(['Roles.identifier' => 'owner'])->firstOrFail()->id;

        foreach ($legacyAccounts as $legacyAccount) {
            //Check if exists already
            $existingAccount = $accountsTable->find()
                ->where([
                    'Accounts.name' => $legacyAccount['company_name'],
                    'Accounts.address' => $legacyAccount['company_address'],
                    'Accounts.phone' => $legacyAccount['company_phone'],
                    'Accounts.bank_reg_number' => $legacyAccount['reg_no'],
                    'Accounts.bank_account_number' => $legacyAccount['konto_no'],
                ]);

            if ($existingAccount->count() > 0) {
                $io->error('Account already exists: ' . $legacyAccount['company_name']);
                $io->err('Moving on...');

                return;
            }

            try {
                $userInCompany = $legacyUsersTable->find()
                    ->where(['id' => $legacyAccount['user_id']])
                    ->firstOrFail();

                $newUser = $accountsTable->Users->find()
                    ->where(['Users.email' => $userInCompany->email])
                    ->firstOrFail();
            } catch (RecordNotFoundException $e) {
                $io->error('No user was found for company: ' . $legacyAccount['company_name']);
                continue;
            }

            $account = $accountsTable->newEntity();
            $account->setAccess('*', true);

            $companyType = $legacyAccount['company_type'];
            $companyType = isset($legacyCompanyTypes[$companyType], $companyTypes[$legacyCompanyTypes[$companyType]]) ?
                $companyTypes[$legacyCompanyTypes[$companyType]] : null;

            $account = $accountsTable->patchEntity($account, [
                'created_by_id' => $newUser->id,
                'company_type_id' => $companyType,
                'country_id' => $denmarkCountryId,
                'name' => $legacyAccount['company_name'] ?: null,
                'address' => $legacyAccount['company_address'] ?: null,
                'zip_code' => $legacyAccount['company_zip'] ?: null,
                'city' => $legacyAccount['company_city'] ?: null,
                'phone' => $legacyAccount['company_phone'] ?: null,
                'vat_number' => $legacyAccount['company_vat_number'] ?: null,
                'ean_number' => $legacyAccount['company_ean_number'] ?: null,
                'website' => $legacyAccount['company_web'] ?: null,
                'bank_reg_number' => $legacyAccount['reg_no'] ?: null,
                'bank_account_number' => $legacyAccount['konto_no'] ?: null,
                'is_company' => !empty($legacyAccount['company_name']),
                'created' => $legacyAccount['created_at'],
                'modified' => $legacyAccount['updated_at'],
                'users' => [
                    [
                        'id' => $newUser->id,
                        '_joinData' => [
                            'role_id' => $accountOwnerRoleId,
                        ],
                    ],
                ],
                'collectors' => [
                    [
                        'id' => $lindorffCollectorId,
                        '_joinData' => [
                            'collector_identifier' => $legacyAccount['k_number'] ?: "",
                        ],
                    ],
                ],
            ]);

            if (!empty($account->getErrors())) {
                debug($account);
                throw new RuntimeException();
            }

            if (
            !$accountsTable->save($account, [
                'skipAutoCreateUser' => true,
            ])
            ) {
                $this->log('Account could not be saved: ' . $legacyAccount['email']);
                debug($account->getErrors());
                throw new RuntimeException();
            }

            //activating the saved account to set as active account for user.
            //Its better if user has atleast one active account
            $this->activateAccount($newUser->id, $account->id);
        }

        $io->success('All accounts saved!');
    }

    protected function decryptValue($payload)
    {
        if ("eyJpdiI6" !== substr($payload, 0, 8)) { //make sure value is encrypted
            throw new RuntimeException('Value does not look encrypted');
        }

        $payload = json_decode(base64_decode($payload), true);

        // If the payload is not valid JSON or does not have the proper keys set we will
        // assume it is invalid and bail out of the routine since we will not be able
        // to decrypt the given value. We'll also check the MAC for this encryption.
        if (!$this->validPayload($payload)) {
            throw new RuntimeException('The payload is invalid.');
        }

        if (!$this->validMac($payload)) {
            throw new RuntimeException('The MAC is invalid.');
        }

        $iv = base64_decode($payload['iv']);

        // Here we will decrypt the value. If we are able to successfully decrypt it
        // we will then unserialize it and return it out to the caller. If we are
        // unable to decrypt this value we will throw out an exception message.
        $decrypted = openssl_decrypt(
            $payload['value'],
            $this->cipher,
            $this->key,
            0,
            $iv
        );

        if ($decrypted === false) {
            throw new RuntimeException('Could not decrypt the data.');
        }

        return unserialize($decrypted);
    }

    protected function validPayload($payload)
    {
        return is_array($payload) && isset(
                $payload['iv'],
                $payload['value'],
                $payload['mac']
            );
    }

    protected function validMac(array $payload)
    {
        $bytes = random_bytes(16);
        $calculated = hash_hmac(
            'sha256',
            hash_hmac('sha256', $payload['iv'] . $payload['value'], $this->key),
            $bytes,
            true
        );

        return hash_equals(
            hash_hmac('sha256', $payload['mac'], $bytes, true),
            $calculated
        );
    }

    /**
     * activateAccount
     *
     * @param $user_id , $account_id
     * @return bool
     */

    private function activateAccount($user_id, $account_id)
    {
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $user = $usersTable->get($user_id);
        $user->active_account_id = $account_id;

        return $usersTable->save($user);
    }

    protected function migrateDebtors(ConsoleIo $io): void
    {
        $legacyDebtorsTable = $this->getLegacyTableClass('LegacyDebtors', [
            'table' => 'debtors',
        ]);
        $legacyUsersTable = $this->getLegacyTableClass('LegacyUsers', [
            'table' => 'users',
        ]);

        /** @var \App\Model\Table\DebtorsTable $debtorsTable */
        $debtorsTable = TableRegistry::getTableLocator()->get('Debtors');

        $legacyDebtors = $legacyDebtorsTable->find()
            ->order(['created_at' => 'DESC'])
            ->map(function ($row) {
                $row = json_decode(json_encode($row), true);

                return array_map(function ($row) {
                    if ("eyJpdiI6" === substr((string)$row, 0, 8)) { //make sure value is encrypted
                        return $this->decryptValue($row);
                    }

                    return $row;
                }, $row);
            })
            ->toArray();

        $denmarkCountryId = $debtorsTable->Countries->findByName('Denmark')->firstOrFail()->id;

        foreach ($legacyDebtors as $legacyDebtor) {
            $createdByLegacyUser = $legacyUsersTable->get($legacyDebtor['user_id']);

            /** @var \App\Model\Entity\User $createdByUser */
            $createdByUser = $debtorsTable->CreatedBy->find()
                ->where([
                    'CreatedBy.email' => $createdByLegacyUser->email,
                ])
                ->contain(['Accounts'])
                ->first();

            if (!$createdByUser) {
                debug($createdByLegacyUser);
                debug($legacyDebtor);
                throw new RuntimeException();
            }

            $entityData = [
                'created_by_id' => $createdByUser->id,
                'account_id' => $createdByUser->accounts[0]->id,
                'latitude' => $legacyDebtor['lat'] ?: null,
                'langitude' => $legacyDebtor['lng'] ?: null,
                'country_id' => $denmarkCountryId,
                'vat_number' => $legacyDebtor['vat_number'] ?: null,
                'company_name' => $legacyDebtor['company_name'] ?: null,
                'first_name' => $legacyDebtor['first_name'] ?: null,
                'last_name' => $legacyDebtor['last_name'] ?: null,
                'address' => $legacyDebtor['address'] ?: null,
                'zip_code' => $legacyDebtor['zipcode'] ?: null,
                'city' => $legacyDebtor['city'] ?: null,
                'phone' => $legacyDebtor['phone_number'] ?: null,
                'email' => $legacyDebtor['email_address'] ?: null,
                'is_company' => !empty($legacyDebtor['company_name']),
                'created' => $legacyDebtor['created_at'],
                'modified' => $legacyDebtor['updated_at'],
                'old_id' => $legacyDebtor['id'],
            ];

            //Check if exists already
            $exisingDebtor = $debtorsTable->find()
                ->where([
                    'old_id' => $legacyDebtor['id'],
                ]);

            if ($exisingDebtor->count() > 0) {
                $io->error('Debtor already exists: ' . $legacyDebtor['company_name']);
                $io->err('Moving on...');

                return;
            }

            $debtor = $debtorsTable->newEntity();
            $debtor->setAccess('*', true);
            $debtor = $debtorsTable->patchEntity($debtor, $entityData);

            if (!empty($debtor->getErrors())) {
                debug($debtor);
                throw new RuntimeException();
            }

            if (!$debtorsTable->save($debtor)) {
                $this->log('Debtor could not be saved: ' . $legacyDebtor['id']);
                pj($debtor->getErrors());
                pj($debtor);
                continue;
            }
        }

        $io->success('Migrated debtors');
    }

    protected function migrateClaims(ConsoleIo $io): void
    {
        $legacyCasesTable = $this->getLegacyTableClass('LegacyCases', [
            'table' => 'cases',
        ]);
        $legacyUsersTable = $this->getLegacyTableClass('LegacyUsers', [
            'table' => 'users',
        ]);

        /** @var \App\Model\Table\ClaimsTable $claimsTable */
        $claimsTable = TableRegistry::getTableLocator()->get('Claims');

        $legacyCases = $legacyCasesTable->find()
            ->order(['id' => 'DESC'])
            ->map(function ($row) {
                $row = json_decode(json_encode($row), true);

                return array_map(function ($row) {
                    if ("eyJpdiI6" === substr((string)$row, 0, 8)) { //make sure value is encrypted
                        return $this->decryptValue($row);
                    }

                    return $row;
                }, $row);
            })
            ->toArray();

        foreach ($legacyCases as $legacyCase) {
            $debtorId = null;
            $createdByLegacyUser = $legacyUsersTable->get($legacyCase['user_id']);

            /** @var \App\Model\Entity\User $createdByUser */
            $createdByUser = $claimsTable->CreatedBy->find()
                ->where([
                    'CreatedBy.email' => $createdByLegacyUser->email,
                ])
                ->contain(['Accounts'])
                ->firstOrFail();

            $claimPhase = $claimsTable->ClaimPhases->findByIdentifier('case_received')->firstOrFail();

            if (!empty($legacyCase['debtor_id'])) {
                $debtor = $claimsTable->Debtors->find()
                    ->where(['Debtors.old_id' => $legacyCase['debtor_id']])
                    ->first();

                if (!$debtor) {
                    debug($legacyCase);
                    throw new RuntimeException();
                }
                $debtorId = $debtor->id;
            }

            $lindorffCollector = $claimsTable->Collectors->findByIdentifier('lindorff')->firstOrFail();

            //Check if exists already
            $existingClaim = $claimsTable->find()
                ->where(['old_id' => $legacyCase['id']]);

            if ($existingClaim->count() > 0) {
                $io->error('Claim already exists: ' . $legacyCase['id']);
                $io->err('Moving on...');

                return;
            }

            $claim = $claimsTable->newEntity();
            $claim->setAccess('*', true);
            $claim = $claimsTable->patchEntity($claim, [
                'created_by_id' => $createdByUser->id,
                'account_id' => $createdByUser->accounts[0]->id,
                'debtor_id' => !empty($debtorId) ? $debtorId : null,
                'collector_id' => $lindorffCollector->id,
                'claim_phase_id' => $claimPhase->id,
                'customer_reference' => $legacyCase['customer_reference'],
                'collector_reference' => $legacyCase['external_reference'],
                'dispute' => $legacyCase['has_dispute'] === true ? $legacyCase['dispute_comment'] ?: 'Ja' : null,
                'completed' => !empty($legacyCase['completed_at']) ? FrozenTime::parse($legacyCase['completed_at']) : null,
                'approved' => !empty($legacyCase['synced_at']) ? FrozenTime::parse($legacyCase['synced_at']) : null,
                'synced' => !empty($legacyCase['synced_at']) ? FrozenTime::parse($legacyCase['synced_at']) : null,
                'ended_at' => !empty($legacyCase['ended_at']) ? FrozenTime::parse($legacyCase['ended_at']) : null,
                'created' => !empty($legacyCase['created_at']) ? FrozenTime::parse($legacyCase['created_at']) : null,
                'modified' => !empty($legacyCase['updated_at']) ? FrozenTime::parse($legacyCase['updated_at']) : null,
                'deleted' => !empty($legacyCase['deleted_at']) ? FrozenTime::parse($legacyCase['deleted_at']) : null,
                'old_id' => $legacyCase['id'],
            ]);

            if (!empty($claim->getErrors())) {
                debug($claim);
                throw new RuntimeException();
            }

            if (!$claimsTable->save($claim)) {
                $this->log('Claim could not be saved: ' . $legacyCase['id']);

                pj($claim->getErrors());
                pj($claim);
                continue;
            }
        }

        $io->success('Migrated claim');
    }

    protected function migrateClaimLines(ConsoleIo $io): void
    {
        $legacyCaseLinesTable = $this->getLegacyTableClass('CaseLines');

        /** @var \App\Model\Table\ClaimLinesTable $claimLinesTable */
        $claimLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');

        $legacyCaseLines = $legacyCaseLinesTable->find()
            ->order(['created_at' => 'DESC'])
            ->toArray();

        $dkkCurrency = $claimLinesTable->Currencies->findByIsoCode('DKK')->firstOrFail();

        foreach ($legacyCaseLines as $legacyCaseLine) {
            if (!empty($legacyCaseLine['currency'])) {
                $currency = $claimLinesTable->Currencies->find()
                    ->where([
                        'iso_code' => $legacyCaseLine['currency'],
                    ])
                    ->firstOrFail();
            }

            $claim = $claimLinesTable->Claims->findByOldId($legacyCaseLine['case_id'])->first();

            if (!$claim) {
                $io->error('No case found for case line id: ' . $legacyCaseLine['id']);
                continue;
            }

            //Check if exists already
            $existingClaimLine = $claimLinesTable->find()
                ->where(['old_id' => $legacyCaseLine['id']]);

            if ($existingClaimLine->count() > 0) {
                $io->error('Claim line already exists: ' . $legacyCaseLine['id']);
                $io->err('Moving on...');

                return;
            }

            $claimLine = $claimLinesTable->newEntity();
            $claimLine->setAccess('*', true);
            $claimLine = $claimLinesTable->patchEntity($claimLine, [
                'claim_id' => $claim->id,
                'claim_line_type_id' => $legacyCaseLine['case_line_type_id'],
                'currency_id' => isset($currency) ? $currency->id : $dkkCurrency->id,
                'placement' => 1,
                'invoice_date' => $legacyCaseLine['date'],
                'maturity_date' => $legacyCaseLine['maturity'],
                'amount' => (int)round($legacyCaseLine['amount'] * 100),
                'invoice_reference' => $legacyCaseLine['invoice_reference'],
                'created' => !empty($legacyCaseLine['created_at']) ? FrozenTime::parse($legacyCaseLine['created_at']) : null,
                'modified' => !empty($legacyCaseLine['updated_at']) ? FrozenTime::parse($legacyCaseLine['updated_at']) : null,
                'deleted' => !empty($legacyCaseLine['deleted_at']) ? FrozenTime::parse($legacyCaseLine['deleted_at']) : null,
                'old_id' => $legacyCaseLine['id'],
            ]);

            if (!empty($claimLine->getErrors())) {
                debug($claimLine);
                throw new RuntimeException();
            }

            if (!$claimLinesTable->save($claimLine)) {
                $this->log('Claim could not be saved: ' . $legacyCaseLine['id']);
                debug($claimLine);

                pj($claimLine->getErrors());
                pj($claimLine);
                continue;
            }
        }
    }

    protected function migrateLindorffFiles(ConsoleIo $io): void
    {
        /** @var \App\Model\Table\CollectorFilesTable $collectorFilesTable */
        $collectorFilesTable = TableRegistry::getTableLocator()->get('CollectorFiles');

        $lindorffFilesTable = $this->getLegacyTableClass('LindorffFiles');
        $lindorffFileTypesTable = $this->getLegacyTableClass('LindorffFileTypes');

        $oldLindorffFileTypes = $lindorffFileTypesTable
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'identifier',
            ])
            ->toArray();

        $newLindorffFileTypes = $collectorFilesTable->CollectorFileTypes
            ->find('list', [
                'keyField' => 'identifier',
                'valueField' => 'id',
            ])
            ->matching('Collectors', function (Query $q) {
                return $q->where(['Collectors.identifier' => 'lindorff']);
            })
            ->toArray();

        $lindorffFiles = $lindorffFilesTable->find()
            ->all();
        $migratedFiles = 0;

        foreach ($lindorffFiles as $lindorffFile) {
            $fileExists = $collectorFilesTable->find()
                    ->where(['CollectorFiles.name' => $lindorffFile->name])
                    ->isEmpty() === false;

            if ($fileExists) {
                $io->verbose(sprintf('File "%s" exists already - skipping', $lindorffFile->name));
                continue;
            }

            $collectorFile = $collectorFilesTable->newEntity([
                'collector_file_type_id' => $newLindorffFileTypes[$oldLindorffFileTypes[$lindorffFile->lindorff_file_type_id]],
                'name' => $lindorffFile->name,
                'extension' => $lindorffFile->extension,
                'mime_type' => $lindorffFile->mime_type,
                'size' => $lindorffFile->size,
                'dir' => $lindorffFile->dir,
                'completed' => $lindorffFile->is_completed ? $lindorffFile->updated_at : null,
            ]);

            if (!$collectorFilesTable->save($collectorFile)) {
                $io->error('Could not save file!');
                $io->error(json_encode($collectorFile, JSON_PRETTY_PRINT));
                continue;
            }
            $migratedFiles++;
        }

        $io->success('Migrated ' . $migratedFiles . ' files!');
    }

    protected function collectorsAdd()
    {
        $legacyAccountsTable = $this->getLegacyTableClass('Companies');
        $legacyAccounts = $legacyAccountsTable->find()
            ->order(['id' => 'DESC'])
            ->map(function ($row) {
                $row = json_decode(json_encode($row), true);

                return array_map(function ($row) {
                    if ("eyJpdiI6" === substr((string)$row, 0, 8)) { //make sure value is encrypted
                        return $this->decryptValue($row);
                    }

                    return $row;
                }, $row);
            })
            ->toArray();

        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');
        $AccountsCollectorsTable = TableRegistry::getTableLocator()->get('AccountsCollectors');
        foreach ($legacyAccounts as $company) {
            if ($company['company_vat_number']) {
                $account = $accountsTable->find()->where(['vat_number' => $company['company_vat_number']])->first();
                if ($account) {
                    $accountCollector = $AccountsCollectorsTable->find()->where([
                        'account_id' => $account->id,
                        'collector_id' => '5dbdf878-1bd2-4fe6-aa53-7e0803f36539',
                    ])->first();
                    if (!$accountCollector) {
                        $accountCollectorNew = $AccountsCollectorsTable->newEntity();
                        $accountCollectorNew->setAccess('*', true);
                        $accountCollectorNew->collector_id = "5dbdf878-1bd2-4fe6-aa53-7e0803f36539";
                        $accountCollectorNew->account_id = $account->id;
                        $accountCollectorNew->collector_identifier = $company['k_number'];
                        $AccountsCollectorsTable->save($accountCollectorNew);
                        if ($company['k_number']) {
                            $this->log($account->vat_number . ";" . $company['company_name'] . ";" . $company['k_number']);
                        }
                    } else {
                        if ($accountCollector && !$accountCollector->collector_identifier) {
                            $accountCollector->collector_identifier = $company['k_number'];
                            $AccountsCollectorsTable->save($accountCollector);
                            if ($company['k_number']) {
                                $this->log($account->vat_number . ";" . $company['company_name'] . ";" . $company['k_number']);
                            }
                        }
                    }
                }
            }
        }
    }
}
