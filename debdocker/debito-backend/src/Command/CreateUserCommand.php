<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

/**
 * CreateUser command.
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class CreateUserCommand extends Command
{
    public $modelClass = 'App.Users';

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $email = $io->ask('Email?');
        $password = $io->ask('Password?');

        $user = $this->Users->newEntity([
            'email' => $email,
            'password' => $password,
        ]);

        $user->is_confirmed = true;
        $user->is_active = true;

        if (!$this->Users->save($user)) {
            $io->error('Could not save user!');
            $io->error(json_encode($user->getErrors()));

            return static::CODE_ERROR;
        }

        return static::CODE_SUCCESS;
    }
}
