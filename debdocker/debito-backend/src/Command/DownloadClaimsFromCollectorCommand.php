<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\I18n\FrozenTime;
use CollectorFiles\DownloadFileHandler\CaseDownloaderFactory;

/**
 * Class SendClaimsToCollectorCommand
 *
 * @package App\Command
 * @property \App\Model\Table\ClaimsTable Claims
 * @property \League\Flysystem\AdapterInterface casesFileSystem
 */
class DownloadClaimsFromCollectorCommand extends Command
{

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @param \Cake\Console\ConsoleOptionParser $parser
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->addOptions([
            'collector' => [
                'help' => __('Collector to use'),
                'short' => 'c',
                'default' => null,
                'choices' => ['mortang', 'lindorff', 'collectia'],
                'required' => true,
            ],
            'type' => [
                'help' => __('Functionality to be done'),
                'short' => 't',
                'default' => null,
                'choices' => ['status', 'receipt', 'financial', 'inb'],
                'required' => true,
            ],
        ]);

        return $parser;
    }

    /**
     * @param \Cake\Console\Arguments $args
     * @param \Cake\Console\ConsoleIo $io
     * @return int|null|void
     * @throws \League\Csv\CannotInsertRecord
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->verbose('Starting at: ' . (new FrozenTime())->i18nFormat(null, 'Europe/Copenhagen'));
        $collector = $args->getOption('collector');

        $caseDownloader = CaseDownloaderFactory::build($collector);

        if ($args->getOption('type') == "receipt") {
            $io->verbose(sprintf('Processing %d Receipts for', $collector));
            $io->out();

            $caseDownloader->downloadClaimReceipts();
        } else {
            if ($args->getOption('type') == "status") {
                $io->verbose(sprintf('Processing %d Statuses for', $collector));
                $io->out();

                $caseDownloader->downloadClaimStatus();
            } else {
                if ($args->getOption('type') == "financial") {
                    $io->verbose(sprintf('Processing %d financials for', $collector));
                    $io->out();

                    $caseDownloader->downloadClaimFinancials();
                } else {
                    if ($args->getOption('type') == "inb") {
                        $io->verbose(sprintf('Processing %d INB financials for', $collector));
                        $io->out();

                        $caseDownloader->downloadClaiminbs();
                    }
                }
            }
        }

        $io->out();

        $io->success('Finished at: ' . (new FrozenTime())->i18nFormat(null, 'Europe/Copenhagen'));
    }
}
