<?php
declare(strict_types=1);
namespace App\Command;
set_time_limit(0);
use App\Exception\Exception;
use App\Utility\HelperFunctions;
use App\Utility\NotifyAdmin;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use GuzzleHttp\Client;
use IntlDateFormatter;
use Cake\Filesystem\File;
use Cake\Utility\Text;



/**
 * Class GetClaimsFromCollectiaCommand
 *
 * @package App\Command
 */
class GetClaimsFromCollectiaCommand extends Command
{

    protected $client;
    protected static $retries = 0;
    protected static  $retriesLimit = 2;
    protected static  $pageSize = 15;
    protected static $apiUrl = 'https://kundewebapi.collectia.dk/api/';
    protected static $filePath = 'webroot/files/ClaimLines/file_name/';
    protected $token = '';
    protected $globalResponseStatusCode;
    protected $caseDebtorProp = "debtors";
    protected $caseDebtorIndex = 0;
    protected $debtorPhoneKey = 'tlf';

    protected $collector;
    protected $collectorName = 'Collectia';
    protected $collectorIdentifier = 'collectia';

    protected $danishCurrencyID;
    protected $claimPhaseDebtoCollectionID;
    protected $claimPhaseEndedID;
    protected $claimLineTypeInvoiceID;
    protected $executionStartTime = 0;
    protected $trimCharsList = " \t\n\r\0\x0B\xC2\xA0";


    public function __construct(){

        $this->executionStartTime = microtime(true); //Store the micro time so that we know when our script started to run.

        $this->client = new Client(['http_errors' => false]);
        $this->loadModel('Claims');
        $this->loadModel('ClaimLines');
        $this->ClaimLines->removeBehavior('Upload');
        $this->loadModel('Collectors');
        $this->loadModel('Accounts');
        $this->loadModel('Debtors');
        $this->loadModel('Countries');

        $this->danishCurrencyID = TableRegistry::getTableLocator()->get('Currencies')->find()->where(['iso_code' => 'DKK'])->first()->id;
        $this->claimPhaseDebtoCollectionID = TableRegistry::getTableLocator()->get('ClaimPhases')->find()->where(['identifier' => 'debt_collection'])->first()->id;
        $this->claimPhaseEndedID = TableRegistry::getTableLocator()->get('ClaimPhases')->find()->where(['identifier' => 'ended'])->first()->id;
        $this->claimLineTypeInvoiceID = TableRegistry::getTableLocator()->get('ClaimLineTypes')->find()->where(['identifier' => 'invoice'])->first()->id;


    }
    public function initialize()
    {
        parent::initialize();
        $this->collector = $this->Collectors->find()->where(['identifier' => $this->collectorIdentifier, 'is_active' => 1])->first();
        if(!$this->collector) {
            Log::error("Collector '$this->collectorIdentifier' not found or is inactive, exiting");
            throw new Exception("Collector '$this->collectorIdentifier' not found or is inactive, exiting");
        }
    }

    /**
     * @param \Cake\Console\Arguments $args
     * @param \Cake\Console\ConsoleIo $io
     * @return int|null|void
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        Log::notice("Starting $this->collectorName claims command at: ".(new FrozenTime())->i18nFormat(IntlDateFormatter::FULL, 'Europe/Copenhagen', 'en-GB'));
        $this->token = $this->fetchToken();
        $casesSearchParams = [
            'currentPage' => 1,
            'pageSize' => self::$pageSize,
            'orderType' => 5,
            'searchText' => '',
            'searchType' => 'CASENO',
            'caseStatusFilter' => '',
            'caseTypeFilter' => '',
            'clientNoFilter' =>[],
            'hasPaymentPlanFilter' => NULL,
            'isClosedFilter' => NULL,
            'yearFilter' => NULL,
            'filtersTouched' => false,
            'keepPage' => false,
        ];
        //fetch all cases, paginate the results and process each case on all paginated pages
        $casesSearchResponse = $this->sendRequest(self::$apiUrl."Cases/Search", $this->token, 'POST', $casesSearchParams);
        if($casesSearchResponse['responseCode'] == 200){
            $casesSearchResponseArr = json_decode($casesSearchResponse['response']);
            $totalCases = $casesSearchResponseArr->totalCount;
            if($totalCases > 0) {
                $cases = $casesSearchResponseArr->result;
                $totalPages = ceil($totalCases/self::$pageSize);
                Log::info("Starting to process $totalCases case(s)");
                do{
                    Log::info("Page {$casesSearchParams['currentPage']}/$totalPages");

                    foreach($cases as $case){

                        //skip if claim exists in DB
                        $claim = $this->Claims->find()->where(['customer_reference' => $case->referenceClientNr])->first();
                        if($claim){
                            Log::info("Claim# $claim->id case# $case->caseNo customer reference# $case->referenceClientNr already exists in DB, skipping");
                            continue;
                        }

                        //get case creditor email to search for accounts in our DB, skip if email does not exist
                        $creditorName = (!empty($case->creditorName)?trim($case->creditorName, $this->trimCharsList):"");
                        $creditorDesc = (!empty($case->creditorDesc)?trim($case->creditorDesc, $this->trimCharsList):"");
                        $descArr = explode(' ', $creditorDesc );
                        $creditorEmail = end($descArr);
                        if(!filter_var($creditorEmail, FILTER_VALIDATE_EMAIL)){
                            Log::error("Case# $case->caseNo, reference# $case->referenceClientNr does not have email $creditorEmail in description '$case->creditorDesc', skipping");
                            continue;
                        }

                        //find account by creditor email, skip if not found
                        $account = $this->Accounts
                            ->find('search', [
                                'search' => $creditorEmail,
                            ])
                            ->enableAutoFields()
                            ->order(['Accounts.created' => 'DESC'])
                            ->contain([
                                'Users',
                            ])
                            ->leftJoinWith('Users')
                            ->group(['Accounts.id'])->first();
                        if(!$account) {
                            Log::error("Case# $case->caseNo, reference# $case->referenceClientNr name:$creditorName, email:$creditorEmail account does not exist in DB, skipping");
                            continue;
                        }
                        Log::info("Processing case# $case->caseNo, reference# $case->referenceClientNr");

                        //get case details
                        $caseAPI = $this->sendRequest(self::$apiUrl."case/$case->caseNo", $this->token);

                        if($caseAPI['responseCode'] != 200){
                            $err = "Server returned Response code: {$caseAPI['responseCode']} for case API case# $case->caseNo\nServer Response:\n{$caseAPI['response']}\n skipping";
                            Log::error($err);
                            continue;
                        }

                        $caseAPIRes = json_decode($caseAPI['response']);

                        $debtorVatNo = $this->customTrim($caseAPIRes->{$this->caseDebtorProp}[$this->caseDebtorIndex]->idNo);
                        $debtorEmail = $this->customTrim($caseAPIRes->{$this->caseDebtorProp}[$this->caseDebtorIndex]->email);
                        $debtorExtraPhonesArr = $caseAPIRes->{$this->caseDebtorProp}[$this->caseDebtorIndex]->extraPhoneNumbers;
                        $debtorName = $this->customTrim($caseAPIRes->{$this->caseDebtorProp}[$this->caseDebtorIndex]->debtorName);

                        $debtorWhereCond = ['account_id' => $account->id, 'OR' => []];
                        //find debtor by case debtor's cvr or phone# or email, create if not found
                        if(!empty($debtorVatNo)){
                            $debtorWhereCond['OR']['vat_number'] = trim($debtorVatNo, $this->trimCharsList);
                        }
                        if(!empty($debtorEmail)){
                            $debtorWhereCond['OR']['email'] = trim($debtorEmail, $this->trimCharsList);
                        }
                        $debtorExtraPhonesArrConcat = '';
                        for ($i = 1; $i <= 3; $i++){
                            $phone = $this->customTrim($caseAPIRes->{$this->caseDebtorProp}[$this->caseDebtorIndex]->{$this->debtorPhoneKey.$i});
                            if(!empty($phone)) {
                                $debtorWhereCond['OR'][] = ['phone' => $phone];
                                $debtorExtraPhonesArrConcat .= "$phone, ";
                            }
                        }
                        if(!empty($debtorExtraPhonesArr)){
                            foreach ($debtorExtraPhonesArr as $debtorExtraPhone){
                                $debtorWhereCond['OR'][] = ['phone' => trim($debtorExtraPhone, $this->trimCharsList)];
                                $debtorExtraPhonesArrConcat .= "$debtorExtraPhone, ";
                            }
                        }
                        if(!empty($debtorWhereCond['OR'])){
                            $debtor = $this->Debtors->find()->where([$debtorWhereCond])->first();
                        }else if(!empty($debtorName)){
                            $debtor = $this->Debtors->find()->where(['account_id' => $account->id])->where(function ($exp, $query) use ($debtorName) {
                                    $conc = $query->func()->concat([
                                        'first_name' => 'identifier', ' ',
                                        'last_name' => 'identifier'
                                    ]);
                                    return $exp->like($conc, $debtorName);
                                })->first();
                        }

                        if(!$debtor) {
                            Log::info("Debtor Name:$debtorName, CVR:$debtorVatNo, Email:$debtorEmail, Phone#:$debtorExtraPhonesArrConcat not found, creating");
                            $debtor = $this->createDebtor($account, $caseAPIRes);
                        }else{
                            Log::info("Debtor exists; ID:$debtor->id, Debtor Name:$debtorName, VAT->$debtor->vat_number:$debtorVatNo, Email->$debtor->email:$debtorEmail, Phone#->$debtor->phone:$debtorExtraPhonesArrConcat");

                        }
                        //create claim
                        $claim = $this->Claims->newEntity();
                        $claim->created_by_id = $account->created_by_id;
                        $claim->debtor_id = $debtor->id;
                        $claim->account_id = $account->id;
                        $claim->collector_id = $this->collector->id;
                        $claim->customer_reference = $case->referenceClientNr;
                        $claim->collector_reference = $case->caseNo;
                        $claim->currency_id = $this->danishCurrencyID;
                        $claim->claim_phase_id = $this->claimPhaseDebtoCollectionID;
                        $claim->created = $case->caseRegDate;
                        $claim->completed = $case->caseRegDate;
                        $claim->approved = $case->caseRegDate;
                        $claim->synced = $case->caseRegDate;
                        
                        // if case is closed then set case to 
                        if($case->closureDate) {
                            $claim->ended = $case->closureDate;
                            $claim->claim_phase_id = $this->claimPhaseEndedID;
                        }
                        $claim = $this->Claims->save($claim);

                        //save case docs
                        $caseDocsArr = $this->saveCaseDocs($this->token, $case->caseNo);

                        //get case invoices for creating claim lines
                        $caseInvoicesRes = $this->getCaseInvoices($this->token, $case->caseNo);
                        if($caseInvoicesRes['responseCode'] != 200){
                            $err = "Server returned Response code: {$caseInvoicesRes['responseCode']} for search case $case->caseNo invoices API\nServer Response:\n{$caseInvoicesRes['response']}\n skipping claim";
                            Log::error($err);
                            continue;
                        }
                        $caseInvoices = json_decode($caseInvoicesRes['response']);

                        //create claim line for each invoice
                        if(!empty($caseInvoices)) {
                            foreach ($caseInvoices as $caseInvoice) {
                                $claimLine = $this->ClaimLines->newEntity();
                                $claimLine->invoice_reference = $caseInvoice->invoiceNo;
                                $claimLine->file_name = (!empty($caseDocsArr) ? $caseDocsArr['file_name'] : null);
                                $claimLine->file_mime_type = (!empty($caseDocsArr) ? $caseDocsArr['content_type'] : null);
                                $claimLine->file_size = (!empty($caseDocsArr) ? $caseDocsArr['file_size'] : null);
                                $claimLine->file_dir = (!empty($caseDocsArr) ? (self::$filePath) : null);
                                $claimLine->amount = HelperFunctions::convertToDanishCurrency(strval($caseInvoice->remainingCapital));
                                $claimLine->invoice_date = explode('T', $caseInvoice->invoiceDate)[0];
                                $claimLine->maturity_date = explode('T', $caseInvoice->dueDate)[0];
                                $claimLine->claim_id = $claim->id;
                                $claimLine->currency_id = $this->danishCurrencyID;
                                $claimLine->synced = 1;
                                $claimLine->claim_line_type_id = $this->claimLineTypeInvoiceID;
                                $this->ClaimLines->save($claimLine);
                            }
                        }

                    }
                    //all cases on the current page processed, fetch next page
                    $casesSearchParams['currentPage']++;
                    $nextCases = $this->sendRequest(self::$apiUrl."Cases/Search", $this->token,'POST', $casesSearchParams);
                    if($nextCases['responseCode'] == 200){
                        $cases = json_decode($nextCases['response'])->result;
                    }else{
                        $err = "Server returned Response code: {$nextCases['responseCode']} for cases search API page {$casesSearchParams['currentPage']}/$totalPages\nServer Response:\n{$casesSearchResponse['response']}\n exiting";
                        Log::error($err);
                        throw new Exception($err);
                    }
                } while($casesSearchParams['currentPage'] <= $totalPages);
            }else{
                Log::info("No cases to process, exiting");
            }
        }

        $endTime = microtime(true) - $this->executionStartTime;
        Log::notice("Finished getting existing claims from $this->collectorName command in $endTime second(s) at: " . (new FrozenTime())->i18nFormat(IntlDateFormatter::FULL, 'Europe/Copenhagen', 'en-GB'));
    }


    public function sendRequest($url, $token, $method = 'GET', $json = []){
        $return = ['responseCode' => '','response' => '', 'contentType' => ''];
        try {
            $headers =   ['headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Content-type' => 'application/json',
                'Authorization' => "Bearer $token"],
            ];
            if(!empty($json)){
                $headers['json'] =  $json;
            }
            $response = $this->client->request($method, $url, $headers);

        }catch (Exception $e) {
            $response = $e->getResponse();
        }

        if($response) {
            $return['contentType'] = $response->getHeaderLine('Content-Type');
            $this->globalResponseStatusCode = $return['responseCode'] = $response->getStatusCode();
            $return['response'] = $response->getBody(true)->getContents();
        }
        if($this->globalResponseStatusCode == 401) {
            if(self::$retries >= self::$retriesLimit){
                $err = "Retries for fetching token exhausted in recursion function, exiting";
                NotifyAdmin::syncFinancialNotWorked($err);
                Log::critical($err);
                throw new Exception($err);
            }else{
                Log::critical(json_encode($response));
                NotifyAdmin::syncFinancialNotWorked(json_encode($response));
                throw new Exception(json_encode($response));
            }
            Log::info("Received 401! starting recursion to fetch token and recall the failed API");
            $this->token = $this->fetchToken();
            self::$retries++;
            return $this->sendRequest($url, $this->token, $method, $json);
        }else{
            return $return;
        }

    }

    public function fetchToken(){

        $retires = 0;
        $token = '';
        do {
            Log::info("Invoking Selenium script for fetching $this->collectorName cookie token");
            Log::info(shell_exec('cd ../debito-collectia-token && npm run test'));
            $token = json_decode(file_get_contents("token.json"))->AT;

            if($token != ''){
                break;
            }

            $retires++;
            Log::alert("Token empty, retry $retires of ".self::$retriesLimit." for fetching token after sleeping for few seconds");
            sleep(rand(3, 5));                  //slow down requests to get passed undetected by the server

        } while ($retires < self::$retriesLimit);

        if($token != '') {
            return $token;
        }else {
            Log::critical("Retries for fetching token exhausted, exiting");
            throw new Exception("Retries for fetching token exhausted, exiting");
        }
    }

    public function saveCaseDocs($token, $caseID){
        $response = [];
        $caseDocsApiRes = $this->sendRequest(self::$apiUrl."Case/$caseID/Documents", $token);
        $caseDocsRes = json_decode($caseDocsApiRes['response']);
        if($caseDocsApiRes['responseCode'] == 200 and !empty($caseDocsRes)){
            foreach($caseDocsRes as $caseDocObj){
                $caseFileRes = $this->sendRequest(self::$apiUrl."File/efiling/$caseDocObj->docID", $token);
                if($caseFileRes['responseCode'] == 200){
                    // Create a new file with 0644 permissions
                    $file = new File(self::$filePath.Text::uuid().'.'.explode('/', $caseFileRes['contentType'])[1], true, 0644);
                    $file->write($caseFileRes['response']);
                    $response = ['file_name' => $file->name, 'content_type' => $file->mime(),
                        'file_size' => $file->size()];
                    $file->close();
                    break;
                }
            }
        }
        return $response;
    }

    public function getCaseInvoices($token, $caseID){
        return $this->sendRequest(self::$apiUrl."Case/$caseID/Invoices", $token);
    }

    public function createDebtor($account, $case){

        $debtor = $this->Debtors->newEntity();
        $debtor->account_id = $account->id;
        $debtor->created_by_id = $account->created_by_id;;
        $debtor->email = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->email);

        for ($i = 1; $i <= 3; $i++){
            $phone = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->{$this->debtorPhoneKey.$i});
            if(!empty($phone)) {
                $debtor->phone = $phone;
                break;
            }
        }

        if(empty($debtor->phone) and !empty($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->extraPhoneNumbers)){
            foreach ($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->extraPhoneNumbers as $extraPhoneNumber){
                $extraPhoneNumber = $this->customTrim($extraPhoneNumber);
                if(!empty($extraPhoneNumber)){
                    $debtor->phone = $extraPhoneNumber;
                    break;
                }

            }

        }

        $debtor->address = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->address);
        $debtor->city = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->city);
        $debtor->zip_code = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->postCode);
        $country_code = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->countryCode,"DK");
        $debtor->country_id = $this->Countries->find()->where(['iso_code' => $country_code])->first()->id;

        $cvr = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->idNo);
        $debtorName = $this->customTrim($case->{$this->caseDebtorProp}[$this->caseDebtorIndex]->debtorName);
        if(!empty($cvr)){
            $debtor->company_name = $debtorName;
            $debtor->vat_number = $cvr;
            $debtor->is_company = 1;

        }else{
            if(!empty($debtorName)){
                $namesArr = HelperFunctions::splitName($debtorName);
                $debtor->first_name = $namesArr[0];
                $debtor->last_name = $namesArr[1];
            }
            $debtor->is_company = 0;
        }

        return $this->Debtors->save($debtor);
    }

    function customTrim($str, $else = null){
        return (!empty($str)?trim($str, $this->trimCharsList):$else);
    }
}
