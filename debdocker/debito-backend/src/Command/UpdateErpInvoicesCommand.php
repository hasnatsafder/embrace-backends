<?php
declare(strict_types=1);

namespace App\Command;

use App\Model\Entity\ErpIntegration;
use App\Utility\ErpIntegrations;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ResultSetInterface;
use Psr\Log\LogLevel;

/**
 * UpdateErpInvoices command.
 *
 * @property \App\Model\Table\ErpIntegrationsTable $ErpIntegrations
 */
class UpdateErpInvoicesCommand extends AppCommand
{
    public $modelClass = 'App.ErpIntegrations';

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io): int
    {
        $this->_getErpIntegrations()->each(function (ErpIntegration $erpIntegration) {
            $this->log(sprintf('Starting ERP sync: %s', $erpIntegration->id), LogLevel::DEBUG);
            ErpIntegrations::syncErpInvoices($erpIntegration->id);
            $this->log(sprintf('Finished ERP sync: %s', $erpIntegration->id), LogLevel::DEBUG);
        });

        return static::CODE_SUCCESS;
    }

    /**
     * Gets the ErpIntegrations that needs to be synced
     *
     * @return \Cake\Datasource\ResultSetInterface
     */
    private function _getErpIntegrations(): ResultSetInterface
    {
        return $this->ErpIntegrations->find()->all();
    }
}
