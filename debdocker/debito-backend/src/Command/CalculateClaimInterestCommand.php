<?php
declare(strict_types=1);

namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Psr\Log\LogLevel;

/**
 * CalculateClaimInterest command.
 *
 * @property \App\Model\Table\ClaimsTable $Claims
 * @property \Queue\Model\Table\QueuedJobsTable QueuedJobs
 */
class CalculateClaimInterestCommand extends Command
{
    public $modelClass = 'App.Claims';

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->loadModel('Queue.QueuedJobs');

        $this->Claims->find('ActivelyAddingInterest')
            ->select(['Claims.id'])
            ->enableHydration(false)
            ->enableBufferedResults(false)
            ->extract('id')
            ->compile()
            ->each(function ($claimId) {
                $this->log(
                    sprintf('Initiating interest calculation for claim id %s', $claimId),
                    LogLevel::DEBUG
                );
                $this->QueuedJobs->createJob('ClaimsCalculateInterest', ['claimId' => $claimId]);
            });

        return static::CODE_SUCCESS;
    }
}