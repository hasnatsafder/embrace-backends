<?php
declare(strict_types=1);

namespace App\Command;

use App\Exception\Exception;
use App\Utility\HelperFunctions;
use App\Utility\NotifyAdmin;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Filesystem\File;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use GuzzleHttp\Client;
use IntlDateFormatter;

/**
 * Class SyncClaimsFromCollectiaCommand
 *
 * @package App\Command
 * @property \App\Model\Table\ClaimsTable Claims
 * @property \League\Flysystem\AdapterInterface casesFileSystem
 */
class SyncClaimsFromCollectiaCommand extends Command
{

    protected static $retries = 0;
    protected static $filePath = 'webroot/files/ClaimLines/file_name/';
    protected static $apiUrl = 'https://kundewebapi.collectia.dk/api/';
    protected $client;
    protected $claimsTable;
    protected $claimsLinesTable;
    protected $claimPhasesTable;
    protected $claimsLineTypesTable;
    protected $claimFinancialsTable;
    protected $collectorsTable;
    protected $collectorName = 'Collectia';
    protected $collectorIdentifier = 'collectia';
    protected static $retriesLimit = 2;
    protected $collector;

    protected $globalResponse;
    protected $globalResponseStatusCode;

    public function __construct()
    {
        $this->client = new Client(['http_errors' => false]);
        $this->claimsTable = TableRegistry::getTableLocator()->get('Claims');
        $this->claimsLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $this->claimsLinesTable->removeBehavior('Upload');
        $this->claimsLineTypesTable = TableRegistry::getTableLocator()->get('ClaimLineTypes');
        $this->claimPhasesTable = TableRegistry::getTableLocator()->get('ClaimPhases');
        $this->claimFinancialsTable = TableRegistry::getTableLocator()->get('ClaimFinancials');
        $this->collectorsTable = TableRegistry::getTableLocator()->get('Collectors');
    }

    public function initialize()
    {
        parent::initialize();
        $this->collector = $this->collectorsTable->find()->where(['identifier' => $this->collectorIdentifier, 'is_active' => 1])->first();
        if (!$this->collector) {
            $err = "Collector not found or is inactive, exiting";
            NotifyAdmin::syncFinancialNotWorked($err);
            Log::critical($err);
            throw new Exception($err);
        }
    }

    /**
     * @param \Cake\Console\Arguments $args
     * @param \Cake\Console\ConsoleIo $io
     * @return int|null|void
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        Log::notice("Starting $this->collectorName claims sync command at: " . (new FrozenTime())->i18nFormat(
                IntlDateFormatter::FULL,
                'Europe/Copenhagen',
                'en-GB'
            ));

        $claims = $this->getClaims();
        $total_claims = $claims->count();
        $legailStatus = $this->claimPhasesTable->getFromIdentifier('legal_collection');
        if ($total_claims > 0) {
            $token = $this->fetchToken();
            Log::info("Starting to sync $total_claims claim(s)");
            while ($claims->valid()) {
                $claim = $claims->current();
                Log::info("Fetching claim id $claim->id, collector_reference $claim->collector_reference from $this->collectorName");
                $this->fetchCaseFromCollectia($token, $claim->collector_reference);
                switch ($this->globalResponseStatusCode) {
                    case 200:
                    {
                        $this->syncFinancialData($claim);
                        if ($claim->claim_phase_id == $legailStatus->id) {
                            Log::info("Getting legal docs for " . $claim->customer_reference);
                            $this->saveCaseDocs($token, $claim);
                        }
                        $claims->next();
                        break;
                    }

                    case 401:
                    {
                        Log::error("Server returned Response code: $this->globalResponseStatusCode for case '$claim->collector_reference'\nServer Response:\n$this->globalResponse");
                        if (self::$retries < $this->retriesLimit) {
                            $token = $this->fetchToken();
                            self::$retries++;
                            $claims->rewind();
                        } else {
                            $err = "Server returned 401 " . self::$retries . " times. Retries for fetching token exhausted, exiting";
                            NotifyAdmin::syncFinancialNotWorked($err);
                            Log::critical($err);
                            throw new Exception($err);
                        }

                        break;
                    }

                    default:
                    {
                        Log::error("Server returned Response code: $this->globalResponseStatusCode for case '$claim->collector_reference'\nServer Response:\n$this->globalResponse");
                        $claims->next();
                        break;
                    }
                }
            }
        } else {
            Log::info("No claims to process, exiting");
        }

        Log::notice("Finished $this->collectorName claims sync command at: " . (new FrozenTime())->i18nFormat(
                IntlDateFormatter::FULL,
                'Europe/Copenhagen',
                'en-GB'
            ));
    }

    /**
     * To get all claims with order 4,5
     *
     * @return Query object of Claims
     */

    public function getClaims()
    {

        $claimActionFourth = $this->claimPhasesTable->find()->where(['identifier' => 'reminder'])->first();
        $claimActionFifth = $this->claimPhasesTable->find()->where(['identifier' => 'debt_collection'])->first();
        $legalCollection = $this->claimPhasesTable->find()->where(['identifier' => 'legal_collection'])->first();

        $claims = $this->claimsTable->find("all")->
        where([
            'claim_phase_id IN' => [$claimActionFourth->id, $claimActionFifth->id, $legalCollection->id],
            'collector_id' => $this->collector->id,
        ]);

        return $claims;
    }

    public function fetchToken()
    {

        $retires = 0;
        $token = '';
        do {
            Log::info("Invoking Selenium script for fetching $this->collectorName cookie token");
            Log::info(shell_exec('cd ../debito-collectia-token && npm run test'));
            $token = json_decode(file_get_contents("token.json"))->AT;

            if ($token != '') {
                break;
            }

            $retires++;
            Log::alert("Token empty, retry $retires of $this->retriesLimit for fetching token after sleeping for few seconds");
            sleep(rand(3, 5));                  //slow down requests to get passed undetected by the server
        } while ($retires < $this->retriesLimit);

        if ($token != '') {
            return $token;
        } else {
            $err = "Retries for fetching token exhausted, exiting";
            NotifyAdmin::syncFinancialNotWorked($err);
            Log::critical($err);
            throw new Exception($err);
        }
    }

    public function fetchCaseFromCollectia($token, $caseID)
    {

        try {
            $response = $this->client->request(
                'GET',
                "https://kundewebapi.collectia.dk/api/case/$caseID",
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Content-type' => 'application/json',
                        'Authorization' => "Bearer $token",
                    ],
                ]
            );
        } catch (Exception $e) {
            $response = $e->getResponse();
        }

        if ($response) {
            $this->globalResponseStatusCode = $response->getStatusCode();
            $this->globalResponse = $response->getBody(true)->getContents();
        }
    }

    function syncFinancialData($claim)
    {

        //insert data Into database
        $decoded_response = json_decode($this->globalResponse);
        $claim_financial = $this->claimFinancialsTable->find()->where(['claim_id' => $claim->id])->first();
        if (!$claim_financial) {
            $claim_financial = $this->claimFinancialsTable->newEntity();
        }
        $claim_financial->hovedstol = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->registeredCapital
        ));
        $claim_financial->rykkergebyrer = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->registeredFee
        ));
        $claim_financial->ovrige_gebyrer = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->registeredCost
        ));
        $claim_financial->renter = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->remainingInterest
        ));
        $claim_financial->retsgebyrer = HelperFunctions::convertToDanishCurrency(strval(
            $this->getAmountFromDebtorCostsByDescription(
                $decoded_response,
                'retsgebyrer'
            )
        ));
        $claim_financial->inkassoomkostninger = HelperFunctions::convertToDanishCurrency(strval(
            $this->getAmountFromDebtorCostsByDescription(
                $decoded_response,
                'Inkassoomkostninger'
            )
        ));
        $claim_financial->tilkendt_modesalar = HelperFunctions::convertToDanishCurrency(strval(
            $this->getAmountFromDebtorCostsByDescription(
                $decoded_response,
                'Advokatsalær'
            )
        ));
        $claim_financial->indbetalt = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->paidCapital +
            $decoded_response->debtors[0]->paidFee + $decoded_response->debtors[0]->paidInterest
            + $decoded_response->debtors[0]->paidCost
        ));
        $claim_financial->restgald = HelperFunctions::convertToDanishCurrency(strval(
            $decoded_response->debtors[0]->remainingCapital
            + $decoded_response->debtors[0]->remainingFee + $decoded_response->debtors[0]->remainingInterest
            + $decoded_response->debtors[0]->remainingCost
        ));
        $claim_financial->fogedrettsgebyr = HelperFunctions::convertToDanishCurrency(strval(
            $this->getAmountFromDebtorCostsByDescription($decoded_response, 'Fogedgebyr')
        ));

        $claim_financial->claim_id = $claim->id;

        $this->claimFinancialsTable->save($claim_financial);
    }

    public function saveCaseDocs($token, $claim){
        $response = [];
        $documentType = $this->claimsLineTypesTable->getFromIdentifier('document');
        $caseDocsApiRes = $this->sendRequest(self::$apiUrl."Case/$claim->collector_reference/Documents", $token);
        $caseDocsRes = json_decode($caseDocsApiRes['response']);
        if($caseDocsApiRes['responseCode'] == 200 and !empty($caseDocsRes)){
            foreach($caseDocsRes as $caseDocObj){
                $caseFileRes = $this->sendRequest(self::$apiUrl."File/efiling/$caseDocObj->docID", $token);

                // check if $claimLine already exists
                $claim_line_found = $this->claimsLinesTable->find()
                    ->disableHydration()
                    ->where(['claim_id' => $claim->id, 'invoice_reference' => "Juridisk kommunikation - " . $caseDocObj->docID])
                    ->isEmpty();

                if($caseFileRes['responseCode'] == 200 && $claim_line_found){
                    // Create a new file with 0644 permissions
                    $file = new File(self::$filePath.Text::uuid().'.'.explode('/', $caseFileRes['contentType'])[1], true, 0644);
                    $file->write($caseFileRes['response']);
                    $response = ['file_name' => $file->name, 'content_type' => $file->mime(),
                        'file_size' => $file->size()];
                    // create Claim Line
                    $claim_line = $this->claimsLinesTable->newEntity();
                    $claim_line->claim_id = $claim->id;
                    $claim_line->claim_line_type_id = $documentType->id;
                    $claim_line->currency_id = $claim->currency_id ?? "e5243bf3-0a70-5924-94f6-af1d88c951c4";
                    $claim_line->invoice_reference = "Juridisk kommunikation - " . $caseDocObj->docID;
                    $claim_line->amount = 0;
                    $claim_line->invoice_date = new FrozenDate();
                    $claim_line->maturity_date = new FrozenDate();
                    $claim_line->file_name = (!empty($response) ? $response['file_name'] : null);
                    $claim_line->file_mime_type = ($response['content_type'] ? $response['content_type'] : ".pdf");
                    $claim_line->file_size = ($response['file_size'] ? $response['file_size'] : "10");
                    $claim_line->file_dir = (!empty($response) ? (self::$filePath) : null);
                    $claim_line->synced = 1;
                    $this->claimsLinesTable->save($claim_line);
                    $file->close();
                }
            }
        }
        return $response;
    }

    public function sendRequest($url, $token, $method = 'GET', $json = []){
        $return = ['responseCode' => '','response' => '', 'contentType' => ''];
        try {
            $headers =   ['headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Content-type' => 'application/json',
                'Authorization' => "Bearer $token"],
            ];
            if(!empty($json)){
                $headers['json'] =  $json;
            }
            $response = $this->client->request($method, $url, $headers);

        }catch (Exception $e) {
            $response = $e->getResponse();
        }

        if($response) {
            $return['contentType'] = $response->getHeaderLine('Content-Type');
            $this->globalResponseStatusCode = $return['responseCode'] = $response->getStatusCode();
            $return['response'] = $response->getBody(true)->getContents();
        }
        if($this->globalResponseStatusCode == 401) {
            if(self::$retries >= self::$retriesLimit){
                $err = "Retries for fetching token exhausted in recursion function, exiting";
                NotifyAdmin::syncFinancialNotWorked($err);
                Log::critical($err);
                throw new Exception($err);
            }else{
                Log::critical(json_encode($response));
                NotifyAdmin::syncFinancialNotWorked(json_encode($response));
                throw new Exception(json_encode($response));
            }
            Log::info("Received 401! starting recursion to fetch token and recall the failed API");
            $this->token = $this->fetchToken();
            self::$retries++;
            return $this->sendRequest($url, $this->token, $method, $json);
        }else{
            return $return;
        }

    }

    function getAmountFromDebtorCostsByDescription($decoded_response, $propVal)
    {
        $costsArr = $decoded_response->debtors[0]->costs;
        $amount = null;
        if (!empty($costsArr)) {
            foreach ($costsArr as $costObj) {
                if (strpos($costObj->description, $propVal) !== false) {
                    $amount += $costObj->amount;
                }
            }
        }

        return $amount;
    }
}
