<?php
declare(strict_types=1);

namespace App\Utility;
use App\Model\Entity\Debtor;
use Cake\ORM\TableRegistry;

class HelperFunctions
{

    /**
     * @param float $value
     * @return int value in danish
     * @description Convert value to danish e.g. 1000.5 to 100050
     */

    public static function convertToDanishCurrency($value)
    {
        if (strpos($value, '.') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, "."), 1)) == 1) {
                $float = number_format((float)$value, 2, '.', '');
                $integer = str_replace('.', '', $float);

                return abs($integer);
            }
            $integer = str_replace('.', '', $value);

            return abs($integer);
        }
        if (strpos($value, ',') !== false) {
            //if string contains one digit after decimal then add a zero at end too e.g. 1.5 to 150
            if (strlen(substr(strrchr($value, ","), 1)) == 1) {
                $float = number_format((float)$value, 2, ',', '');
                $integer = str_replace(',', '', $float);

                return abs($integer);
            }
            $integer = str_replace(',', '', $value);

            return abs($integer);
        }
        $float = number_format((float)$value, 2, '.', '');
        $integer = str_replace('.', '', $float);

        return abs($integer);
    }

    /**
     * @param array $array of objects
     * @param string $prop property to be searched for
     * @param mixed $val value of the property to compare
     * @return mixed false if not found or object
     * @description Find object by object property, value from an array of objects
     */
    public static function findInArrayOfObjectsByPropValue($array, $prop, $val){
        $return = false;
        if(!empty($array)) {
            foreach ($array as $obj) {
                if ($obj->$prop == $val) {
                    $return = $obj;
                    break;
                }
            }
        }
        return $return;
    }
    /**
     * @param string $name full name
     * @return array having first & last names respectively.
     * @description Convert string to first and last name array.
     * It makes the assumption the last name will be a single word.
     * Uses regex that accepts any word character or hyphen in last name.
     * Makes no assumption about middle names, that all just gets grouped into first name.
     */
    public static function splitName($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }

    /**
     * Calculate total sum of a claim
     *
     * @param App\Entity\Model\Claim
     */
    public static function getTotalClaimSum($claim)
    {
        $claimLinesTable = TableRegistry::getTableLocator()->get('ClaimLines');
        $claimLineTypeTable = TableRegistry::getTableLocator()->get('claim_line_types');
        $krediNotePhase = $claimLineTypeTable->find()->where(['identifier' => 'credit_note'])->select(['id'])->first();
        $paymentPhase = $claimLineTypeTable->find()->where(['identifier' => 'payment'])->select(['id'])->first();
        $claimLines = $claimLinesTable->find('all')
            ->where(['claim_id' => $claim->id]);
        $total = 0;
        foreach ($claimLines as $claimLine) {
            switch ($claimLine->claim_line_type_id) {
                case $krediNotePhase->id:
                case $paymentPhase->id:
                    $total = $total - abs($claimLine->amount);
                    break;
                default:
                    $total = $total + $claimLine->amount;
            }
        }

        return $total;
    }

    /**
     * Get debtor name
     *
     * @param App\Utility\Entity\Claim
     */
    public static function getDebtorsName($claim)
    {
        $debtorsTable = TableRegistry::getTableLocator()->get('Debtors');
        $debtor = $debtorsTable->get($claim->debtor_id);
        if (!$debtor) {
            return "";
        }

        if ($debtor->company_name) {
            return $debtor->company_name;
        } else {
            return $debtor->first_name . " " . $debtor->last_name;
        }
    }

    /**
     * Get debtor name
     *
     * @param App\Utility\Entity\Debtor
     */
    public static function getDebtorsFullName(Debtor $debtor)
    {
        if (!$debtor) {
            return "";
        }

        if ($debtor->company_name) {
            return $debtor->company_name;
        } else {
            return $debtor->first_name . " " . $debtor->last_name;
        }
    }

    /**
     * Convery string to danish money
     *
     * @param Integer $number
     */
    public static function convertToDanish($number)
    {
        //convert to string
        $number = strval($number);
        if (strlen($number) < 2) {
            return $number;
        }
        // add dot before two digits
        $number = substr_replace($number, ".", -2, 0);
        // convert to float
        $number = floatval($number);

        return number_format($number, 2, ',', '.');
    }

    /**
     * English currency converter
     *
     * @param Integer $number
     */
    public static function convertToEnglish($number) {
        $number = substr_replace($number, ".", -2, 0);
        $number = floatval($number);
        return number_format($number, 2, '.', '');
    }

    /**
     * Convert to currency
     *
     * @param Integer $number
     * @param String $currency_iso_code
     */
    public static function converToCurrency($number, $currency_iso_code){
        if ($currency_iso_code == "DKK"){
            return static::convertToDanish($number);
        }
        return static::convertToEnglish($number);
    }
}
