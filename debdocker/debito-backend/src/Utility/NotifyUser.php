<?php
declare(strict_types=1);

namespace App\Utility;

use App\Model\Entity\Account;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class NotifyUser
{

    private $email;
    private $subject;
    private $body;
    private $account_id;
    private $claim;
    private $userEmail;
    private $viewVars;
    private $template;
    private $sendBCC;

    public function __construct()
    {
        $this->email = new Email('sparkpost');
        $this->subject = "";
        $this->body = "";
        $this->userEmail = null;
        $this->account_id = "";
        $this->viewVars = [];
        $this->template = "";
        $this->sendBCC = false;
    }

    /**
     * Notify user for rejected claims
     *
     * @param String $rejected_reason
     * @return bool
     */

    public static function sendRejected($claim, $subject, $body)
    {
        $instance = new NotifyUser();
        $instance->body = $body;
        $instance->account_id = $claim->account_id;
        $instance->subject = $subject;
        $instance->sendBCC = true;

        // get currency symbol
        $currencyTable = TableRegistry::getTableLocator()->get('Currencies');
        $currency = $currencyTable->get($claim->currency_id);
        $total = HelperFunctions::getTotalClaimSum($claim);

        $instance->viewVars = [
            'claim' => $claim,
            'data' => $body,
            'total' => HelperFunctions::convertToDanish($total),
            'currency' => $currency,
            'frontendUrl' => Configure::read('Frontendurl'),
        ];
        $instance->template = 'debtor_bankrupt';
        $instance->sendEmail();
    }

    /**
     * Send the email to user and account if provided
     *
     * @param $message
     */
    private function sendEmail()
    {
        $usersTable = TableRegistry::getTableLocator()->get('Users');
        $accountsTable = TableRegistry::getTableLocator()->get('Accounts');

        $account = $accountsTable->get($this->account_id);
        $user = $usersTable->get($account->created_by_id);
        $email_user = $this->userEmail ?? $user->email;

        $this->email->setFrom(['kundeservice@debito.dk' => 'Debito'])
            ->setTo($email_user)
            ->setSubject($this->subject)
            ->setViewVars($this->viewVars)
            ->template($this->template)
            ->setEmailFormat('html');
        $this->sendBCC ? $this->email->bcc(Configure::read('Adminemail')) : false;
        if ($account->email && $account->email !== $user->email) {
            $this->email->cc($account->email);
        }
        $this->email->send();
    }

    /**
     * Send email to account user that scripts are fetch now.
     * @param $account
     * @return bool
     */
    public static function sendIntegrationConfirmationEmail($account): bool
    {
        $instance = new NotifyUser();
        $instance->account_id = $account->id;
        $instance->subject = "Din konto er klar til brug!";

        $instance->template = 'integration_confirmation';
        $instance->sendEmail();

        return true;
    }

    /**
     * Send email for account verification
     * @param $account
     * @param $verification_record
     * @return void
     */
    public static function sendAccountVerificationEmail($account, $verification_record) : void {
        $url = Configure::read('Frontendurl') . "accountverification?token=" . $verification_record->form_key;
        $instance = new NotifyUser();
        $instance->account_id = $account->id;
        $instance->sendBCC = false;
        $instance->subject = "KYC verificering af konto";
        $instance->userEmail = $verification_record->email;
        $instance->viewVars = ['url' => $url];

        $instance->template = 'verification_email';
        $instance->sendEmail();
    }
 }
