<?php
declare(strict_types=1);

namespace App\Utility;

use Cake\Core\Configure;
use Cake\Mailer\Email;

class NotifyAdmin
{

    private $email;
    private $subject;
    private $body;

    public function __construct()
    {
        $this->email = new Email('sparkpost');
        $this->subject = "";
        $this->body = "";
    }

    /**
     * upload claims to collector faced an error
     *
     * @param String $error
     * @param String $collector
     * @param String $claim_reference
     * @return bool
     */
    public static function uploadClaimsToCollectorErrors(string $error, string $collector, string $claim_reference)
    {
        $instance = new NotifyAdmin();
        $instance->subject = "A claim failed to upload";
        $instance->body = "Upload Claims to Collector, " . $collector . "Faced following error";
        $instance->body .= "<br>Claim Reference is: " . $claim_reference;
        $instance->body .= "<br>Error is: " . $error;

        return $instance->sendEmail();
    }

    /**
     * Send the email to admn
     *
     * @param $message
     */
    private function sendEmail()
    {
        return $this->email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to(Configure::read('Adminemail'))
            ->subject($this->subject)
            ->cc(Configure::read('Developer.email'))
            ->emailFormat('html')
            ->send($this->body);
    }

    /**
     * upload claims to collector faced an error
     *
     * @param String $error
     * @param String $collector
     * @param String $claim_reference
     * @return bool
     */
    public static function DownloadClaimsReceiptErrors(string $error, string $collector, string $claim_reference)
    {
        $instance = new NotifyAdmin();
        $instance->subject = "Resynced the claim";
        $instance->body = "Downloading Claims to Collector, " . $collector . "Faced following error";
        $instance->body .= "<br>Claim Reference is: " . $claim_reference;
        $instance->body .= "<br>Error is: " . $error;

        return $instance->sendEmail();
    }

    /**
     * A collectia account with empty account vat_number
     *
     * @param String account name
     * @param String Claim_reference
     * @return bool
     */
    public static function emptyVatForCollectia(string $accountName, string $claim_reference)
    {
        $instance = new NotifyAdmin();
        $instance->subject = "A collectia account with empty account vat_number";
        $instance->body = "We found a collecita account with empty vat_number";
        $instance->body .= "<br>Claim referene is " . $claim_reference;
        $instance->body .= "<br>Account name is " . $accountName;
        $instance->sendEmail();
    }

    /**
     * Send a claim rejected post to admin
     *
     * @param String claim_reference
     * @param String reason
     * @param String collectorName
     * @return bool
     */
    public static function claimRejected(string $claim_reference, string $reason, string $collectorName)
    {
        $instance = new NotifyAdmin();
        $instance->subject = "A claim was rejected " . $collectorName;
        $instance->body = "Following is the detail of rejected claim";
        $instance->body .= "<br>Claim referene is " . $claim_reference;
        $instance->body .= "<br><b>Reason for rejected</b> is " . $reason;
        $instance->sendEmail();
    }

    /**
     * If sync claim financials not worked for collecita
     *
     * @param String $reason
     */

    public static function syncFinancialNotWorked(string $reason)
    {
        $instance = new NotifyAdmin();
        $instance->subject = "Sync claim financials not worked for collecita";
        $instance->body = "Reason : " . $reason;
        $instance->notifyDeveloper();
    }

    /**
     *
     */
    private function notifyDeveloper()
    {
        return $this->email->from(['kundeservice@debito.dk' => 'Debito'])
            ->to(Configure::read('Developer.email'))
            ->subject($this->subject)
            ->emailFormat('html')
            ->send($this->body);
    }
}
