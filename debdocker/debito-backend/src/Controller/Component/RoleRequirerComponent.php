<?php
declare(strict_types=1);

namespace App\Controller\Component;

use App\Model\Entity\User;
use Cake\Controller\Component;
use Cake\Http\Exception\UnauthorizedException;
use Cake\ORM\Locator\LocatorAwareTrait;
use Cake\ORM\Query;

/**
 * RoleRequirer component
 */
class RoleRequirerComponent extends Component
{
    use LocatorAwareTrait;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * @param array $roleIdentifiers
     * @param bool $onlyInActiveAccount
     * @return void
     */
    public function requireAnyOf(array $roleIdentifiers, bool $onlyInActiveAccount = false): void
    {
        $roles = $this->getRoles($roleIdentifiers, $onlyInActiveAccount);

        foreach ($roleIdentifiers as $requiredRole) {
            if (in_array($requiredRole, $roles)) {
                return;
            }
        }

        throw new UnauthorizedException();
    }

    /**
     * @param array $roleIdentifiers
     * @param bool $onlyInActiveAccount
     * @return void
     */
    public function requireAllOf(array $roleIdentifiers, bool $onlyInActiveAccount = false): void
    {
        $roles = $this->getRoles($roleIdentifiers, $onlyInActiveAccount);

        foreach ($roleIdentifiers as $requiredRoles) {
            if (!in_array($requiredRoles, $roles)) {
                throw new UnauthorizedException();
            }
        }
    }

    /**
     * @param string $role
     * @param bool $onlyInActiveAccount
     */
    public function requireRole(string $role, bool $onlyInActiveAccount = false): void
    {
        $this->requireAllOf([$role], $onlyInActiveAccount);
    }

    /**
     * @return \App\Model\Entity\User
     */
    private function getUser(): User
    {
        return $this->getTableLocator()->get('Users')
            ->get(
                $this->getController()->Auth->user('id'),
                ['finder' => 'withTrashed']
            );
    }

    /**
     * @param array $roleIdentifiers
     * @param bool $onlyInActiveAccount
     * @return array
     */
    private function getRoles(array $roleIdentifiers = [], bool $onlyInActiveAccount = false): array
    {
        if (empty($roleIdentifiers)) {
            return [];
        }

        $roles = $this->getTableLocator()->get('Roles')->find()
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'identifier',
            ])
            ->select([
                'Roles.id',
                'Roles.identifier',
            ])
            ->where(['Roles.identifier IN' => $roleIdentifiers])
            ->matching('AccountsUsers', function (Query $q) use ($onlyInActiveAccount) {
                $user = $this->getUser();

                $where = ['AccountsUsers.user_id' => $user->id];

                if ($onlyInActiveAccount) {
                    $where['AccountsUsers.account_id'] = $user->active_account_id;
                }

                return $q->where($where);
            })
            ->distinct(['Roles.id'])
            ->toArray();

        return array_values($roles);
    }
}
