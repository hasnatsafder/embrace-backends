<?php

function greetings() {
  $timeOfDay = "morning";

  return ( function ($name) use (&$timeOfDay) {
    $timeOfDay = ucfirst($timeOfDay);
    return "Good $timeOfDay $name";
  });
}

$hasnat = greetings();
echo $hasnat("hasnat");