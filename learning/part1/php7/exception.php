<?php 
declare(strict_types = 1);

function add(int $left, int $right)
{
    return $left + $right;
}

try {

  $value = 1 % 0;

} catch (Throwable $t) {
  echo "throw\n";
  var_dump($t->getMessage());
  // Executed only in PHP 7, will not match in PHP 5

} catch (DivisionByZeroError $e) {
  echo "exception";
  var_dump($e);
  // Executed only in PHP 5, will not be reached in PHP 7

}
echo "\n";