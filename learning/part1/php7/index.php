<?php

declare(strict_types = 1);

$names = ['Hasnat', 'Shevchenko', 'Ibrahimovic', 'Nesta', 'Cannavaro'];

usort($names, function($a, $b){
  return strlen($b) <=> strlen($a);
});

//var_dump($names);

function age(bool $age) {
  return $age;
}

echo age(true);