<?php 

$books = ['title' => 'Harry Potter', 'author' => 'JK Rowling'];

['title' => $bookTitle, 'author' => $bookAuhtor] = $books;

//var_dump($bookAuhtor);

// nullable types
function age (?int $age) {
  var_dump($age);
}

//age(null);

// iterable

function dump(iterable $data) {
  foreach ($data as $item) {
    var_dump($item);
  }
}

$arr = ['Hasnat', 1, null];
//dump($arr);
// create iterbale class
class Collection implements IteratorAggregate {
  protected  $items;
  public function __construct($items)
  {
    $this->items = $items;
  }

  public function getIterator()
  {
    return new ArrayIterator($this->items);
  }
}
$myColl = new Collection(['1', 2, '3']);
// dump($myColl);

// support for -ve string
var_dump("abcdef"[-2]);
$str = "abcdef";
echo $str[-3] . "\n"; // d


// php objects
function dumpObject(object $obj) {
  var_dump($obj);
}

$a = Object [1,2,3];
