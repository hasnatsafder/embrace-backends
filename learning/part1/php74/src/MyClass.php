<?php 

namespace App;

class MyClass {
  public function __construct()
  {
    echo "-- WELCOME TO MY CLASS --\n";
    $variable = 10;
    $mul = array_map(fn($value) => $variable * $value, [1,2,3,4]);

    //print_r($mul);
  }

  function nullCoalesing() {
    $arr = [
      'name' => 'Hasnat',
      // 'title' => 'Safder'
    ];

    echo $arr['title'] ??= 'No title';
    echo $arr['title'];
  }

  function spreadOperator(...$numbers) {
    return array_sum($numbers);
  }
}